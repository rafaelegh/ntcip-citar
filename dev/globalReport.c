
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"

int negro;
int maxEventLogConfigs = 1;
int maxEventLogSize    = 1;

struct eventLogConfigTable_entry * entry27[1];
struct eventLogTable_entry * entry28[1];

/* create a new row in the table */
netsnmp_tdata_row *
globalReport_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int indice2, int tabla) 
{
    netsnmp_tdata_row *row;

	int i=0;

	switch(tabla)
	{
		case 27:

    	entry27[indice] = SNMP_MALLOC_TYPEDEF(struct eventLogConfigTable_entry);
    	if (!entry27[indice])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry27[indice]);
	        return NULL;
	    }
    	row->data = entry27[indice];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry27[indice]->eventConfigID = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry27[indice]->eventConfigID),
                                 sizeof(entry27[indice]->eventConfigID));

		break;
		case 28:

    	entry28[indice][indice2] = SNMP_MALLOC_TYPEDEF(struct eventLogTable_entry);
    	if (!entry28[indice][indice2])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry28[indice][indice2]);
	        return NULL;
	    }
    	row->data = entry28[indice][indice2];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry28[indice][indice2]->eventLogClass = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry28[indice][indice2]->eventLogClass),
                                 sizeof(entry28[indice][indice2]->eventLogClass));

    	entry28[indice][indice2]->eventLogNumber = indice2+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 	&(entry28[indice][indice2]->eventLogNumber),
                                 	sizeof(entry28[indice][indice2]->eventLogNumber));
		break;
		
    }
			if (tabla_data)
        	netsnmp_tdata_add_row( tabla_data, row );
        return row;

    
}

void
initialize_globalReport_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	unsigned char min_columna, unsigned char max_columna, unsigned char tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i=0, n=0, j=0;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 27:
		reg = netsnmp_create_handler_registration(
              nombre,     globalReport_handler,
              oid_table, oid_len,
              HANDLER_CAN_RONLY
              );
		n=maxEventLogConfigs;
		break;
		case 28:
		reg = netsnmp_create_handler_registration(
              nombre,     globalReport_handler,
              oid_table, oid_len,
              HANDLER_CAN_RONLY
              );
		n=maxEventLogSize;
		break;
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }


		if(tabla==28)
	{
    	netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: splitNumber */
						   ASN_INTEGER,  /* index: splitPhase */
                           0);
	}
	else
	{
		netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: splitNumber */
                           0);
	}


    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{		
		if(tabla==28)
		{
				for(j=0;j<1;j++)
				{
					row=globalReport_tables_createEntry(table_data, i, j, tabla);
				}
		}
		else row=globalReport_tables_createEntry(table_data, i, j, tabla);
	}
}

void
init_globalReport(void)
{

    const oid maxEventLogConfigs_oid[] 	= { 1,3,6,1,4,1,1206,4,2,6,4,1 };
    const oid eventLogConfigTable_oid[] = { 1,3,6,1,4,1,1206,4,2,6,4,2};

   	const size_t eventLogConfigTable_oid_len   = OID_LENGTH(eventLogConfigTable_oid);
    const oid maxEventLogSize_oid[] 	= { 1,3,6,1,4,1,1206,4,2,6,4,3 };
	const oid eventLogTable_oid[] 		= {1,3,6,1,4,1,1206,4,2,6,4,4};
    const size_t eventLogTable_oid_len  = OID_LENGTH(eventLogTable_oid);

  DEBUGMSGTL(("maxEventLogConfigs", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxEventLogConfigs", handle_globalReport,
                               maxEventLogConfigs_oid, OID_LENGTH(maxEventLogConfigs_oid),
                               HANDLER_CAN_RONLY
        ));

	initialize_globalReport_tables("eventLogConfigTable",eventLogConfigTable_oid,
eventLogConfigTable_oid_len, COLUMN_EVENTCONFIGID, COLUMN_EVENTCONFIGSTATUS,27);



  DEBUGMSGTL(("maxEventLogSize", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxEventLogSize", handle_globalReport,
                               maxEventLogSize_oid, OID_LENGTH(maxEventLogSize_oid),
                               HANDLER_CAN_RONLY
        ));

	initialize_globalReport_tables("eventLogTable",eventLogTable_oid,
eventLogTable_oid_len, COLUMN_EVENTLOGCLASS, COLUMN_EVENTLOGTIMEMILLISECONDS,28);

}

int
handle_globalReport(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
    /* We are never called for a GETNEXT if it's registered as a
       "instance", as it's "magically" handled for us.  */

    /* a instance handler also only hands us one request at a time, so
       we don't need to loop over a list of requests; we'll only get one. */
    int resul=0;
    switch(reqinfo->mode) {

        case MODE_GET:
           			
				resul=compara_oid_global(requests->requestvb->name);
	
				if(resul==1) snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxEventLogConfigs, sizeof(int));
				else if(resul==3) snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxEventLogSize, sizeof(int));
				
				
			
            break;


        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_globalSetIDParameter\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    }

    return SNMP_ERR_NOERROR;
}
