DIR0=src
DIR1=src/NTCIP1201
DIR2=src/NTCIP1202
DIR3=src/NTCIPMANTELECTRIC

all: ntcip1 ntcip2 ntcip3 ntcip

ntcip: 	
	cd $(DIR0);\
	$(MAKE) -B ; \

ntcip1:
	cd $(DIR1);\
	$(MAKE) -B ; \

ntcip2:
	cd $(DIR2);\
	$(MAKE) -B ; \

ntcip3:
	cd $(DIR3);\
	$(MAKE) -B ; \

clean:
	cd $(DIR1);\
	$(MAKE) clean -B ; \
