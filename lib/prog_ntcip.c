#include "prog_envio.h"
#include "asc.h"
#include "citar.h"
#include "bytes.h"
#include "agendas_ntcip.h"

//Inicio de	variables Dani
unsigned char Datos[470], IndiceA=0, IndiceB=0, IndiceC=0, IndiceD=0, IndiceF=0;
unsigned char NumVehic=0, CanalAsociado[8], PreTimeVehic=0,PreTimePeat=0, TiempoCiclo=0;
unsigned char Matriz[16][5];
unsigned char SecuenceData[4][8], RingDelay[4];
unsigned char TiemposAux[120], TiemposAcumulados[32], TiemposIntervalos[32];
unsigned char OverLaps[32][8], TiposOverLaps[16], OL_Sitra[16], EstructuraOL[8][5], PreTimeOL=0;
unsigned char MatrizOL[16][16], ModifiersSITRA[16];
unsigned char TipoActual=0, ColorActual=0;
unsigned char OperAux=0, IndiceGT=0;
unsigned char Telegrama69[250], Telegrama6A[250], Telegrama72[76], Channel[16][2];
unsigned char ConteoCanales=0;
unsigned int ix=0, jx=0;
//fin de variables Dani

//inicio funciones de debugeo

void imprimir_matriz(void) {
	
	printf("matriz 16X5\n");
	
	for(int i=0;i<16;i++) {	
		for(int j=0;j<5;j++) {
			
			printf("%i ",Matriz[i][j]);
		}
		printf("\n");
	}
}

void imprimir_arreglo(unsigned char arr[], int n, char nombre[]) {
	
	printf("%s\n",nombre);
	
	for(int i=0;i<n;i++){
		printf("%i ",arr[i]);
	}
	printf("\n");
	
}

//fin funciones de debugeo


void VerificarCanal(char FaseF, char TipoF)// Si existe la Fase buscada (Vehicular o Peatonal), retorna 1.

{								// 2: Vehicular. 3: Peatonal
	unsigned char IndF=0;
	unsigned char CanalID = 0xFF;

	for(ix=0;ix<8;ix++)	CanalAsociado[ix] = 0xFF;	// Se inicializa vacío por si no hay ninguna coincidencia.
	ConteoCanales = 0;	// Se inicializa su contador.

	for(IndF=0;IndF<16;IndF++)	
	{
		if(Channel[IndF][0] == FaseF && Channel[IndF][1] == TipoF)

		{
			CanalAsociado[ConteoCanales] = IndF;	// Se guarda en el array cada channel que esté asociado
			ConteoCanales++;						// con la Fase buscada, y se cuenta.
		}
	}
}

void LlenarSecuenceData(void)	// Función que carga el Secuence Data con los valores obtenidos por SMB.
{
	IndiceA = 33;	// Se apunta a la ubicación del primer Byte del Secuence Data contenido en Datos [].
	for(ix=0;ix<4;ix++)	// 4 Filas: los Rings.
	{
		for(jx=0;jx<8;jx++)	// 8 Columnas: Las Fases.
		{
			SecuenceData[ix][jx] = Datos[IndiceA];
			IndiceA++;	// Se apunta al siguiente Dato del Secuence Data
		}
	}
	for(ix=0;ix<4;ix++)	RingDelay[ix] = Datos[ix+65];	// Se almacenan los 4 Bytes de Ring Delay
	TiempoCiclo = Datos[0];
}

void LlenarCanales(void)
{
	NumVehic = 0;	// Número de Grupos Vehiculares.
	IndiceA = 1;
	for(ix=0;ix<16;ix++)	// 16 Filas: Los Canales.
	{
		for(jx=0;jx<2;jx++)	// 2 Columnas: Los Datos de Canales.
		{
			Channel[ix][jx] = Datos[IndiceA];
			IndiceA++;
		}
		if(Channel[ix][1] == 2)	// Se cuentan los Grupos Vehiculares que tengan un Canal asociado.
		{
			NumVehic++;
		}
	}
}

void LlenarOverLaps(void)
{
	for(ix=0;ix<16;ix++)	
	{
		TiposOverLaps[ix] = Datos[ix+181];	// Los tipos están entre Datos [181] y [196].
		OL_Sitra[ix] = Datos[ix+197];	// Vehicular o Peatonal están entre Datos [197] y [212].
		for(jx=0;jx<8;jx++)	// 8 Columnas: Las Fases.
		{
			OverLaps[ix][jx] = Datos[213+(ix*8)+jx];
		}
	}
}

void CargarVehicularEnMatriz(void)
{
	PreTimeVehic += Datos[IndiceD];	// Se carga el tiempo en que termina el Color actual, según el Delay.
	Matriz[IndiceF][IndiceC]  = PreTimeVehic;	// Rojo con Amarillo en el Vehicular
	IndiceD++;	IndiceC++;	// Se apunta a la dirección del Verde Vehicular.

	PreTimeVehic += Datos[IndiceD];	// Se actualiza el Time Previo para el Color en cuestión.
	Matriz[IndiceF][IndiceC]  = PreTimeVehic;	// Verde en el Vehicular
	IndiceD++;	IndiceC++;	// Ahora se apunta al Amarillo

	PreTimeVehic += Datos[IndiceD];
	Matriz[IndiceF][IndiceC]  = PreTimeVehic;	// Amarillo.
	IndiceD -= 2;	IndiceC++;		// Se apunta de vuelta al inicio (R/A) para determinar el Rojo.

	if(Matriz[IndiceF][0] < Datos[IndiceD])	// Si la Columna 0 es menor que el R/A, se adapta con el ciclo
	{										// para que el resultado no se desborde por ser negativo.
		Matriz[IndiceF][IndiceC] = (Matriz[IndiceF][0] + TiempoCiclo) - Datos[IndiceD];	// Tiempo de Rojo.
	}
	else
	{
		Matriz[IndiceF][IndiceC] = Matriz[IndiceF][0] - Datos[IndiceD];	// Tiempo de Rojo.
	}
	// El tiempo en que termina el Rojo se obtiene restando el tiempo de R/A al de la Primera Columna.
	PreTimeVehic += Datos[IndiceD+3];	// Se suma el Tiempo de Red Clear para la Próxima Fase.
}

void CargarPeatonalEnMatriz(void)
{
	if(Datos[IndiceD+4] == 0xFF)	// Si el Desfasaje Peatonal es Nulo, el Blanco va en Fase con el Verde.
	{
		PreTimePeat = PreTimePeat + Datos[IndiceD];	// No se usa el Desfasaje Peatonal
			// El Rojo dura hasta que termina el Rojo/Amarillo.
	}
	else	// Si el desfasaje Peatonal no es nulo, se toma como origen el segundo Cero.
	{
		PreTimePeat = Datos[IndiceD+4];	// El Rojo dura hasta donde termina el Delay Peatonal.			
	}
	Matriz[IndiceF][IndiceC] = PreTimePeat;	// Tiempo de Rojo en el Peatonal.
	IndiceD += 5;	IndiceC++;	// Se Apunta al Blanco Peatonal.

	PreTimePeat += Datos[IndiceD];	
	Matriz[IndiceF][IndiceC]  = PreTimePeat;	// Blanco Peatonal.
	IndiceD++;	IndiceC++;	// Ahora se apunta al Despeje Peatonal

	PreTimePeat += Datos[IndiceD];	
	Matriz[IndiceF][IndiceC]  = PreTimePeat;	// Despeje Peatonal.
	IndiceD -= 6;	IndiceC++;	// Se apunta al inicial: R/A
		// En peatonales, las últimas dos columnas se dejan igual a la Cero + El Ciclo
	Matriz[IndiceF][IndiceC] = Matriz[IndiceF][0] + TiempoCiclo;
}

void LlenarMatriz16x5(int n)
{
	unsigned char i_aux = 0, j_aux = 0, k_aux = 0;
	for(i_aux=0;i_aux<4;i_aux++)
	{
		PreTimeVehic = RingDelay[i_aux];	// Hay un Tiempo Previo para cada Ring.
		for(j_aux=0;j_aux<8;j_aux++)
		{
			if(PreTimeVehic >= TiempoCiclo)	// Se ajusta el punto de inicio al ciclo.	
			{
				PreTimeVehic -= TiempoCiclo;
			}
			PreTimePeat = PreTimeVehic;	// Auxiliar para aplicarlo al Peatonal.		
			if(SecuenceData[i_aux][j_aux] != 0)		// Si la Fase apuntada existe; no es igual a cero.
			{				// Se verifica que la Fase apuntada tenga al menos un Canal Asociado:
				VerificarCanal(SecuenceData[i_aux][j_aux],2);	// Se guarda en Canal Asociado [].
				if(CanalAsociado[0] != 0xFF)	// Si esta Fase Vehicular Posee un Canal Asociado:
				{
					// La Fase 1 está A partir del 69. La 2 a partir del 76 y así cada 7 bytes:
					IndiceD = 62 + (SecuenceData[i_aux][j_aux] * 7);
					IndiceC = 0;	// Se inicializa el índice de Columnas de la Matriz
					IndiceF = CanalAsociado[0];	// Será el Indice de Filas de la Matríz resultante.						
					CargarVehicularEnMatriz();

					IndiceB = 1;	// Apuntará a los canales asociados para armarlos en la Matríz
					ConteoCanales--;
					while(ConteoCanales > 0)	// Si hubo almenos un canal más con la misma fase, se copia.
					{
						for(k_aux=0;k_aux<5;k_aux++) 
						{
							Matriz[CanalAsociado[IndiceB]][k_aux] = Matriz[CanalAsociado[0]][k_aux];
						}
						ConteoCanales--;	IndiceB++;
					}
				}
				else	// Si la Fase no posee un canal asociado, se actualiza el Tiempo Vehicular para
				{		// evitar que la próxima Fase del Secuence Data se salga de lugar.
					IndiceD = 62 + (SecuenceData[i_aux][j_aux] * 7);
					PreTimeVehic = PreTimeVehic + Datos[IndiceD] + Datos[IndiceD+1] 
									+ Datos[IndiceD+2]+Datos[IndiceD+3];
					// Se suman los tiempos de R/A, V, A y RC para apuntar al inicio de la siguiente Fase.
				}
				VerificarCanal(SecuenceData[i_aux][j_aux],3);
				if(CanalAsociado[0] != 0xFF)	// Si esta Fase Peatonal Posee un Canal Asociado:
				{
					IndiceD = 62 + (SecuenceData[i_aux][j_aux] * 7);
					IndiceC = 0;	// Ya Indice D se regresó al dato inicial.
					IndiceF = CanalAsociado[0];	// Será el Indice de Filas de la Matríz resultante.
					CargarPeatonalEnMatriz();

					IndiceB = 1;	// Apuntará a los canales asociados para armarlos en la Matríz
					ConteoCanales--;
					while(ConteoCanales > 0)	// Si hubo almenos un canal más con la misma fase, se copia.
					{
						for(k_aux=0;k_aux<5;k_aux++) 
						{
							Matriz[CanalAsociado[IndiceB]][k_aux] = Matriz[CanalAsociado[0]][k_aux];
						}
						ConteoCanales--;	IndiceB++;
					}
				}
			}
		}
	}
	
	printf("primera matriz\n");
	if(n== 0 || n==1) imprimir_matriz();
}

void OrdenarTiempos(int n)
{
	char PivoteAux = 0, DireccionP,DireccionH;
	char DetectarTC, PuntoVacio;

	DireccionP = 0;		// Índice de Tiempos Aux para ir llenando todos los acumulados usados en las matrices.
	for(ix=0;ix<16;ix++)
	{
		for(jx=0;jx<4;jx++)
		{			// Se almacenan todos los tiempos obtenidos en la Matriz.
			if(Matriz[ix][jx] != 0xFF)	// Si esta celda NO está Vacías.
			{
				while(Matriz[ix][jx] >= TiempoCiclo)	// Se ajusta el formato de todo tiempo >= que el Ciclo.
				{
					Matriz[ix][jx] -= TiempoCiclo;
				}
				TiemposAux[DireccionP] = Matriz[ix][jx];
				if(TiemposAux[DireccionP] == 0)		// Se ajusta el cero al valor del ciclo para evitar
				{									// errores de armado de la trama de intervalos.
					TiemposAux[DireccionP] = TiempoCiclo;
				}
				DireccionP++;	// Tras guardar un tiempo no vacío, se avanza a la siguiente posición.
			}
		}
		if(Matriz[ix][0] != 0xFF)	// Se exceptúan las Filas Vacías.
		{							// Se añade una columna auxiliar para posicionar ciertas fases.
			Matriz[ix][4] = Matriz[ix][0] + TiempoCiclo;	// Columna Auxiliar: Columna Cero + Ciclo.
		}
	}
	
	if(n== 0 || n==1) imprimir_matriz();
	
	if(n== 0 || n==1) imprimir_arreglo(TiemposAux, 128, "Tiempos Aux"); 
	
	for(ix=0;ix<16;ix++)	// Al final, se le suma el ciclo a los valores necesarios para que todas
	{					// las columnas hacia la derecha sean mayores que el tiempo anterior.
		for(jx=0;jx<4;jx++)
		{
			if(jx < 3 && Matriz[ix][jx] > Matriz[ix][jx+1] && Matriz[ix][jx+1] != 0xFF)
			{
				Matriz[ix][jx+1] += TiempoCiclo;
			}
		}
	}
	
	if(n== 0 || n==1) imprimir_matriz();
	
	for(ix=0;ix<16;ix+=2)		// Se añaden los tiempos de Overlaps por si en ellos se usan Fases Fantasmas.
	{
		for(jx=0;jx<16;jx++)	// 16 Filas (de 2 en 2) y  16 Columnas.
		{
			if(MatrizOL[ix][jx] != 0xFF && MatrizOL[ix][jx] != 0)	// Se toman los no vacíos y no ceros.
			{
				TiemposAux[DireccionP] = MatrizOL[ix][jx];	// Se añaden los Tiempos de Overlaps.
				DireccionP++;
			}
		}
	}
	
	for(ix=0;ix<120;ix++)		// Se organizan los tiempos de de Menor a Mayor.
	{
		for(jx=0;jx<119;jx++)
		{
			if(TiemposAux[jx] > TiemposAux[jx+1])
			{
				PivoteAux = TiemposAux[jx];	// Indice D se está usando como Auxiliar.
				TiemposAux[jx] = TiemposAux[jx+1];
				TiemposAux[jx+1] = PivoteAux;
			}
		}
	}
	
	if(n== 1 || n==0)imprimir_arreglo(TiemposAux, 128, "Tiempos Aux Ordenados"); 
	
	PivoteAux = 1;	// Se inicializa el índice del nuevo Array.
	TiemposAcumulados[0] = TiemposAux[0];
	for(ix=0;ix<120;ix++)					// Se Toman en cuentan únicamente los Tiempos Acumulados Diferentes.
	{
		if(TiemposAux[ix] != TiemposAcumulados[PivoteAux - 1] && TiemposAux[ix] != 0xFF)
		{
			TiemposAcumulados[PivoteAux] = TiemposAux[ix];
			PivoteAux++;
		}
	}
	
	if(n== 1 || n==0)imprimir_arreglo(TiemposAcumulados, 128, "Tiempos Acumulados"); 
	DetectarTC = 0;
	PuntoVacio = 0;
	TiemposIntervalos[0] = TiemposAcumulados[0];
	for(ix=1;ix<32;ix++)	// Se añade el Tiempo Ciclo si no está contenido aún en Tiempos Acumulados.
	{
		if(TiemposAcumulados[ix] == TiempoCiclo || TiemposAcumulados[ix] == 2*TiempoCiclo)
		{
			DetectarTC = 1;	// Flag de que ya está contenido el Tiempo Ciclo.
		}
		if(TiemposAcumulados[ix] == 0xFF && !PuntoVacio)	// Por las dudas, se guarda la dirección del
		{													// primer byte vacío del array.
			PuntoVacio = 1;
			DireccionH = ix;
		}
	}
	if(!DetectarTC)	// Si el array aún no incluye el Tiempo Ciclo, se añade en el primer punto vacío.
	{
		TiemposAcumulados[DireccionH] = TiempoCiclo;
	}

	for(ix=1;ix<31;ix++)	// Se obtienen los Tiempos de Intervalos a partir de los Acumulados
	{
		if(TiemposAcumulados[ix] != 0xFF)	// Al encontrar un cero significa que ya se recorrieron 
		{								// todos los tiempos que se utilizan.
			TiemposIntervalos[ix] = TiemposAcumulados[ix] - TiemposAcumulados[ix-1];
		}
	}
}

char EstudiarTypeModifier(char N_Overlap, char N_Fase, char N_Seleccion)
{		// Retorna 0 o 1 si la fase está incluida en los Modifiers. Para la Fase actual, carga Tipo y Color.
	 unsigned char DatoF=0, IndH=0;
	 unsigned char EncontradoB = 0;

	if(N_Fase == 0xFF)	// Si la Fase está vacía se le da por defecto el valor 2: Vacío.
	{
		DatoF = 2;
	}
	else
	{
		for(IndH=0;IndH<8;IndH++)	// Se recorre el Vector de Modifiers del Overlap en cuestión
		{
			if(OverLaps[N_Overlap+1][IndH] == N_Fase)	// Si la Fase en cuestión está siendo modificada, se
			{											// activa el flag.
				EncontradoB = 1;
			}
		}
		if(!EncontradoB)	// Si la Fase actual no está siendo modificada, se Retorna 0 (Normal).
		{
			DatoF = 0;
		}
		else			// Si la Fase Actual está siendo modificada, Inicialmente se retorna 1 (Modificada)
		{
			DatoF = 1;		// Modificado  --- Puede cambiarse más adelante si el caso es Peatonal Normal.
			if(!N_Seleccion)	// Si se trata de la Fase Actual (No la Siguiente), se llena lo demás.
			{
				if(TiposOverLaps[N_Overlap/2] == 1)		// Si este overlap es Sitra:
				{
					if(OL_Sitra[N_Overlap/2] == 0x1F)	// Si es Peatonal Normal.
					{
						DatoF = 0;	// Sin Modificar.
						TipoActual = 1;		// Peatonal.
						ColorActual = 0x1F;
					}
					else if(OL_Sitra[N_Overlap/2] & 0x10 == 0x10)	// Si es Peatonal - No Normal
					{
						TipoActual = 1;		// Peatonal.
						ColorActual = OL_Sitra[N_Overlap/2] & 0x0F;
					}
					else	// Si es Vehicular
					{
						TipoActual = 0;		// Vehicular.
						ColorActual = OL_Sitra[N_Overlap/2] & 0x0F;
					}
				}
				else 	// Si este overlap es Vehicular
				{
					TipoActual = 0;		// Vehicular.
					if(TiposOverLaps[N_Overlap/2] == 2)
					{
						ColorActual = 0;	// Apagado.
					}
					else if(TiposOverLaps[N_Overlap/2] == 3)
					{
						ColorActual = 1;	// Rojo.
					}
				}
			}
			else		// Si se trata de la Fase Siguiente (No la Actual)
			{
				if(TiposOverLaps[N_Overlap/2] == 1 && OL_Sitra[N_Overlap/2] == 0x1F)
				{			// Si la siguiente Fase es Peatonal Normal
					DatoF = 0;	// Se retorna como No Modificada.
				}
			}
		}
	}
	return DatoF;
}


void ArmarOverLaps(void)
{
	unsigned char i_aux=0, j_aux=0, k_aux=0, h_aux=0, m_aux=0, ModifActual=0, ModifSiguiente=0;
	unsigned char UltimoTCargado, PuntoInicial, Pivote2Aux, ColorDetectado, PosicionDetectada;
	char DeteccionTC, DeteccionD;

	IndiceA = 0; IndiceB = 0; IndiceC = 0; IndiceD = 0; IndiceF = 0;
	while(i_aux < 16 && OverLaps[i_aux][0] != 0xFF && OverLaps[i_aux][0] != 0)	// Se recorren los Overlaps.
	{
		IndiceF = 0;	// Inicio de Fila para cada OverLap.
		j_aux = 0;									// Se recorren las Fases Incluidas.
		while(j_aux < 16 && OverLaps[i_aux][j_aux] != 0xFF && OverLaps[i_aux][j_aux] != 0)
		{
			for(k_aux=0;k_aux<4;k_aux++)	// Se guarda la ubicación de la Fase en el Secuence Data
			{								// para aplicar el Ring Delay y los tiempos de Colores.
				for(h_aux=0;h_aux<8;h_aux++)
				{
					if(SecuenceData[k_aux][h_aux] == OverLaps[i_aux][j_aux])
					{
						IndiceA = k_aux;	// Ring
						IndiceB = h_aux;	// N° de Fase dentro del Ring.
					}
				}
			}
			PreTimeOL = RingDelay[IndiceA];	// El tiempo se cuenta a partir del Ring Delay
			if(IndiceB > 0)		// Si NO es la primera Fase de su Ring, se deben sumar los retardos que le
			{					// representan las fases previas del ring.
				for(k_aux=0;k_aux<IndiceB;k_aux++)
				{
					IndiceC = 62 + (SecuenceData[IndiceA][k_aux] * 7);			// La Fase 1 está a partir
					PreTimeOL = PreTimeOL + Datos[IndiceC] + Datos[IndiceC+1]	// de datos [69]
									+ Datos[IndiceC+2]+Datos[IndiceC+3];
				}
			}
			while(PreTimeOL > TiempoCiclo)	// Se ajusta el punto de inicio para que sea menor que el ciclo.
			{
				PreTimeOL -= TiempoCiclo;
			}
				// Se llenan los tiempos acumulados de Fases para tener base en la Estructura de OverLaps.
			// La Fase 1 está A partir del 69. La 2 a partir del 76 y así cada 7 bytes:
			IndiceD = 62 + (OverLaps[i_aux][j_aux] * 7);
			EstructuraOL[IndiceF][0] = PreTimeOL;		// Tiempo de Inicio
			EstructuraOL[IndiceF][1] = EstructuraOL[IndiceF][0] + Datos[IndiceD];	// R/A
			EstructuraOL[IndiceF][2] = EstructuraOL[IndiceF][1] + Datos[IndiceD+1];	// Verde
			EstructuraOL[IndiceF][3] = EstructuraOL[IndiceF][2] + Datos[IndiceD+2];	// Amarillo
			EstructuraOL[IndiceF][4] = EstructuraOL[IndiceF][3] + Datos[IndiceD+3];	// Red Clear

			if(TiposOverLaps[i_aux/2] == 1 && OL_Sitra[i_aux/2] == 0x1F)	// Si este Overlap es Peatonal se
			{	// reemplaza el Verde por Blanco y el Amarillo por Pedestrian Clear. El Red Clear no se modifica.
				EstructuraOL[IndiceF][2] = EstructuraOL[IndiceF][1] + Datos[IndiceD+5];	// Blanco
				EstructuraOL[IndiceF][3] = EstructuraOL[IndiceF][2] + Datos[IndiceD+6];	// Pedestrian Clear
			}
			IndiceF++;	// Se apunta a la siguiente Fila de la Estructura OL.
			j_aux++;	
		}
		for(h_aux=0;h_aux<IndiceF;h_aux++)
		{
			if(h_aux > 0 && EstructuraOL[h_aux][0] < EstructuraOL[h_aux-1][0])
			{
				for(k_aux=0;k_aux<5;k_aux++)	// Se ajustan todos los puntos de inicio para que cada
				{								//  Fase siguiente inicie en un tiempo acumulado mayor.
					EstructuraOL[h_aux][k_aux] += TiempoCiclo;
				}
			}
		}
		UltimoTCargado = 0;
		h_aux = 0;	// Columnas para llenar.
		k_aux = 0;	// Filas para llenar
		while(h_aux < 8 && EstructuraOL[h_aux][0] != 0xFF)		// LLENANDO LA MATRIZ DE OVERLAPS
		{
	// Se envía el N° de Overlap, el N° de Fase y el indicador de Actual(0) o Siguiente (1), y se recibe 
	// un 1 o 0 (Modificado o No). Se carga "Color Actual" y "Tipo Actual" indica si es Peatonal o Vehicular 
			ModifActual = EstudiarTypeModifier(i_aux,OverLaps[i_aux][h_aux],0);
			if(h_aux < 7)	// Si la última fase no es la última, averigua si la siguiente tiene modificador.
			{
				ModifSiguiente = EstudiarTypeModifier(i_aux,OverLaps[i_aux][h_aux+1],1);
			}
			else
			{
				ModifSiguiente = 0;	// La actual es la última Fase del OL, se asume que la siguiente es normal.
			}
			if(k_aux == 0)	// Si este es el primer valor que se está cargando, se almacena el punto de inicio
			{				// de su movimiento + Ciclo para determinar el fin del overlap.
				PuntoInicial = EstructuraOL[h_aux][0] + TiempoCiclo;
			}
			if(ModifActual == 0)	// 0: Fase Normal: sin modificar (No está incluida en los Modifiers)
			{						// o Peatonal normal (0x1F)
				if(EstructuraOL[h_aux][1] > UltimoTCargado)	// Si el Tiempo de R/A no se ha pasado, se añade.
				{
					MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][1];	// Se guarda el R/A de la Fase actual.
					MatrizOL[i_aux +1][k_aux] = 3;		// Color R/A.
					if(TipoActual == 1)	// Si este Overlap es Peatonal, no va R/A, sino Rojo
					{
						MatrizOL[i_aux +1][k_aux] = 1;		// Color Rojo (Peatonal).					
					}
					k_aux++;	// Se apunta a la siguiente dirección de guardado.
					UltimoTCargado = EstructuraOL[h_aux][1];	// Se almacena para controlar.
				}
				if(EstructuraOL[h_aux+1][0] != 0xFF)	// Si esta NO es la última Fase del Overlap.
				{
					if(EstructuraOL[h_aux][4] >= EstructuraOL[h_aux+1][0]) // Si las Fases Se Solapan 
					{
						if(ModifSiguiente == 0)	// Si la Siguiente fase es Verde (Normal)		CASO N° 1.
						{								// Se guarda Verde (o Blanco) de la Siguiente Fase.
							MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux+1][2];
							MatrizOL[i_aux +1][k_aux] = 4;	// Color Verde o Color Blanco.
							k_aux++;
							UltimoTCargado = EstructuraOL[h_aux+1][2];	// Se almacena para controlar.
						}
						else	// Si la siguiente Fase Sí Está Modificada.						CASO N° 3.
						{
							MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][2]; // Se guarda el Verde de la Fase Actual.
							MatrizOL[i_aux +1][k_aux] = 4;		// Color Verde.
							k_aux++;
							UltimoTCargado = EstructuraOL[h_aux][2];
		
							MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][3]; // Se guarda el Amarillo de la Fase Actual.
							MatrizOL[i_aux +1][k_aux] = 2;		// Color Amarillo.
							if(TipoActual == 1)	// Si este Overlap es Peatonal, va Rojo Titilante.
							{
								
								MatrizOL[i_aux +1][k_aux] = 9;		// Rojo Titilante (Peatonal).
							}
							k_aux++;
							UltimoTCargado = EstructuraOL[h_aux][3];

							MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][4];	// Se guarda el Rojo de Fin de Fase
							MatrizOL[i_aux +1][k_aux] = 1;	// Color Rojo.
							k_aux++;
							UltimoTCargado = EstructuraOL[h_aux][4];										
						}
					}
					else				// Si las Fases NO Se Solapan							CASOS N° 2 y 4.
					{
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][2]; // Se guarda el Verde de la Fase Actual.
						MatrizOL[i_aux +1][k_aux] = 4;		// Color Verde o Blanco.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux][2];
		
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][3]; // Se guarda el Amarillo de la Fase Actual.
						MatrizOL[i_aux +1][k_aux] = 2;		// Color Amarillo.
						if(TipoActual == 1)		// Si este Overlap es Peatonal, va Rojo Titilante.
						{
							MatrizOL[i_aux +1][k_aux] = 9;		// Rojo Titilante (Peatonal).
						}
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux][3];

						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux+1][0];	// Se extiende el Rojo hasta el 
						MatrizOL[i_aux +1][k_aux] = 1;	// Color Rojo.		// Inicio de la siguiente Fase.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux+1][0];
					}
				}
				else		// Si esta SÍ es la última Fase del Overlap.
				{
					if(EstructuraOL[h_aux][2] > UltimoTCargado)	// Si el Verde de esta Fase no ha sido Cargado aún
					{
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][2]; // Se guarda el Verde de la Fase Actual.
						MatrizOL[i_aux +1][k_aux] = 4;		// Color Verde o Blanco.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux][2];	// Se almacena para controlar.					
					}
					// Independientemente del if anterior, se guarda el Amarillo y Rojo
					MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][3]; // Se guarda el Amarillo de la Fase Actual.
					MatrizOL[i_aux +1][k_aux] = 2;		// Color Amarillo.
					if(TipoActual == 1)		// Si este Overlap es Peatonal, va Rojo Titilante.
					{
						MatrizOL[i_aux +1][k_aux] = 9;		// Rojo Titilante (Peatonal).
					}
					k_aux++;
					UltimoTCargado = EstructuraOL[h_aux][3];
						
					MatrizOL[i_aux][k_aux] = PuntoInicial; // Se extiende el Rojo hasta el fin del Overlap.
					MatrizOL[i_aux +1][k_aux] = 1;	// Color Rojo.
					k_aux++;
					UltimoTCargado = PuntoInicial;
				}
			}
			else if(ModifActual == 1)		//		Esta Fase Sí está siendo Modificada.
			{			// El Tipo y el Color ya fueron cargados en Type Modifier
				if(EstructuraOL[h_aux+1][0] != 0xFF)	// Si esta NO es la última Fase del Overlap.
				{
					if(EstructuraOL[h_aux][4] >= EstructuraOL[h_aux+1][0])	// Si están Solapadas	// CASOS N° 5 y 7.
					{		// se asigna el Color Cargado hasta el Punto de Inicio de la siguiente Fase
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux+1][0];
						MatrizOL[i_aux +1][k_aux] = ColorActual;	// Color de Modificador.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux+1][0];	// Se almacena para controlar.
					}
					else	// Si las Fases NO están Solapadas										// CASOS N° 6 y 8.
					{
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][4]; // Se asigna el color hasta el Fin de Fase.
						MatrizOL[i_aux +1][k_aux] = ColorActual;	// Color de Modificador.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux][4];
		
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux+1][0];	// Se rellena con Rojo hasta el Inicio
						MatrizOL[i_aux +1][k_aux] = 1;	// Color Rojo.		// de la Siguiente Fase.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux+1][0];
					}
				}
				else			// Si esta SÍ es la última Fase del Overlap.
				{					
					if(ColorActual == 0x1F)			// Si esta Fase es una Peatonal Normal.
					{
						if(EstructuraOL[h_aux][2] > UltimoTCargado)	// Si el Blanco de esta Fase no ha sido Cargado
						{
							MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][2]; // Se guarda el Blanco de la Fase.
							MatrizOL[i_aux +1][k_aux] = 4;		// Color Blanco.
							k_aux++;
							UltimoTCargado = EstructuraOL[h_aux][2];	// Se almacena para controlar.					
						}

						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][3]; // Se guarda el Despeje Peatonal.
						MatrizOL[i_aux +1][k_aux] = 9;		// Rojo Titilante.
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux][3];
						
						MatrizOL[i_aux][k_aux] = PuntoInicial; // Se extiende el Rojo hasta el fin del Overlap.
						MatrizOL[i_aux +1][k_aux] = 1;	// Color Rojo.
						k_aux++;
						UltimoTCargado = PuntoInicial;						
					}
					else		// Si esta Fase NO es una Peatonal Normal, se arma el Color Actual normalmente.
					{
						MatrizOL[i_aux][k_aux] = EstructuraOL[h_aux][4]; // Se guarda el Tiempo de Red Clear Actual.
						MatrizOL[i_aux +1][k_aux] = ColorActual;	// Color de Modificador
						k_aux++;
						UltimoTCargado = EstructuraOL[h_aux][4];	// Se almacena para controlar.					

						MatrizOL[i_aux][k_aux] = PuntoInicial; // Se extiende el Rojo hasta el fin del Overlap.
						MatrizOL[i_aux +1][k_aux] = 1;	// Color Rojo.
						k_aux++;
						UltimoTCargado = PuntoInicial;
					}
				}
			}
			h_aux++;
		}
		i_aux+=2;
	}

	k_aux = 0;
	Pivote2Aux = 0;
	while(k_aux < 16 && MatrizOL[k_aux][0] != 0xFF)	// Se recorre toda la Matriz para incluir
	{												// Tiempo Ciclo donde haga falta
		DeteccionTC = 0;	// Permite detectar si hay un Tiempo Ciclo incluido en el Overlap.
		for(i_aux=0;i_aux<16;i_aux++)	// Se recorre el Overlap en busca de Tiempo Ciclo (o múltiplo)
		{
			if(MatrizOL[k_aux][i_aux] == TiempoCiclo || MatrizOL[k_aux][i_aux] == 2*TiempoCiclo)
			{
				DeteccionTC = 1; // Si este Overlap ya incluye TiempoCiclo, se marca con 1.
			}
		}
		if(!DeteccionTC)	// De no estar incluido el Tiempo Ciclo, debe añadirse.
		{
			for(i_aux=0;i_aux<16;i_aux++)	// Primero, se organiza el Vector parcial de la Matriz para
			{								// poder hallar el punto donde va el Tiempo Ciclo.
				for(j_aux=0;j_aux<15;j_aux++)
				{
					if(MatrizOL[k_aux][j_aux] > MatrizOL[k_aux][j_aux+1])
					{
						Pivote2Aux = MatrizOL[k_aux][j_aux];	// Se intercambian los puntos (Tiempo)
						MatrizOL[k_aux][j_aux] = MatrizOL[k_aux][j_aux+1];
						MatrizOL[k_aux][j_aux+1] = Pivote2Aux;

						Pivote2Aux = MatrizOL[k_aux+1][j_aux];	// Se intercambian los puntos (Color)
						MatrizOL[k_aux+1][j_aux] = MatrizOL[k_aux+1][j_aux+1];
						MatrizOL[k_aux+1][j_aux+1] = Pivote2Aux;
					}
				}
			}
			DeteccionD = 0;
			ColorDetectado = 0;
			for(i_aux=0;i_aux<16;i_aux++)
			{
				if(MatrizOL[k_aux][i_aux] < TiempoCiclo && MatrizOL[k_aux][i_aux+1] > TiempoCiclo)
				{
					ColorDetectado = MatrizOL[k_aux+1][i_aux+1];	// Se guarda el Color del siguiente intervalo
				}													// como Color en Tiempo Ciclo.
				if(MatrizOL[k_aux][i_aux] == 0xFF && !DeteccionD)
				{
					PosicionDetectada = i_aux;	// Se guarda la posición del primer byte vacío de ese Vector.
					DeteccionD = 1;
				}
			}
			MatrizOL[k_aux][PosicionDetectada] = TiempoCiclo;	// Se carga el registro de Tiempo Ciclo.
			MatrizOL[k_aux+1][PosicionDetectada] = ColorDetectado;
		}
		k_aux += 2;
	}
	for(i_aux=0;i_aux<16;i_aux+=2)	// Se ajustan todos los valores para que no sean mayores que Tiempo Ciclo
	{							
		for(j_aux=0;j_aux<16;j_aux++)
		{
			if(MatrizOL[i_aux][j_aux] != 0xFF && MatrizOL[i_aux][j_aux] > TiempoCiclo)
			{
				MatrizOL[i_aux][j_aux] -= TiempoCiclo;
			}
		}
	}
	k_aux = 0;
	Pivote2Aux = 0;
	while(k_aux < 16 && MatrizOL[k_aux][0] != 0xFF)	// Ordena Nuevamente cada Vector de la Matríz de menor a mayor.
	{												
		for(i_aux=0;i_aux<16;i_aux++)	// Primero, se organiza el Vector parcial de la Matriz para
		{								// poder hallar el punto donde va el Tiempo Ciclo.
			for(j_aux=0;j_aux<15;j_aux++)
			{
				if(MatrizOL[k_aux][j_aux] > MatrizOL[k_aux][j_aux+1])
				{
					Pivote2Aux = MatrizOL[k_aux][j_aux];	// Se intercambian los puntos (Tiempo)
					MatrizOL[k_aux][j_aux] = MatrizOL[k_aux][j_aux+1];
					MatrizOL[k_aux][j_aux+1] = Pivote2Aux;

					Pivote2Aux = MatrizOL[k_aux+1][j_aux];	// Se intercambian los puntos (Color)
					MatrizOL[k_aux+1][j_aux] = MatrizOL[k_aux+1][j_aux+1];
					MatrizOL[k_aux+1][j_aux+1] = Pivote2Aux;
				}
			}
		}
		k_aux += 2;
	}

	i_aux = 0;
	Pivote2Aux = 0;
	while(i_aux < 16 && MatrizOL[i_aux][0] != 0xFF)		// Se Comprimen los Arrays cuando el mismo color está
	{													// Repetido en dos intervalos seguidos.
		for(j_aux=0;j_aux<15;j_aux++)	// Primero, se organiza el Vector parcial de la Matriz para
		{								// poder hallar el punto donde va el Tiempo Ciclo.
			if(MatrizOL[i_aux+1][j_aux+1] != 0xFF && MatrizOL[i_aux+1][j_aux] == MatrizOL[i_aux+1][j_aux+1])
			{		// Si el color del siguiente intervalo es el mismo y no es vacío, se comprime.
				for(k_aux=j_aux;k_aux<15;k_aux++)	// Se guarda cada valor siguiente en el actual.
				{
					MatrizOL[i_aux][k_aux] = MatrizOL[i_aux][k_aux+1];
					MatrizOL[i_aux+1][k_aux] = MatrizOL[i_aux+1][k_aux+1];
				}
			}
		}
		i_aux += 2;
	}
}


void ArmarTelegramas(int n)
{
	/* TELEGRAMA 0x69: Parte Alta de Estructura: */

	int i=0, _num_grupo=0,_num_cruce=0,num_cruce_separado[2], NumeroTelegrama=0;
	
	_num_grupo = get_variable(7);
	_num_cruce = get_variable(6);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	Telegrama69[0] = 0x00;	
	Telegrama69[1] = 0x00;	
	Telegrama69[2] = 0x00;	
	Telegrama69[3] = 0x00;
	Telegrama69[4] = 0xFF;	
	Telegrama69[5] = 0x00;	
	Telegrama69[6] = 0x00;	
	Telegrama69[7] = _num_grupo;
	Telegrama69[8] = 0x69;	// Identificador de Trama.
	Telegrama69[9] = 0x00;
	Telegrama69[10]= 0xF6;	// Longitud de Trama.
	
	for(i=0;i<11;i++) Telegrama69[11]^=Telegrama69[i];

//	Telegrama69[11]= 0x00; 	// BCC temporalmente Vacío (Posición 12).
	Telegrama69[12]= num_cruce_separado[0];
	Telegrama69[13]= num_cruce_separado[1];	
	Telegrama69[14]= n; 	// Número de Estructura

	for(ix=15;ix<249;ix++)	Telegrama69[ix] = Telegrama69[ix];	// Se cargan los Datos ya calculados.

	for(i=11;i<249;i++) Telegrama69[249]^=Telegrama69[i];

//		Telegrama69[11] = Calcular_BCC(0,11);
//		Telegrama69[249] = Calcular_BCC(11,249);



	/*	TELEGRAMA 0x6A: Parte Baja de Estructura:		*/
	Telegrama6A[0] = 0x00;	
	Telegrama6A[1] = 0x00;	
	Telegrama6A[2] = 0x00;	
	Telegrama6A[3] = 0x00;
	Telegrama6A[4] = 0xFF;	
	Telegrama6A[5] = 0x00;	
	Telegrama6A[6] = 0x00;	
	Telegrama6A[7] = _num_grupo; 
	Telegrama6A[8] = 0x6A;	// Identificador de Trama.
	Telegrama6A[9] = 0x00;
	Telegrama6A[10]= 0xF6;	// Longitud de Trama.

	for(i=0;i<11;i++) Telegrama6A[11]^=Telegrama6A[i];

//	UART_Pila[11]= 0x00; 	// BCC temporalmente Vacío (Posición 12).
	Telegrama6A[12]= num_cruce_separado[0];
	Telegrama6A[13]= num_cruce_separado[1];
	
	Telegrama6A[14]= n; 	// Número de Estructura

	for(ix=15;ix<222;ix++)	Telegrama6A[ix] = Telegrama6A[ix];	// Se carga el Telegrema Armado.

	IndiceA = 223;	// Ubicación del Paso A del Grupo 1.
	IndiceC = NumVehic;		// Número de Grupos Vehiculares.
	while(IndiceC >= 1)		// Se cargan tantos pares de Grupos en A y B como Vehiculares Declarados haya.
	{
		Telegrama6A[IndiceA] = 0xAA;		// Amarillo Titilante.
		Telegrama6A[IndiceA+13] = 0x11;	// Rojo Fijo.
		IndiceA++;		// Se apunta al siguiente paso.
		IndiceC--;	
	}
//		UART_Pila[11] = Calcular_BCC(0,11);
//		UART_Pila[249] = Calcular_BCC(11,249);
	for(i=11;i<249;i++) Telegrama6A[249]^=Telegrama6A[i];

	/*	TELEGRAMA 0x72: Programa de Tiempos: */

	Telegrama72[0] = 0x00;	
	Telegrama72[1] = 0x00;	
	Telegrama72[2] = 0x00;	
	Telegrama72[3] = 0x00;
	Telegrama72[4] = 0xFF;	
	Telegrama72[5] = 0x00;	
	Telegrama72[6] = 0x00;	
	Telegrama72[7] = _num_grupo;
	Telegrama72[8] = 0x72;	// Identificador de Trama.
	Telegrama72[9] = 0x00;
	Telegrama72[10]= 0x48;	// Longitud de Trama.

	for(i=0;i<11;i++) Telegrama72[11]^=Telegrama72[i];
	Telegrama72[12]= num_cruce_separado[0];
	Telegrama72[13]= num_cruce_separado[1];

	Telegrama72[14]= n; 	// Número de Plan de Tiempos

	i=0;
	while(TiemposIntervalos[i] != 0xFF && i<32)
	{
		Telegrama72[i+15] = TiemposIntervalos[i];	// Se cargan los Datos ya calculados.
		i++;
	}
	Telegrama72[49] = 5;	// Tiempos de la PEM.
	Telegrama72[50] = 5;
	Telegrama72[52] = TiempoCiclo;
	Telegrama72[54] = TiempoCiclo * 2;

//	Telegrama72[11] = Calcular_BCC(0,11);
//	Telegrama72[75] = Calcular_BCC(11,75);
	for(i=11;i<75;i++) Telegrama72[75]^=Telegrama72[i];
	
}

char ObtenerColor(char TiempoF,char GrupoF)
{
	char ColorC=0, DatoC=0, IndR=0, n_aux=0;

	if(Channel[GrupoF][1] == 4)				// Si en este Channel Sí va un Overlap (Vehicular o Peatonal), 
	{										// se opera con sus valores.
		IndR = (Channel[GrupoF][0] - 1) * 2;	// Se apunta al índice de la MatrízOL []
		for(n_aux=0;n_aux<15;n_aux++)
		{
			if(TiempoF <= MatrizOL[IndR][0])	// Si el Tiempo buscado es abarcado por el primer intervalo
			{									// del Overlap, se retorna su color.
				ColorC = MatrizOL[IndR+1][0];
			}
			else if(TiempoF > MatrizOL[IndR][n_aux] && TiempoF <= MatrizOL[IndR][n_aux+1])
			{									// Si el tiempo buscado está entre el tiempo actual y el siguiente,
				ColorC = MatrizOL[IndR+1][n_aux+1];	// se retorna el tiempo del siguiente.
			}
		}
	}
	else			// Si en este Channel NO va un Overlap, se opera normalmente.
	{
		if(Matriz[GrupoF][0] != 0xFF)	// Si esta fila no está vacía, se opera.
		{
			for(IndR=0;IndR<4;IndR++)
			{
				if((TiempoF > Matriz[GrupoF][IndR] && TiempoF <= Matriz[GrupoF][IndR + 1]) || 
				((TiempoF + TiempoCiclo) > Matriz[GrupoF][IndR] && 
				(TiempoF + TiempoCiclo) <= Matriz[GrupoF][IndR+1]))
				{
					DatoC = IndR + 2;	// Las Columnas Válidas están en orden: 2, 3, 4 y 1.
					if(DatoC == 5)	DatoC = 1;	// Se ajusta la aparición del 1 como 5 Auxiliar.
				}
			}
			if(DatoC != 0 && Channel[GrupoF][1] == 3)	// Si este Grupo es Peatonal, se ajusta el valor para la Tabla.
			{
				DatoC += 4;
			}
		}
		switch(DatoC)
		{
			case 0:		ColorC = 0x00;	break;		// Apagado.
			case 1:		ColorC = 0x03;	break;		// Rojo + Amarillo.
			case 2:		ColorC = 0x04;	break;		// Verde.
			case 3:		ColorC = 0x02;	break;		// Amarillo.
			case 4:		ColorC = 0x01;	break;		// Rojo.
			case 5:		ColorC = 0x01;	break;		// Rojo - Peatonal.
			case 6:		ColorC = 0x04;	break;		// Verde - Blanco Peatonal.
			case 7:		ColorC = 0x09;	break;		// Rojo Titilante - Petatonal.
			case 8:		ColorC = 0x01;	break;		// Rojo - Peatonal - Redundante.
			default:	ColorC = 0x00;	break;	// Apagado
		}
	}
	return ColorC;
}


void CargarDatosObjetos(int plan)
{

	int j=0, verde_v[16], verde_p[16];
	
	struct phaseTable_entry       ** p_phase=NULL;
	struct patternTable_entry     ** p_pattern=NULL;
	struct channelTable_entry     ** p_channel=NULL;
	struct sequenceTable_entry       sequencia[30][4];
	int								 splits[32][16];
	struct tiemposTable_entry     ** p_tiempos=NULL;
//	struct anilloTable_entry 	  ** p_anillo=NULL;
	struct anilloTable_entry 	  	 anillos[30][4];
	struct overlapTable_entry 	  ** p_overlap=NULL;
	struct overlapPlusTable_entry ** p_overlapPlus=NULL;

/*	####################  EJEMPLO 9  ############################  */

	p_phase 	  = get_phase_table();
	p_pattern 	  = get_pattern_table();
	p_channel 	  = get_channel_table();
	get_sequence_table(sequencia);
	get_splits(splits);
	p_tiempos 	  = get_tiempos_table();
//	p_anillo 	  = get_anillo_table();
	get_anillo_table (anillos);
	p_overlap 	  = get_overlap_table();
	p_overlapPlus = get_overlapPlus_table();
	
	printf("Desfasaje anillos\n");
	
	for(int j=0;j<4;j++) {
		printf("%i ",anillos[plan][j].desfasajeAnillo);
	}
	printf("\n");
	
	
	Datos[0] = p_pattern[plan]->patternCycleTime;	// Ciclo

//	CHANNELS:
	// N° de Fase			// Tipo (Vehic/Peat)	// N° de Fase			// Tipo (Vehic/Peat)

	for(int i=0;i<16;i++)
	{
		Datos[j+1] = p_channel[i]->channelControlSource;
		Datos[j+2] = p_channel[i]->channelControlType;
		j+=2;
	}			

// obteniendo datos de SEQUENCE DATA, solo se lee los primeros 8 movimientos de los 4 Rings del
// 1° SequenceData

	j=0;

	for(int k=0;k<4;k++)
	{
		for(int l=0;l<8;l++)
		{
			Datos[j+33] = sequencia[plan][k].sequenceData[l];
//			printf("%i ",sequencia[i][k].sequenceData[l]);
			j++;	
		}
	}

	
//	printf("\n");

// RING DELAYS:
	
	for(int i=0;i<4;i++)
	{
		//Datos[i+65] = p_anillo[i]->desfasajeAnillo;
		  Datos[i+65] = anillos[plan][i].desfasajeAnillo;
	}
//	Datos[65] = 95;			Datos[66] = 28;			Datos[67] = 60;			Datos[68] = 0;


// TIEMPOS DE FASES:		R/A	V	A	RC			PD	W	PC
	// Fase 1:		

//	printf("Rojo Amarillo:\n");
	j=0;
	
	for(int i=0;i<16;i++)
	{
		//aplicando formula para encontrar verde sobrante del split
		// verde_vehicular = split - yellow - Red Clear - Rojo_Amarillo
		verde_v[i] = splits[plan][i] - convertir_decena_seg(p_phase[i]->phaseYellowChange) 
		- convertir_decena_seg(p_phase[i]->phaseRedClear) - p_tiempos[i]->tiemposRojoAmarillo;
		
		verde_p[i] = verde_v[i] - p_phase[i]->phasePedestrianClear;
		
		if(verde_v[i] < 0) 
		{
			verde_v[i] = 0;
			verde_p[i] = 0;
		}
		
		Datos[j+69] = p_tiempos[i]->tiemposRojoAmarillo;
//		printf("verde vehicular: %i\n",verde_v[i]);
//		Datos[j+70] = p_phase[i]->phaseMinimumGreen;
		
		Datos[j+70] = verde_v[i];
		Datos[j+71] = convertir_decena_seg(p_phase[i]->phaseYellowChange);
		Datos[j+72] = convertir_decena_seg(p_phase[i]->phaseRedClear);

		Datos[j+73] = p_tiempos[i]->desfasajePeatonal;	//Phase Delay
//		printf("verde peatonal: %i\n",verde_p[i]);	
//		Datos[j+74] = p_phase[i]->phaseWalk;
		Datos[j+74] = verde_p[i];						
		Datos[j+75] = p_phase[i]->phasePedestrianClear;

		j+=7;
	}
	
	for(int i=0;i<16;i++)
	{
		Datos[i+181] = p_overlap[i]->overlapType;
		Datos[i+197] = p_overlapPlus[i]->overlapTypePlus;
	}
	
	
	j=0;
	for(int i=0;i<8;i++)
	{
		for(int k=0;k<8;k++)
		{
			Datos[j+213+k] = p_overlap[i]->overlapIncludedPhases[k];
			Datos[j+221+k] = p_overlap[i]->overlapModifierPhases[k];

		}
		j+=16;
	}	
	


//	printf("Desfasaje peatonal 1: %i\n",p_tiempos[0]->desfasajePeatonal);

//imprimir datos
/*	
	for(int i=0;i<470;i++)
	{
		printf("%i ",Datos[i]);
	}
	printf("\n\n");
*/

/*	#############################################################  */

/*	####################  EJEMPLO 9  ############################  */
/*
	Datos[0] = 100;	// Ciclo
//	CHANNELS:
	// N° de Fase			// Tipo (Vehic/Peat)	// N° de Fase			// Tipo (Vehic/Peat)
	Datos[1] = 1;			Datos[2] = 2;			Datos[3] = 2;			Datos[4] = 2;
	Datos[5] = 2;			Datos[6] = 2;			Datos[7] = 2;			Datos[8] = 2;
	Datos[9] = 3;			Datos[10] = 2;			Datos[11] = 4;			Datos[12] = 2;
	Datos[13] = 4;			Datos[14] = 3;			Datos[15] = 5;			Datos[16] = 3;
	Datos[17] = 6;			Datos[18] = 3;			Datos[19] = 7;			Datos[20] = 3;
	Datos[21] = 2;			Datos[22] = 3;			Datos[23] = 0;			Datos[24] = 0;
	Datos[25] = 0;			Datos[26] = 0;			Datos[27] = 0;			Datos[28] = 0;
	Datos[29] = 0;			Datos[30] = 0;			Datos[31] = 0;			Datos[32] = 0;
// SECUENCE DATA (4 Rings de hasta 8 Fases):
		// Ring 1:
	Datos[33] = 4;			Datos[34] = 3;			Datos[35] = 1;			Datos[36] = 0;
	Datos[37] = 0;			Datos[38] = 0;			Datos[39] = 0;			Datos[40] = 0;
		// Ring 2:	
	Datos[41] = 6;			Datos[42] = 2;			Datos[43] = 0;			Datos[44] = 0;
	Datos[45] = 0;			Datos[46] = 0;			Datos[47] = 0;			Datos[48] = 0;
		// Ring 3:
	Datos[49] = 7;			Datos[50] = 5;			Datos[51] = 0;			Datos[52] = 0;
	Datos[53] = 0;			Datos[54] = 0;			Datos[55] = 0;			Datos[56] = 0;
		// Ring 4:
	Datos[57] = 0;			Datos[58] = 0;			Datos[59] = 0;			Datos[60] = 0;
	Datos[61] = 0;			Datos[62] = 0;			Datos[63] = 0;			Datos[64] = 0;
// RING DELAYS:
	Datos[65] = 47;			Datos[66] = 47;			Datos[67] = 85;			Datos[68] = 0;

// TIEMPOS DE FASES:		R/A	V	A	RC			PD	W	PC
	// Fase 1:									Fase 1:
	Datos[69] = 2;			Datos[70] = 44;			Datos[71] = 3;			Datos[72] = 0;
	Datos[73] = 0xFF;		Datos[74] = 0;			Datos[75] = 0;
	// Fase 2:									Fase 2:
	Datos[76] = 2;			Datos[77] = 57;			Datos[78] = 3;			Datos[79] = 0;
	Datos[80] = 0xFF;		Datos[81] = 51;			Datos[82] = 6;
	// Fase 3:									Fase 3:
	Datos[83] = 2;			Datos[84] = 8;			Datos[85] = 3;			Datos[86] = 0;
	Datos[87] = 0xFF;		Datos[88] = 0;			Datos[89] = 0;
	// Fase 4:									Fase 4:
	Datos[90] = 2;			Datos[91] = 30;			Datos[92] = 3;			Datos[93] = 3;
	Datos[94] = 0xFF;		Datos[95] = 15;			Datos[96] = 15;
	// Fase 5:									Fase 5:
	Datos[97] = 2;			Datos[98] = 38;			Datos[99] = 3;			Datos[100] = 0;
	Datos[101] = 0xFF;		Datos[102] = 30;		Datos[103] = 8;
	// Fase 6:									Fase 6:
	Datos[104] = 2;			Datos[105] = 33;		Datos[106] = 3;			Datos[107] = 0;
	Datos[108] = 0xFF;		Datos[109] = 22;		Datos[110] = 8;
	// Fase 7:									Fase 7:
	Datos[111] = 0;			Datos[112] = 59;		Datos[113] = 3;			Datos[114] = 0;
	Datos[115] = 0xFF;		Datos[116] = 53;		Datos[117] = 6;
	// Fase 8:									Fase 8:
	Datos[118] = 0;			Datos[119] = 0;			Datos[120] = 0;			Datos[121] = 0;
	Datos[122] = 0xFF;		Datos[123] = 0;			Datos[124] = 0;
	// Fase 9:									Fase 9:
	Datos[125] = 0;			Datos[126] = 0;			Datos[127] = 0;			Datos[128] = 0;
	Datos[129] = 0xFF;		Datos[130] = 0;			Datos[131] = 0;
	// Fase 10:									Fase 10:
	Datos[132] = 0;			Datos[133] = 0;			Datos[134] = 0;			Datos[135] = 0;
	Datos[136] = 0xFF;		Datos[137] = 0;			Datos[138] = 0;
	// Fase 11:									Fase 11:
	Datos[139] = 0;			Datos[140] = 0;			Datos[141] = 0;			Datos[142] = 0;
	Datos[143] = 0xFF;		Datos[144] = 0;			Datos[145] = 0;
	// Fase 12:									Fase 12:
	Datos[146] = 0;			Datos[147] = 0;			Datos[148] = 0;			Datos[149] = 0;
	Datos[150] = 0xFF;		Datos[151] = 0;			Datos[152] = 0;
	// Fase 13:									Fase 13:
	Datos[153] = 0;			Datos[154] = 0;			Datos[155] = 0;			Datos[156] = 0;
	Datos[157] = 0xFF;		Datos[158] = 0;			Datos[159] = 0;
	// Fase 14:									Fase 14:
	Datos[160] = 0;			Datos[161] = 0;			Datos[162] = 0;			Datos[163] = 0;
	Datos[164] = 0xFF;		Datos[165] = 0;			Datos[166] = 0;
	// Fase 15:									Fase 15:
	Datos[167] = 0;			Datos[168] = 0;			Datos[169] = 0;			Datos[170] = 0;
	Datos[171] = 0xFF;		Datos[172] = 0;			Datos[173] = 0;
	// Fase 16:									Fase 16:
	Datos[174] = 0;			Datos[175] = 0;			Datos[176] = 0;			Datos[177] = 0;
	Datos[178] = 0xFF;		Datos[179] = 0;			Datos[180] = 0;
*/

/*	####################  EJEMPLO 13 - Municipalidad   ############################  */

/*
	Datos[0] = 100;	// Ciclo
//	CHANNELS:
	// N° de Fase			// Tipo (Vehic/Peat)	// N° de Fase			// Tipo (Vehic/Peat)
	Datos[1] = 1;			Datos[2] = 4;			Datos[3] = 2;			Datos[4] = 1;
	Datos[5] = 2;			Datos[6] = 2;			Datos[7] = 2;			Datos[8] = 3;
	Datos[9] = 3;			Datos[10] = 4;			Datos[11] = 5;			Datos[12] = 3;
	Datos[13] = 7;			Datos[14] = 2;			Datos[15] = 8;			Datos[16] = 3;
	Datos[17] = 0;			Datos[18] = 0;			Datos[19] = 0;			Datos[20] = 0;
	Datos[21] = 0;			Datos[22] = 0;			Datos[23] = 0;			Datos[24] = 0;
	Datos[25] = 0;			Datos[26] = 0;			Datos[27] = 0;			Datos[28] = 0;
	Datos[29] = 0;			Datos[30] = 0;			Datos[31] = 0;			Datos[32] = 0;
// SECUENCE DATA (4 Rings de hasta 8 Fases):
		// Ring 1:
	Datos[33] = 1;			Datos[34] = 2;			Datos[35] = 3;			Datos[36] = 0;
	Datos[37] = 0;			Datos[38] = 0;			Datos[39] = 0;			Datos[40] = 0;
		// Ring 2:	
	Datos[41] = 4;			Datos[42] = 5;			Datos[43] = 6;			Datos[44] = 0;
	Datos[45] = 0;			Datos[46] = 0;			Datos[47] = 0;			Datos[48] = 0;
		// Ring 3:
	Datos[49] = 8;			Datos[50] = 7;			Datos[51] = 0;			Datos[52] = 0;
	Datos[53] = 0;			Datos[54] = 0;			Datos[55] = 0;			Datos[56] = 0;
		// Ring 4:
	Datos[57] = 0;			Datos[58] = 0;			Datos[59] = 0;			Datos[60] = 0;
	Datos[61] = 0;			Datos[62] = 0;			Datos[63] = 0;			Datos[64] = 0;
// RING DELAYS:
	Datos[65] = 0;			Datos[66] = 0;			Datos[67] = 0;			Datos[68] = 0;

// TIEMPOS DE FASES:		R/A		V	A	RC				PD	W	PC
	// Fase 1:									Fase 1:
	Datos[69] = 0;			Datos[70] = 42;			Datos[71] = 3;			Datos[72] = 1;
	Datos[73] = 0xFF;		Datos[74] = 30;			Datos[75] = 12;
	// Fase 2:									Fase 2:
	Datos[76] = 2;			Datos[77] = 39;			Datos[78] = 3;			Datos[79] = 1;
	Datos[80] = 0xFF;		Datos[81] = 21;			Datos[82] = 18;
	// Fase 3:									Fase 3:
	Datos[83] = 2;			Datos[84] = 2;			Datos[85] = 3;			Datos[86] = 2;
	Datos[87] = 0xFF;		Datos[88] = 1;			Datos[89] = 1;
	// Fase 4:									Fase 4:
	Datos[90] = 0;			Datos[91] = 46;			Datos[92] = 3;			Datos[93] = 1;
	Datos[94] = 0xFF;		Datos[95] = 45;			Datos[96] = 1;
	// Fase 5:									Fase 5:
	Datos[97] = 0;			Datos[98] = 39;			Datos[99] = 1;			Datos[100] = 0;
	Datos[101] = 0xFF;		Datos[102] = 18;		Datos[103] = 21;
	// Fase 6:									Fase 6:
	Datos[104] = 2;			Datos[105] = 2;			Datos[106] = 3;			Datos[107] = 3;
	Datos[108] = 0xFF;		Datos[109] = 1;			Datos[110] = 1;
	// Fase 7:									Fase 7:
	Datos[111] = 2;			Datos[112] = 60;		Datos[113] = 3;			Datos[114] = 1;
	Datos[115] = 0xFF;		Datos[116] = 56;		Datos[117] = 4;
	// Fase 8:									Fase 8:
	Datos[118] = 0;			Datos[119] = 30;		Datos[120] = 3;			Datos[121] = 1;
	Datos[122] = 0xFF;		Datos[123] = 18;		Datos[124] = 12;
	// Fase 9:									Fase 9:
	Datos[125] = 0;			Datos[126] = 0;			Datos[127] = 0;			Datos[128] = 0;
	Datos[129] = 0xFF;		Datos[130] = 0;			Datos[131] = 0;
	// Fase 10:									Fase 10:
	Datos[132] = 0;			Datos[133] = 0;			Datos[134] = 0;			Datos[135] = 0;
	Datos[136] = 0xFF;		Datos[137] = 0;			Datos[138] = 0;
	// Fase 11:									Fase 11:
	Datos[139] = 0;			Datos[140] = 0;			Datos[141] = 0;			Datos[142] = 0;
	Datos[143] = 0xFF;		Datos[144] = 0;			Datos[145] = 0;
	// Fase 12:									Fase 12:
	Datos[146] = 0;			Datos[147] = 0;			Datos[148] = 0;			Datos[149] = 0;
	Datos[150] = 0xFF;		Datos[151] = 0;			Datos[152] = 0;
	// Fase 13:									Fase 13:
	Datos[153] = 0;			Datos[154] = 0;			Datos[155] = 0;			Datos[156] = 0;
	Datos[157] = 0xFF;		Datos[158] = 0;			Datos[159] = 0;
	// Fase 14:									Fase 14:
	Datos[160] = 0;			Datos[161] = 0;			Datos[162] = 0;			Datos[163] = 0;
	Datos[164] = 0xFF;		Datos[165] = 0;			Datos[166] = 0;
	// Fase 15:									Fase 15:
	Datos[167] = 0;			Datos[168] = 0;			Datos[169] = 0;			Datos[170] = 0;
	Datos[171] = 0xFF;		Datos[172] = 0;			Datos[173] = 0;
	// Fase 16:									Fase 16:
	Datos[174] = 0;			Datos[175] = 0;			Datos[176] = 0;			Datos[177] = 0;
	Datos[178] = 0xFF;		Datos[179] = 0;			Datos[180] = 0;


// Tipos de Overlaps: 16 Bytes.		||  0: Normal. ||  1: SITRA. ||  2: Apagado. ||  3: Rojo. ||
	Datos[181] = 2;			Datos[182] = 2;			Datos[183] = 2;			Datos[184] = 2;
	Datos[185] = 2;			Datos[186] = 2;			Datos[187] = 2;			Datos[188] = 2;
	Datos[189] = 2;			Datos[190] = 2;			Datos[191] = 2;			Datos[192] = 2;
	Datos[193] = 2;			Datos[194] = 2;			Datos[195] = 2;			Datos[196] = 2;

// Overlaps Vehicular o Peatonal: 		Vehicular = 0.	|| 	Peatonal = 1.
	Datos[197] = 0;			Datos[198] = 1;			Datos[199] = 0;			Datos[200] = 0;
	Datos[201] = 0;			Datos[202] = 0;			Datos[203] = 0;			Datos[204] = 0;
	Datos[205] = 0;			Datos[206] = 0;			Datos[207] = 0;			Datos[208] = 0;
	Datos[209] = 0;			Datos[210] = 0;			Datos[211] = 0;			Datos[212] = 0;

// Fases del Overlap 1: 
	Datos[213] = 3;			Datos[214] = 1;			Datos[215] = 0;		Datos[216] = 0;
	Datos[217] = 0;			Datos[218] = 0;			Datos[219] = 0;		Datos[220] = 0;

// Modificadores del Overlap 1:
	Datos[221] = 0;			Datos[222] = 0;			Datos[223] = 0;			Datos[224] = 0;
	Datos[225] = 0;			Datos[226] = 0;			Datos[227] = 0;			Datos[228] = 0;

// Fases del Overlap 2:
	Datos[229] = 3;			Datos[230] = 1;			Datos[231] = 0;		Datos[232] = 0;
	Datos[233] = 0;			Datos[234] = 0;			Datos[235] = 0;		Datos[236] = 0;

// Modificadores del Overlap 2:
	Datos[237] = 0;			Datos[238] = 0;			Datos[239] = 0;			Datos[240] = 0;
	Datos[241] = 0;			Datos[242] = 0;			Datos[243] = 0;			Datos[244] = 0;

// Fases del Overlap 3:
	Datos[245] = 6;			Datos[246] = 4;			Datos[247] = 0;		Datos[248] = 0;
	Datos[249] = 0;			Datos[250] = 0;			Datos[251] = 0;		Datos[252] = 0;

// Modificadores del Overlap 3:
	Datos[253] = 0;			Datos[254] = 0;			Datos[255] = 0;			Datos[256] = 0;
	Datos[257] = 0;			Datos[258] = 0;			Datos[259] = 0;			Datos[260] = 0;

// Fases del Overlap 4:
	Datos[261] = 0;		Datos[262] = 0;		Datos[263] = 0;		Datos[264] = 0;
	Datos[265] = 0;		Datos[266] = 0;		Datos[267] = 0;		Datos[268] = 0;

// Modificadores del Overlap 4:
	Datos[269] = 0;			Datos[270] = 0;			Datos[271] = 0;			Datos[272] = 0;
	Datos[273] = 0;			Datos[274] = 0;			Datos[275] = 0;			Datos[276] = 0;

// Fases del Overlap 5:
	Datos[277] = 0;		Datos[278] = 0;		Datos[279] = 0;		Datos[280] = 0;
	Datos[281] = 0;		Datos[282] = 0;		Datos[283] = 0;		Datos[284] = 0;

// Modificadores del Overlap 5:
	Datos[285] = 0;			Datos[286] = 0;			Datos[287] = 0;			Datos[288] = 0;
	Datos[289] = 0;			Datos[290] = 0;			Datos[291] = 0;			Datos[292] = 0;

// Fases del Overlap 6:
	Datos[293] = 0;			Datos[294] = 0;			Datos[295] = 0;			Datos[296] = 0;
	Datos[297] = 0;			Datos[298] = 0;			Datos[299] = 0;			Datos[300] = 0;

// Modificadores del Overlap 6:
	Datos[301] = 0;			Datos[302] = 0;			Datos[303] = 0;			Datos[304] = 0;
	Datos[305] = 0;			Datos[306] = 0;			Datos[307] = 0;			Datos[308] = 0;

// Fases del Overlap 7:
	Datos[309] = 0;			Datos[310] = 0;			Datos[311] = 0;			Datos[312] = 0;
	Datos[313] = 0;			Datos[314] = 0;			Datos[315] = 0;			Datos[316] = 0;
	
// Modificadores del Overlap 7:
	Datos[317] = 0;			Datos[318] = 0;			Datos[319] = 0;			Datos[320] = 0;
	Datos[321] = 0;			Datos[322] = 0;			Datos[323] = 0;			Datos[324] = 0;

// Fases del Overlap 8:
	Datos[325] = 0;			Datos[326] = 0;			Datos[327] = 0;			Datos[328] = 0;
	Datos[329] = 0;			Datos[330] = 0;			Datos[331] = 0;			Datos[332] = 0;

// Modificadores del Overlap 8:
	Datos[333] = 0;			Datos[334] = 0;			Datos[335] = 0;			Datos[336] = 0;
	Datos[337] = 0;			Datos[338] = 0;			Datos[339] = 0;			Datos[340] = 0;

/*	#############################################################  */

/*	####################  EJEMPLO 12 - Overlaps Peatonales  ############################  */

/*
	Datos[0] = 100;	// Ciclo
//	CHANNELS:
	// N° de Fase			// Tipo (Vehic/Peat)	// N° de Fase			// Tipo (Vehic/Peat)
	Datos[1] = 1;			Datos[2] = 4;			Datos[3] = 2;			Datos[4] = 4;
	Datos[5] = 3;			Datos[6] = 4;			Datos[7] = 4;			Datos[8] = 4;
	Datos[9] = 5;			Datos[10] = 4;			Datos[11] = 6;			Datos[12] = 4;
	Datos[13] = 7;			Datos[14] = 4;			Datos[15] = 8;			Datos[16] = 4;
	Datos[17] = 1;			Datos[18] = 2;			Datos[19] = 2;			Datos[20] = 2;
	Datos[21] = 3;			Datos[22] = 2;			Datos[23] = 4;			Datos[24] = 2;
	Datos[25] = 5;			Datos[26] = 2;			Datos[27] = 1;			Datos[28] = 3;
	Datos[29] = 2;			Datos[30] = 3;			Datos[31] = 3;			Datos[32] = 3;
// SECUENCE DATA (4 Rings de hasta 8 Fases):
		// Ring 1:
	Datos[33] = 1;			Datos[34] = 2;			Datos[35] = 3;			Datos[36] = 0;
	Datos[37] = 0;			Datos[38] = 0;			Datos[39] = 0;			Datos[40] = 0;
		// Ring 2:	
	Datos[41] = 4;			Datos[42] = 5;			Datos[43] = 6;			Datos[44] = 0;
	Datos[45] = 0;			Datos[46] = 0;			Datos[47] = 0;			Datos[48] = 0;
		// Ring 3:
	Datos[49] = 0;			Datos[50] = 0;			Datos[51] = 0;			Datos[52] = 0;
	Datos[53] = 0;			Datos[54] = 0;			Datos[55] = 0;			Datos[56] = 0;
		// Ring 4:
	Datos[57] = 0;			Datos[58] = 0;			Datos[59] = 0;			Datos[60] = 0;
	Datos[61] = 0;			Datos[62] = 0;			Datos[63] = 0;			Datos[64] = 0;
// RING DELAYS:
	Datos[65] = 0;			Datos[66] = 14;			Datos[67] = 0;			Datos[68] = 0;

// TIEMPOS DE FASES:		R/A	V	A	RC			PD	W	PC
	// Fase 1:									Fase 1:
	Datos[69] = 2;			Datos[70] = 17;			Datos[71] = 3;			Datos[72] = 0;
	Datos[73] = 0xFF;		Datos[74] = 12;			Datos[75] = 5;
	// Fase 2:									Fase 2:
	Datos[76] = 2;			Datos[77] = 28;			Datos[78] = 3;			Datos[79] = 3;
	Datos[80] = 0xFF;		Datos[81] = 21;			Datos[82] = 7;
	// Fase 3:									Fase 3:
	Datos[83] = 2;			Datos[84] = 24;			Datos[85] = 3;			Datos[86] = 0;
	Datos[87] = 0xFF;		Datos[88] = 19;			Datos[89] = 5;
	// Fase 4:									Fase 4:
	Datos[90] = 2;			Datos[91] = 26;			Datos[92] = 3;			Datos[93] = 5;
	Datos[94] = 0xFF;		Datos[95] = 0;			Datos[96] = 0;
	// Fase 5:									Fase 5:
	Datos[97] = 2;			Datos[98] = 24;			Datos[99] = 3;			Datos[100] = 3;
	Datos[101] = 0xFF;		Datos[102] = 0;			Datos[103] = 0;
	// Fase 6:									Fase 6:
	Datos[104] = 2;			Datos[105] = 32;		Datos[106] = 3;			Datos[107] = 0;
	Datos[108] = 0xFF;		Datos[109] = 0;			Datos[110] = 0;
	// Fase 7:									Fase 7:
	Datos[111] = 0;			Datos[112] = 0;			Datos[113] = 0;			Datos[114] = 0;
	Datos[115] = 0xFF;		Datos[116] = 0;			Datos[117] = 0;
	// Fase 8:									Fase 8:
	Datos[118] = 0;			Datos[119] = 0;			Datos[120] = 0;			Datos[121] = 0;
	Datos[122] = 0xFF;		Datos[123] = 0;			Datos[124] = 0;
	// Fase 9:									Fase 9:
	Datos[125] = 0;			Datos[126] = 0;			Datos[127] = 0;			Datos[128] = 0;
	Datos[129] = 0xFF;		Datos[130] = 0;			Datos[131] = 0;
	// Fase 10:									Fase 10:
	Datos[132] = 0;			Datos[133] = 0;			Datos[134] = 0;			Datos[135] = 0;
	Datos[136] = 0xFF;		Datos[137] = 0;			Datos[138] = 0;
	// Fase 11:									Fase 11:
	Datos[139] = 0;			Datos[140] = 0;			Datos[141] = 0;			Datos[142] = 0;
	Datos[143] = 0xFF;		Datos[144] = 0;			Datos[145] = 0;
	// Fase 12:									Fase 12:
	Datos[146] = 0;			Datos[147] = 0;			Datos[148] = 0;			Datos[149] = 0;
	Datos[150] = 0xFF;		Datos[151] = 0;			Datos[152] = 0;
	// Fase 13:									Fase 13:
	Datos[153] = 0;			Datos[154] = 0;			Datos[155] = 0;			Datos[156] = 0;
	Datos[157] = 0xFF;		Datos[158] = 0;			Datos[159] = 0;
	// Fase 14:									Fase 14:
	Datos[160] = 0;			Datos[161] = 0;			Datos[162] = 0;			Datos[163] = 0;
	Datos[164] = 0xFF;		Datos[165] = 0;			Datos[166] = 0;
	// Fase 15:									Fase 15:
	Datos[167] = 0;			Datos[168] = 0;			Datos[169] = 0;			Datos[170] = 0;
	Datos[171] = 0xFF;		Datos[172] = 0;			Datos[173] = 0;
	// Fase 16:									Fase 16:
	Datos[174] = 0;			Datos[175] = 0;			Datos[176] = 0;			Datos[177] = 0;
	Datos[178] = 0xFF;		Datos[179] = 0;			Datos[180] = 0;

// Tipos de Overlaps: 16 Bytes.		||  1: SITRA. ||  2: Vehicular Apagado. ||  3: Vehicular Rojo. ||
	Datos[181] = 2;			Datos[182] = 2;			Datos[183] = 2;			Datos[184] = 2;
	Datos[185] = 2;			Datos[186] = 2;			Datos[187] = 1;			Datos[188] = 1;
	Datos[189] = 2;			Datos[190] = 2;			Datos[191] = 2;			Datos[192] = 2;
	Datos[193] = 2;			Datos[194] = 2;			Datos[195] = 2;			Datos[196] = 2;

// OL_Sitra.	 0x1F: Peatonal Normal. 
	Datos[197] = 0;			Datos[198] = 0;			Datos[199] = 0;			Datos[200] = 0;
	Datos[201] = 0;			Datos[202] = 0;			Datos[203] = 0x1F;		Datos[204] = 0x1F;
	Datos[205] = 0;			Datos[206] = 0;			Datos[207] = 0;			Datos[208] = 0;
	Datos[209] = 0;			Datos[210] = 0;			Datos[211] = 0;			Datos[212] = 0;

// Fases del Overlap 1: 
	Datos[213] = 2;			Datos[214] = 3;			Datos[215] = 0;			Datos[216] = 0;
	Datos[217] = 0;			Datos[218] = 0;			Datos[219] = 0;			Datos[220] = 0;

// Modificadores del Overlap 1:
	Datos[221] = 0;			Datos[222] = 0;			Datos[223] = 0;			Datos[224] = 0;
	Datos[225] = 0;			Datos[226] = 0;			Datos[227] = 0;			Datos[228] = 0;

// Fases del Overlap 2:
	Datos[229] = 1;			Datos[230] = 3;			Datos[231] = 0;			Datos[232] = 0;
	Datos[233] = 0;			Datos[234] = 0;			Datos[235] = 0;			Datos[236] = 0;

// Modificadores del Overlap 2:
	Datos[237] = 0;			Datos[238] = 0;			Datos[239] = 0;			Datos[240] = 0;
	Datos[241] = 0;			Datos[242] = 0;			Datos[243] = 0;			Datos[244] = 0;

// Fases del Overlap 3:
	Datos[245] = 4;			Datos[246] = 2;			Datos[247] = 0;			Datos[248] = 0;
	Datos[249] = 0;			Datos[250] = 0;			Datos[251] = 0;			Datos[252] = 0;

// Modificadores del Overlap 3:
	Datos[253] = 2;			Datos[254] = 0;			Datos[255] = 0;			Datos[256] = 0;
	Datos[257] = 0;			Datos[258] = 0;			Datos[259] = 0;			Datos[260] = 0;

// Fases del Overlap 4:
	Datos[261] = 4;			Datos[262] = 3;			Datos[263] = 0;			Datos[264] = 0;
	Datos[265] = 0;			Datos[266] = 0;			Datos[267] = 0;			Datos[268] = 0;

// Modificadores del Overlap 4:
	Datos[269] = 3;			Datos[270] = 0;			Datos[271] = 0;			Datos[272] = 0;
	Datos[273] = 0;			Datos[274] = 0;			Datos[275] = 0;			Datos[276] = 0;

// Fases del Overlap 5:
	Datos[277] = 5;			Datos[278] = 3;			Datos[279] = 0;			Datos[280] = 0;
	Datos[281] = 0;			Datos[282] = 0;			Datos[283] = 0;			Datos[284] = 0;

// Modificadores del Overlap 5:
	Datos[285] = 5;			Datos[286] = 0;			Datos[287] = 0;			Datos[288] = 0;
	Datos[289] = 0;			Datos[290] = 0;			Datos[291] = 0;			Datos[292] = 0;

// Fases del Overlap 6:
	Datos[293] = 4;			Datos[294] = 3;			Datos[295] = 0;			Datos[296] = 0;
	Datos[297] = 0;			Datos[298] = 0;			Datos[299] = 0;			Datos[300] = 0;

// Modificadores del Overlap 6:
	Datos[301] = 4;			Datos[302] = 0;			Datos[303] = 0;			Datos[304] = 0;
	Datos[305] = 0;			Datos[306] = 0;			Datos[307] = 0;			Datos[308] = 0;

// Fases del Overlap 7:
	Datos[309] = 1;			Datos[310] = 2;			Datos[311] = 0;			Datos[312] = 0;
	Datos[313] = 0;			Datos[314] = 0;			Datos[315] = 0;			Datos[316] = 0;

// Modificadores del Overlap 7:
	Datos[317] = 1;			Datos[318] = 2;			Datos[319] = 0;			Datos[320] = 0;
	Datos[321] = 0;			Datos[322] = 0;			Datos[323] = 0;			Datos[324] = 0;

// Fases del Overlap 8:
	Datos[325] = 1;			Datos[326] = 3;			Datos[327] = 0;			Datos[328] = 0;
	Datos[329] = 0;			Datos[330] = 0;			Datos[331] = 0;			Datos[332] = 0;

// Modificadores del Overlap 8:
	Datos[333] = 1;			Datos[334] = 3;			Datos[335] = 0;			Datos[336] = 0;
	Datos[337] = 0;			Datos[338] = 0;			Datos[339] = 0;			Datos[340] = 0;

	for(int i=0;i<128;i++)	Datos[341+i] = 0;
*/
/*	#############################################################  */


//imprimir datos
/*
	for(int i=0;i<470;i++)
	{
		printf("%i ",Datos[i]);
	}
	printf("\n\n");
*/
}

void prepara_envio_estructura2(char *t, int n,int parte)
{
	// n       :numero de estructura (0,1,2)
	// parte=1 :parte alta de la trama de envio de estructura
	// parte=0 :parte baja de la trama de envio de estructura

	int i=0;

	int num_cruce_separado[2],_num_grupo=0,_num_cruce=0;
	
	_num_grupo = get_variable(7);
	_num_cruce = get_variable(6);	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	
	if(parte==1)		
	{	
		t[8]=0x69;			
		t[10]=0xF6;
	}
	else if(parte==0)
	{
		t[8]=0x6A;
		t[10]=0xF6;
	} 

	t[9]=0x00;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	i=15;
	for(i=16;i<249;i++)
	{
		t[i]=0;
	}
	t[249]=0;
	for(i=11;i<249;i++) t[249]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<250;i++) printf("%X ", t[i]);
	printf("\n ");
*/	
}

void prepara_envio_programa_tiempos2(char *t, int n)
{
	int num_cruce_separado[2], _num_grupo=0,_num_cruce=0;
	
	_num_grupo = get_variable(7);
	_num_cruce = get_variable(6);	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	int i=0, k=0;

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x72;
	t[9]=0x00;			
	t[10]=0x48;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	for(k=15;k<57;k++)
	{
		t[k]=0;
	}

	i=0;

	for(i=57;i<74;i++) 
	{
		t[i]=0x7F;
		t[i+1]=0xFF;
	}
	t[74]=0;
	for(i=11;i<74;i++) t[74]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<75;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

//metodo nuevo desarrollado por Dani G.
void conversion_ntcip_citar(int * _conexion)
{

	//tramas de envio
	char te1[250], te2[45], te3[42], te4[76], te5[56], te6[135], te7[159], te8[15];
	
	// un arreglo de 3 telegramas de 69, 6A y 76
	char t69[30][250]; 
	char t6A[30][250];
	char t72[30][76];
	char t78[12][56];

	//tramas de recepcion
	char tr1[20], tr2[26];
	
	int _delay_programacion=0, _delay_eepromm=0;
	
	_delay_programacion = get_variable(8);
	_delay_eepromm = get_variable(9);
	
	for(int k=0;k<30;k++) {
		
		ix=0;
		jx=0;
		ConteoCanales = 0;
		ColorActual = 0;
		IndiceA = 0;
		IndiceB = 0;
		IndiceC = 0;
		IndiceD = 0;
		IndiceF = 0;
		TiempoCiclo = 0;
		TipoActual = 0;
		NumVehic = 0;
		OperAux = 0;
		PreTimeOL = 0;
		PreTimePeat = 0;
		PreTimeVehic = 0;
		
		printf("conversion %i\n",k);

		for(int i=0;i<120;i++)
		{
			TiemposAux[i] = 0xFF;
			if(i<32) 
			{
				TiemposIntervalos[i] = 0xFF;
				TiemposAcumulados[i] = 0xFF;
				for(int j=0;j<8;j++)	OverLaps[i][j] = 0xFF;
			}
		}
		
		for(int i=0;i<8;i++)
		{
			for(int j=0;j<5;j++)
			{
				EstructuraOL[i][j]=0xFF;
			}
		}

		for(int i=0;i<16;i++)
		{
			for(int j=0;j<5;j++)
			{
				Matriz[i][j]=0xFF;
			}
			
			for(int j=0;j<2;j++)	Channel[i][j] = 0;
			TiposOverLaps[i] = 0xFF;
			OL_Sitra[i] = 0;		// 		0: Vehicular.		1: Peatonal
			ModifiersSITRA[i] = 0;
			for(int j=0;j<16;j++)	MatrizOL[i][j] = 0xFF;
		}

		for(int i=0;i<250;i++)
		{
			Telegrama69[i]=0;
			Telegrama6A[i]=0;
		}

		for(int i=0;i<76;i++)
		{
			Telegrama72[i]=0;
		}

		CargarDatosObjetos(k);
		LlenarCanales();	// Función que carga los Canales con los valores cargados en Datos [].
		LlenarSecuenceData();	// Función que carga el Secuence Data con los valores cargados en Datos [].
		LlenarOverLaps();	// Función que carga los valores relacionados a los Overlaps desde Datos []
		LlenarMatriz16x5(k);
		ArmarOverLaps();	// Se usan los Tiempos de Fases y el Secuence Data para armarlos.
	/*	
		for(int i=0;i<16;i++)
		{
			for(int j=0;j<16;j++)
			{
				printf("%i ",MatrizOL[i][j]);		
			}
		}
		printf("\n");
	*/	
		OrdenarTiempos(k);
	// -------------------------------------------------------------		
		ix=0;
		IndiceA = 15;	// Se apunta al Primer Intervalo de Colores en el Telegrama 0x69.
		IndiceB = 15;		// Primer Intervalo	de Colores del Telegrama 0x6A.
		IndiceC = 0;	// Cuenta los Intervalos Guardados para pasar al Telegrama 0x6A.
		while(TiemposIntervalos[ix] != 0xFF && ix <= 34)
		{		// Intervalos válidos dentro de las dos partes de la Extructura
			jx=0;
			while(jx<16)	// Se obtiene el color según el tiempo en el que termina el intervalo y el número
			{			// de Grupo según la Matríz.
				OperAux	= ObtenerColor(TiemposAcumulados[ix],jx); //Se envía el Tiempo Total y el N° de Grupo.
				if(IndiceC < 18)	// Si aún no se han llenado los 18 intervalos del Telegrama 0x69.
				{
					Telegrama69[IndiceA] |= OperAux;
				}
				else
				{
					Telegrama6A[IndiceB] |= OperAux;
				}
				jx++;
				OperAux	= ObtenerColor(TiemposAcumulados[ix],jx);
				OperAux <<= 4;
				if(IndiceC < 18)	// Si aún no se han llenado los 18 intervalos del Telegrama 0x69.
				{
					Telegrama69[IndiceA] |= OperAux;
					IndiceA++;
				}
				else
				{
					Telegrama6A[IndiceB] |= OperAux;
					IndiceB++;
				}
				jx++;
			}
			ix++;
			if(IndiceC < 18)	IndiceA += 5;	// Se apunta al siguiente intervalo, para cual sea el 
			else	IndiceB += 5;				// telegrama que se esté llenando.
			IndiceC++;		// Se cuenta la fase recientemente guardada.
		}
		ArmarTelegramas(k);
	/*
		for(int i=0;i<250;i++)
		{
			printf("%X ",Telegrama69[i]);
		}
		printf("\n\n");

		for(int i=0;i<250;i++)
		{
			printf("%X ",Telegrama6A[i]);
		}
		printf("\n\n");

		for(int i=0;i<76;i++)
		{
			printf("%X ",Telegrama72[i]);
		}
		printf("\n");

		printf("Tiempos Acumulados: ");

		for(int i=0;i<64;i++)
		{
			printf("%X ",TiemposAcumulados[i]);
		}
		printf("\n");


		printf("Tiempos Intervalos: ");

		for(int i=0;i<32;i++)
		{
			printf("%X ",TiemposIntervalos[i]);
		}
		printf("\n");
	*/

		for(int i=0;i<250;i++) {
			t69[k][i] = Telegrama69[i];
			t6A[k][i] = Telegrama6A[i];
		}
		
		for(int i=0;i<76;i++) {
			t72[k][i] = Telegrama72[i];
		}
	}
	
	//Envio de telegramas al equipo
	
	//Trama de Estructura

	for(int k=0;k<30;k++)
	{
		printf("Trama de Estructura Alta %i: \n",k);
		enviar_trama(_conexion,t69[k],tr1,250,20);
		sleep(_delay_programacion);
		printf("Trama de Estructura Baja %i: \n",k);
		enviar_trama(_conexion,t6A[k],tr1,250,20);
		sleep(_delay_programacion);
	}
/*
		prepara_envio_estructura2(te1, i,1);
		enviar_trama(_conexion,te1,tr1,250,20);
		sleep(_delay_programacion);
		prepara_envio_estructura2(te1, i,0);
		enviar_trama(_conexion,te1,tr1,250,20);
		sleep(_delay_programacion);
*/	
/*
	printf("Trama de matriz de conflictos: \n");
	prepara_envio_matriz_conflictos(te2);
	enviar_trama(_conexion,te2,tr1,45,20);
	sleep(_delay_programacion);

	for(int i=0;i<32;i++)
	{
		printf("Trama de funciones %i: \n",i);
		prepara_envio_funciones(te3,i);
		enviar_trama(_conexion,te3,tr1,42,20);
		sleep(_delay_programacion);
	}
*/

	//Trama de programa de tiempos

	for(int i=0;i<30;i++) {
		enviar_trama(_conexion,t72[i],tr1,76,20);
		sleep(_delay_programacion);
	}
 /*
	for(int i=3;i<16;i++)
	{
		prepara_envio_programa_tiempos2(te4, i); 
		enviar_trama(_conexion,te4,tr1,76,20);
		sleep(_delay_programacion); 
	}
*/

	//trama de agendas diarias
/*	
	for(int i=0;i<12;i++)
	{
		printf("Trama de agenda diarias %i: \n",i);
		//prepara_envio_agenda_diaria(t78[i],i);
		prepara_trama_agenda_diaria_ntcip(t78[i],i);
		enviar_trama(_conexion,t78[i],tr1,56,20);
		sleep(_delay_programacion);
	}

	printf("Trama de agenda anual y semanal: \n");
	//prepara_envio_agenda_anual_semanal(te6);
	//prepara_trama_agenda_semanal_anual_ntcip(te6);
	enviar_trama(_conexion,te6,tr1,135,20);
	sleep(_delay_programacion);
				
	printf("Trama de agenda feriados y especiales: \n");
	prepara_envio_agenda_feriados_especiales(te7);
	enviar_trama(_conexion,te7,tr1,159,20);
	sleep(_delay_programacion);
*/
	conversion_schedule_ntcip_citar();
				
	printf("Trama de preajustes: \n");
	prepara_envio_preajustes(te8);
	enviar_trama(_conexion,te8,tr1,85,20);
	sleep(_delay_programacion);	

	printf("Trama de grabacion de EEPROM: \n");
	prepara_envio_grabacion_eeprom(te8);
	enviar_trama(_conexion,te8,tr2,15,26);
	sleep(_delay_eepromm);	
	
/*	printf("Tiempos Aux\n");

	for(int i=0;i<120;i++)
	{
		printf("%i ",TiemposAux[i]);
	}
	printf("\n");
	printf("TiemposIntervalos\n");
	for(int i=0;i<120;i++)
	{	
		
		printf("%i ",TiemposIntervalos[i]);
	}
	printf("\n");

	printf("TiemposAcumulados\n");
	for(int i=0;i<120;i++)
	{
		
		printf("%i ",TiemposAcumulados[i]);
		
	}	
	printf("\n");
	*/
}
