#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bson.h>
#include <mongoc.h>
#include "jsmn.h"
#include "keepalive.h"

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
  if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
      strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
    return 0;
  }
  return -1;
}

unsigned long check_keepalive(void) {
	
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *cursor;
  const bson_t *doc;
  bson_t *query;
  char *str, *eptr;
  char * keepalive;
  	
  int i;
  int r;
  jsmn_parser p;
  jsmntok_t t[128]; /* We expect no more than 128 tokens */
  
  unsigned long keepalive_int=0;
  
  //inicio de consulta mongodb

  mongoc_init ();

  client =
  mongoc_client_new ("mongodb://localhost:27017/?appname=find-keepalive");
  collection = mongoc_client_get_collection (client, "sitra", "status");
  query = bson_new ();
  cursor = mongoc_collection_find_with_opts (collection, query, NULL, NULL);
  
  while (mongoc_cursor_next (cursor, &doc)) {
     str = bson_as_canonical_extended_json (doc, NULL);
     //printf ("%s\n", str);  
  }
  
  jsmn_init(&p);
  r = jsmn_parse(&p, str, strlen(str), t,
                 sizeof(t) / sizeof(t[0]));
  if (r < 0) {
    printf("Failed to parse JSON: %d\n", r);
    return 1;
  }

  /* Assume the top-level element is an object */
  if (r < 1 || t[0].type != JSMN_OBJECT) {
    printf("Object expected\n");
    return 1;
  }

  /* Loop over all keys of the root object */
  for (i = 1; i < r; i++) {
    if (jsoneq(str, &t[i], "keepalive") == 0) {
      i+=2;
      /* We may use strndup() to fetch string value */
      //printf("- keepalive: %.*s\n", t[i + 1].end - t[i + 1].start,
      //       str + t[i + 1].start);
      keepalive = strndup(str + t[i + 1].start,t[i + 1].end - t[i + 1].start);
      //printf("%s\n",keepalive);
      keepalive_int = strtol(keepalive,&eptr,10);
      //printf("%li\n",keepalive_int);
    } 
  }
  
   bson_destroy (query);
   mongoc_cursor_destroy (cursor);
   mongoc_collection_destroy (collection);
   mongoc_client_destroy (client);
   mongoc_cleanup ();
  
  return keepalive_int;
}

