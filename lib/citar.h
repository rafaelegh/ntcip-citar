#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#ifndef CITAR_H
#define CITAR_H

struct movimiento
{
	int 	numero;
	int		nible_color;
};

int get_secuencia_programacion(void);
int get_variable(int);
void set_variable(int,int);
int verificacion_trama_respuesta(char *,int, int);
int codigos(int);

int * get_conexion(void);

char * get_cadena(void);

void prepara_trama_comando(char *);
void prepara_trama_comando2(char *);
void prepara_trama_consulta_extendido(char *, int );
void enviar_trama(int * , char*, char *,int, int);
void carga_estadoExtendido(int , int);
void analiza_lamparas(char *,struct movimiento *);
void actualiza_phaseStatus(struct movimiento *);
void estado_extendido(char *);
void prepara_trama_fecha_hora(struct tm *, char *);
void convertir_estructura_ntcip_citar(int *);
void limpiar_secuencia(void);
void imprimir_trama(char *, int);
void realizar_consulta_generica(void);


#endif
