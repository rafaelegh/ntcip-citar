#ifndef BYTES_H
#define BYTES_H

void dec_to_bin(int ,int *);
void dec_to_bin_n(unsigned long,int[], int n);
int dec_to_hex(int);
int dec_to_BCD_hex(int );
int unir_byte_hex(int, int);

unsigned long unir_4nibble_hex(int,int,int, int);

void separar_byte_hex(int *,unsigned int);
void separar_4byte_hex(int *,unsigned long);
int get_array_length(int *);


#endif
