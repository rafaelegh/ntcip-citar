#include <stdio.h>
#include <time.h> //Libreria que contiene las funciones de tiempo del sistema.
#include <stdlib.h>
#include <string.h>
#include "calendario.h"

int calculoDiaSemana(int _anio, int _mes, int _febrero) {
	
	int moduloMes = 0,
		diaSemana = 0;
	
	if (_febrero==29) //ya calcule el anio bisisesto, asi que no volvere a hacerlo.
		{
			switch (_mes)
			{
				case 0:moduloMes=0;break;
				case 1:moduloMes=3;break;
				case 2:moduloMes=4;break;
				case 3:moduloMes=0;break;
				case 4:moduloMes=2;break;
				case 5:moduloMes=5;break;
				case 6:moduloMes=0;break;
				case 7:moduloMes=3;break;
				case 8:moduloMes=6;break;
				case 9:moduloMes=1;break;
				case 10:moduloMes=4;break;
				case 11:moduloMes=6;break;
			}

		}else
		{
			switch (_mes)
			{
				case 0:moduloMes=0;break;
				case 1:moduloMes=3;break;
				case 2:moduloMes=3;break;
				case 3:moduloMes=6;break;
				case 4:moduloMes=1;break;
				case 5:moduloMes=4;break;
				case 6:moduloMes=6;break;
				case 7:moduloMes=2;break;
				case 8:moduloMes=5;break;
				case 9:moduloMes=0;break;
				case 10:moduloMes=3;break;
				case 11:moduloMes=5;break;
			}
		}
	
	
	diaSemana = ((_anio-1)%7+ ((_anio-1)/4 -(3*((_anio-1)/100+1)/4))%7+moduloMes+1%7)%7; 

	return diaSemana;
}

int diasDelMes(int _mes) {
	
	int _total_mes = 0;
	
	switch (_mes) {
		case 3: case 5: case 8: case 10: 
		_total_mes=30; 
		break; 
		case 0: case 2: case 4: case 6: case 7: case 9: case 11: 
		_total_mes=31; 
		break; 
	}
	return _total_mes;
}

int calculaDiasTranscurridos(int _mes, int _febrero){
	int resultado = 0;
	int cantidadMes = 0;
	
	for(int i=0; i< _mes; i++) {
		if(i != 1) {
			cantidadMes += diasDelMes(_mes);
		}
		else {
			cantidadMes += _febrero;
		}
	}
	
}


void llenarMes(struct Calendar * c, int _mes, int _febrero, int _anio, int * _diasTranscurridos) {
	
	
	int diaSemana = 0;
	int longitudMes = 0;
	int primerDiaSemana = 0;
	
	
	if(_mes == 1) {
		longitudMes = _febrero;
	}
	else {
		longitudMes = diasDelMes(_mes);
	}
	
	diaSemana = calculoDiaSemana(_anio,_mes, _febrero);
/*		
	printf("contador: %i\n",*_diasTranscurridos);
	printf("primer dia de semana: %i\n",diaSemana);
	//printf("array 300 %i\n",c[300].mes);
*/
	
	for(int i=0; i< longitudMes; i++) {
		
		c[*_diasTranscurridos].diaMes = i + 1;
		c[*_diasTranscurridos].mes = _mes;
		c[*_diasTranscurridos].diaSemana = diaSemana;
		
//		printf("dia del mes: %i\n",c[*_diasTranscurridos].diaMes);
//		printf("Mes: %i\n",c[(*_diasTranscurridos)].mes);
//		printf("dia de Semana: %i\n",c[*_diasTranscurridos].diaSemana);
		
		//incrementamos dia de semana
		diaSemana++;
		if(diaSemana == 7) diaSemana = 0;
		
		//incrementamos cuenta de dias transcurridos del arreglo
		*_diasTranscurridos += 1;

	}	
	
}

void bisiesto(int _arreglo_bisiesto[]){
	
	int dia,
		mes,
		anio,
		febrero,
		diasanio;
	
	time_t sisTime; 	//time_t es un typedef, asigno una varible de este tipo (sisTime);
	struct tm *tiempo;	//Creo un puntero a la estructura tm, (ya definida en time.h).

	time(&sisTime);		//time obtiene el tiempo del sistema y se lo pasa a la direccion de sisTime.
	tiempo=localtime(&sisTime);
	//Convierte los valores de sisTime a compatibles con la estructura tm y luego asigno ese valor a tiempo.

	//Una estructura tm, esta compuesta por datos de tipo int (vean la seccion de time.h)

	dia=tiempo->tm_mday;		//accedo al valor de dia de la estructura tm y se la asigno a dia
	mes=tiempo->tm_mon;		//accedo a mes de la estrutura tm y se la asigno a mes
	anio=(tiempo->tm_year)+1900;	//year es un contador a partir de 1900.
	
	//Bisiesto.
	//Debemos determinar si el anio es bisiesto para luego realizar las agrupaciones

	if ((anio%4==0) && (anio%100!=0) || anio%400==0) {
		febrero = 29;
		diasanio = 366;
	}
	else {
		febrero = 28;
		diasanio = 365;
	}
	
	_arreglo_bisiesto[0] = dia;
	_arreglo_bisiesto[1] = mes;
	_arreglo_bisiesto[2] = anio;
	_arreglo_bisiesto[3] = febrero;
	_arreglo_bisiesto[4] = diasanio;
	
	//return diasanio;
	
}

void indicarDiaSemana(int diaEntrada, char * diaRespuesta){
	
	switch(diaEntrada){
		case 0: strcpy(diaRespuesta, "Domingo");   break;
		case 1: strcpy(diaRespuesta, "Lunes");     break;
		case 2: strcpy(diaRespuesta, "Martes");    break;
		case 3: strcpy(diaRespuesta, "Miercoles"); break;
		case 4: strcpy(diaRespuesta, "Jueves");    break;
		case 5: strcpy(diaRespuesta, "Viernes");   break;
		case 6: strcpy(diaRespuesta, "Sabado");    break;
		default: strcpy(diaRespuesta, ""); 		   break;
	}
	//printf("prueba: %s\n", diaRespuesta);
}

void indicarMes(int mesEntrada, char * mesRespuesta){
	
	switch(mesEntrada){
		case 0:  strcpy(mesRespuesta, "Enero");   	 break;
		case 1:  strcpy(mesRespuesta, "Febrero"); 	 break;
		case 2:  strcpy(mesRespuesta, "Marzo");   	 break;
		case 3:  strcpy(mesRespuesta, "Abril");   	 break;
		case 4:  strcpy(mesRespuesta, "Mayo");    	 break;
		case 5:  strcpy(mesRespuesta, "Junio");      break;
		case 6:  strcpy(mesRespuesta, "Julio");      break;
		case 7:  strcpy(mesRespuesta, "Agosto");     break;
		case 8:  strcpy(mesRespuesta, "Septiembre"); break;
		case 9:  strcpy(mesRespuesta, "Octubre");    break;
		case 10: strcpy(mesRespuesta, "Noviembre");  break;
		case 11: strcpy(mesRespuesta, "Diciembre");  break;
		default: strcpy(mesRespuesta, ""); 		   	 break;
	}
	//printf("prueba: %s\n", diaRespuesta);
}



void hacerCalendario( struct Calendar * calendar , int _arreglo_bisiesto[])
{	

	int dia,
		mes,
		anio,
		semana,
		febrero,
		total_mes,
		diasTranscurridos = 0, 
		diasanio = 0;
/*		
	time_t sisTime; 	//time_t es un typedef, asigno una varible de este tipo (sisTime);
	struct tm *tiempo;	//Creo un puntero a la estructura tm, (ya definida en time.h).

	time(&sisTime);		//time obtiene el tiempo del sistema y se lo pasa a la direccion de sisTime.
	tiempo=localtime(&sisTime);
	//Convierte los valores de sisTime a compatibles con la estructura tm y luego asigno ese valor a tiempo.

	//Una estructura tm, esta compuesta por datos de tipo int (vean la seccion de time.h)

	dia=tiempo->tm_mday;		//accedo al valor de dia de la estructura tm y se la asigno a dia
	mes=tiempo->tm_mon;		//accedo a mes de la estrutura tm y se la asigno a mes
	anio=(tiempo->tm_year)+1900;	//year es un contador a partir de 1900.

	system ("clear");//system ("cls") en windows, es para limpiar la pantalla.

	/* my code*/

	//struct Calendar * calendar = NULL;

		//Bisiesto.
		//Debemos determinar si el anio es bisiesto para luego realizar las agrupaciones
/*
		if ((anio%4==0) && (anio%100!=0) || anio%400==0) {
			febrero = 29;
			diasanio = 366;
		}
		else {
			febrero = 28;
			diasanio = 365;
		}
*/
	dia = _arreglo_bisiesto[0];
	mes = _arreglo_bisiesto[1];
	anio = _arreglo_bisiesto[2];
	febrero = _arreglo_bisiesto[3];


	//*calendar = malloc(diasanio * sizeof(struct Calendar));
	//calendar[1].mes = 10;
	//printf("array 300 %i\n",calendar[1].mes);
	//printf("dia %i, mes %i, anio %i\n",dia,mes,anio);


	for(mes = 0; mes < 12; mes++){
					
		//llenarMes(calendar,mes, febrero, anio, &diasTranscurridos);
		llenarMes(calendar,mes, febrero, anio, &diasTranscurridos);
		//total_mes = diasDelMes(mes);

	}

}//llave del main.
