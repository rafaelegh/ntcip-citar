

void prepara_consulta_estructura_alta(char *, int);
void prepara_consulta_estructura_baja(char *, int);
void prepara_consulta_matriz(char *);
void prepara_consulta_funciones(char *, int);
void prepara_consulta_tiempos(char *, int);
void prepara_consulta_agenda_diaria(char *,int);
void prepara_consulta_agenda_as(char *);
void prepara_consulta_agenda_fe(char *);
void prepara_consulta_preajustes(char *t);
void cargar_consultas_prog(int *);
