

struct datos_lista_objetos
{
	int 	tabla;
	int		indice1;
	int 	indice2;
	int		columna;
	int 	valor;  
};

typedef struct ns
{
	struct 	datos_lista_objetos data;
	struct 	ns 	* Sig;
}NODO;

typedef NODO * ptnodo;

void imprimir_lista(ptnodo);
ptnodo ultimo_lista(ptnodo);
void agregar_nodo(ptnodo *, struct datos_lista_objetos *);
void borrar_nodo(ptnodo *);
ptnodo* get_lista(void);

