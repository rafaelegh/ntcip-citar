#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "prog_consulta.h"
#include "asc.h"
#include "citar.h"
#include "bytes.h"

void prepara_consulta_estructura_alta(char *t, int e)
{
	int i=0,_num_cruce=0,_num_grupo=0;	
	
	int _num_cruce_separado[2];
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x67;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=e;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void prepara_consulta_estructura_baja(char *t, int e)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x68;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=e;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void prepara_consulta_matriz(char *t)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x6B;
	t[9]=0x00;
	t[10]=0x0B;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=0;
	for(i=11;i<14;i++) t[14]^=t[i];
}

void prepara_consulta_funciones(char *t, int n)
{
	int i=0, _num_cruce=0,_num_grupo;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x6D;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=n;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void prepara_consulta_tiempos(char *t, int p)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x71;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=p;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void prepara_consulta_agenda_diaria(char *t, int p)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x77;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=p;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void prepara_consulta_agenda_as(char *t)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x75;
	t[9]=0x00;
	t[10]=0x0B;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=0;
	for(i=11;i<14;i++) t[14]^=t[i];
}

void prepara_consulta_agenda_fe(char *t)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x73;
	t[9]=0x00;
	t[10]=0x0B;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];;
	t[14]=0;
	for(i=11;i<14;i++) t[14]^=t[i];
}

void prepara_consulta_preajustes(char *t)
{
	int i=0, _num_cruce=0,_num_grupo=0;
	int _num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x6F;
	t[9]=0x00;
	t[10]=0x0B;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=_num_cruce_separado[0];
	t[13]=_num_cruce_separado[1];
	t[14]=0;
	for(i=11;i<14;i++) t[14]^=t[i];
}

void cargar_consultas_prog(int * _conexion)
{
	int i=0;	
	char te1[16],te3[15],tr3[255],tr4[50],tr5[47],tr6[81],tr7[61],tr8[140],
		 tr9[26],tr10[92],tr11[164];

	//Consulta de estructuras
	for(i=0;i<3;i++)
	{
		prepara_consulta_estructura_alta(te1, i);
		enviar_trama(_conexion,te1,tr3,16,255);
		cargar_estructura_alta(tr3, i);
		prepara_consulta_estructura_baja(te1, i);
		enviar_trama(_conexion,te1,tr3,16,255);
		cargar_estructura_baja(tr3, i);
	}

	//Consulta de matriz
	prepara_consulta_matriz(te3);
	enviar_trama(_conexion,te3,tr4,15,50);
	cargar_matriz(tr4);

	//Consulta de funciones
	for(i=0;i<16;i++)
	{
		prepara_consulta_funciones(te1, i);
		enviar_trama(_conexion,te1,tr5,16,47);
		cargar_funciones(tr5, i);
	}

	//Consulta de programa de tiempos
	for(i=0;i<16;i++)
	{
		prepara_consulta_tiempos(te1, i);
		enviar_trama(_conexion,te1,tr6,16,81);
		cargar_tiempos_1(tr6, i);
		cargar_tiempos_2(tr6,i);
		cargar_tiempos_3(tr6, i);
	}

	//Consulta de agenda diaria
		for(i=0;i<12;i++)
	{
		prepara_consulta_agenda_diaria(te1, i);
		enviar_trama(_conexion,te1,tr7,16,61);
		cargar_agenda_diaria(tr7, i);
	}

	//Consulta de agenda semanal y anual
	prepara_consulta_agenda_as(te3);
	enviar_trama(_conexion,te3,tr8,15,140);
	cargar_agenda_as(tr8);

	//Consulta de agenda Feriados y Especial
	prepara_consulta_agenda_fe(te3);
	enviar_trama(_conexion,te3,tr8,15,164);
	cargar_agenda_f(tr8);
	cargar_agenda_e(tr8);
}
