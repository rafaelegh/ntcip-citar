#include "prog_envio.h"
#include "asc.h"
#include "citar.h"
#include "bytes.h"
#include "agendas_ntcip.h"

//tramas de programacion remota

//trama de envio de estructura

void prepara_envio_estructura(char *t, int n,int parte)
{
	// n       :numero de estructura (0,1,2)
	// parte=1 :parte alta de la trama de envio de estructura
	// parte=0 :parte baja de la trama de envio de estructura

	int i=0,k=0,l1=0,l2=0, _num_grupo=0,_num_cruce=0;
	extern struct estructuraTable_entry *entry100[3][36]; 
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	int num_cruce_separado[2];
	separar_byte_hex(num_cruce_separado,_num_cruce);

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	
	if(parte==1)		
	{	
		t[8]=0x69;			
		t[10]=0xF6;
		l1=0;
		l2=18;		
	}
	else if(parte==0)
	{
		t[8]=0x6A;
		t[10]=0xF6;
		l1=18;
		l2=36;
	} 

	t[9]=0x00;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	i=15;
	for(k=l1;k<l2;k++)
	{
		t[i]=entry100[n][k]->estadoIntervalo21;
		t[i+1]=entry100[n][k]->estadoIntervalo43;
		t[i+2]=entry100[n][k]->estadoIntervalo65;
		t[i+3]=entry100[n][k]->estadoIntervalo87;
		t[i+4]=entry100[n][k]->estadoIntervalo109;
		t[i+5]=entry100[n][k]->estadoIntervalo1211;
		t[i+6]=entry100[n][k]->estadoIntervalo1413;
		t[i+7]=entry100[n][k]->estadoIntervalo1615;
		t[i+8]=entry100[n][k]->funcionIntervalo;
		t[i+9]=entry100[n][k]->parametro1;
		t[i+10]=entry100[n][k]->parametro2;
		t[i+11]=entry100[n][k]->parametro3;
		t[i+12]=entry100[n][k]->parametro4;
		i+=13;
	}
	t[249]=0;
	for(i=11;i<249;i++) t[249]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<250;i++) printf("%X ", t[i]);
	printf("\n ");
*/	
}

//trama de envio de matriz conflictos
void prepara_envio_matriz_conflictos(char *t)
{
	int i=0, _num_cruce=0, _num_grupo=0;
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);

	int num_cruce_separado[2];
	separar_byte_hex(num_cruce_separado,_num_cruce);

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x6C;
	t[9]=0x00;			
	t[10]=0x29;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];

	for(i=14;i<44;i++)
	{
		t[i]=0;
	}
	t[44]=0;
	for(i=11;i<44;i++) t[44]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<45;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

//trama de envio de funciones

void prepara_envio_funciones(char *t, int n)
{
	int i=0,_num_cruce=0, _num_grupo=0;
	
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x6E;
	t[9]=0x00;			
	t[10]=0x26;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	for(i=15;i<41;i++)
	{
		t[i]=0;
	}
	t[41]=0;
	for(i=11;i<41;i++) t[41]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<42;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

//trama de envio de programa de tiempos

void prepara_envio_programa_tiempos(char *t, int n)
{
	int i=0, k=0,_num_cruce=0, _num_grupo=0;
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	
	extern struct programaTiempos1Table_entry *entry103[16][36];
	extern struct programaTiempos2Table_entry *entry104[16];
	extern struct programaTiempos3Table_entry *entry105[16][9];
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x72;
	t[9]=0x00;			
	t[10]=0x48;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	for(k=0;k<36;k++)
	{
		t[k+15]=entry103[n][k]->tiempoIntervalo;
	}
	t[51]=entry104[n]->duracionCicloMSB;
	t[52]=entry104[n]->duracionCicloLSB;
	t[53]=entry104[n]->tiempoSuplementarioMSB;
	t[54]=entry104[n]->tiempoSuplementarioLSB;
	t[55]=entry104[n]->desfasajeMSB;
	t[56]=entry104[n]->desfasajeLSB;
	i=0;
/*	for(k=0;k<9;k++)
	{
		t[i+57]=entry105[n][k]->tiempoAvanceMSB;
		t[i+58]=entry105[n][k]->tiempoAvanceLSB;
		i+=2;
	}*/
	for(i=57;i<74;i++) 
	{
		t[i]=0x7F;
		t[i+1]=0xFF;
	}
	t[74]=0;
	for(i=11;i<74;i++) t[74]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<75;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

//trama de envio de programa de tiempos

void prepara_envio_agenda_diaria(char *t, int n)
{

	int i=0, k=0, _num_cruce=0, _num_grupo=0;
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	extern struct agendaDiariaTable_entry *entry106[12][10];
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x78;
	t[9]=0x00;			
	t[10]=0x34;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	i=0;
	for(k=0;k<10;k++)
	{
		t[i+15]=entry106[n][k]->horaCambio;
		t[i+16]=entry106[n][k]->minutoCambio;
		t[i+17]=entry106[n][k]->programaCambio;
		t[i+18]=entry106[n][k]->demandaAlmacenada;
		i+=4;
	}

	t[55]=0;
	for(i=11;i<55;i++) t[55]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<56;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void prepara_envio_agenda_anual_semanal(char *t)
{
	int i=0, k=0,_num_cruce=0, _num_grupo=0;
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);
	
	extern struct agendaAnualSemanalTable_entry *entry107[12];
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x76;
	t[9]=0x00;			
	t[10]=0x83;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	i=0;
	for(k=0;k<12;k++)
	{
		t[i+14]=entry107[k]->diaCambio;
		t[i+15]=entry107[k]->mesCambio;
		t[i+16]=entry107[k]->numeroAgendaSemanal;
		t[i+17]=entry107[k]->agendaDiariaDomingoAgendaSemanal;
		t[i+18]=entry107[k]->agendaDiariaLunesAgendaSemanal;
		t[i+19]=entry107[k]->agendaDiariaMartesAgendaSemanal;
		t[i+20]=entry107[k]->agendaDiariaMiercolesAgendaSemanal;
		t[i+21]=entry107[k]->agendaDiariaJuevesAgendaSemanal;
		t[i+22]=entry107[k]->agendaDiariaViernesAgendaSemanal;
		t[i+23]=entry107[k]->agendaDiariaSabadoAgendaSemanal;
		i+=10;
	}

	t[134]=0;
	for(i=11;i<134;i++) t[134]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<135;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void prepara_envio_agenda_feriados_especiales(char *t)
{
	int i=0, k=0,_num_cruce=0, _num_grupo=0;	
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	extern struct agendaEspecialesTable_entry *entry110[16];
	extern struct agendaFeriadosTable_entry *entry111[32];
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x74;
	t[9]=0x00;			
	t[10]=0x9B;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	i=0;
//	t[14]=0XFF;
	//printf("%i \n",i);
	for(k=0;k<16;k++)
	{
		t[i+14]=entry110[k]->diaAgendaEspecialesCambio;
		t[i+15]=entry110[k]->mesAgendaEspecialesCambio;
		i+=2;
	}
	i=0;
	for(k=0;k<32;k++)
	{
		t[i+45]=entry111[k]->diaAgendaFeriadosCambio;
		t[i+46]=entry111[k]->mesAgendaFeriadosCambio;
		i+=2;
	}

	for(k=0;k<16;k++)
	{
		t[k+109]=entry110[k]->agendaDiariaAgendaEspecialesCambio;
	}

	for(k=0;k<32;k++)
	{
		t[k+125]=entry111[k]->agendaDiariaAgendaFeriadosCambio;
	}

	t[158]=0;
	for(i=11;i<158;i++) t[158]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<159;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void prepara_envio_preajustes(char *t)
{
	int i=0, k=0,_num_cruce=0, _num_grupo=0;
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x70;
	t[9]=0x00;			
	t[10]=0x51;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=0;
	t[15]=0x01;
	t[16]=0;
	t[17]=0x01;
	t[18]=num_cruce_separado[0];
	t[19]=num_cruce_separado[1];
	for(i=20;i<85;i++) t[i]=0;
	for(i=44;i<63;i+=2) t[i]=0x7F;
	
	for(i=11;i<84;i++) t[84]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<85;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void prepara_envio_grabacion_eeprom(char *t)
{
	int i=0,_num_cruce=0, _num_grupo=0;
	int num_cruce_separado[2];

	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);		

	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x7E;
	t[9]=0x00;			
	t[10]=0xB;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=0;
	for(i=11;i<14;i++) t[14]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<15;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void programacion_objetos_propietarios(int * __conexion)
{

	//retardo de envios de programacion
	int _delay_programacion=0, _delay_eepromm=0;

	//tramas de envios de programacion
	char t_envio_estructura[250],
		 t_envio_matrizC[45],
		 t_envio_funciones[42],
		 t_envio_tiempos[76],
		 t_envio_agenda_diaria[56],
		 t_envio_agenda_semanal[135],
		 t_envio_agenda_feriados_especiales[159],
		 t_envio_preajustes[85], 
		 t_envio_eeprom[15],
	
	//trama de recepcion generica de envios
		 t_rec_envio[20], 
		 t_rec_eeprom[26];
	
	_delay_programacion = get_variable(8);
	_delay_eepromm 		= get_variable(9);

	printf("Iniciando programacion:\n");

	for(int i=0;i<3;i++)
	{
		printf("Trama de estructura parte alta %i:\n",i);
		prepara_envio_estructura(t_envio_estructura,i,1);
		enviar_trama(__conexion,t_envio_estructura,t_rec_envio,250,20);
		sleep(_delay_programacion);
		printf("Trama de estructura parte baja %i:\n",i);
		prepara_envio_estructura(t_envio_estructura,i,0);
		enviar_trama(__conexion,t_envio_estructura,t_rec_envio,250,20);
		sleep(_delay_programacion);
	}

	printf("Trama de matriz de conflictos:\n");
	prepara_envio_matriz_conflictos(t_envio_matrizC);
	enviar_trama(__conexion,t_envio_matrizC,t_rec_envio,45,20);
	sleep(_delay_programacion);

	for(int i=0;i<32;i++)
	{
		printf("Trama de funciones %i:\n",i);
		prepara_envio_funciones(t_envio_funciones,i);
		enviar_trama(__conexion,t_envio_funciones,t_rec_envio,42,20);
		sleep(_delay_programacion);
	}
				
	for(int i=0;i<16;i++)
	{
		printf("Trama de programa de tiempos %i:\n",i);
		prepara_envio_programa_tiempos(t_envio_tiempos,i);
		enviar_trama(__conexion,t_envio_tiempos,t_rec_envio,76,20);
		sleep(_delay_programacion);
	}
/*
	for(int i=0;i<12;i++)
	{
		printf("Trama de agenda diarias %i:\n",i);
		prepara_envio_agenda_diaria(t_envio_agenda_diaria,i);
		enviar_trama(__conexion,t_envio_agenda_diaria,t_rec_envio,56,20);
		sleep(_delay_programacion);
	}

	printf("Trama de agenda anual y semanal:\n");
	prepara_envio_agenda_anual_semanal(t_envio_agenda_semanal);
	enviar_trama(__conexion,t_envio_agenda_semanal,t_rec_envio,135,20);
	sleep(_delay_programacion);
				
	printf("Trama de agenda feriados y especiales:\n");
	prepara_envio_agenda_feriados_especiales(t_envio_agenda_feriados_especiales);
	enviar_trama(__conexion,t_envio_agenda_feriados_especiales,t_rec_envio,159,20);
	sleep(_delay_programacion);
*/

	//algoritmo de conversion de agendas ntcip a agendas citar y envio de las tramas
	conversion_schedule_ntcip_citar();
	
	printf("Trama de preajustes:\n");
	prepara_envio_preajustes(t_envio_preajustes);
	enviar_trama(__conexion,t_envio_preajustes,t_rec_envio,85,20);
	sleep(_delay_programacion);	

//	for(i=0;i<18;i++)
//	{
		printf("Trama de grabacion de EEPROM %i:\n");
		prepara_envio_grabacion_eeprom(t_envio_eeprom);
		enviar_trama(__conexion,t_envio_eeprom,t_rec_eeprom,15,26);
		sleep(_delay_eepromm);
//	}
				
	printf("programacion completa!!!\n");
	set_variable(3,0);
	sleep(5);
	
}
