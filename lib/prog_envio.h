

void prepara_envio_estructura(char *, int, int);
void prepara_envio_matriz_conflictos(char *);
void prepara_envio_funciones(char *, int);
void prepara_envio_programa_tiempos(char *, int);
void prepara_envio_agenda_diaria(char *, int);
void prepara_envio_agenda_anual_semanal(char *);
void prepara_envio_agenda_feriados_especiales(char *);
void prepara_envio_preajustes(char *);
void prepara_envio_grabacion_eeprom(char *t);
void programacion_objetos_propietarios(int *);

struct tramas_programacion
{
	char 	estructura_alta[250];
	char	estructura_baja[250];
	char	matriz_conflicto[25];
	char 	funciones[42];
	char	programa_tiempos[76];
	char	agenda_diaria[56];
	char	agenda_anual_sem[135];
	char	agenda_fer_esp[159];
	char	preajustes[85];
	char	grabacion_eeprom[15];
};
