#ifndef CALENDARIO_H
#define CALENDARIO_H

struct Calendar{
		int diaMes;
		int mes;
		int diaSemana;
		int planDiario;
};

void hacerCalendario( struct Calendar * , int []);
int diasDelMes(int);
void bisiesto(int []);
void indicarDiaSemana(int diaEntrada, char * diaRespuesta);
void indicarMes(int mesEntrada, char * mesRespuesta);


#endif

