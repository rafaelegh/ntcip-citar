#include <wiringPi.h>
#include <wiringSerial.h>
#include <errno.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>


#ifndef CONEXION_H
#define CONEXION_H


int conectar(char *, int);
void enviar_ethernet(int *, char *, char *, int, int);
void enviar_serial(int *, char *, char *, int, int);
void debugging_led(void);


#endif
