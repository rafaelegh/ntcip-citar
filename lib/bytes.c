#include "bytes.h"
#include <stdio.h>

int get_array_length(int array[]) {
	
	size_t n = sizeof(*array)/sizeof(array[0]);
	return n;
}

int dec_to_hex(int dec)
{
	int hex=0,modulo=0, cont=1;
	while(dec!=0)
	{
		modulo=dec%16;
		modulo*=cont;
		hex+=modulo;
		dec/=16;
		cont*=0x10;
	}
	return hex;
}

// unir 2 bytes en hexadecimal
int unir_byte_hex(int MSB, int LSB)
{
	int hex = 0,aux = 0;
	hex = MSB;
	hex <<= 8;
	hex += LSB;
	//printf("%X \n",hex);
	
	return hex;
}

int unir_nibble_hex(int MSB, int LSB)
{
	int hex=0,aux=0;
	hex=MSB;
	hex<<=4;
	hex+=LSB;
	//printf("%X \n",hex);
	return hex;
}

unsigned long unir_4nibble_hex(int parte3,int parte2,int parte1, int parte0)
{
	unsigned long hex=0,auxi=0;
	auxi=parte3;
	auxi<<=24;
	hex+=auxi;

	auxi=parte2;
	auxi<<=16;
	hex+=auxi;

	auxi=parte1;
	auxi<<=8;
	hex+=auxi;

	hex+=parte0;
	//printf("%X \n",hex);
	return hex;
}

//separar un numero entero en 2 bytes hexadecimales
void separar_byte_hex(int * bytes,unsigned int hex)
{
	bytes[1]=hex;
	bytes[1]&=0xFF;
	
	bytes[0]=hex;
	bytes[0]&=0xFF00;
	bytes[0]>>=8;
}

void separar_4byte_hex(int * bytes,unsigned long hex)
{
	unsigned long auxi=0;

	auxi=hex;
	auxi&=0xFF000000;
	auxi>>=24;
	bytes[3]=auxi;

	auxi=hex;
	auxi&=0xFF0000;
	auxi>>=16;
	bytes[2]=auxi;	

	bytes[1]=hex;
	bytes[1]&=0xFF00;
	bytes[1]>>=8;

	bytes[0]=hex;
	bytes[0]&=0xFF;

}

void dec_to_bin(int dec,int * binario)
{
	int i=0,modulo=0;
	for(i=0;i<8;i++) binario[i]=0;
	i=0;
	while(dec!=0 && i<8)
	{
		modulo=dec%2;
		dec/=2;
		//printf("%i ",modulo);
		//printf("%i ",dec);
		binario[i]=modulo;
		i++;
	}
/*
	printf("Binario: ");
	for(i=0;i<8;i++) printf("%i ",binario[i]);
	printf("\n");
*/
}

void dec_to_bin_n(unsigned long dec,int binario[], int array_length)
{
	int i = 0;
	unsigned long modulo=0;
	
	/*n = get_array_length(binario);
	printf("longitud del array: %i\n",n);*/
	
	for(i=0; i<array_length; i++) binario[i]=0;
	
	i=0;
	while(dec != 0 && i < array_length)
	{
		modulo=dec%2;
		dec/=2;
		//printf("%i ",modulo);
		//printf("%i ",dec);
		binario[i]=modulo;
		i++;
	}
/*
	printf("Binario: ");
	for(i=0;i<array_length;i++) printf("%i ",binario[i]);
	printf("\n");
*/
}

int dec_to_BCD_hex(int dec)
{
	int hex=0,b=0;
		
	b=dec/10;
	dec=dec-(b*10);
	hex=b;
	hex<<=4;
	hex+=dec;
	return hex;
}


