#include "conexion.h"
#include "citar.h"


//funcion para conectar por ethernet, requiere ip y puerto
int conectar(char * direccion_ip, int puerto)
{
  struct sockaddr_in cliente; //Declaración de la estructura con información para la conexión
  struct hostent *servidor; //Declaración de la estructura con información del host
  struct timeval tv;
  tv.tv_sec=5;
  tv.tv_usec=0;
  servidor = gethostbyname(direccion_ip); //Asignacion
  
  if(servidor == NULL) { 
    printf("Host erróneo\n");
    return 1;
  }
  
  int conexion, i=0;
  conexion = socket(AF_INET, SOCK_STREAM, 0); //Asignación del socket
  //puerto=(atoi(argv[2])); //conversion del argumento
  bzero((char *)&cliente, sizeof((char *)&cliente)); //Rellena toda la estructura de 0's
     
  cliente.sin_family = AF_INET; //asignacion del protocolo
  cliente.sin_port = htons(puerto); //asignacion del puerto
  bcopy((char *)servidor->h_addr, (char *)&cliente.sin_addr.s_addr, sizeof(servidor->h_length));
  setsockopt(conexion,SOL_SOCKET,SO_RCVTIMEO,(const char*)&tv,sizeof(tv));
  if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0) { 
	//conectando con el host
	//printf("Error conectando con el host\n");
    close(conexion);
    return 2;
  }
  printf("Conectado con %s:%d\n",inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));
  
  return conexion;
}


void enviar_ethernet(int * conexion, char * tout, char *tin, int tamano_1, int tamano_2) {
	
	int puerto = 0;
	int codigo = 0;
	char * direccion_ip = NULL;
	
	direccion_ip = get_cadena();
	puerto = get_variable(4);

	*conexion=conectar(direccion_ip,puerto);
	if(send(*conexion, tout, tamano_1, 0)>0)
	{
		imprimir_trama(tout, tamano_1);

		recv(*conexion, tin, tamano_2, MSG_WAITALL); //recepción
	}

	close(*conexion);

}

//funcion para enviar por serial
void enviar_serial(int * conexion, char * tout, char *tin, int tamano_1, int tamano_2){
	
	int bytes = 0;
	int recibido = 0;
	
	if((*conexion = serialOpen("/dev/ttyS0", 9600)) < 0)
	{
		fprintf(stderr, "Unable to open serial device: %s\n",strerror(errno));
	}
	
	if(wiringPiSetup() == -1)
	{
		fprintf(stderr, "Unable to start wiring pi: %s\n",strerror(errno));
	}
	
	bytes = write(*conexion,tout, tamano_1);
	imprimir_trama(tout, tamano_1);
	
	//recibido = serialDataAvail(*conexion);
	//printf("recibido: %i \n",recibido);
	
	for(int j=0;j< tamano_2;j++) {
		
		recibido = 0;
		recibido = serialGetchar(*conexion);
		
		if(recibido == -1) {
			printf("recibido: %i \n",recibido);
			break;
		}

		tin[j] = recibido;
	}

	serialClose(*conexion);

}

void msleep(int tms) {
	struct timeval tv;
	tv.tv_sec = tms / 1000;
	tv.tv_usec = (tms % 1000) * 1000;
	select(0, NULL, NULL, NULL, &tv);
}

void debugging_led(void){
	
	pinMode (0, OUTPUT);

	digitalWrite(0, HIGH);
	msleep(500);
		
	digitalWrite(0, LOW);
	msleep(500);
}
