
void VerificarCanal(char FaseF, char TipoF);
void LlenarSecuenceData(void);
void LlenarCanales(void);
void LlenarOverLaps(void);
void CargarVehicularEnMatriz(void);
void CargarPeatonalEnMatriz(void);
void LlenarMatriz16x5(void);
void OrdenarTiempos(void);
void ArmarOverLaps(void);
void ArmarTelegramas(void);
char ObtenerColor(char TiempoF,char GrupoF);
void prepara_envio_estructura2(char *t, int n,int parte);
void prepara_envio_programa_tiempos2(char *t, int n);
void conversion_ntcip_citar(int * _conexion);
