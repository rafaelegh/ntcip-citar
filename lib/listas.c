
#include<stdio.h>
#include<stdlib.h>
#include"listas.h"

ptnodo lista=NULL;

ptnodo* get_lista(void)
{
	return &lista;
}

void imprimir_lista(ptnodo _lista)
{
	if(_lista == NULL)
	{
		printf("lista esta vacia \n");
	}
	
	else
	{
		printf("datos de lista:\n");	
		while(_lista!=NULL)
		{
			printf("tabla: %i, indice1: %i, indice2: %i, columna: %i, valor: %i \n", _lista->data.tabla, 
			_lista->data.indice1, _lista->data.indice2, _lista->data.columna,_lista->data.valor);
			_lista=_lista->Sig;
		}
		printf("\n");
	}	
}

ptnodo ultimo_lista(ptnodo _lista)
{
	ptnodo AUX=NULL;

	AUX=_lista;

	while(AUX->Sig!=NULL)
	{
		AUX=AUX->Sig;
	}

	return AUX;
}

void agregar_nodo(ptnodo * _lista, struct datos_lista_objetos * _data)
{
	ptnodo NUEVO, AUXI;
	NUEVO = malloc(sizeof(NODO));

	NUEVO->data.tabla = _data->tabla;
	NUEVO->data.indice1 = _data->indice1;
	NUEVO->data.indice2 = _data->indice2;
	NUEVO->data.columna = _data->columna;
	NUEVO->data.valor = _data->valor;
	NUEVO->Sig=NULL;

	if(*_lista==NULL) 
	{
		*_lista=NUEVO;
	}
	else
	{
		AUXI=ultimo_lista(*_lista);
		AUXI->Sig=NUEVO;	
	}
	
}



void borrar_nodo(ptnodo * _lista)
{
	ptnodo AUXI;

	//Pregunto si la lista esta vacio
	if(*_lista!=NULL)
	{
		//Pregunto si la lista solo tiene 1 miembro
		if((*_lista)->Sig!=NULL)
		{
		 	AUXI   = *_lista;
			*_lista = (*_lista)->Sig;
			free(AUXI);
		}
		else 
		{
			AUXI   = *_lista;
			*_lista = NULL;
			free(AUXI);
		}
	}
	else printf("No se puede borrar, la lista vacia \n");
}	
