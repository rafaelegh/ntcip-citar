#include "citar.h"
#include "asc.h"
#include "bytes.h"

//se actualizo de 8 a 19 detectores, ahora se pase de tr12 a tr13


struct volumeOccupancyTable_entry conteos[19];

time_t 
	ini_conteo=0, 
	timeout_conteo=0;
	
int T_conteo = 0, 
	T_conteo_viejo = 0, 
	nIntegracion = 0, 
	nConteos = 0;
	
int *noActividad=NULL;
int *maxPresencia=NULL;

void limpiar_cuenta_conteos(void)
{
	for(int x=0;x<19;x++) 
	{	
		conteos[x].detectorVolume=0;
		conteos[x].detectorOccupancy=0;
	}
	
}

void inicializa_conteos(void) 
{
	noActividad     = malloc(get_detector_variable(1) * sizeof(int));
	maxPresencia    = malloc(get_detector_variable(1) * sizeof(int));
	
	ini_conteo = time(NULL);
	limpiar_cuenta_conteos();

}

void prepara_consulta_detectores(char *t)
{

	int num_cruce_separado[2];
	int _num_grupo = 0, _num_cruce = 0, _comando = 0;
	
	_comando   = get_variable(2);
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x7C;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(int i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=_comando;
	t[15]=0;
	for(int i=11;i<15;i++) t[15]^=t[i];
}

void prepara_consulta_detectores2(char *t)
{
	//este trama consulta el EC y y la respuesta es hasta 19 espiras

	int num_cruce_separado[2];
	int _num_grupo = 0, _num_cruce = 0, _comando = 0;
	
	_comando   = get_variable(2);
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);

	int i=0;
	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x57;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=_comando;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void cargar_conteos(void)
{
	int 	nMaxDetectors=0;
	float	OccupancyData=0; 
	struct volumeOccupancyTable_entry ** p_volumeOccupancy = NULL;

	p_volumeOccupancy = get_volumeOccupancy_table();
	nMaxDetectors = get_detector_variable(1);

	for(int i=0;i<nMaxDetectors;i++)
	{
		//conversion de porcentaje de ocupacion
		OccupancyData =  conteos[i].detectorOccupancy;
		OccupancyData /= 60;
		p_volumeOccupancy[i]->detectorVolume 	= conteos[i].detectorVolume;
		p_volumeOccupancy[i]->detectorOccupancy = OccupancyData*200;
	}
	printf("conteos cargados\n");
    limpiar_cuenta_conteos();
	set_detector_variable(4,0);

}

void acumular_conteos(char * t)
{
	int nMaxDetectors=0;
	
	nMaxDetectors = get_detector_variable(1);
/*	
	for(int y=0;y<19;y++)
	{
		t[19+y]=20;
		t[38+y]=60;
	}
*/
	for(int x=0;x<nMaxDetectors;x++) 
	{	
		conteos[x].detectorVolume += t[19+x];
		conteos[x].detectorOccupancy += t[38+x];
//		conteos[x].detectorVolume += 10;

		printf("Vol %i: %i, Ocu %i: %i\n", x+1,conteos[x].detectorVolume,x+1, conteos[x].detectorOccupancy);
	}
	printf("\n");
	
}

void imprimir_conteos(char * t,char * t2)
{
	int nMaxDetectors=0;
	int binario[8];
	
	nMaxDetectors = get_detector_variable(1);

	for(int k=0;k<8;k++) binario[k]=0;

	dec_to_bin(t2[75],binario);
	for(int k=0;k<8;k++)
	{
		printf("Demanda %i: ",k+1);
		if(binario[k] == 1) printf("On");
		else printf("Off");
		printf(" Volumen: %i, Ocupacion: %i\n",t[19+k],t[27+k]);
	}
	printf("\n");
}

void revisar_fallas_detectores(char * t, int * _noActividad, int * _maxPresencia)
{

	int maxDetector=0, bit_falla=0, grupo_alarmas=0;
	struct vehicleDetectorTable_entry ** p_detector=NULL;
	struct vehicleDetectorStatusGroupTable_entry ** p_detector_status=NULL;
	struct detectoresTable_entry ** p_detectores=NULL; 
	struct pedestrianDetectorTable_entry ** p_peatonal=NULL;

	p_detector        = get_vehicleDetector_table();
	p_detector_status = get_vehicleStatus_table();
	p_detectores 	  = get_detectores_plus();
	p_peatonal		  = get_pedestrian_table();

	maxDetector = get_detector_variable(1);

	for(int i=0;i<maxDetector;i++)
	{
		if(t[i+19]==0 && t[i+38]==0)
		{
			_noActividad[i]++;
		}
		else if(t[i+19]>0 && t[i+38]>0)
		{
			_noActividad[i]=0;
		}

		if(t[i+19]==0 && t[i+38]>=58)
		{
			_maxPresencia[i]++;
		}
		else if(t[i+19]>=0 && t[i+38]>=0)
		{
			_maxPresencia[i]=0;
		}

		if(i>=0 && i<=7)
		{
			grupo_alarmas=0;
		}
		else if(i>=8 && i<=15)
		{
			grupo_alarmas=1;
		}
		else if(i>=16 && i<=23)
		{
			grupo_alarmas=2;
		}
		else if(i>=24 && i<=31)
		{
			grupo_alarmas=3;
		}
		//chequeo de los parametros limite de fallas de las alarmas
		//vehicular
		if(p_detectores[i]->tipoDetector == 2)
		{
			//No actividad
			
			if (_noActividad[i] >= p_detector[i]->vehicleDetectorNoActivity && 
				p_detector[i]->vehicleDetectorNoActivity != 0)
			{
				bit_falla = p_detector[i]->vehicleDetectorAlarms & 1;
				if(bit_falla == 0)
				{
					p_detector[i]->vehicleDetectorAlarms += 1;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms += pow(2,i);
				}
			}
			else
			{
				bit_falla = p_detector[i]->vehicleDetectorAlarms & 1;
				if(bit_falla == 1)
				{
					p_detector[i]->vehicleDetectorAlarms -= 1;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms -= pow(2,i);
				}
			}
			//maxima presencia
			if (_maxPresencia[i] >= p_detector[i]->vehicleDetectorMaxPresence && 
				p_detector[i]->vehicleDetectorMaxPresence != 0)
			{
				bit_falla = p_detector[i]->vehicleDetectorAlarms & 2;
				if(bit_falla == 0)
				{	
					p_detector[i]->vehicleDetectorAlarms += 2;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms += pow(2,i);
				}
			}
			else
			{
				bit_falla = p_detector[i]->vehicleDetectorAlarms & 2;
				if(bit_falla == 2)
				{	
					p_detector[i]->vehicleDetectorAlarms -= 2;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms -= pow(2,i);
				}
			}
			//cuenta erratica
			if (t[i+19] >= p_detector[i]->vehicleDetectorErraticCounts && 
				p_detector[i]->vehicleDetectorErraticCounts != 0)
			{
				bit_falla = p_detector[i]->vehicleDetectorAlarms & 4;
				if(bit_falla == 0)
				{					
					p_detector[i]->vehicleDetectorAlarms += 4;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms += pow(2,i);
				}
			}
			else
			{
				bit_falla = p_detector[i]->vehicleDetectorAlarms & 4;
				if(bit_falla == 4)
				{					
					p_detector[i]->vehicleDetectorAlarms -= 4;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms -= pow(2,i);
				}

			}
		}
		//peatonal
		else if(p_detectores[i]->tipoDetector == 3)
		{
			//No actividad
			if (_noActividad[i] >= p_peatonal[i]->pedestrianDetectorNoActivity && 
				p_peatonal[i]->pedestrianDetectorNoActivity != 0)
			{
				bit_falla = p_peatonal[i]->pedestrianDetectorAlarms & 1;
				if(bit_falla == 0)
				{
					p_peatonal[i]->pedestrianDetectorAlarms += 1;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms += pow(2,i);
				}
			}
			else
			{
				bit_falla = p_peatonal[i]->pedestrianDetectorAlarms & 1; 
				if(bit_falla == 1)
				{
					p_peatonal[i]->pedestrianDetectorAlarms -= 1;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms -= pow(2,i);
				}
			}
			//maxima presencia
			if (_maxPresencia[i] >= p_peatonal[i]->pedestrianDetectorMaxPresence && 
				p_peatonal[i]->pedestrianDetectorMaxPresence != 0)
			{
				bit_falla = p_peatonal[i]->pedestrianDetectorAlarms & 2; 
				if(bit_falla == 0)
				{
					p_peatonal[i]->pedestrianDetectorAlarms += 2;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms += pow(2,i);
				}
			}
			else
			{
				bit_falla = p_peatonal[i]->pedestrianDetectorAlarms & 2; 
				if(bit_falla == 2)
				{
					p_peatonal[i]->pedestrianDetectorAlarms -= 2;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms -= pow(2,i);
				}
			}
			//Cuentas erraticas
			if (t[i+19] >= p_peatonal[i]->pedestrianDetectorErraticCounts && 
				p_peatonal[i]->pedestrianDetectorErraticCounts != 0)
			{
				bit_falla = p_peatonal[i]->pedestrianDetectorAlarms & 1;
				if(bit_falla == 0)
				{
					p_peatonal[i]->pedestrianDetectorAlarms += 4;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms += pow(2,i);
				}
			}
			else 
			{
				bit_falla = p_peatonal[i]->pedestrianDetectorAlarms & 4; 
				if(bit_falla == 4)
				{
					p_peatonal[i]->pedestrianDetectorAlarms -= 4;
					p_detector_status[grupo_alarmas]->vehicleDetectorStatusGroupAlarms -= pow(2,i);
				}
			}
		}
	}
}

void hacer_conteos(void) 
{
	unsigned char trama_conteo[16],
				  trama_rec_conteo[78];
				 
	int *_conexion = NULL;
	
	_conexion = get_conexion(); 
		
	T_conteo = get_detector_variable(5);
	if(T_conteo != T_conteo_viejo)
	{
		ini_conteo = time(NULL);
		T_conteo_viejo = T_conteo;
		nIntegracion = T_conteo_viejo/60;
		limpiar_cuenta_conteos();
	}
	
	timeout_conteo = (time(NULL))-ini_conteo;

	if(timeout_conteo >= 60)
	{
		ini_conteo = time(NULL);
		timeout_conteo = 0;
		prepara_consulta_detectores2(trama_conteo);
		enviar_trama(_conexion,trama_conteo,trama_rec_conteo,16,78);
		acumular_conteos(trama_rec_conteo);
		revisar_fallas_detectores(trama_rec_conteo,noActividad,maxPresencia);
		nConteos++;

		if(nIntegracion == nConteos)
		{
			cargar_conteos();
			nConteos=0;
		}
		//prepara_trama_comando(te1);
	}
	
}
