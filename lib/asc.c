#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include <math.h>



int compara_oid(oid * oidrequest)
{
	int i, asc=0,resul=0;
	oid asc_oid[]= { 1,3,6,1,4,1,1206,4,2,1};
	oid * oidref= asc_oid; 
	oid * oidfirst=oidrequest;
		
	for(i=0;i<10;i++)
	{
		if(*(oidrequest+i)==*(oidref+i)) asc++;

	}
	if (asc==10)
	{
		oidrequest=oidfirst;
		switch(*(oidrequest+10))
		{
			case 1:								//phase
			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:
			
											//maxPhases
				return 1;
				break;

				case 3:							//maxPhaseGroups
				return 3;
				break;
				
			}
			break;
			case 2:								//detector
			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//maxVehicleDetectors
				return 1;
				break;

				case 3:							//maxVehicleDetectors
				return 3;
				break;

				case 5:							//VolumeOccupancyReport

				if(*(oidrequest+12)==1)	return 51;			//volumeOccupancySequence
				else if(*(oidrequest+12)==2) return 52;		//volumeOccupancyPeriod
				else if(*(oidrequest+12)==2) return 53;		//activeVolumeOccupancyDetectors
				
				case 6:							//maxPedestrianDetectors
				return 6;
				break;

				
			}
			break;
			case 3:								//unit
			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//unitStartupFlash
				return 1;
				break;

				case 2:							//unitAutoPedestrianClear
				return 2;
				break;
				
				case 3:							//unitBackupTime
				return 3;
				break;

				case 4:							//unitRedRevert
				return 4;
				break;

				case 5:							//unitControlStatus
				return 5;
				break;
				
				case 6:							//unitFlashStatus
				return 6;
				break;

				case 7:							//unitAlarmStatus2
				return 7;
				break;

				case 8:							//unitAlarmStatus1
				return 8;
				break;
				
				case 9:							//shortAlarmStatus
				return 9;
				break;

				case 10:							//unitControl
				return 10;
				break;

				case 11:							//maxAlarmsGroups
				return 11;
				break;
				
				case 13:							//maxSpecialFunctionOutputs
				return 13;
				break;
				
			}


			break;
			case 4:								//coord

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//unitStartupFlash
				return 1;
				break;

				case 2:							//unitAutoPedestrianClear
				return 2;
				break;
				
				case 3:							//unitBackupTime
				return 3;
				break;

				case 4:							//unitRedRevert
				return 4;
				break;

				case 5:							//unitControlStatus
				return 5;
				break;
				
				case 6:							//unitFlashStatus
				return 6;
				break;

				case 8:							//unitAlarmStatus1
				return 8;
				break;

				case 10:							//unitControl
				return 10;
				break;

				case 11:							//maxAlarmsGroups
				return 11;
				break;

				case 12:							//maxAlarmsGroups
				return 12;
				break;
				
				case 13:							//maxSpecialFunctionOutputs
				return 13;
				break;

				case 14:							//maxAlarmsGroups
				return 14;
				break;
				
				case 15:							//maxSpecialFunctionOutputs
				return 15;
				break;
				
				
			}

			break;
			case 5:								//timebaseAsc

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//unitStartupFlash
				return 1;
				break;

				case 2:							//unitAutoPedestrianClear
				return 2;
				break;

				case 4:							//unitRedRevert
				return 4;
				break;	
				
			}

			break;
			case 6:								//preempt

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//unitStartupFlash
				return 1;
				break;
			
			}

			break;
			case 7:								//ring

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//maxRings
				return 1;
				break;

				case 2:							//maxSequences
				return 2;
				break;

				case 4:							//maxRingControlGroups
				return 4;
				break;
				
			}

			break;
			case 8:								//channel

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//unitStartupFlash
				return 1;
				break;
				
				case 3:							//unitBackupTime
				return 3;
				break;
			
			}

			break;
			case 9:								//overlap

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//maxOverlaps
				return 1;
				break;

				case 3:							//maxOverlapStatusGroups
				return 3;
				break;
				
			}

			break;
			case 10:							//ts2port1

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//unitStartupFlash
				return 1;
				break;
			
			}

			break;
			case 11:							//ascBlock

			oidrequest=oidfirst;
			switch(*(oidrequest+11))
			{
				case 1:							//ascBlockGetControl
				return 1;
				break;

				case 2:							//ascBlockData
				return 2;
				break;
				
				case 3:							//ascBlockErrorStatus
				return 3;
				break;				
			}

			break;

		}
	}
	else return 0;
}	

int compara_oid_global(oid * oidrequest)
{
	int i, global=0,resul=0;
	      oid global_oid[]= { 1,3,6,1,4,1,1206,4,2,6};
		  oid * oidref= global_oid; 
	      oid * oidfirst=oidrequest;
		
	for(i=0;i<10;i++)
	{
		if(*(oidrequest+i)==*(oidref+i)) global++;

	}
	if (global==10)
	{
		oidrequest=oidfirst;
		switch(*(oidrequest+10))
		{
			case 1:										//globalConfiguration
			oidrequest=oidfirst;
			if(*(oidrequest+11)==1) resul=1;
			else if(*(oidrequest+11)==2) resul=2;
			else if (*(oidrequest+11)==4) resul=4;
			else resul=0;

			break;

			case 2:			
														//globalDBManagement
			oidrequest=oidfirst;

			if(*(oidrequest+11)==1) resul=1;
			else if(*(oidrequest+11)==6) resul=6;
			else if (*(oidrequest+11)==7) resul=7;
			else resul=0;

			break;

			case 3:								//globalTimeManagement

			oidrequest=oidfirst;

			if(*(oidrequest+11)==1) resul=1;
			else if(*(oidrequest+11)==2) resul=2;

			else if (*(oidrequest+11)==3) 
			{
				if(*(oidrequest+12)==1) resul=31;
				else if (*(oidrequest+11)==3) resul=33;
				else if(*(oidrequest+11)=4) resul=34;
				else if (*(oidrequest+11)==6) resul=36;
				else if(*(oidrequest+11)=7) resul=37;
				else resul=0;
			}

			else if(*(oidrequest+11)==4) resul=4;
			else if (*(oidrequest+11)==5) resul=5;
			else if(*(oidrequest+11)==6) resul=6;

			else if (*(oidrequest+11)==7) 
			{
				if(*(oidrequest+12)==1) resul=71;
				else resul=0;
			}
			else resul=0;
			break;


		}
	}
	else resul=0;

	return resul;
}

int compara_oid_mantelectric(oid * oidrequest)
{
	int i, mantelectric=0,resul=0;
	oid mantelectric_oid[]= { 1,3,6,1,4,1,54879 };
	oid * oidref= mantelectric_oid; 
	oid * oidfirst=oidrequest;
		
	for(i=0;i<7;i++)
	{
		if(*(oidrequest+i)==*(oidref+i)) mantelectric++;

	}
	
	if (mantelectric==7)
	{
		
		oidrequest=oidfirst;
		switch(*(oidrequest+7))
		{
			case 1:										//progIntervalos
			
			oidrequest=oidfirst;
			if (*(oidrequest+8)==11) 	  resul=11;		//progRemota
			else if (*(oidrequest+8)==12) resul=12;		//progPublica
			else if (*(oidrequest+8)==13) resul=13;		//comandoEstado
			else resul=0;

			break;

			case 2:										//estadoExtendido
			oidrequest=oidfirst;

			if (*(oidrequest+8)==1) 	  resul=1;		//numeroControlador
			else if (*(oidrequest+8)==2)  resul=2;		//estatus1
			else if (*(oidrequest+8)==3)  resul=3;		//estatus2
			else if (*(oidrequest+8)==4)  resul=4;		//estatus3
			else if (*(oidrequest+8)==5)  resul=5;		//estructura
			else if (*(oidrequest+8)==6)  resul=6;		//programaTiempos
			else if (*(oidrequest+8)==7)  resul=7;		//numeroPaso
			else if (*(oidrequest+8)==8)  resul=8;		//estadoPaso
			else if (*(oidrequest+8)==9)  resul=9;		//duracionPaso	
			else if (*(oidrequest+8)==10) resul=10;		//tiempoSuplementario
			else if (*(oidrequest+8)==11) resul=11;		//preajusteTiempoSuple
			else if (*(oidrequest+8)==12) resul=12;		//estado
			else if (*(oidrequest+8)==13) resul=13;		//duracionCiclo
			else if (*(oidrequest+8)==14) resul=14;		//desfasaje
			else if (*(oidrequest+8)==15) resul=15;		//segundoCiclo
			else resul=0;

			break;

			case 3:										//ntcipPlus
			
			oidrequest=oidfirst;

			break;
			
			case 4:										//estadoControlador
			
			oidrequest=oidfirst;
			
			if (*(oidrequest+8)==1) 	  resul = TEMPERATURADISPLAY;		
			else if (*(oidrequest+8)==2)  resul = TEMPERATURAMOTHER;		
			else if (*(oidrequest+8)==3)  resul = TEMPERATURAEXTENSION1;		
			else if (*(oidrequest+8)==4)  resul = TEMPERATURAEXTENSION2;		
			else if (*(oidrequest+8)==5)  resul = TEMPERATURAEXTENSION3;		
			else if (*(oidrequest+8)==6)  resul = TEMPERATURAMODULOCOM1;		
			else if (*(oidrequest+8)==7)  resul = TEMPERATURAMODULOCOM2;		
			break;


		}
	}
	else resul=0;

	return resul;
}

int convertir_decena_seg(float valor)
{
	float aux=0;
	int resp=0;

	aux = valor/10;

	if(aux > 0 && aux < 1)	resp = 1;
	else resp = round(aux);	

	return resp;
}	

