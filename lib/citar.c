#include "citar.h"
#include "asc.h"
#include "prog_envio.h"
#include "bytes.h"
#include "conexion.h"
#include <ctype.h>


//variables externas

int actividad=0, 
	comando=0,
	ultimo_comando=0, 
	programacion=0, 
	secuencia_prog[16],
	cambiarFecha = 0;

//datos de conexion
char * direccion_ip="192.168.5.50";
int puerto=1000;
int conexion=0;

//datos del cruce
int num_cruce = 0;
int num_grupo = 0;

//datos de consulta generica de cruce
int num_cruce_generico = 9999;
//31
int num_grupo_generico = 0;

//retardos de envio de tramas
int delay_programacion=1;
int delay_eepromm=2;

//Modo de envio de comunicacion con EC
int SERIAL = 1;

//Variable de estado de comunicacion del EC
/*
	INICADO = 0, EC no ha realizado la consulta generica
	INCIADO = 1, EC ya ha realizado la consulta generica
*/
int INICIADO = 0;

char * get_cadena(void)
{
	return direccion_ip;
}

void set_secuencia_programacion(int a)
{
	if(a>16) for(int x=0;x<16;x++) secuencia_prog[x]=0;
	else secuencia_prog[a-1]=1;	
}

int * get_conexion(void)
{
	return &conexion;
}

int get_secuencia_programacion(void)
{	
	int cont=0,resp=0;
	for(int x=0;x<16;x++)
	{
		if(secuencia_prog[x]==1)
		{
			cont++;
		}
	}

	printf("Secuencia de programacion: \n");

	for(int x=0;x<16;x++)
	{
		printf("%i ",secuencia_prog[x]);
	}

	printf("\n");

	if(cont==16) resp=1;
	else 		 resp=0;

	return resp;
}

void limpiar_secuencia(void)
{
	for(int x=0;x<16;x++)
	{
		secuencia_prog[x]=0;
	}
}

void set_variable(int a, int b)
{
	switch(a)
	{
		//bandera de actividad SNMP
		case 1:
		actividad=b;
		break;
		
		//comando actual
		case 2:
		comando=b;
		break;

		//bandera para disparar programacion 
		case 3:
		programacion=b;
		break;
		
		//comando anterior al actual

		case 5:
		ultimo_comando=b;
		break;
		
		//numero de cruce del EC
		case 6:
		num_cruce = b;
		break;
		
		//numero de grupo del EC
		case 7:
		num_grupo = b;
		break;
		
		//bandera para cambiar Fecha
		case 10:
		cambiarFecha=b;
		break;
		
		//variable de consulta generica
		case 13:
		INICIADO = b;
		break;
	}
}

int get_variable(int a)
{
	int variable=0;

	// actividad
	
	switch(a)
	{
		//bandera de actividad SNMP
		case 1: 
		variable = actividad;
		break;
		
		//comando actual
		case 2: 
		variable = comando;
		break;
		
		//bandera para disparar programacion 
		case 3: 
		variable = programacion;
		break;
		
		//puerto de programacion del EC
		case 4: 
		variable = puerto;
		break;
		
		//comando anterior al actual
		case 5:
		variable = ultimo_comando;
		break;
		
		//numero de cruce del EC
		case 6:
		variable = num_cruce;
		break;
		
		//numero de grupo del EC
		case 7:
		variable = num_grupo;
		break;
		
		//retardo entre tramas de programacion
		case 8:
		variable = delay_programacion;
		break;
		
		//retardo entre tramas de eeprom
		case 9:
		variable = delay_eepromm;
		break;
		
		//numero de cruce para consulta generica
		case 11: 
		variable = num_cruce_generico;
		break;
		
		//numero de grupo para consulta generica
		case 12: 
		variable = num_grupo_generico;
		break;
		
		case 13:
		variable = INICIADO;
		break;
	}
	
	return variable;
}


int codigos(int codigo1)
{
	int codigo2=0;
	switch(codigo1)
	{
		//consulta descentralizada sin comando
		case 0x60:
		codigo2=0xC4;
		break;
		//consulta centralizada con comando
		case 0x61:
		codigo2=0xC5;
		break;
		
		case 0x64:
		codigo2=0xC8;
		break;
	
		case 0x66:
		codigo2=0xCA;
		break;

		//Envio de estructura parte alta
		case 0x6A:
		codigo2=0xCE;
		break;

		//Envio de estructura parte baja
		case 0x69:
		codigo2=0xCD;
		break;

		//consulta de estructura parte alta
		case 0x67:
		codigo2=0xCB;
		break;

		//consulta de estructura parte baja
		case 0x68:
		codigo2=0xCC;
		break;

		//Envio de matriz de conflicto
		case 0x6C:
		codigo2=0xD0;
		break;

		//Consulta de matriz de conflicto
		case 0x6B:
		codigo2=0xCF;
		break;

		//Envio de Funciones
		case 0x6E:
		codigo2=0xD2;
		break;

		//Consulta de Funciones
		case 0x6D:
		codigo2=0xD1;
		break;

		//Envio de programa de tiempos
		case 0x72:
		codigo2=0xD6;
		break;

		//Consulta programa de tiempos
		case 0x71:
		codigo2=0xD5;
		break;

		//Envio de agenda diaria
		case 0x78:
		codigo2=0xDC;
		break;

		//Consulta de agenda diaria
		case 0x77:
		codigo2=0xDB;
		break;

		//Envio de Agenda Anual y Semanal
		case 0x76:
		codigo2=0xDA;
		break;

		//Consulta de agenda Anual y semanal
		case 0x75:
		codigo2=0xD9;
		break;

		//Envio de agenda de Feriados y Especiales
		case 0x74:
		codigo2=0xD8;
		break;

		//Consulta de agenda de Feriados y Especiales
		case 0x73:
		codigo2=0xD7;
		break;

		//Envio de grabacion de EEPROM
		case 0x7E:
		codigo2=0xE2;
		break;

		//Envio de preajustes
		case 0x70:
		codigo2=0xD4;
		break;

		//Consulta de preajustes
		case 0x6F:
		codigo2=0xD3;
		break;
		//consulta de conteos
		case 0x7C:
		codigo2=0xE0;
		break;
		//consulta de conteos 2
		case 0x57:
		codigo2=0xC1;
		break;		
	}
	return codigo2;
}

void imprimir_trama(char * trama, int tamano) {
	
	for(int i=0; i<tamano; i++)
	{ 
		printf("%X ",trama[i]);
	}
	printf("\n");
}

void enviar_trama(int * _conexion, char *tout, char *tin, int tamano_1, int tamano_2)
{
		int codigo=0, valido=0, intentos=0;
		int _iniciado = 0;
		int _tiempo_espera = 0;
		time_t _timeout=0, _ini_timeout=0;
		
		_ini_timeout = time(NULL);

		_iniciado = get_variable(13);
		
		do
		{
			//printf("No se recibio trama valida \n");
/*
			if(intentos>3)
			{
				close(*conexion);
				sleep(1);
				direccion=get_cadena();
				*conexion=conectar(direccion,puerto);
				intentos=0;
			}
*/
			if(SERIAL == 1) {
				enviar_serial(_conexion, tout, tin, tamano_1, tamano_2);
			}
			else {
				enviar_ethernet(_conexion, tout, tin, tamano_1, tamano_2);
			}

			codigo = codigos(tout[8]);
			valido = verificacion_trama_respuesta(tin,codigo,tamano_2);
			
			_timeout = (time(NULL)) - _ini_timeout;
			
			printf("timeout de envio: %i\n", _timeout);
			
			if(_iniciado == 0) {
				
				_tiempo_espera = 3600;
			} else if(_iniciado == 1){
				
				_tiempo_espera = 300;
			}
			
			if(_timeout > _tiempo_espera) {
				system("killall asc_demon");
			}
			
		}
		while(valido == 0);

		if(valido == 1) {
			
			printf("Trama valida recibida \n");
	 		printf("Recibido: \n");
	 		imprimir_trama(tin, tamano_2);

		}
		printf("\n");
}

int verificacion_trama_respuesta(char *t,int codigo, int tamano)
{
	int i=0, val[3], bcc1=0, bcc2=0;
	for(i=0;i<3;i++)val[i]=0;
	//se validan 3 aspectos: cabecera, codigo y los bcc's
	
    //validando cabecera
	for(i=0;i<4;i++) if(t[i]==0)val[0]++;
	if(t[4]==0xFF) val[0]++;
	
	//validando codigo
	if(t[8]==codigo)val[1]++;
	
	//validando bcc1
	for(i=0;i<11;i++) bcc1^=t[i];
	if(bcc1==t[11])val[2]++;
	
	//validando bcc2
	for(i=11;i<tamano-1;i++) bcc2^=t[i];
	if(bcc2==t[tamano-1])val[2]++;
	
	//printf("validaciones: %i %i %X \n",val[0], val[1], bcc2);
	if(val[0]==5 && val[1]==1 && val[2]==2) i=1;
	else i=0;
	
	return i;
}


void prepara_trama_comando(char *t)
{
	int num_cruce_separado[2];
	separar_byte_hex(num_cruce_separado,num_cruce);

	int i=0;
	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0;
	t[7]=num_grupo;
	t[8]=0x61;
	t[9]=0x00;
	t[10]=0x0C;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=comando;
	t[15]=0;
	for(i=11;i<15;i++) t[15]^=t[i];
}

void prepara_trama_comando2(char *t) {
	
	int num_cruce_separado[2];
	separar_byte_hex(num_cruce_separado,num_cruce);

	int i=0;
	//trama de comando corto 0x61
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0;
	t[7]=num_grupo;
	t[8]=0x61;
	t[9]=0x00;
	t[10]=0x0D;
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=comando;
	t[15]=0xFF;
	t[16]=0;
	for(i=11;i<16;i++) t[16]^=t[i];
}

void prepara_trama_consulta_extendido(char * t, int generico) {
	
	int _num_cruce_separado[2];
	int _num_cruce = 0;
	int _num_grupo = 0;
	
	//si se pide consulta generica rellenamos los datos de consulta genericos
		
	if(generico == 1) {
	
		_num_cruce = get_variable(11);
		_num_grupo = get_variable(12);
		
	} else if(generico == 0) {
		
		_num_cruce = get_variable(6);
		_num_grupo = get_variable(7);
	}
	
	separar_byte_hex(_num_cruce_separado,_num_cruce);

	//trama de comando corto 0x60
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7] = _num_grupo;
	t[8]=0x60;
	t[9]=0x00;
	t[10]=0x0B;
	t[11]=0;
	for(int i=0;i<11;i++) t[11]^=t[i];
	
	t[12] = _num_cruce_separado[0];
	t[13] = _num_cruce_separado[1];
	
	t[14]=0;
	for(int i=11;i<14;i++) t[14]^=t[i];
}



void analiza_lamparas(char * t,struct movimiento * f) {
	
	int j=0;
	for(int i=0;i<16;i+=2)
	{
			f[i+0].nible_color=t[j+19]&0xF;             //mov.1
			f[i+1].nible_color=t[j+19]&0xF0;			//mov.2
			f[i+1].nible_color>>=4;
			
/*
 			printf("nible color.%i: %i\n",i+1,f[i+0].nible_color);
			printf("nible color.%i: %i\n",i+2,f[i+1].nible_color);
*/
			j++;	
	}
/*			f[2].nible_color=t[20]&0xF;				//mov.3
			f[3].nible_color=t[20]&0xF0;			//mov.4
			f[3].nible_color>>=4;
			f[4].nible_color=t[21]&0xF;             //mov.5
			f[5].nible_color=t[21]&0xF0;			//mov.6
			f[5].nible_color>>=4;
			f[6].nible_color=t[22]&0xF;				//mov.7
			f[7].nible_color=t[22]&0xF0;			//mov.8
			f[7].nible_color>>=4;*/

}



void actualiza_phaseStatus(struct movimiento * f) {
	
	int i=0, j=0, grupo_ntcip=0;
	//phase
	extern struct phaseStatusGroupTable_entry *entry2[2];
	
	for(i=0;i<2;i++) {
		
		entry2[i]->phaseStatusGroupReds=0;
		entry2[i]->phaseStatusGroupYellows=0;  
		entry2[i]->phaseStatusGroupGreens=0;
		entry2[i]->phaseStatusGroupPhaseOns=0;
	}

	for(i=0;i<16;i++) {
		
		if(i>=0 && i<=7) {
			
			grupo_ntcip=0;
			j=i;
		}
		else if(i>=8 && i<=15) {
			
			grupo_ntcip=1;
			j=i-8;
		}
		
		switch(f[i].nible_color) {
			
			case 0:
/*			entry2[grupo_ntcip]->phaseStatusGroupReds-=pow(2,i);
			entry2[grupo_ntcip]->phaseStatusGroupYellows-=pow(2,i);
			entry2[grupo_ntcip]->phaseStatusGroupGreens-=pow(2,i);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns-=pow(2,i);*/		
			break;
			//rojo
			case 1:
			entry2[grupo_ntcip]->phaseStatusGroupReds += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;
			//amarillo
			case 2:
			entry2[grupo_ntcip]->phaseStatusGroupYellows += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;

			case 3:
			entry2[grupo_ntcip]->phaseStatusGroupReds += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupYellows += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;
			//verde
			case 4:
			entry2[grupo_ntcip]->phaseStatusGroupGreens += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;

			case 9:
			entry2[grupo_ntcip]->phaseStatusGroupReds += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;

			case 10:
			entry2[grupo_ntcip]->phaseStatusGroupYellows += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;

			case 11:
			entry2[grupo_ntcip]->phaseStatusGroupReds += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupYellows += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;

			case 12:
			entry2[grupo_ntcip]->phaseStatusGroupGreens += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;

			case 13:
/*			entry2[grupo_ntcip]->phaseStatusGroupReds-=pow(2,i);
			entry2[grupo_ntcip]->phaseStatusGroupYellows-=pow(2,i);
			entry2[grupo_ntcip]->phaseStatusGroupGreens-=pow(2,i);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns-=pow(2,i);*/
			break;

			case 14:
			entry2[grupo_ntcip]->phaseStatusGroupYellows += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupGreens += pow(2,j);
			entry2[grupo_ntcip]->phaseStatusGroupPhaseOns += pow(2,j);
			break;
			
			default:
			break;
		}
	}
	printf("\033[0;31m");
	printf("NTCIP1202-2000::phaseStatusGroupReds.1 = INTEGER: %i \n",entry2[0]->phaseStatusGroupReds);
	printf("NTCIP1202-2000::phaseStatusGroupReds.2 = INTEGER: %i \n",entry2[1]->phaseStatusGroupReds);
	printf("\033[0;33m");
	printf("NTCIP1202-2000::phaseStatusGroupYellows.1 = INTEGER: %i \n",entry2[0]->phaseStatusGroupYellows);
	printf("NTCIP1202-2000::phaseStatusGroupYellows.2 = INTEGER: %i \n",entry2[1]->phaseStatusGroupYellows);
	printf("\033[0;32m");
	printf("NTCIP1202-2000::phaseStatusGroupGreens.1 = INTEGER: %i \n",entry2[0]->phaseStatusGroupGreens);
	printf("NTCIP1202-2000::phaseStatusGroupGreens.2 = INTEGER: %i \n",entry2[1]->phaseStatusGroupGreens);
	printf("\033[0m");

}

void prepara_trama_fecha_hora(struct tm *tm, char *t2) {
		

	int num_cruce_separado[2];
	separar_byte_hex(num_cruce_separado,num_cruce);	

	int i;
	//trama de imposicion de fecha y hora
	t2[0]=0x00;
	t2[1]=0x00;
	t2[2]=0x00;
	t2[3]=0x00;
	t2[4]=0xFF;
	t2[5]=0x00;
	t2[6]=0x00;
	t2[7]=num_grupo;
	t2[8]=0x66;
	t2[9]=0x00;
	t2[10]=0x12;

	t2[11]=0;
	for(i=0;i<11;i++) t2[11]^=t2[i];

	t2[12]=num_cruce_separado[0];
	t2[13]=num_cruce_separado[1];

	//año
	t2[14]=dec_to_BCD_hex(tm->tm_year-100);
	//mes
	t2[15]=dec_to_BCD_hex(tm->tm_mon+1);
	//dia
	t2[16]=dec_to_BCD_hex(tm->tm_mday);
	//hora
	t2[17]=dec_to_BCD_hex(tm->tm_hour);
	//minuto
	t2[18]=dec_to_BCD_hex(tm->tm_min);
	//segundo
	t2[19]=dec_to_BCD_hex(tm->tm_sec);
	//dia de la semana
	t2[20]=tm->tm_wday++;
	//calculo de bcc2
	t2[21]=0;
	for(i=11;i<21;i++) t2[21] ^= t2[i];
}

void obtener_temperatura_raspberry(void) {
	
	FILE *temperaturaArchivo;
	double T = 0;
	int t_cpu = 0;
	int t_gpu = 0;
	int gpu_text_temp[2];
	char respuesta[20];
	
	//temperatura CPU
	temperaturaArchivo = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
	
	if(temperaturaArchivo == NULL) {
		printf("Error al leer temperatura de CPU\n");
	}
	
	fscanf(temperaturaArchivo, "%lf", &T);
	T /= 1000;
	t_cpu = T;
	
	fclose(temperaturaArchivo);
	
	temperaturaArchivo = NULL;
	
	//temperatura GPU
	
	temperaturaArchivo = popen("vcgencmd measure_temp", "r");
	
	if(temperaturaArchivo == NULL) {
		printf("Error al leer temperatura de GPU\n");
	} else {
		
		fgets(respuesta, 20, temperaturaArchivo);
		
		if(isdigit(respuesta[5]) > 0) {
			gpu_text_temp[0] = respuesta[5] - 48;
		}
		
		if(isdigit(respuesta[6]) > 0) {
			gpu_text_temp[1] = respuesta[6] - 48;
		}
		
		t_gpu = gpu_text_temp[0] * 10 + gpu_text_temp[1];
	}
	
	pclose(temperaturaArchivo);
	
	set_estadoControlador_variable(TEMPERATURAMODULOCOM1, t_cpu);
	set_estadoControlador_variable(TEMPERATURAMODULOCOM2, t_gpu);
}

void estado_extendido(char * t2) {
	
	//detector 
	struct vehicleDetectorStatusGroupTable_entry ** p_vehicle_status=NULL;

	//unit
	extern int unitFlashStatus, unitControlStatus;
	struct alarmGroupTable_entry ** p_alarmGroup=NULL;

	//coord
	int _coordPatternStatus=0,_localFreeStatus=0;

	//global
	long _globalTime;	

	struct tm tm;
	struct tm *tm2;
	int i=0, estadoEx=0, cambiando_plan=0, plan_ntcip=0, alarma_HC=0, binario[8];
	
	char tFecha[22], tFechaRes[20];

	for(i=0;i<8;i++) binario[i]=0;

	//obteniendo punteros de tablas de objetos
	p_alarmGroup = get_alarmGroup_table();
	p_vehicle_status = get_vehicleStatus_table();

	//llenando objetos ntcip con la trama de estado extendido
	// byte de estado
	
	if(t2[14]==0xF0) 				//titilante
	{
		unitFlashStatus=3;
	}
	else if(t2[14]==0xF1)			//apagado
	{
		unitFlashStatus=2;
	}
	else
	{
		unitFlashStatus=2;
		_coordPatternStatus=t2[14];	//plan
	}

	//byte de status 1

/*	if(t[15]&1) 				//bit:0 Titilante normal o por llave
	{
		printf("equipo titilante");
	}
	else
	{
		printf("equipo no itilante");
	}

	if(t[15]&2) 				//bit:1 Titilante apagado o por llave
	{
		printf("equipo Apagado");
	}
	else
	{
		printf("equipo no apagado");
	}
*/
	if(t2[15]&4) 				//bit:2 0:local o 1:centralizado
	{
		_localFreeStatus=2; 		//not free
		unitControlStatus=2;	//control by central
	}
	else
	{
		_localFreeStatus=6;		//coordfree
	}
/*

	if(t[15]&8) 				//bit:4 0:Normal o 1:feriado
	{
		localFreeStatus=2; 		//not free
	}
	else
	{
		localFreeStatus=6;		//coordfree
	}

	if(t[15]&16) 				//bit:5 llave panel 0:local o 1:central
	{
		//localFreeStatus=2; 		//not free
	}
	else
	{
		//localFreeStatus=6;		//coordfree
	}

	if(t[15]&32) 				//bit:6 plan forzado CC 0: Normal o 1:Forzado
	{
		//localFreeStatus=2; 		//not free
	}
	else
	{
		//localFreeStatus=6;		//coordfree
	}

	if(t[15]&128) 				//bit:7 verde 1 0:apagado o 1:encendido
	{
		//localFreeStatus=2; 		//not free
	}
	else
	{
		//localFreeStatus=6;		//coordfree
	}
*/
	//conversion hexadecimal de fecha y hora a timestamp
	if( cambiarFecha == 1) {
		
		_globalTime = get_globalTime_variable();
		tm2=localtime(&_globalTime);
		prepara_trama_fecha_hora(tm2,tFecha);
		enviar_trama(&conexion,tFecha,tFechaRes,22,20);
		
		cambiarFecha = 0;

	}
	else {
		
		tm.tm_year=t2[27]&0xF0;
		tm.tm_year>>=4;
		tm.tm_year*=10;
		tm.tm_year+=t2[27]&0xF;
		tm.tm_year+=100;
		
		tm.tm_mon=t2[28]&0xF0;
		tm.tm_mon>>=4;
		tm.tm_mon*=10;
		tm.tm_mon+=t2[28]&0xF;
		tm.tm_mon--;

		tm.tm_mday=t2[29]&0xF0;
		tm.tm_mday>>=4;
		tm.tm_mday*=10;
		tm.tm_mday+=t2[29]&0xF;

		tm.tm_hour=t2[30]&0xF0;
		tm.tm_hour>>=4;
		tm.tm_hour*=10;
		tm.tm_hour+=t2[30]&0xF;

		tm.tm_min=t2[31]&0xF0;
		tm.tm_min>>=4;
		tm.tm_min*=10;
		tm.tm_min+=t2[31]&0xF;

		tm.tm_sec=t2[32]&0xF0;
		tm.tm_sec>>=4;
		tm.tm_sec*=10;
		tm.tm_sec+=t2[32]&0xF;

		tm.tm_isdst=-3;
		_globalTime=mktime(&tm);
		set_globalTime_variable(_globalTime);

		printf("NTCIP1201-2008::globalTime.0 = Counter32: %ld \n", _globalTime);		
	}

/*	
	printf("Hora hexadecimal en decimal \n");
	printf("%i \n", tm.tm_year);
	printf("%i \n", tm.tm_mon);
	printf("%i \n", tm.tm_mday);
	printf("%i \n", tm.tm_hour);
	printf("%i \n", tm.tm_min);
	printf("%i \n", tm.tm_sec);
	printf("%ld \n", globalTime);
*/
	
	//programa de tiempo utilizado, actualiza el corrdOperationalMode
	cambiando_plan=t2[15]&16;
	plan_ntcip=get_coord_variable(1);

	if(cambiando_plan==0 && plan_ntcip!=t2[35])
	{
		set_coord_variable(1,t2[35]);
	}

	printf("NTCIP1202-2000::coordOperationalMode.0 = INTEGER: %i \n", plan_ntcip);

	//Byte de alarmas:

	//entry7[0]->alarmGroupState=t2[17];
	printf("\n");
	printf("Alarmas:\n");
	p_alarmGroup[0]->alarmGroupState=t2[17];
	if(p_alarmGroup[0]->alarmGroupState > 0)
	{
		dec_to_bin(p_alarmGroup[0]->alarmGroupState,binario);
		for(int k=0;k<8;k++)
		{
			if(binario[k] == 1)
			{
				if(k == 1)	printf("Falla de tiempo suplementario\n");
				else if (k == 2)	printf("Falla de conflicto\n");
				else if (k == 3)	printf("Falla de Baja Tension\n");
				else if (k == 4)	printf("Puerta Abierta\n");
				else if (k == 5)	printf("Falla de GPS\n");
				else if (k == 6)	printf("Falla de Falta de Lampara:\n");
				else if (k == 7)	printf("Funcionamiento Modo Manual:\n");
			}
		}
	}

	for(i=1;i<18;i++)
	{ 
		p_alarmGroup[i]->alarmGroupState=t2[i+56];		
		if(p_alarmGroup[i]->alarmGroupState > 0)
		{
			if(i == 1)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Rojo: %i\n",k+1);
					}
				}
			}
			else if(i == 2)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Rojo: %i\n",k+9);
					}
				}
			}
			else if(i == 3)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Amarillo: %i\n",k+1);
					}
				}
			}
			else if(i == 4)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Amarillo: %i\n",k+9);
					}
				}
			}
			else if(i == 5)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Verde: %i\n",k+1);
					}
				}
			}
			else if(i == 6)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Verde: %i\n",k+9);
					}
				}
			}
			else if(i == 7)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Conflicto de Rojo: %i\n",k+1);
					}
				}
			}
			else if(i == 8)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Conflicto de Rojo: %i\n",k+9);
					}
				}
			}
			else if(i == 9)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Conflicto de Amarillo: %i\n",k+1);
					}
				}
			}
			else if(i == 10)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Conflicto de Amarillo: %i\n",k+9);
					}
				}
			}
			else if(i == 11)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Conflicto de Verde: %i\n",k+1);
					}
				}
			}
			else if(i == 12)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Conflicto de Verde: %i\n",k+9);
					}
				}
			}
			else if(i == 13)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Fusible Rojo: %i\n",k+1);
					}
				}
			}
			else if(i == 14)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Fusible Rojo: %i\n",k+9);
					}
				}
			}
			else if(i == 15)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Fusible Amarillo: %i\n",k+1);
					}
				}
			}
			else if(i == 16)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Fusible Amarillo: %i\n",k+9);
					}
				}
			}
			else if(i == 17)
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Fusible Verde: %i\n",k+1);
					}
				}
			}
			else 
			{
				dec_to_bin(p_alarmGroup[i]->alarmGroupState,binario);
				for(int k=0;k<8;k++)
				{
					if(binario[k] == 1)
					{
						printf("Falta de Fusible Verde: %i\n",k+9);
					}
				}
			}
		}	
	}
	printf("\n");


	//carga de los objetos propietarios del estado extendido 
	estadoEx=unir_byte_hex(t2[12],t2[13]);
	
	carga_estadoExtendido(estadoEx,1);
	
	carga_estadoExtendido(t2[15],2);
	carga_estadoExtendido(t2[16],3);
	carga_estadoExtendido(t2[18],4);
	carga_estadoExtendido(t2[34],5);
	carga_estadoExtendido(t2[35],6);
	carga_estadoExtendido(t2[37],7);
	carga_estadoExtendido(t2[38],8);
	carga_estadoExtendido(t2[42],9);
	estadoEx=unir_byte_hex(t2[45],t2[46]);
	carga_estadoExtendido(estadoEx,10);
	estadoEx=unir_byte_hex(t2[49],t2[50]);
	carga_estadoExtendido(estadoEx,11);
	carga_estadoExtendido(t2[14],12);
	estadoEx=unir_byte_hex(t2[47],t2[48]);
	carga_estadoExtendido(estadoEx,13);
	estadoEx=unir_byte_hex(t2[51],t2[52]);
	carga_estadoExtendido(estadoEx,14);
	estadoEx=unir_byte_hex(t2[43],t2[44]);
	carga_estadoExtendido(estadoEx,15);
	
	//temperaturas
	set_estadoControlador_variable(TEMPERATURADISPLAY, 	  t2[80]);
	set_estadoControlador_variable(TEMPERATURAMOTHER, 	  t2[81]);
	set_estadoControlador_variable(TEMPERATURAEXTENSION1, t2[82]);
	set_estadoControlador_variable(TEMPERATURAEXTENSION1, t2[83]);
	set_estadoControlador_variable(TEMPERATURAEXTENSION3, t2[84]);
	obtener_temperatura_raspberry();


	//detectores

	p_vehicle_status[0]->vehicleDetectorStatusGroupActive = t2[75];
	p_vehicle_status[1]->vehicleDetectorStatusGroupActive = t2[76];
	p_vehicle_status[2]->vehicleDetectorStatusGroupActive = t2[77];
	p_vehicle_status[3]->vehicleDetectorStatusGroupActive = t2[77];
}

void realizar_consulta_generica(void) {
	
	unsigned char trama_consulta[15];
	unsigned char trama_respuesta_consulta[100];
	
	int * _conexion = 0;
	int _num_cruce_obtenido = 0;
	int _num_grupo_obtenido = 0;
	
	int _num_cruce_separado[2];
	
	_conexion = get_conexion();
	
	prepara_trama_consulta_extendido(trama_consulta, 1);
	enviar_trama(_conexion,trama_consulta,trama_respuesta_consulta,15,100);
	
	_num_grupo_obtenido = trama_respuesta_consulta[7];
	
	//MSB del Numero de cruce del EC 
	_num_cruce_separado[0] = trama_respuesta_consulta[12];
	//LSB del Numero de cruce del EC
	_num_cruce_separado[1] = trama_respuesta_consulta[13];

	//uniendo los dos bytes en un solo entero
	_num_cruce_obtenido = unir_byte_hex(_num_cruce_separado[0], _num_cruce_separado[1]);
	
	set_variable(6, _num_cruce_obtenido);
	set_variable(7, _num_grupo_obtenido);
	
	//seteando variable de consulta generica
	set_variable(13, 1);

}
