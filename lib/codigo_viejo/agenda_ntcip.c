void prepara_trama_agenda_semanal_anual_ntcip(char * t, agenda_semanal_anual _agenda_semanal_anual_seleccionada[]) 
{
	int i=0, _num_cruce=0, _num_grupo=0, agendaDiaria=0, resul=0;
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x76;
	t[9]=0x00;			
	t[10]=0x83;
		
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	
	i=0;
	
	for(int k=0;k<12;k++) {
		
		t[i+14] = _agenda_semanal_anual_seleccionada[k].diaCambio;
		t[i+15] = _agenda_semanal_anual_seleccionada[k].mesCambio;
		i+=2;
	}
	
	for(int k=0;k<12;k++){
		t[k+38] = k;
	}
	
	i=0;
	
	for(int k=0;k<12;k++) {

		t[i+50] = _agenda_semanal_anual_seleccionada[k].semana[0];
		t[i+51] = _agenda_semanal_anual_seleccionada[k].semana[1];
		t[i+52] = _agenda_semanal_anual_seleccionada[k].semana[2];
		t[i+53] = _agenda_semanal_anual_seleccionada[k].semana[3];
		t[i+54] = _agenda_semanal_anual_seleccionada[k].semana[4];
		t[i+55] = _agenda_semanal_anual_seleccionada[k].semana[5];
		t[i+56] = _agenda_semanal_anual_seleccionada[k].semana[6];
		i+=7;                                          
	}

	t[134]=0;
	for(i=11;i<134;i++) t[134]^=t[i];
/*	
	printf("trama formada: \n ");
	for(i=0;i<135;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void imprimir_calendario(struct Calendar * _calendario, int _diasDelAgnio){
	
	char diaSemana[15] = "";
	char mes[15] = "";
	
	printf("\n");

	for(int i=0; i<_diasDelAgnio; i++) {
		if(_calendario[i].mes > 6) {
					
			printf("dia del mes: %i\n",_calendario[i].diaMes);
			
			indicarMes(_calendario[i].mes, mes);
			printf("Mes: %i (%s)\n",_calendario[i].mes, mes);
			
			indicarDiaSemana(_calendario[i].diaSemana, diaSemana);
			printf("dia de Semana: %i (%s)\n",_calendario[i].diaSemana, diaSemana);
			
			printf("plan diario: %i\n",_calendario[i].planDiario);
			printf("\n");

		}
	}
}

//para generar los tipo de semana del calendario

void agregar_semana(int _semanasTipoDelAgnio[], agenda_semanal_anual * _agenda_semanal_anual_seleccionada) {
	
	for(int i=0; i<7; i++) {
		//_semanasTipoSeleccionadas[i] = _semanasTipoDelAgnio[i];
		_agenda_semanal_anual_seleccionada->semana[i] = _semanasTipoDelAgnio[i];
	}
	_agenda_semanal_anual_seleccionada->seleccionado = 255;
}

void calcular_fecha_cambios(agenda_semanal_anual _agenda_semanal_anual_seleccionada[]) {

	int semanaSinConvertir = 0;
	int mes = 0;

	for(int i=0; i<12; i++) {
		if(_agenda_semanal_anual_seleccionada[i].seleccionado == 0xFF) {
			
			semanaSinConvertir = _agenda_semanal_anual_seleccionada[i].semanaInicio;
			mes = (semanaSinConvertir + 1)/4;
			_agenda_semanal_anual_seleccionada[i].mesCambio = mes + 1;
			_agenda_semanal_anual_seleccionada[i].diaCambio = 1;
		}
	}
}

void generar_tipo_semanas(struct Calendar * _calendario, int _diasDelAgnio, agenda_semanal_anual _agenda_semanal_anual_seleccionada[]) 
{
	int mesEnCurso = 0;
	int j=0;
	int contadorDiaSemana = 0; 
	int semanasTipoSeleccionadas[12][8];
	int semanasTipoDelAgnio[48][7];
	int semanasTipoDelAgnio2[64][7];
	int contadorMes = 0;
	int contadorSemana = 0;
	int valido = 0;
	int semanaTipoRepetida = 0;
	int semanasAgregadas = 0;
	
	//agenda_semanal_anual agenda_semanal_anual_seleccionada[12];
	
	char mes[15] = "";
	
	//limpiamos los arreglos de datos
	
	for(int k=0; k<12; k++) {
		for(int l=0; l<8; l++) {
			semanasTipoSeleccionadas[k][l] = 0;
			_agenda_semanal_anual_seleccionada[k].semana[l]=0;
		}
		_agenda_semanal_anual_seleccionada[k].diaCambio = 0;
		_agenda_semanal_anual_seleccionada[k].mesCambio = 0;
		_agenda_semanal_anual_seleccionada[k].semanaInicio = 0;
		_agenda_semanal_anual_seleccionada[k].semanaFin = 0;
		_agenda_semanal_anual_seleccionada[k].seleccionado = 0;

	}
	
	for(int k=0; k<48; k++) {
		for(int l=0; l<7; l++) {
			semanasTipoDelAgnio[k][l] = 0;
		}
	}
	
	for(int k=0; k<64; k++) {
		for(int l=0; l<7; l++) {
			semanasTipoDelAgnio2[k][l] = -1;
		}
	}

	//recorremos mes por mes el objeto calendario, para copiar los tipo de semana
/*	for(int i=0; i<_diasDelAgnio; i++) {
		
		if(_calendario[i].mes == mesEnCurso) {

			if(contadorDiaSemana <= 6) {
				semanasTipoDelAgnio[j][_calendario[i].diaSemana] = _calendario[i].planDiario;
				
				/*if(_calendario[i].mes <= 2 ) {
					printf("diaDelMes: %i,diaSemana: %i, planDiario: %i, contadorDiaSemana: %i\n",
					_calendario[i].diaMes,_calendario[i].diaSemana, 
					_calendario[i].planDiario, contadorDiaSemana);
				}*/
/*				contadorDiaSemana++;

			}
			else {
				contadorDiaSemana = 0;
				j++;
				i--;
			}
			
			if(_calendario[i].diaMes == 28) {
				mesEnCurso++;
				contadorDiaSemana = 0;
				j++;
			}
		}
	}
*/

	for(int i=0; i<_diasDelAgnio; i++) {
		
		mesEnCurso = _calendario[i].mes;

		semanasTipoDelAgnio2[j][_calendario[i].diaSemana] = _calendario[i].planDiario;
							
		if(_calendario[i].diaSemana == 6 || _calendario[i+1].mes != mesEnCurso) {
			j++;
		}
	}

/*	
	printf("semana tipo del año:\n");
	for(int k=0; k<52; k++) {
		
		indicarMes(contadorMes, mes);
		printf("mes %i %s:", contadorMes, mes);
		printf("semana %i : ", k);
		
		for(int l=0; l<7; l++) {
			printf("%i ", semanasTipoDelAgnio2[k][l]);
		}
		printf("\n");
		
		//si la semana es multiplo de 4 y no es 0, se aumenta el mes en curso
		if((k+1)%4 == 0 && k!=0) contadorMes++;

	}
*/
	for(int k=0; k<64; k++) {
		
		printf("semana %i : ", k);
		
		for(int l=0; l<7; l++) {
			
			if(semanasTipoDelAgnio2[k][l] != -1) {
				printf("\t%i ", semanasTipoDelAgnio2[k][l]);
			} else {
				printf("\t");
			} 
		}
		printf("\n");
	}
	//determinar semanas tipo que estan al menos 1 vez
/*	
	for(int k=0; k<48; k++) {
		//menor a 47 para que no desborde el arreglo al consultar el siguiente
		if(k < 47) {
			for(int l=0; l<7; l++) {
				//si la semana tiene los mismos planes que la siguiente semana
				if(semanasTipoDelAgnio[k][l] == semanasTipoDelAgnio[k+1][l]) {
					valido++;
				}
			}
			if(valido == 7) {
				semanaTipoRepetida++;
			}
			else if(semanaTipoRepetida > 0 && semanasAgregadas <= 11) {
				agregar_semana(semanasTipoDelAgnio[k], 
				&_agenda_semanal_anual_seleccionada[semanasAgregadas]
				//semanasTipoSeleccionadas[semanasAgregadas]
				);
				_agenda_semanal_anual_seleccionada[semanasAgregadas].semanaFin = k;
				
				_agenda_semanal_anual_seleccionada[semanasAgregadas].semanaInicio = k - semanaTipoRepetida;
				
				semanasAgregadas++;
				semanaTipoRepetida = 0;
			}
			valido = 0;	
		}
		else if(k == 47 && semanaTipoRepetida > 0 && semanasAgregadas <= 11) {
			agregar_semana(semanasTipoDelAgnio[k],
			&_agenda_semanal_anual_seleccionada[semanasAgregadas] 
			//semanasTipoSeleccionadas[semanasAgregadas]
			);
			
			_agenda_semanal_anual_seleccionada[semanasAgregadas].semanaFin = k;	
			_agenda_semanal_anual_seleccionada[semanasAgregadas].semanaInicio = k - semanaTipoRepetida;
		}
	}
*/

	

	
	//calcular diaCambio y mesCambio
/*	calcular_fecha_cambios(_agenda_semanal_anual_seleccionada);
	
	//imprimir tipo de semanas filtradas
	printf("semana tipo del año:\n");
	for(int k=0; k<12; k++) {
		
		printf("semana %i : ", k);
		
		for(int l=0; l<7; l++) {
			printf("%i ", 
			_agenda_semanal_anual_seleccionada[k].semana[l]
			//semanasTipoSeleccionadas[k][l]
			);
		}
		printf("%i inicio: %i, fin: %i, diaCambio: %i, mesCambio: %i\n", 
		_agenda_semanal_anual_seleccionada[k].seleccionado,
		_agenda_semanal_anual_seleccionada[k].semanaInicio,
		_agenda_semanal_anual_seleccionada[k].semanaFin,
		_agenda_semanal_anual_seleccionada[k].diaCambio,
		_agenda_semanal_anual_seleccionada[k].mesCambio
		);

	}
	*/
}

//generacion de tramas citar con la info procesada en la estructura agenda_semanal_anual
void generar_tramas_agendas_ntcip(agenda_semanal_anual _agenda_semanal_anual_seleccionada[], agenda_feriados _agenda_feriados_procesada[]) {
	
	//trama de envio de agendas
	char trama_agenda_diaria[56];
	char trama_agenda_semanal_anual[135];
	char trama_agenda_feriados_especiales[159];
	
	//trama de respuesta de agendas
	char trama_respuesta_agendas[20];
	
	memset(trama_agenda_semanal_anual, 		 0, sizeof(trama_agenda_semanal_anual));
	memset(trama_agenda_feriados_especiales, 0, sizeof(trama_agenda_feriados_especiales));

	int * _conexion = 0;
	int _delay_programacion = 0;
	
	//obteniendo socket de conexion y retardo de programacion
	_conexion = get_conexion();
	_delay_programacion = get_variable(8);
	
	//trama de agendas diarias
	for(int i=0;i<12;i++) {
		
		printf("Trama de agenda diarias %i: \n",i);
		prepara_trama_agenda_diaria_ntcip(trama_agenda_diaria,i);
		enviar_trama(_conexion,trama_agenda_diaria, trama_respuesta_agendas, 56, 20);
		sleep(_delay_programacion);
	}

	//trama de agenda semanal anual
	printf("Trama de agenda anual y semanal: \n");
	prepara_trama_agenda_semanal_anual_ntcip(trama_agenda_semanal_anual, _agenda_semanal_anual_seleccionada);
	
	enviar_trama(_conexion,trama_agenda_semanal_anual, trama_respuesta_agendas,135,20);
	sleep(_delay_programacion);
	
	//trama de agenda de feriados y especiales
	prepara_trama_agenda_feriados_especiales_ntcip(trama_agenda_feriados_especiales, _agenda_feriados_procesada);
	enviar_trama(_conexion,trama_agenda_feriados_especiales, trama_respuesta_agendas,159,20);
	sleep(_delay_programacion);
}
