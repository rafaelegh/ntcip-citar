#ifndef AGENDAS_NTCIP_H
#define AGENDAS_NTCIP_H

//structs
	struct holidays_ntcip {
		int mes;
		int semana[7];
		int diaMes[2];
		int planDiario;
		int conteoBits;
	}typedef feriados_ntcip;
	
	struct agenda_ntcip_prototipo {
		int mes;
		int diaInicio;
		int diaFin;
		int semana[7];
		int planDiario;
	}typedef agenda_ntcip_prototipo;
	
	struct agenda_semanal_anual {
		int semana[7];
		int diaCambio;
		int mesCambio;
		int semanaInicio;
		int semanaFin;
		int seleccionado;
	} typedef agenda_semanal_anual;
	
	struct agenda_feriados {
		int diaCambio;
		int mesCambio;
		int planDiarioCambio;
	} typedef agenda_feriados;
	
	struct agenda_especiales {
		int diaCambio;
		int mesCambio;
		int planDiarioCambio;
	} typedef agenda_especiales;

	void prepara_trama_agenda_diaria_ntcip(char *, int);
	void prepara_trama_agenda_semanal_anual_ntcip(char * , agenda_semanal_anual []);
	void conversion_schedule_ntcip_citar(void);

#endif
