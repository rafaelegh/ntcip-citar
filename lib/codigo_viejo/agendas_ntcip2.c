#include "bytes.h"
#include "asc.h"
#include "citar.h"
#include "agendas_ntcip.h"
#include "calendario.h"

void prepara_trama_agenda_diaria_ntcip(char * t, int n) {
	
	int i=0, k=0, _num_cruce=0, _num_grupo=0;
	int numero_accion_asc = 0;
	int asc_pattern = 0;
	int num_cruce_separado[2];
	
	struct timeBaseDayPlanTable_entry dayPlan_Table[12][10];
	struct timebaseAscActionTable_entry ** p_timeBaseAscAction = NULL;
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);
	
	//obteniendo tabla dayPlan
	get_dayplan_table(dayPlan_Table);
	
	//obteniendo tabla timeBaseAscAction
	p_timeBaseAscAction = get_timebaseAsc_table();
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x78;
	t[9]=0x00;			
	t[10]=0x34;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	t[14]=n;
	i=0;
	for(k=0;k<10;k++)
	{
		t[i+15] = dec_to_BCD_hex(dayPlan_Table[n][k].dayPlanHour);
		t[i+16] = dec_to_BCD_hex(dayPlan_Table[n][k].dayPlanMinute);
		
		numero_accion_asc = dec_to_hex(dayPlan_Table[n][k].dayPlanActionNumberOID[14]) - 1;
		
		asc_pattern = p_timeBaseAscAction[numero_accion_asc]->timebaseAscPattern;
		
		t[i+17] = asc_pattern;
/*		
		printf("OID del dayPlan Action:\n");
		for(int l=0; l<15; l++) {
			printf("%i ",dayPlan_Table[n][k].dayPlanActionNumberOID[l]);
		}
		printf("\n\n");
*/
		t[i+18] = 0;      
		i+=4;
	}

	t[55]=0;
	for(i=11;i<55;i++) t[55]^=t[i];
}

int obtener_seleccionado(int * arreglo, int n) {
	
	int seleccionado = 0;
	
	//buscando bit en 1 y guardandolo en seleccionado
	for(int i=0;i < n; i++){
		if( arreglo[i] == 1) {
			seleccionado = i + 1;
			break;
		}
	}
	
	//printf("n: %i, seleccionado: %i\n",n,seleccionado);
	
	//si no encontro algun mes o dia seleccionado returna anulado (0x7FFF)
	if(seleccionado == 0 && n == 32) {
		seleccionado = 0x7F;
	} 
	else if (seleccionado == 0 && n == 13 ) {
		seleccionado = 0xFF;
	}
	//printf("n: %i, seleccionado: %i\n",n,seleccionado);
	
	return seleccionado;
}


void prepara_trama_agenda_semanal_anual_ntcip2(char * t, agenda_ntcip_prototipo _agendaNTCIP4[]) 
{
	int i=0, _num_cruce=0, _num_grupo=0, agendaDiaria=0, resul=0;
	int num_cruce_separado[2];
	
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);
	
	separar_byte_hex(num_cruce_separado,_num_cruce);
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x76;
	t[9]=0x00;			
	t[10]=0x83;
		
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	
	i=0;
	
	for(int k=0;k<12;k++) {
		
/*		if(k==7) {
			printf("dia y mes jodiendo: %i %i\n", _agendaNTCIP4[k].diaInicio, _agendaNTCIP4[k].mes);
		}*/
		t[i+14] = dec_to_BCD_hex(_agendaNTCIP4[k].mes);
		t[i+15] = dec_to_BCD_hex(_agendaNTCIP4[k].diaInicio);
		i+=2;
	}
	
	for(int k=0;k<12;k++){
		t[k+38] = k;
	}
	
	i=0;
	
	for(int k=0;k<12;k++) {

		t[i+50] = _agendaNTCIP4[k].semana[0];
		t[i+51] = _agendaNTCIP4[k].semana[1];
		t[i+52] = _agendaNTCIP4[k].semana[2];
		t[i+53] = _agendaNTCIP4[k].semana[3];
		t[i+54] = _agendaNTCIP4[k].semana[4];
		t[i+55] = _agendaNTCIP4[k].semana[5];
		t[i+56] = _agendaNTCIP4[k].semana[6];
		i+=7;                                          
	}

	t[134]=0;
	for(i=11;i<134;i++) t[134]^=t[i];
/*	
	printf("trama formada: \n ");
	for(i=0;i<135;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}

void prepara_trama_agenda_feriados_especiales_ntcip(char * t, agenda_feriados _agenda_feriados_procesada[]) {
	
	int i=0, k=0,_num_cruce=0, _num_grupo=0;	
	int num_cruce_separado[2];
	 
	_num_cruce = get_variable(6);
	_num_grupo = get_variable(7);	
	
	separar_byte_hex(num_cruce_separado,_num_cruce);
	
	t[0]=0x00;
	t[1]=0x00;
	t[2]=0x00;
	t[3]=0x00;
	t[4]=0xFF;
	t[5]=0x00;
	t[6]=0x00;
	t[7]=_num_grupo;
	t[8]=0x74;
	t[9]=0x00;			
	t[10]=0x9B;	
	t[11]=0;
	for(i=0;i<11;i++) t[11]^=t[i];
	t[12]=num_cruce_separado[0];
	t[13]=num_cruce_separado[1];
	i=0;
	
	// dia y mes de agenda especial en 1 
	
	for(k=0; k<16; k++) {
		
		t[i+14]=0;
		t[i+15]=0;
		i+=2;
	}
		
	i=0;
	
	//dia y mes de agenda feriados en 1 
	
	for(k=0;k<32;k++) {
		
		if(k<16) {
				
			t[i+46] = dec_to_BCD_hex(_agenda_feriados_procesada[k].mesCambio);
			t[i+47] = dec_to_BCD_hex(_agenda_feriados_procesada[k].diaCambio);
			i+=2;
		} 
		else {
			t[i+46] = 0;
			t[i+47] = 0;
			i+=2;
		}
	}
	
	//Cambio de agenda diaria para los especiales
	for(k=0;k<16;k++){
		t[k+110] = 0;
	}

	//Cambio de agenda diaria para los feriados
	for(k=0;k<32;k++) {
		
		if(k<16) {
			t[k+126] = _agenda_feriados_procesada[k].planDiarioCambio;
		} 
		else t[k+126] = 0;
	}

	t[158]=0;
	for(i=11;i<158;i++) t[158]^=t[i];
/*	printf("trama formada \n ");
	for(i=0;i<159;i++) printf("%X ", t[i]);
	printf("\n ");
*/
}




//----------------------------------------------------------------------
//ETAPA 1
//filtrar feriados y especiales de los objetos ntcip

void check_set_array(int array_in[], feriados_ntcip * _feriado_auxiliar, int array_length ) {			
	
	int counter = 0, j = 0;
	
	_feriado_auxiliar->conteoBits = 0;
	
	for(int i=0;i < array_length; i++) {
		if( array_in[i] == 1) {
			_feriado_auxiliar->conteoBits++;
		}
	}
	
	switch(array_length) {
		case 16:
				
		//printf("counter bit: %i\n",_feriado_auxiliar->conteoBits);	
		if(_feriado_auxiliar->conteoBits == 1) {
			
			for(int i=0;i < array_length; i++) {
				if( array_in[i] == 1) {
					_feriado_auxiliar->mes = i;
				}
			}
			
			//printf("mes seleccionado: %i\n\n",_feriado_auxiliar->mes);
		}
		break;
				
		case 8:
		
		//printf("counter bit: %i\n",_feriado_auxiliar->conteoBits);			
		if(_feriado_auxiliar->conteoBits < 8 && _feriado_auxiliar->conteoBits >= 1) {
			
			for(int i=0;i < array_length; i++) {
				if( array_in[i] == 1) {
					_feriado_auxiliar->semana[counter] = i;
					//printf("dias de la semana seleccionados: %i\n\n",_feriado_auxiliar->semana[counter]);
					counter++;
				}
			}
		}				
				
		break;
				
		case 32:
		
		//printf("counter bit: %i\n",_feriado_auxiliar->conteoBits);			
		if(_feriado_auxiliar->conteoBits <= 2) {
			
			for(int i=0;i < array_length; i++) {
				if( array_in[i] == 1 && counter <= 2) {
					_feriado_auxiliar->diaMes[counter] = i + 1;
					//printf("dia %i del mes seleccionados: %i\n\n",counter+1,_feriado_auxiliar->diaMes[counter]);
					counter++;
				}
			}
		}
		break;
	}
}

void filtrar_feriados(feriados_ntcip _feriados_filtrados[], int _agendas_feriados_usados[]) {
	
	int maxTimeBaseSchedule = 0;
	int feriadoContador = 0;
	int valorDia = 0;
	unsigned int mesValor = 0;
	unsigned long diaMesValor = 0;
	
	feriados_ntcip feriadoAuxiliar;
	
	struct timeBaseScheduleTable_entry ** p_timeBaseS = NULL;
	
	int arreglo_mes[16];
	int arreglo_semana[8];
	int arreglo_diaMes[32];
	
	memset(arreglo_mes, 0, sizeof(arreglo_mes));
	memset(arreglo_semana, 0, sizeof(arreglo_semana));
	memset(arreglo_diaMes, 0, sizeof(arreglo_diaMes));
	
	memset(feriadoAuxiliar.semana , 0, sizeof(feriadoAuxiliar.semana));
	memset(feriadoAuxiliar.diaMes , 0, sizeof(feriadoAuxiliar.semana));
	
	feriadoAuxiliar.conteoBits = 0;
	feriadoAuxiliar.mes = 0;
	feriadoAuxiliar.planDiario = 0;

	//get max entries number for time base schedule table
	maxTimeBaseSchedule = get_globalTimeManagement_variable(1);
	
	//obteniendo puntero de tabla de agenda de fechas
	p_timeBaseS = get_schedule_table();
	
	//loop every row in the table
	for(int i=0; i<maxTimeBaseSchedule; i++) {
		
		//procesando mes del año
		mesValor = p_timeBaseS[i]->timeBaseScheduleMonth;
		
		dec_to_bin_n(mesValor, arreglo_mes,16);
		check_set_array(arreglo_mes, &feriadoAuxiliar,16);
		if( feriadoAuxiliar.conteoBits == 1 ) {
			
			//processing days of the week
			valorDia = p_timeBaseS[i]->timeBaseScheduleDay;
			dec_to_bin_n(valorDia, arreglo_semana,8);
			check_set_array(arreglo_semana, &feriadoAuxiliar,8);

			if( feriadoAuxiliar.conteoBits >= 1) {
				
				//processing day of the month
				diaMesValor = p_timeBaseS[i]->timeBaseScheduleDate;
				dec_to_bin_n(diaMesValor, arreglo_diaMes,32);
				check_set_array(arreglo_diaMes, &feriadoAuxiliar,32);
				
				if( feriadoAuxiliar.conteoBits == 1 || feriadoAuxiliar.conteoBits == 2) {
				
					printf("Entrada de feriado: %i\n", i);
					printf("mes en decimal: %li\n", mesValor);
					printf("dias de la semana en decimal: %li\n", valorDia);
					printf("dias del mes en decimal: %li\n", diaMesValor);
				
					if(feriadoContador < 16) {
						
						_feriados_filtrados[feriadoContador].mes = feriadoAuxiliar.mes;
						feriadoAuxiliar.mes = 0;
						
						for(int k=0; k<7; k++) {	
							_feriados_filtrados[feriadoContador].semana[k] = feriadoAuxiliar.semana[k];
							feriadoAuxiliar.semana[k] = 0;
						}
						for(int k=0; k<2; k++) {	
							_feriados_filtrados[feriadoContador].diaMes[k] = feriadoAuxiliar.diaMes[k];
							feriadoAuxiliar.diaMes[k] = 0;
						}
						
						_feriados_filtrados[feriadoContador].planDiario = p_timeBaseS[i]->timeBaseScheduleDayPlan;
						
						_agendas_feriados_usados[feriadoContador] = i;
						
						feriadoContador++;

					}
					printf("\n");

				}
			}
		
		}
	}
	
}

void procesar_feriados_filtrados(agenda_feriados _agenda_feriados_procesada[], feriados_ntcip _feriados_filtrados[]) {
	
	int j = 0;
	
	for(int i=0; i<16; i++) {
		
		if(_feriados_filtrados[i].diaMes[0] > 0) {
			
			_agenda_feriados_procesada[j].mesCambio = _feriados_filtrados[i].mes;
			_agenda_feriados_procesada[j].diaCambio = _feriados_filtrados[i].diaMes[0];
			_agenda_feriados_procesada[j].planDiarioCambio =  _feriados_filtrados[i].planDiario;
			
			if(_feriados_filtrados[i].diaMes[0] > 0 && _feriados_filtrados[i].diaMes[1] > 0) {
				
				_agenda_feriados_procesada[j+1].mesCambio = _feriados_filtrados[i].mes;
				_agenda_feriados_procesada[j+1].diaCambio = _feriados_filtrados[i].diaMes[1];
				_agenda_feriados_procesada[j+1].planDiarioCambio = _feriados_filtrados[i].planDiario;
				
				j++;
			}
			j++;
		}
	}
}

//----------------------------------------------------------------------

//ETAPA 2, 
//llenar el calendario con la info de los objetos ntcip

//----------------------------------------------------------------------

//ETAPA 3 




void generar_tramas_agendas_ntcip2(agenda_ntcip_prototipo _agendaNTCIP4[], agenda_feriados _agenda_feriados_procesada[]) {
	
	//trama de envio de agendas
	char trama_agenda_diaria[56];
	char trama_agenda_semanal_anual[135];
	char trama_agenda_feriados_especiales[159];
	
	//trama de respuesta de agendas
	char trama_respuesta_agendas[20];
	
	memset(trama_agenda_semanal_anual, 		 0, sizeof(trama_agenda_semanal_anual));
	memset(trama_agenda_feriados_especiales, 0, sizeof(trama_agenda_feriados_especiales));

	int * _conexion = 0;
	int _delay_programacion = 0;
	
	//obteniendo socket de conexion y retardo de programacion
	_conexion = get_conexion();
	_delay_programacion = get_variable(8);
	
	//trama de agendas diarias
	for(int i=0;i<12;i++) {
		
		printf("Trama de agenda diarias %i: \n",i);
		prepara_trama_agenda_diaria_ntcip(trama_agenda_diaria,i);
		enviar_trama(_conexion,trama_agenda_diaria, trama_respuesta_agendas, 56, 20);
		sleep(_delay_programacion);
	}

	//trama de agenda semanal anual
	printf("Trama de agenda anual y semanal: \n");
	prepara_trama_agenda_semanal_anual_ntcip2(trama_agenda_semanal_anual, _agendaNTCIP4);
	
	enviar_trama(_conexion,trama_agenda_semanal_anual, trama_respuesta_agendas,135,20);
	sleep(_delay_programacion);
	
	//trama de agenda de feriados y especiales
	printf("Trama de agenda feriados y especiales: \n");
	prepara_trama_agenda_feriados_especiales_ntcip(trama_agenda_feriados_especiales, _agenda_feriados_procesada);
	enviar_trama(_conexion,trama_agenda_feriados_especiales, trama_respuesta_agendas,159,20);
	sleep(_delay_programacion);
	
}

//llenando agenda ntcip prototipo

void llenar_agenda_ntcip_prototipo(agenda_ntcip_prototipo _agendaNTCIPProto[], int n, int arreglo[], int tamano) {
	
	int banderaInicio 	= 0;
	int banderaFin 		= 0;
	
	switch(tamano) {
		default:
		break;
		
		case 8:
		for(int i=0; i<8; i++) {
			_agendaNTCIPProto[n].semana[i] = arreglo[i + 1];
		}
		break;
		
		case 16:
		for(int i=0; i<tamano; i++) {
			if(arreglo[i] == 1) {
				_agendaNTCIPProto[n].mes = i;
				break;
			}
		}
		break;
		
		case 32:
		for(int i=0; i<tamano; i++) {
			if(arreglo[i] == 1) {
				
				if(banderaInicio == 0) {
					_agendaNTCIPProto[n].diaInicio = i + 1;
					banderaInicio = 1;
				}
			} else {
				if(banderaInicio == 1 && banderaFin == 0 && arreglo[i] == 0) {
					_agendaNTCIPProto[n].diaFin = i;
					banderaFin = 1;
				}
			}
		}
		
		if(banderaInicio == 1 && banderaFin == 0) {
			_agendaNTCIPProto[n].diaFin = tamano + 1;
		}
		
		break;
	}
}

void llenar_agenda_ntcip_prototipo_fase2(agenda_ntcip_prototipo _agendaNTCIP2[], agenda_ntcip_prototipo _agendaNTCIPProto[] , int n) {
	
	int planDiario = 0;
	
	for(int i=0; i<n; i++) {
		for(int j=0; j<7; j++) {
				
			if (_agendaNTCIPProto[i].semana[j] == 1) {
				
				_agendaNTCIP2[i].semana[j] = _agendaNTCIPProto[i].planDiario;
			} 
		}
		
		_agendaNTCIP2[i].mes = _agendaNTCIPProto[i].mes;
		_agendaNTCIP2[i].diaInicio = _agendaNTCIPProto[i].diaInicio;
		_agendaNTCIP2[i].diaFin = _agendaNTCIPProto[i].diaFin;
		_agendaNTCIP2[i].planDiario = _agendaNTCIPProto[i].planDiario;

	}
	
}

void llenar_agenda_ntcip_prototipo_fase3(agenda_ntcip_prototipo _agendaNTCIP3[], agenda_ntcip_prototipo _agendaNTCIP2[] , int n) {
	
	int planDiario = 0;
	int l=0; 
	int m=0;
	int eliminados[50];
	int agregar = 0;
	int eliminado = 0;
	int semanaUnida[7];
	
	memset(eliminados, -1, sizeof(eliminados));
	memset(semanaUnida, 0, sizeof(semanaUnida));

	
	for(int i=0; i<n; i++) {
				
		if(_agendaNTCIP2[i].mes > 0 && 
		   _agendaNTCIP2[i].diaInicio > 0 && 
		   _agendaNTCIP2[i].diaFin > 0) {
			   
		for(int x=0; x<n; x++) {
					
			if(i == eliminados[x]) {
				eliminado = 1;
				break;
			}
		}
			   
		if(eliminado == 0) {
			
			for(int k=0; k<7; k++) {
				semanaUnida[k] = _agendaNTCIP2[i].semana[k];
			}
		
			for(int j=i+1; j<n; j++) {
				
				for(int x=0; x<n; x++) {
					
					if(j == eliminados[x]) {
						eliminado = 1;
						break;
					}
				}
				
				if(_agendaNTCIP2[i].mes 	  == _agendaNTCIP2[j].mes && 
				   _agendaNTCIP2[i].diaInicio == _agendaNTCIP2[j].diaInicio &&
				   _agendaNTCIP2[i].diaFin 	  == _agendaNTCIP2[j].diaFin &&
				   eliminado == 0) {
				   
				   //printf("id: %i, id2: %i\n\n", i, j);
				   
				   for(int k=0; k<7; k++) {
	/*				
						if(_agendaNTCIP2[i].semana[k] > 0 && 
						   _agendaNTCIP2[j].semana[k] == 0) {
							
							//_agendaNTCIP3[l].semana[k] = _agendaNTCIP2[i].semana[k];
							semanaUnida[k] = _agendaNTCIP2[i].semana[k];
						} 
						else if(_agendaNTCIP2[i].semana[k] == 0 && 
								_agendaNTCIP2[j].semana[k] > 0) {
							
							//_agendaNTCIP3[l].semana[k] = _agendaNTCIP2[j].semana[k];
							semanaUnida[k] = _agendaNTCIP2[j].semana[k];

						}
						else if(_agendaNTCIP2[i].semana[k] == 0 && 
								_agendaNTCIP2[j].semana[k] == 0) {
							
							//_agendaNTCIP3[l].semana[k] = 0;
							semanaUnida[k] = 0;
						}*/
						
						if(semanaUnida[k] == 0 && 
							_agendaNTCIP2[j].semana[k] > 0) {
							
							//_agendaNTCIP3[l].semana[k] = _agendaNTCIP2[j].semana[k];
							semanaUnida[k] = _agendaNTCIP2[j].semana[k];

						}
						
					}
/*					
					if(i == 20) {
						printf("SemanaUnida: ");
						for(int k=0; k<7; k++) {
							printf("%i", semanaUnida[k]);
						}
						printf("\n\n");
					}
					/*
					_agendaNTCIP3[l].mes  		= _agendaNTCIP2[i].mes;
					_agendaNTCIP3[l].diaInicio  = _agendaNTCIP2[i].diaInicio;
					_agendaNTCIP3[l].diaFin 	= _agendaNTCIP2[i].diaFin;
					_agendaNTCIP3[l].planDiario = _agendaNTCIP2[i].planDiario;
					
					*/
					agregar = 1;

					//incrementando id de las agendas seleccionadas
					//l++;
					
					//agregando id de entrada repetida
					//eliminados[m] = i;
					//m++;
					eliminados[m] = j;
					m++;
					
					eliminado = 0;
					
				}
				
			}		
			
			//if(agregado == 0) {
				_agendaNTCIP3[l].mes  		= _agendaNTCIP2[i].mes;
				_agendaNTCIP3[l].diaInicio  = _agendaNTCIP2[i].diaInicio;
				_agendaNTCIP3[l].diaFin 	= _agendaNTCIP2[i].diaFin;
				_agendaNTCIP3[l].planDiario = _agendaNTCIP2[i].planDiario;
				
				if(agregar == 1) {
					for(int k=0; k<7; k++) {
						//_agendaNTCIP3[l].semana[k] = _agendaNTCIP2[i].semana[k];
						_agendaNTCIP3[l].semana[k] = semanaUnida[k];
					}
					agregar = 0;	

				} else {
					for(int k=0; k<7; k++) {
						_agendaNTCIP3[l].semana[k] = _agendaNTCIP2[i].semana[k];
					}
				}
					
				//incrementando id de las agendas seleccionadas
				l++;
				
				eliminados[m] = i;
				m++;
			//}
			
			//agregado = 0;
			
				memset(semanaUnida, 0, sizeof(semanaUnida));

		}
		
		eliminado = 0;

		}
	}	
}

void llenar_agenda_ntcip_prototipo_fase4(agenda_ntcip_prototipo _agendaNTCIP4[], agenda_ntcip_prototipo _agendaNTCIP3[] , int n) {

	int validarSemana = 0;
	int l=0;
	int eliminados[50];
	int m=0;
	int eliminado = 0;
	int agregado = 0; 

	memset(eliminados, -1, sizeof(eliminados));

	for(int i=0; i<n; i++) {
		
		if(_agendaNTCIP3[i].mes > 0 && 
		   _agendaNTCIP3[i].diaInicio > 0) {
		
			for(int x=0; x<50; x++) {
						
				if(i == eliminados[x]) {
					eliminado = 1;
					break;
				}
			}
				
			if(eliminado == 0) {
			
				for(int j=i+1; j<n; j++) {
					
					//comparando semanas iguales
					for(int k=0; k<7; k++) {
						
						if(_agendaNTCIP3[j].mes > 0 && 
							_agendaNTCIP3[i].diaInicio > 0) {
								
							if(_agendaNTCIP3[i].semana[k] == _agendaNTCIP3[j].semana[k]) {
								validarSemana++;
							}
						}
					}
					
					//s
					if(validarSemana == 7) {
						
						if(_agendaNTCIP3[i].mes == 9) printf("mes 9 encontrado\n\n");
						
						if(_agendaNTCIP3[i].mes < _agendaNTCIP3[j].mes ) {
														
								_agendaNTCIP4[l].mes = _agendaNTCIP3[i].mes;
								_agendaNTCIP4[l].diaInicio = _agendaNTCIP3[i].diaInicio;
									
								for(int k=0; k<7; k++) {
						
									_agendaNTCIP4[l].semana[k] = _agendaNTCIP3[i].semana[k];
								}
										
								eliminados[m] = j;
								m++;
								agregado = 1;
						}
						else if(_agendaNTCIP3[i].mes == _agendaNTCIP3[j].mes ) {
								
							if(_agendaNTCIP3[i].diaInicio < _agendaNTCIP3[j].diaInicio) {
								
								_agendaNTCIP4[l].mes = _agendaNTCIP3[i].mes;
								_agendaNTCIP4[l].diaInicio = _agendaNTCIP3[i].diaInicio;
								
								for(int k=0; k<7; k++) {
						
									_agendaNTCIP4[l].semana[k] = _agendaNTCIP3[i].semana[k];
								}
								
								
								eliminados[m] = j;
								m++;
								agregado = 1;
							}
							else if(_agendaNTCIP3[i].diaInicio > _agendaNTCIP3[j].diaInicio) {
									
								_agendaNTCIP4[l].mes = _agendaNTCIP3[j].mes;
								_agendaNTCIP4[l].diaInicio = _agendaNTCIP3[j].diaInicio;
								
								for(int k=0; k<7; k++) {
						
									_agendaNTCIP4[l].semana[k] = _agendaNTCIP3[j].semana[k];
								}
								
								
								eliminados[m] = i;
								m++;
								agregado = 1;
							}	
						}
						else if(_agendaNTCIP3[i].mes > _agendaNTCIP3[j].mes) {
																						
							_agendaNTCIP4[l].mes 	   = _agendaNTCIP3[j].mes;
							_agendaNTCIP4[l].diaInicio = _agendaNTCIP3[j].diaInicio;
									
							for(int k=0; k<7; k++) {
						
								_agendaNTCIP4[l].semana[k] = _agendaNTCIP3[j].semana[k];
							}
									
							eliminados[m] = i;
							m++;
							agregado = 1;
						}
						
					}
					validarSemana = 0;
				}
				if(agregado == 0) {
					_agendaNTCIP4[l].mes = _agendaNTCIP3[i].mes;
					_agendaNTCIP4[l].diaInicio = _agendaNTCIP3[i].diaInicio;
					
					for(int k=0; k<7; k++) {
						
						_agendaNTCIP4[l].semana[k] = _agendaNTCIP3[i].semana[k];
					}
					
					eliminados[m] = i;
					m++;
				}
				l++;
			}
			eliminado = 0;
			agregado = 0;
		}
	}
}

//----------------------------------------------------------------------

//funcion principal que ejecuta las etapas 1, 2 y 3
void conversion_schedule_ntcip_citar(void) {
	
	int maxTimeBaseSchedule = 0;
	int j = 0;
	int contadorDias = 0, monthDays = 0;
	int diaSemana = 0, diaMes = 0;
	int diasDelAgnio = 0;
	int agendas_feriados_usados[32];
	int feriado_usado = 0;
	
	int valorSemana = 0;
	unsigned int valorMes = 0;
	unsigned long valorDiaMes = 0;
	
	int bandera = 0;
	
	// month 0 - 65,535
	// day of the week 0 - 255
	// day of the Month 0 - 2,147,483,648
	
	//arrays
	int arreglo_bisiesto[5];
	/*
	 arreglo_bisiesto[0] = dia actual
	 arreglo_bisiesto[1] = mes actual
	 arreglo_bisiesto[2] = anio actual
	 arreglo_bisiesto[3] = dias del mes de febrero de ese anio
	 arreglo_bisiesto[4] = dias del anio de ese anio 
	 */
	
	int array_month[16];
	int array_day[8];
	int array_date[32];

	struct timeBaseScheduleTable_entry ** p_timeBaseS = NULL;
	struct timeBaseDayPlanTable_entry dayPlanTable[12][10];

	//estructura para filtrar los feriados
	feriados_ntcip feriados_filtrados[16];
	
	//estructura para guardar los datos finales para agenda feriados y especiales
	agenda_especiales agenda_especiales_procesada[32];
	agenda_feriados agenda_feriados_procesada[16];
	
	//estructura para calendario
	struct Calendar * calendar = NULL;
	
	//estructura para guardar los datos finales para agenda semanal anual
	agenda_semanal_anual agenda_semanal_anual_seleccionada[12];
	
	//
	agenda_ntcip_prototipo agendaNTCIPProto[50];
	agenda_ntcip_prototipo agendaNTCIP2[50];
	agenda_ntcip_prototipo agendaNTCIP3[50];
	agenda_ntcip_prototipo agendaNTCIP4[50];

	
	p_timeBaseS = get_schedule_table();
	get_dayplan_table(dayPlanTable);

	//limpiando arreglos
	memset(arreglo_bisiesto, 0, sizeof(arreglo_bisiesto));
	memset(agendas_feriados_usados, -1, sizeof(agendas_feriados_usados));

	memset(array_month, 0, sizeof(array_month));
	memset(array_day, 0, sizeof(array_day));
	memset(array_date, 0, sizeof(array_date));
	
	for(int i=0;i<16;i++) {
		
		feriados_filtrados[i].mes = 0;
		memset(feriados_filtrados[i].semana , 0, sizeof(feriados_filtrados[i].semana));
		memset(feriados_filtrados[i].diaMes , 0, sizeof(feriados_filtrados[i].diaMes));
		
		agenda_feriados_procesada[i].diaCambio = 0;
		agenda_feriados_procesada[i].mesCambio = 0;
		agenda_feriados_procesada[i].planDiarioCambio = 0;
	}
	
	for(int i=0;i<50;i++) {
		
		agendaNTCIPProto[i].mes = 0;
		agendaNTCIPProto[i].diaInicio = 0;
		agendaNTCIPProto[i].diaFin = 0;
		
		agendaNTCIP2[i].mes = 0;
		agendaNTCIP2[i].diaInicio = 0;
		agendaNTCIP2[i].diaFin = 0;
		
		agendaNTCIP3[i].mes = 0;
		agendaNTCIP3[i].diaInicio = 0;
		agendaNTCIP3[i].diaFin = 0;
		
		agendaNTCIP4[i].mes = 0;
		agendaNTCIP4[i].diaInicio = 0;
		agendaNTCIP4[i].diaFin = 0;
		
		for(int z=0;z<7;z++){
			agendaNTCIPProto[i].semana[z] = 0;
			agendaNTCIP2[i].semana[z] = 0;
			agendaNTCIP3[i].semana[z] = 0;
			agendaNTCIP4[i].semana[z] = 0;
		}
	}

	filtrar_feriados(feriados_filtrados, agendas_feriados_usados);
	
	procesar_feriados_filtrados(agenda_feriados_procesada ,feriados_filtrados);
	
	printf("tabla de filtrados:\n");

	for(int i=0;i<5;i++) {
		
		printf("mes: ");
		printf("%i: ",feriados_filtrados[i].mes);
		
		printf("diasSemana: ");
		for(int k=0; k<7; k++) {
			printf("%i ",feriados_filtrados[i].semana[k]);	
		}
		
		printf("diasMes: ");
		for(int k=0; k<2; k++) {
			printf("%i ",feriados_filtrados[i].diaMes[k]);	
		}
		printf("\n");
	}
	
	//diasDelAgnio = bisiesto(arreglo_bisiesto);
	bisiesto(arreglo_bisiesto);
	
	diasDelAgnio = arreglo_bisiesto[4];
	
	calendar = malloc(diasDelAgnio * sizeof(struct Calendar));
	//printf("dias del anio: %i\n",arreglo_bisiesto[4]);
	
	//calendar[1].mes = 10;
	//printf("array 300 %i\n",calendar[1].mes);
	
	hacerCalendario(calendar, arreglo_bisiesto);
	
//	printf("dia del mes: %i\n",calendar[0].diaMes);
//	printf("Mes: %i\n",calendar[0].mes);
//	printf("dia de Semana: %i\n",calendar[0].diaSemana);

	//llenar el calendario
	//iterando la tabla de fechas
	for(int i=0; i<50; i++) {
		
		for(int k=0; k<32; k++) {
			
			if(agendas_feriados_usados[k] == i) {
				
				feriado_usado = 1;
				printf("brinca feriado: %i\n",i);
				break;
			}
			
		}
				
		valorMes = p_timeBaseS[i]->timeBaseScheduleMonth;
		dec_to_bin_n(valorMes, array_month,16);
		
		valorSemana = p_timeBaseS[i]->timeBaseScheduleDate;
		dec_to_bin_n(valorSemana, array_date,32);
		
		valorDiaMes = p_timeBaseS[i]->timeBaseScheduleDay;
		dec_to_bin_n(valorDiaMes, array_day,8);
		
		llenar_agenda_ntcip_prototipo(agendaNTCIPProto, i, array_month, 16);
		llenar_agenda_ntcip_prototipo(agendaNTCIPProto, i, array_date, 32);
		llenar_agenda_ntcip_prototipo(agendaNTCIPProto, i, array_day, 8);

		agendaNTCIPProto[i].planDiario = p_timeBaseS[i]->timeBaseScheduleDayPlan;	

//		printf("feriado usado: %i\n\n",feriado_usado);
		
		if(valorMes > 0 && valorSemana > 0 && valorDiaMes > 0 && !feriado_usado) {
			
/*			printf("Entrada: %i\n",i+1);
			printf("mes en decimal: %li\n",valorMes);
			printf("dias del mes en decimal: %li\n",valorSemana);
			printf("dias de la semana en decimal: %li\n\n",valorDiaMes);
*/			
			bandera = 1;
			
			//iterando cada bit del arreglo de bits de los meses
			//empieza de 1 porque el bit 0 no se usa, hasta el bit 12
			for(int k=1; k<13; k++) {
				
				//printf("k: %i\n",k);
				
				contadorDias = 0;
				
				if(array_month[k] == 1) {
					
					//si es febrero
					if(k == 2) {
						monthDays = arreglo_bisiesto[3];
					}
					else {
						monthDays = diasDelMes(k-1);
					}
					
					//printf("dia del mes: %i\n",monthDays);
					
					if(k>1) {
					
						for(int l=1; l<k; l++) {
							//si el mes es febrero, sumar el respectivo febrero
							//calculado con la funcion bisiesto
							//printf("indice l: %i\n",l);
							if(l == 2) {
								contadorDias += arreglo_bisiesto[3];
							}
							else {
								contadorDias += diasDelMes(l-1);
							}
						}
					}					
					
					//printf("dias acumulados: %i\n",contadorDias);

					
					for(int l=0; l< monthDays; l++ ){
						
						//printf("evaluando dia del mes: %i\n",l);
						
						if( array_date[l] == 1 ) {
							
							diaSemana = calendar[contadorDias + l].diaSemana;
							
							//printf("dia semana: %i\n",diaSemana);

							//printf("arreglo dia semana: %i\n",array_day[diaSemana + 1]);

							if(array_day[diaSemana + 1] == 1) {
								
								calendar[contadorDias + l].planDiario = p_timeBaseS[i]->timeBaseScheduleDayPlan;
								
								//if(k == 7 ) printf("plan diario de este mes: %i\n",calendar[contadorDias + l].planDiario);
							}
							
						} 
						   
					} 

				}

			}						
			
		}// fin del if
		
		feriado_usado = 0;	
			
	} //fin del for
	
	//imprimir_calendario(calendar, diasDelAgnio);
	
	llenar_agenda_ntcip_prototipo_fase2(agendaNTCIP2, agendaNTCIPProto, 44);
	llenar_agenda_ntcip_prototipo_fase3(agendaNTCIP3, agendaNTCIP2, 44);
	llenar_agenda_ntcip_prototipo_fase4(agendaNTCIP4, agendaNTCIP3, 44);

	//generar_tramas_agendas_ntcip2(agendaNTCIP4);
	
	printf("mes\tdia inicio\tdia fin\t\tSEMANA\tPlan diario\n");
	
	for(int x=0; x<44; x++) {
		
		printf("%i\t%i\t\t%i\t\t", agendaNTCIPProto[x].mes, agendaNTCIPProto[x].diaInicio, agendaNTCIPProto[x].diaFin); 
		
		for(int y=6; y>-1; y--) {
			printf("%i",agendaNTCIPProto[x].semana[y]);
		}
		printf("\t\t%i\n", agendaNTCIPProto[x].planDiario);

	}
	printf("\n");
	
	printf("mes\tdia inicio\tdia fin\t\tSEMANA\n");
	
	for(int x=0; x<44; x++) {
		
		printf("%i\t%i\t\t%i\t\t", 
				agendaNTCIP2[x].mes, 
				agendaNTCIP2[x].diaInicio, 
				agendaNTCIP2[x].diaFin); 
		
		for(int y=0; y<7; y++) {
			printf("%i",agendaNTCIP2[x].semana[y]);
		}
		printf("\n");
	}
	printf("\n");
	
	
	printf("Fase 3: juntando fechas comunes y mezclar agendas diarias de la semana\n\n");

	printf("mes\tdia inicio\tdia fin\t\tSEMANA\n");
	
	for(int x=0; x<44; x++) {
		
		printf("%i\t%i\t\t%i\t\t", 
				agendaNTCIP3[x].mes, 
				agendaNTCIP3[x].diaInicio, 
				agendaNTCIP3[x].diaFin); 
		
		for(int y=0; y<7; y++) {
			printf("%i",agendaNTCIP3[x].semana[y]);
		}
		printf("\n");
	}
	printf("\n");

	printf("Fase 4: juntando las semanas comunes y conectar las fechas\n\n");

	printf("mes\tdia inicio\t\tSEMANA\n");
	
	for(int x=0; x<22; x++) {
		
		printf("%i\t%i\t\t", 
				agendaNTCIP4[x].mes, 
				agendaNTCIP4[x].diaInicio); 
		
		for(int y=0; y<7; y++) {
			printf("%i",agendaNTCIP4[x].semana[y]);
		}
		printf("\n");
	}
	printf("\n");

	if(bandera == 1) {
		//generar_tipo_semanas(calendar, diasDelAgnio, agenda_semanal_anual_seleccionada);
		//generar_tramas_agendas_ntcip(agenda_semanal_anual_seleccionada, agenda_feriados_procesada);
		generar_tramas_agendas_ntcip2(agendaNTCIP4, agenda_feriados_procesada);

	}
}

