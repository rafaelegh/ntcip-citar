
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"

int maxVehicleDetectors            = 19;
int maxVehicleDetectorStatusGroups = 4;
int volumeOccupancySequence        = 0;
int volumeOccupancyPeriod          = 60;
int activeVolumeOccupancyDetectors = 0;
int maxPedestrianDetectors         = 19;

struct vehicleDetectorTable_entry            *entry4[19];
struct vehicleDetectorStatusGroupTable_entry *entry5[4];
struct volumeOccupancyTable_entry            *entry6[19];
struct pedestrianDetectorTable_entry         *entry7[19];



struct vehicleDetectorTable_entry ** get_vehicleDetector_table(void)
{
	return entry4;
}

struct vehicleDetectorStatusGroupTable_entry ** get_vehicleStatus_table(void)
{
	return entry5;
}

struct volumeOccupancyTable_entry ** get_volumeOccupancy_table(void)
{
	return entry6;
}

struct pedestrianDetectorTable_entry ** get_pedestrian_table(void)
{
	return entry7;
}



int get_detector_variable(int _numero)
{
	int resp=-1;

	if(_numero==1)
	{
		resp=maxVehicleDetectors;
	}
	else if(_numero==2)
	{
		resp=maxVehicleDetectorStatusGroups;
	}

	else if(_numero==3)
	{
		resp=maxPedestrianDetectors;
	}
	else if(_numero==4)
	{
		resp=volumeOccupancySequence;
	}
	else if(_numero==5)
	{
		resp=volumeOccupancyPeriod;
	}
	else if(_numero==6)
	{
		resp=activeVolumeOccupancyDetectors;
	}

	return resp;
}
void set_detector_variable(int _numero, int _valor)
{
	int resp=-1;

	if(_numero==4)
	{
		volumeOccupancySequence++;
		if(volumeOccupancySequence > 255) volumeOccupancySequence = 0;
	}
	else if(_numero==5)
	{
		volumeOccupancyPeriod = _valor;
	}
	else if(_numero==6)
	{
		activeVolumeOccupancyDetectors = _valor;
	}

}

struct vehicleDetectorTable_entry ** get_detector_table(void)
{
	return entry4;
}

/* create a new row in the table */
netsnmp_tdata_row *
detector_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int tabla) 
{
    netsnmp_tdata_row *row;
	
	

	switch(tabla)
	{
		case 4:
		
    	entry4[indice] = SNMP_MALLOC_TYPEDEF(struct vehicleDetectorTable_entry);
    	if (!entry4[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry4[indice]);
        	return NULL;
    	}
    	row->data = entry4[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry4[indice]->vehicleDetectorNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry4[indice]->vehicleDetectorNumber),
                                 sizeof(entry4[indice]->vehicleDetectorNumber));
		
  		break;

		case 5:		

    	entry5[indice] = SNMP_MALLOC_TYPEDEF(struct vehicleDetectorStatusGroupTable_entry);
    	if (!entry5[indice]) return NULL;

    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry5[indice]);
	        return NULL;
    	}
    	row->data = entry5[indice];

    	DEBUGMSGT(("phaseStatusGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry5[indice]->vehicleDetectorStatusGroupNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry5[indice]->vehicleDetectorStatusGroupNumber),
                                 sizeof(entry5[indice]->vehicleDetectorStatusGroupNumber));
		break;

		case 6:

    	entry6[indice] = SNMP_MALLOC_TYPEDEF(struct volumeOccupancyTable_entry);
    	if (!entry6[indice]) return NULL;

    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry6[indice]);
	        return NULL;
	    }
	    row->data = entry6[indice];

	    DEBUGMSGT(("phaseControlGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
	    entry6[indice]->vehicleDetectorNumber = indice+1;
	    netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry6[indice]->vehicleDetectorNumber),
                                 sizeof(entry6[indice]->vehicleDetectorNumber));
		break;

		case 7:

    	entry7[indice] = SNMP_MALLOC_TYPEDEF(struct pedestrianDetectorTable_entry);
    	if (!entry7[indice]) return NULL;

    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry7[indice]);
	        return NULL;
	    }
	    row->data = entry7[indice];

	    DEBUGMSGT(("phaseControlGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
	    entry7[indice]->pedestrianDetectorNumber = indice+1;
	    netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry7[indice]->pedestrianDetectorNumber),
                                 sizeof(entry7[indice]->pedestrianDetectorNumber));
		break;
		
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_detector_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	int min_columna, int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 4:
    	reg = netsnmp_create_handler_registration( nombre, vehicleDetectorTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxVehicleDetectors;
		break;
		case 5:
		reg = netsnmp_create_handler_registration( nombre, vehicleDetectorStatusGroupTable_handler, 
		oid_table, oid_len,HANDLER_CAN_RONLY);
		n=maxVehicleDetectorStatusGroups;
		break;
		case 6:
    	reg = netsnmp_create_handler_registration( nombre, volumeOccupancyTable_handler, oid_table, 
		oid_len, HANDLER_CAN_RONLY);
		n=maxVehicleDetectors;
		break;
		case 7:
    	reg = netsnmp_create_handler_registration( nombre,  pedestrianDetectorTable_handler, oid_table, 
		oid_len, HANDLER_CAN_RWRITE);
		n=maxPedestrianDetectors;
		break;
		
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=detector_tables_createEntry(table_data, i, tabla);
	}
}

void
init_detector(void)
{

    const oid maxVehicleDetectors_oid[] = { 1,3,6,1,4,1,1206,4,2,1,2,1 };

    const oid vehicleDetectorTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,2,2};
    const size_t vehicleDetectorTable_oid_len   = OID_LENGTH(vehicleDetectorTable_oid);

	const oid maxVehicleDetectorStatusGroups_oid[] = { 1,3,6,1,4,1,1206,4,2,1,2,3 };

    const oid vehicleDetectorStatusGroupTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,2,4};
    const size_t vehicleDetectorStatusGroupTable_oid_len   = OID_LENGTH(vehicleDetectorStatusGroupTable_oid);

	const oid volumeOccupancySequence_oid[] = { 1,3,6,1,4,1,1206,4,2,1,2,5,1 };

	const oid volumeOccupancyPeriod_oid[] = { 1,3,6,1,4,1,1206,4,2,1,2,5,2 };

    const oid activeVolumeOccupancyDetectors_oid[] = { 1,3,6,1,4,1,1206,4,2,1,2,5,3 };

    const oid volumeOccupancyTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,2,5,4};
    const size_t volumeOccupancyTable_oid_len   = OID_LENGTH(volumeOccupancyTable_oid);

    const oid maxPedestrianDetectors_oid[] = { 1,3,6,1,4,1,1206,4,2,1,2,6 };

    const oid pedestrianDetectorTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,2,7};
    const size_t pedestrianDetectorTable_oid_len   = OID_LENGTH(pedestrianDetectorTable_oid);

  DEBUGMSGTL(("maxVehicleDetectors", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxVehicleDetectors", handle_detector,
                               maxVehicleDetectors_oid, OID_LENGTH(maxVehicleDetectors_oid),
                               HANDLER_CAN_RONLY
        ));

   initialize_detector_tables("vehicleDetectorTable",vehicleDetectorTable_oid,vehicleDetectorTable_oid_len,
				COLUMN_VEHICLEDETECTORNUMBER, COLUMN_VEHICLEDETECTORRESET,4);

  DEBUGMSGTL(("maxVehicleDetectorStatusGroups", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxVehicleDetectorStatusGroups", handle_detector,
                               maxVehicleDetectorStatusGroups_oid, OID_LENGTH(maxVehicleDetectorStatusGroups_oid),
                               HANDLER_CAN_RONLY
        ));

   initialize_detector_tables("vehicleDetectorStatusGroupTable",vehicleDetectorStatusGroupTable_oid,vehicleDetectorStatusGroupTable_oid_len,
				COLUMN_VEHICLEDETECTORSTATUSGROUPNUMBER, COLUMN_VEHICLEDETECTORSTATUSGROUPALARMS,5);

  DEBUGMSGTL(("volumeOccupancySequence", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("volumeOccupancySequence", handle_detector,
                               volumeOccupancySequence_oid, OID_LENGTH(volumeOccupancySequence_oid),
                               HANDLER_CAN_RONLY
        ));

  DEBUGMSGTL(("volumeOccupancyPeriod", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("volumeOccupancyPeriod", handle_detector,
                               volumeOccupancyPeriod_oid, OID_LENGTH(volumeOccupancyPeriod_oid),
                               HANDLER_CAN_RWRITE
        ));



  DEBUGMSGTL(("activeVolumeOccupancyDetectors", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("activeVolumeOccupancyDetectors", handle_detector,
                               activeVolumeOccupancyDetectors_oid, OID_LENGTH(activeVolumeOccupancyDetectors_oid),
                               HANDLER_CAN_RONLY
        ));

   initialize_detector_tables("volumeOccupancyTable",volumeOccupancyTable_oid,volumeOccupancyTable_oid_len,
				COLUMN_DETECTORVOLUME, COLUMN_DETECTOROCCUPANCY,6);

  DEBUGMSGTL(("maxPedestrianDetectors", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxPedestrianDetectors", handle_detector,
                               maxPedestrianDetectors_oid, OID_LENGTH(maxPedestrianDetectors_oid),
                               HANDLER_CAN_RONLY
        ));

   initialize_detector_tables("volumeOccupancyTable",pedestrianDetectorTable_oid,pedestrianDetectorTable_oid_len,
				COLUMN_PEDESTRIANDETECTORNUMBER, COLUMN_PEDESTRIANDETECTORALARMS,7);


}

//--------------------------Manejadores de objetos------------------------------

int
handle_detector(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxVehicleDetectors
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==3)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxVehicleDetectorStatusGroups
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==51)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&volumeOccupancySequence
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==52)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&volumeOccupancyPeriod
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==53)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&activeVolumeOccupancyDetectors
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==6)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxPedestrianDetectors
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}

            break;

        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_INTEGER);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:
            /* XXX malloc "undo" storage buffer */
   //         if (/* XXX if malloc, or whatever, failed: */) {
   //             netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_RESOURCEUNAVAILABLE);
   //         }
            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
  			resul=compara_oid(requests->requestvb->name);
				
			if(resul == 52) 
			{
				volumeOccupancyPeriod = *requests->requestvb->val.integer;
				if(volumeOccupancyPeriod < 60) volumeOccupancyPeriod = 60;
				else if(volumeOccupancyPeriod >= 60) 
				{
					volumeOccupancyPeriod /= 60;
					volumeOccupancyPeriod *= 60;
				}
			}
		break;

        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */
     //       if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_COMMITFAILED);
    //        }
            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */
    //        if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_UNDOFAILED);
    //        }
            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_unitRedRevert\n", reqinfo->mode );
            return SNMP_ERR_GENERR;

    }

    return SNMP_ERR_NOERROR;
}

/** handles requests for the vehicleDetectorTable table */
int
vehicleDetectorTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct vehicleDetectorTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("vehicleDetectorTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct vehicleDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_VEHICLEDETECTORNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorNumber);
                break;
            case COLUMN_VEHICLEDETECTOROPTIONS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorOptions);
                break;
            case COLUMN_VEHICLEDETECTORCALLPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorCallPhase);
                break;
            case COLUMN_VEHICLEDETECTORSWITCHPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorSwitchPhase);
                break;
            case COLUMN_VEHICLEDETECTORDELAY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorDelay);
                break;
            case COLUMN_VEHICLEDETECTOREXTEND:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorExtend);
                break;
            case COLUMN_VEHICLEDETECTORQUEUELIMIT:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorQueueLimit);
                break;
            case COLUMN_VEHICLEDETECTORNOACTIVITY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorNoActivity);
                break;
            case COLUMN_VEHICLEDETECTORMAXPRESENCE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorMaxPresence);
                break;
            case COLUMN_VEHICLEDETECTORERRATICCOUNTS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorErraticCounts);
                break;
            case COLUMN_VEHICLEDETECTORFAILTIME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorFailTime);
                break;
            case COLUMN_VEHICLEDETECTORALARMS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorAlarms);
                break;
            case COLUMN_VEHICLEDETECTORREPORTEDALARMS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorReportedAlarms);
                break;
            case COLUMN_VEHICLEDETECTORRESET:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorReset);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct vehicleDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_VEHICLEDETECTOROPTIONS:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORCALLPHASE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORSWITCHPHASE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORDELAY:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTOREXTEND:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORQUEUELIMIT:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORNOACTIVITY:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORMAXPRESENCE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORERRATICCOUNTS:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORFAILTIME:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_VEHICLEDETECTORRESET:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct vehicleDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_VEHICLEDETECTOROPTIONS:
                table_entry->old_vehicleDetectorOptions = table_entry->vehicleDetectorOptions;
                table_entry->vehicleDetectorOptions     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORCALLPHASE:
                table_entry->old_vehicleDetectorCallPhase = table_entry->vehicleDetectorCallPhase;
                table_entry->vehicleDetectorCallPhase     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORSWITCHPHASE:
                table_entry->old_vehicleDetectorSwitchPhase = table_entry->vehicleDetectorSwitchPhase;
                table_entry->vehicleDetectorSwitchPhase     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORDELAY:
                table_entry->old_vehicleDetectorDelay = table_entry->vehicleDetectorDelay;
                table_entry->vehicleDetectorDelay     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTOREXTEND:
                table_entry->old_vehicleDetectorExtend = table_entry->vehicleDetectorExtend;
                table_entry->vehicleDetectorExtend     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORQUEUELIMIT:
                table_entry->old_vehicleDetectorQueueLimit = table_entry->vehicleDetectorQueueLimit;
                table_entry->vehicleDetectorQueueLimit     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORNOACTIVITY:
                table_entry->old_vehicleDetectorNoActivity = table_entry->vehicleDetectorNoActivity;
                table_entry->vehicleDetectorNoActivity     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORMAXPRESENCE:
                table_entry->old_vehicleDetectorMaxPresence = table_entry->vehicleDetectorMaxPresence;
                table_entry->vehicleDetectorMaxPresence     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORERRATICCOUNTS:
                table_entry->old_vehicleDetectorErraticCounts = table_entry->vehicleDetectorErraticCounts;
                table_entry->vehicleDetectorErraticCounts     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORFAILTIME:
                table_entry->old_vehicleDetectorFailTime = table_entry->vehicleDetectorFailTime;
                table_entry->vehicleDetectorFailTime     = *request->requestvb->val.integer;
                break;
            case COLUMN_VEHICLEDETECTORRESET:
                table_entry->old_vehicleDetectorReset = table_entry->vehicleDetectorReset;
                table_entry->vehicleDetectorReset     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct vehicleDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_VEHICLEDETECTOROPTIONS:
                table_entry->vehicleDetectorOptions     = table_entry->old_vehicleDetectorOptions;
                table_entry->old_vehicleDetectorOptions = 0;
                break;
            case COLUMN_VEHICLEDETECTORCALLPHASE:
                table_entry->vehicleDetectorCallPhase     = table_entry->old_vehicleDetectorCallPhase;
                table_entry->old_vehicleDetectorCallPhase = 0;
                break;
            case COLUMN_VEHICLEDETECTORSWITCHPHASE:
                table_entry->vehicleDetectorSwitchPhase     = table_entry->old_vehicleDetectorSwitchPhase;
                table_entry->old_vehicleDetectorSwitchPhase = 0;
                break;
            case COLUMN_VEHICLEDETECTORDELAY:
                table_entry->vehicleDetectorDelay     = table_entry->old_vehicleDetectorDelay;
                table_entry->old_vehicleDetectorDelay = 0;
                break;
            case COLUMN_VEHICLEDETECTOREXTEND:
                table_entry->vehicleDetectorExtend     = table_entry->old_vehicleDetectorExtend;
                table_entry->old_vehicleDetectorExtend = 0;
                break;
            case COLUMN_VEHICLEDETECTORQUEUELIMIT:
                table_entry->vehicleDetectorQueueLimit     = table_entry->old_vehicleDetectorQueueLimit;
                table_entry->old_vehicleDetectorQueueLimit = 0;
                break;
            case COLUMN_VEHICLEDETECTORNOACTIVITY:
                table_entry->vehicleDetectorNoActivity     = table_entry->old_vehicleDetectorNoActivity;
                table_entry->old_vehicleDetectorNoActivity = 0;
                break;
            case COLUMN_VEHICLEDETECTORMAXPRESENCE:
                table_entry->vehicleDetectorMaxPresence     = table_entry->old_vehicleDetectorMaxPresence;
                table_entry->old_vehicleDetectorMaxPresence = 0;
                break;
            case COLUMN_VEHICLEDETECTORERRATICCOUNTS:
                table_entry->vehicleDetectorErraticCounts     = table_entry->old_vehicleDetectorErraticCounts;
                table_entry->old_vehicleDetectorErraticCounts = 0;
                break;
            case COLUMN_VEHICLEDETECTORFAILTIME:
                table_entry->vehicleDetectorFailTime     = table_entry->old_vehicleDetectorFailTime;
                table_entry->old_vehicleDetectorFailTime = 0;
                break;
            case COLUMN_VEHICLEDETECTORRESET:
                table_entry->vehicleDetectorReset     = table_entry->old_vehicleDetectorReset;
                table_entry->old_vehicleDetectorReset = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the vehicleDetectorStatusGroupTable table */
int
vehicleDetectorStatusGroupTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct vehicleDetectorStatusGroupTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("vehicleDetectorStatusGroupTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct vehicleDetectorStatusGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_VEHICLEDETECTORSTATUSGROUPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorStatusGroupNumber);
                break;
            case COLUMN_VEHICLEDETECTORSTATUSGROUPACTIVE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorStatusGroupActive);
                break;
            case COLUMN_VEHICLEDETECTORSTATUSGROUPALARMS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->vehicleDetectorStatusGroupAlarms);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the volumeOccupancyTable table */
int
volumeOccupancyTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct volumeOccupancyTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("volumeOccupancyTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct volumeOccupancyTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_DETECTORVOLUME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->detectorVolume);
                break;
            case COLUMN_DETECTOROCCUPANCY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->detectorOccupancy);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the pedestrianDetectorTable table */
int
pedestrianDetectorTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct pedestrianDetectorTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("pedestrianDetectorTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct pedestrianDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PEDESTRIANDETECTORNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->pedestrianDetectorNumber);
                break;
            case COLUMN_PEDESTRIANDETECTORCALLPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->pedestrianDetectorCallPhase);
                break;
            case COLUMN_PEDESTRIANDETECTORNOACTIVITY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->pedestrianDetectorNoActivity);
                break;
            case COLUMN_PEDESTRIANDETECTORMAXPRESENCE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->pedestrianDetectorMaxPresence);
                break;
            case COLUMN_PEDESTRIANDETECTORERRATICCOUNTS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->pedestrianDetectorErraticCounts);
                break;
            case COLUMN_PEDESTRIANDETECTORALARMS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->pedestrianDetectorAlarms);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct pedestrianDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PEDESTRIANDETECTORCALLPHASE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PEDESTRIANDETECTORNOACTIVITY:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PEDESTRIANDETECTORMAXPRESENCE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PEDESTRIANDETECTORERRATICCOUNTS:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct pedestrianDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PEDESTRIANDETECTORCALLPHASE:
                table_entry->old_pedestrianDetectorCallPhase = table_entry->pedestrianDetectorCallPhase;
                table_entry->pedestrianDetectorCallPhase     = *request->requestvb->val.integer;
                break;
            case COLUMN_PEDESTRIANDETECTORNOACTIVITY:
                table_entry->old_pedestrianDetectorNoActivity = table_entry->pedestrianDetectorNoActivity;
                table_entry->pedestrianDetectorNoActivity     = *request->requestvb->val.integer;
                break;
            case COLUMN_PEDESTRIANDETECTORMAXPRESENCE:
                table_entry->old_pedestrianDetectorMaxPresence = table_entry->pedestrianDetectorMaxPresence;
                table_entry->pedestrianDetectorMaxPresence     = *request->requestvb->val.integer;
                break;
            case COLUMN_PEDESTRIANDETECTORERRATICCOUNTS:
                table_entry->old_pedestrianDetectorErraticCounts = table_entry->pedestrianDetectorErraticCounts;
                table_entry->pedestrianDetectorErraticCounts     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct pedestrianDetectorTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PEDESTRIANDETECTORCALLPHASE:
                table_entry->pedestrianDetectorCallPhase     = table_entry->old_pedestrianDetectorCallPhase;
                table_entry->old_pedestrianDetectorCallPhase = 0;
                break;
            case COLUMN_PEDESTRIANDETECTORNOACTIVITY:
                table_entry->pedestrianDetectorNoActivity     = table_entry->old_pedestrianDetectorNoActivity;
                table_entry->old_pedestrianDetectorNoActivity = 0;
                break;
            case COLUMN_PEDESTRIANDETECTORMAXPRESENCE:
                table_entry->pedestrianDetectorMaxPresence     = table_entry->old_pedestrianDetectorMaxPresence;
                table_entry->old_pedestrianDetectorMaxPresence = 0;
                break;
            case COLUMN_PEDESTRIANDETECTORERRATICCOUNTS:
                table_entry->pedestrianDetectorErraticCounts     = table_entry->old_pedestrianDetectorErraticCounts;
                table_entry->old_pedestrianDetectorErraticCounts = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}
