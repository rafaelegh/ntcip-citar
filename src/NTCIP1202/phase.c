#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"
#include "listas.h"
//Declaracion de variables

int   maxPhases = 16;
const oid maxPhases_oid[] = { 1,3,6,1,4,1,1206,4,2,1,1,1 };

const oid phaseTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,1,2};
const size_t phaseTable_oid_len   = OID_LENGTH(phaseTable_oid);

int  maxPhaseGroups = 2;
const oid maxPhasesGroups_oid[] = { 1,3,6,1,4,1,1206,4,2,1,1,3 };

const oid phaseStatusGroupTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,1,4};
const size_t phaseStatusGroupTable_oid_len   = OID_LENGTH(phaseStatusGroupTable_oid);

const oid phaseControlGroupTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,1,5};
const size_t phaseControlGroupTable_oid_len   = OID_LENGTH(phaseControlGroupTable_oid);

int indice = 0;

/** Initialize the phaseTable table by defining its contents and how it's structured */
//* Typical data structure for a row entry */


struct phaseTable_entry *entry1[16];
struct phaseStatusGroupTable_entry *entry2[2];
struct phaseControlGroupTable_entry *entry3[2];

struct phaseTable_entry ** get_phase_table(void)
{
	return entry1;
}
 
int get_phase_variable(int _numero)
{
	int resp=-1;

	if(_numero==1)
	{
		resp=maxPhases;
	}

	return resp;
}



/* create a new row in the table */
netsnmp_tdata_row *
phase_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int tabla) 
{
    netsnmp_tdata_row *row;
	
	switch(tabla)
	{
		case 1:
		
    	entry1[indice] = SNMP_MALLOC_TYPEDEF(struct phaseTable_entry);
    	if (!entry1[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry1[indice]);
        	return NULL;
    	}
		entry1[indice]->phaseStartup = 2;
    	row->data = entry1[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry1[indice]->phaseNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry1[indice]->phaseNumber),
                                 sizeof(entry1[indice]->phaseNumber));
		
  		break;

		case 2:		

    	entry2[indice] = SNMP_MALLOC_TYPEDEF(struct phaseStatusGroupTable_entry);
    	if (!entry2[indice]) return NULL;

    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry2[indice]);
	        return NULL;
    	}
    	row->data = entry2[indice];

    	DEBUGMSGT(("phaseStatusGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry2[indice]->phaseStatusGroupNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry2[indice]->phaseStatusGroupNumber),
                                 sizeof(entry2[indice]->phaseStatusGroupNumber));
		break;

		case 3:

    	entry3[indice] = SNMP_MALLOC_TYPEDEF(struct phaseControlGroupTable_entry);
    	if (!entry3[indice]) return NULL;

    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry3[indice]);
	        return NULL;
	    }
	    row->data = entry3[indice];

	    DEBUGMSGT(("phaseControlGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
	    entry3[indice]->phaseControlGroupNumber = indice+1;
	    netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry3[indice]->phaseControlGroupNumber),
                                 sizeof(entry3[indice]->phaseControlGroupNumber));
		break;
		
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_phase_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	int min_columna, int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 1:
    	reg = netsnmp_create_handler_registration( nombre, phaseTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxPhases;
		break;
		case 2:
		reg = netsnmp_create_handler_registration( nombre, phaseStatusGroupTable_handler, 
		oid_table, oid_len,HANDLER_CAN_RONLY);
		n=maxPhaseGroups;
		break;
		case 3:
    	reg = netsnmp_create_handler_registration( nombre, phaseControlGroupTable_handler, oid_table, 
		oid_len, HANDLER_CAN_RWRITE);
		n=maxPhaseGroups;
		break;
		
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=phase_tables_createEntry(table_data, i, tabla);
	}
}

void
init_phase(void)
{
	/* here we initialize all the tables we're planning on supporting */
		DEBUGMSGTL(("maxPhases", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxPhases", handle_phase,
                               maxPhases_oid, OID_LENGTH(maxPhases_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("maxPhaseGroups", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxPhaseGroups", handle_phase,
                               maxPhasesGroups_oid, OID_LENGTH(maxPhasesGroups_oid),
                               HANDLER_CAN_RONLY));
  
   initialize_phase_tables("phaseTable",phaseTable_oid,phaseTable_oid_len,
				COLUMN_PHASENUMBER, COLUMN_PHASECONCURRENCY,1);

	initialize_phase_tables("phaseStatusGroupTable",phaseStatusGroupTable_oid,
				phaseStatusGroupTable_oid_len,COLUMN_PHASESTATUSGROUPNUMBER,
				COLUMN_PHASESTATUSGROUPPHASENEXTS,2);

	initialize_phase_tables("phaseControlGroupTable",phaseControlGroupTable_oid,
				phaseControlGroupTable_oid_len,COLUMN_PHASECONTROLGROUPNUMBER, 
				COLUMN_PHASECONTROLGROUPPEDCALL,3);


}

//--------------------------Manejadores de objetos------------------------------

int
handle_phase(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxPhases
                                     /* XXX: a pointer to the scalar's data */,sizeof(long)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==3)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxPhaseGroups
                                     /* XXX: a pointer to the scalar's data */,sizeof(long)
                                     /* XXX: the length of the data in bytes */);
			}
            break;

    }

    return SNMP_ERR_NOERROR;
}


/** handles requests for the phaseTable table */
int
phaseTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests)
 {

    netsnmp_request_info               *request;
    netsnmp_table_request_info         *table_info;
    netsnmp_tdata                      *table_data;
    netsnmp_tdata_row                  *table_row;
    struct phaseTable_entry            *table_entry;
    int                                 ret;
	ptnodo * l_dbt=NULL;
	struct datos_lista_objetos phase_dbt;

    DEBUGMSGTL(("phaseTable:handler", "Processing request (%d)\n", reqinfo->mode));
	if(get_variable(1)==0) set_variable(1,1);
    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
           if (request->processed)
                continue;

            table_entry = (struct phaseTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASENUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
								
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseNumber);
                break;
            case COLUMN_PHASEWALK:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
				
				
	            snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseWalk);
                break;
            case COLUMN_PHASEPEDESTRIANCLEAR:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phasePedestrianClear);
                break;
            case COLUMN_PHASEMINIMUMGREEN:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseMinimumGreen);
                break;
            case COLUMN_PHASEPASSAGE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phasePassage);
                break;
            case COLUMN_PHASEMAXIMUM1:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseMaximum1);
                break;
            case COLUMN_PHASEMAXIMUM2:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseMaximum2);
                break;
            case COLUMN_PHASEYELLOWCHANGE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseYellowChange);
                break;
            case COLUMN_PHASEREDCLEAR:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseRedClear);
                break;
            case COLUMN_PHASEREDREVERT:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseRedRevert);
                break;
            case COLUMN_PHASEADDEDINITIAL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseAddedInitial);
                break;
            case COLUMN_PHASEMAXIMUMINITIAL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseMaximumInitial);
                break;
            case COLUMN_PHASETIMEBEFOREREDUCTION:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseTimeBeforeReduction);
                break;
            case COLUMN_PHASECARSBEFOREREDUCTION:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseCarsBeforeReduction);
                break;
            case COLUMN_PHASETIMETOREDUCE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseTimeToReduce);
                break;
            case COLUMN_PHASEREDUCEBY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseReduceBy);
                break;
            case COLUMN_PHASEMINIMUMGAP:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseMinimumGap);
                break;
            case COLUMN_PHASEDYNAMICMAXLIMIT:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseDynamicMaxLimit);
                break;
            case COLUMN_PHASEDYNAMICMAXSTEP:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseDynamicMaxStep);
                break;
            case COLUMN_PHASESTARTUP:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStartup);
                break;
            case COLUMN_PHASEOPTIONS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseOptions);
                break;
            case COLUMN_PHASERING:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseRing);
                break;
            case COLUMN_PHASECONCURRENCY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->phaseConcurrency,
                                          table_entry->phaseConcurrency_len);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASEWALK:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEPEDESTRIANCLEAR:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEMINIMUMGREEN:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEPASSAGE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEMAXIMUM1:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEMAXIMUM2:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEYELLOWCHANGE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEREDCLEAR:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEREDREVERT:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEADDEDINITIAL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEMAXIMUMINITIAL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASETIMEBEFOREREDUCTION:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECARSBEFOREREDUCTION:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASETIMETOREDUCE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEREDUCEBY:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEMINIMUMGAP:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEDYNAMICMAXLIMIT:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEDYNAMICMAXSTEP:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASESTARTUP:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASEOPTIONS:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASERING:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECONCURRENCY:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->phaseConcurrency));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:

        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);


			//pregunto si hay transaccion de DB 
			if(get_globalDBManagement_variable(1)==2)
			{
				l_dbt=get_lista();
				phase_dbt.tabla	 	=1;
				phase_dbt.indice1	=table_info->index_oid[0];
				phase_dbt.columna	=table_info->colnum;
				agregar_nodo(&(*l_dbt), &phase_dbt);
			}    		
	
			if((table_info->colnum==COLUMN_PHASESTARTUP || table_info->colnum==COLUMN_PHASESTARTUP 
				|| table_info->colnum==COLUMN_PHASERING || table_info->colnum==COLUMN_PHASECONCURRENCY 
				|| table_info->colnum==COLUMN_PHASEOPTIONS) 
				&& get_globalDBManagement_variable(1)!=2)
			{
				netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
			}
			//indice= table_info->index_oid[0];

            switch (table_info->colnum) {
            case COLUMN_PHASEWALK:
                table_entry->old_phaseWalk = table_entry->phaseWalk;
                table_entry->phaseWalk     = *request->requestvb->val.integer;
				set_secuencia_programacion(4);
                break;
            case COLUMN_PHASEPEDESTRIANCLEAR:
                table_entry->old_phasePedestrianClear = table_entry->phasePedestrianClear;
                table_entry->phasePedestrianClear     = *request->requestvb->val.integer;
				set_secuencia_programacion(5);
                break;
            case COLUMN_PHASEMINIMUMGREEN:
                table_entry->old_phaseMinimumGreen = table_entry->phaseMinimumGreen;
                table_entry->phaseMinimumGreen     = *request->requestvb->val.integer;
				set_secuencia_programacion(1);
                break;
            case COLUMN_PHASEPASSAGE:
                table_entry->old_phasePassage = table_entry->phasePassage;
                table_entry->phasePassage     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEMAXIMUM1:
                table_entry->old_phaseMaximum1 = table_entry->phaseMaximum1;
                table_entry->phaseMaximum1     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEMAXIMUM2:
                table_entry->old_phaseMaximum2 = table_entry->phaseMaximum2;
                table_entry->phaseMaximum2     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEYELLOWCHANGE:
                table_entry->old_phaseYellowChange = table_entry->phaseYellowChange;
                ret = *request->requestvb->val.integer;
				if(ret >= 3) table_entry->phaseYellowChange = ret;
				else  table_entry->phaseYellowChange = table_entry->old_phaseYellowChange;
				set_secuencia_programacion(2);
                break;
            case COLUMN_PHASEREDCLEAR:
                table_entry->old_phaseRedClear = table_entry->phaseRedClear;
                table_entry->phaseRedClear     = *request->requestvb->val.integer;
				set_secuencia_programacion(3);
                break;
            case COLUMN_PHASEREDREVERT:
                table_entry->old_phaseRedRevert = table_entry->phaseRedRevert;
                table_entry->phaseRedRevert     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEADDEDINITIAL:
                table_entry->old_phaseAddedInitial = table_entry->phaseAddedInitial;
                table_entry->phaseAddedInitial     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEMAXIMUMINITIAL:
                table_entry->old_phaseMaximumInitial = table_entry->phaseMaximumInitial;
                table_entry->phaseMaximumInitial     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASETIMEBEFOREREDUCTION:
                table_entry->old_phaseTimeBeforeReduction = table_entry->phaseTimeBeforeReduction;
                table_entry->phaseTimeBeforeReduction     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASECARSBEFOREREDUCTION:
                table_entry->old_phaseCarsBeforeReduction = table_entry->phaseCarsBeforeReduction;
                table_entry->phaseCarsBeforeReduction     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASETIMETOREDUCE:
                table_entry->old_phaseTimeToReduce = table_entry->phaseTimeToReduce;
                table_entry->phaseTimeToReduce     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEREDUCEBY:
                table_entry->old_phaseReduceBy = table_entry->phaseReduceBy;
                table_entry->phaseReduceBy     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEMINIMUMGAP:
                table_entry->old_phaseMinimumGap = table_entry->phaseMinimumGap;
                table_entry->phaseMinimumGap     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEDYNAMICMAXLIMIT:
                table_entry->old_phaseDynamicMaxLimit = table_entry->phaseDynamicMaxLimit;
                table_entry->phaseDynamicMaxLimit     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASEDYNAMICMAXSTEP:
                table_entry->old_phaseDynamicMaxStep = table_entry->phaseDynamicMaxStep;
                table_entry->phaseDynamicMaxStep     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASESTARTUP:
                	table_entry->old_phaseStartup = table_entry->phaseStartup;
                	table_entry->phaseStartup     = *request->requestvb->val.integer;
                	break;
            case COLUMN_PHASEOPTIONS:
                table_entry->old_phaseOptions = table_entry->phaseOptions;
                table_entry->phaseOptions     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASERING:
                table_entry->old_phaseRing = table_entry->phaseRing;
                table_entry->phaseRing     = *request->requestvb->val.integer;
				set_secuencia_programacion(6);
                break;
            case COLUMN_PHASECONCURRENCY:
                memcpy( table_entry->old_phaseConcurrency,
                        table_entry->phaseConcurrency,
                        sizeof(table_entry->phaseConcurrency));
                table_entry->old_phaseConcurrency_len =
                        table_entry->phaseConcurrency_len;
                memset( table_entry->phaseConcurrency, 0,
                        sizeof(table_entry->phaseConcurrency));
                memcpy( table_entry->phaseConcurrency,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->phaseConcurrency_len =
                        request->requestvb->val_len;
				set_secuencia_programacion(7);
                break;
            }
        }
	
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASEWALK:
                table_entry->phaseWalk     = table_entry->old_phaseWalk;
                table_entry->old_phaseWalk = 0;
				set_secuencia_programacion(4);
                break;
            case COLUMN_PHASEPEDESTRIANCLEAR:
                table_entry->phasePedestrianClear     = table_entry->old_phasePedestrianClear;
                table_entry->old_phasePedestrianClear = 0;
				set_secuencia_programacion(5);
                break;
            case COLUMN_PHASEMINIMUMGREEN:
                table_entry->phaseMinimumGreen     = table_entry->old_phaseMinimumGreen;
                table_entry->old_phaseMinimumGreen = 0;
				set_secuencia_programacion(1);
                break;
            case COLUMN_PHASEPASSAGE:
                table_entry->phasePassage     = table_entry->old_phasePassage;
                table_entry->old_phasePassage = 0;
                break;
            case COLUMN_PHASEMAXIMUM1:
                table_entry->phaseMaximum1     = table_entry->old_phaseMaximum1;
                table_entry->old_phaseMaximum1 = 0;
                break;
            case COLUMN_PHASEMAXIMUM2:
                table_entry->phaseMaximum2     = table_entry->old_phaseMaximum2;
                table_entry->old_phaseMaximum2 = 0;
                break;
            case COLUMN_PHASEYELLOWCHANGE:
                table_entry->phaseYellowChange     = table_entry->old_phaseYellowChange;
                table_entry->old_phaseYellowChange = 0;
				set_secuencia_programacion(2);
                break;
            case COLUMN_PHASEREDCLEAR:
                table_entry->phaseRedClear     = table_entry->old_phaseRedClear;
                table_entry->old_phaseRedClear = 0;
				set_secuencia_programacion(3);
                break;
            case COLUMN_PHASEREDREVERT:
                table_entry->phaseRedRevert     = table_entry->old_phaseRedRevert;
                table_entry->old_phaseRedRevert = 0;
                break;
            case COLUMN_PHASEADDEDINITIAL:
                table_entry->phaseAddedInitial     = table_entry->old_phaseAddedInitial;
                table_entry->old_phaseAddedInitial = 0;
                break;
            case COLUMN_PHASEMAXIMUMINITIAL:
                table_entry->phaseMaximumInitial     = table_entry->old_phaseMaximumInitial;
                table_entry->old_phaseMaximumInitial = 0;
                break;
            case COLUMN_PHASETIMEBEFOREREDUCTION:
                table_entry->phaseTimeBeforeReduction     = table_entry->old_phaseTimeBeforeReduction;
                table_entry->old_phaseTimeBeforeReduction = 0;
                break;
            case COLUMN_PHASECARSBEFOREREDUCTION:
                table_entry->phaseCarsBeforeReduction     = table_entry->old_phaseCarsBeforeReduction;
                table_entry->old_phaseCarsBeforeReduction = 0;
                break;
            case COLUMN_PHASETIMETOREDUCE:
                table_entry->phaseTimeToReduce     = table_entry->old_phaseTimeToReduce;
                table_entry->old_phaseTimeToReduce = 0;
                break;
            case COLUMN_PHASEREDUCEBY:
                table_entry->phaseReduceBy     = table_entry->old_phaseReduceBy;
                table_entry->old_phaseReduceBy = 0;
                break;
            case COLUMN_PHASEMINIMUMGAP:
                table_entry->phaseMinimumGap     = table_entry->old_phaseMinimumGap;
                table_entry->old_phaseMinimumGap = 0;
                break;
            case COLUMN_PHASEDYNAMICMAXLIMIT:
                table_entry->phaseDynamicMaxLimit     = table_entry->old_phaseDynamicMaxLimit;
                table_entry->old_phaseDynamicMaxLimit = 0;
                break;
            case COLUMN_PHASEDYNAMICMAXSTEP:
                table_entry->phaseDynamicMaxStep     = table_entry->old_phaseDynamicMaxStep;
                table_entry->old_phaseDynamicMaxStep = 0;
                break;
            case COLUMN_PHASESTARTUP:
                table_entry->phaseStartup     = table_entry->old_phaseStartup;
                table_entry->old_phaseStartup = 0;
                break;
            case COLUMN_PHASEOPTIONS:
                table_entry->phaseOptions     = table_entry->old_phaseOptions;
                table_entry->old_phaseOptions = 0;
                break;
            case COLUMN_PHASERING:
                table_entry->phaseRing     = table_entry->old_phaseRing;
                table_entry->old_phaseRing = 0;
				set_secuencia_programacion(6);
                break;
            case COLUMN_PHASECONCURRENCY:
                memcpy( table_entry->phaseConcurrency,
                        table_entry->old_phaseConcurrency,
                        sizeof(table_entry->phaseConcurrency));
                memset( table_entry->old_phaseConcurrency, 0,
                        sizeof(table_entry->phaseConcurrency));
                table_entry->phaseConcurrency_len =
                        table_entry->old_phaseConcurrency_len;
				set_secuencia_programacion(7);
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the phaseStatusGroupTable table */
int
phaseStatusGroupTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct phaseStatusGroupTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("phaseStatusGroupTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseStatusGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASESTATUSGROUPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupNumber);
                break;
            case COLUMN_PHASESTATUSGROUPREDS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupReds);
                break;
            case COLUMN_PHASESTATUSGROUPYELLOWS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupYellows);
                break;
            case COLUMN_PHASESTATUSGROUPGREENS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupGreens);
                break;
            case COLUMN_PHASESTATUSGROUPDONTWALKS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupDontWalks);
                break;
            case COLUMN_PHASESTATUSGROUPPEDCLEARS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupPedClears);
                break;
            case COLUMN_PHASESTATUSGROUPWALKS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupWalks);
                break;
            case COLUMN_PHASESTATUSGROUPVEHCALLS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupVehCalls);
                break;
            case COLUMN_PHASESTATUSGROUPPEDCALLS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupPedCalls);
                break;
            case COLUMN_PHASESTATUSGROUPPHASEONS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupPhaseOns);
                break;
            case COLUMN_PHASESTATUSGROUPPHASENEXTS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseStatusGroupPhaseNexts);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the phaseControlGroupTable table */
int
phaseControlGroupTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct phaseControlGroupTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("phaseControlGroupTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseControlGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASECONTROLGROUPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupNumber);
                break;
            case COLUMN_PHASECONTROLGROUPPHASEOMIT:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupPhaseOmit);
                break;
            case COLUMN_PHASECONTROLGROUPPEDOMIT:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupPedOmit);
                break;
            case COLUMN_PHASECONTROLGROUPHOLD:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupHold);
                break;
            case COLUMN_PHASECONTROLGROUPFORCEOFF:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupForceOff);
                break;
            case COLUMN_PHASECONTROLGROUPVEHCALL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupVehCall);
                break;
            case COLUMN_PHASECONTROLGROUPPEDCALL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->phaseControlGroupPedCall);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseControlGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASECONTROLGROUPPHASEOMIT:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECONTROLGROUPPEDOMIT:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECONTROLGROUPHOLD:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECONTROLGROUPFORCEOFF:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECONTROLGROUPVEHCALL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PHASECONTROLGROUPPEDCALL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseControlGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASECONTROLGROUPPHASEOMIT:
                table_entry->old_phaseControlGroupPhaseOmit = table_entry->phaseControlGroupPhaseOmit;
                table_entry->phaseControlGroupPhaseOmit     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASECONTROLGROUPPEDOMIT:
                table_entry->old_phaseControlGroupPedOmit = table_entry->phaseControlGroupPedOmit;
                table_entry->phaseControlGroupPedOmit     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASECONTROLGROUPHOLD:
                table_entry->old_phaseControlGroupHold = table_entry->phaseControlGroupHold;
                table_entry->phaseControlGroupHold     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASECONTROLGROUPFORCEOFF:
                table_entry->old_phaseControlGroupForceOff = table_entry->phaseControlGroupForceOff;
                table_entry->phaseControlGroupForceOff     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASECONTROLGROUPVEHCALL:
                table_entry->old_phaseControlGroupVehCall = table_entry->phaseControlGroupVehCall;
                table_entry->phaseControlGroupVehCall     = *request->requestvb->val.integer;
                break;
            case COLUMN_PHASECONTROLGROUPPEDCALL:
                table_entry->old_phaseControlGroupPedCall = table_entry->phaseControlGroupPedCall;
                table_entry->phaseControlGroupPedCall     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct phaseControlGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PHASECONTROLGROUPPHASEOMIT:
                table_entry->phaseControlGroupPhaseOmit     = table_entry->old_phaseControlGroupPhaseOmit;
                table_entry->old_phaseControlGroupPhaseOmit = 0;
                break;
            case COLUMN_PHASECONTROLGROUPPEDOMIT:
                table_entry->phaseControlGroupPedOmit     = table_entry->old_phaseControlGroupPedOmit;
                table_entry->old_phaseControlGroupPedOmit = 0;
                break;
            case COLUMN_PHASECONTROLGROUPHOLD:
                table_entry->phaseControlGroupHold     = table_entry->old_phaseControlGroupHold;
                table_entry->old_phaseControlGroupHold = 0;
                break;
            case COLUMN_PHASECONTROLGROUPFORCEOFF:
                table_entry->phaseControlGroupForceOff     = table_entry->old_phaseControlGroupForceOff;
                table_entry->old_phaseControlGroupForceOff = 0;
                break;
            case COLUMN_PHASECONTROLGROUPVEHCALL:
                table_entry->phaseControlGroupVehCall     = table_entry->old_phaseControlGroupVehCall;
                table_entry->old_phaseControlGroupVehCall = 0;
                break;
            case COLUMN_PHASECONTROLGROUPPEDCALL:
                table_entry->phaseControlGroupPedCall     = table_entry->old_phaseControlGroupPedCall;
                table_entry->old_phaseControlGroupPedCall = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}



