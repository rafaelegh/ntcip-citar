
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"

//Declaracion de variables

int maxPort1Addresses = 48;

/** Initialize the phaseTable table by defining its contents and how it's structured */
//* Typical data structure for a row entry */

int get_port1_variable(int _n)
{
	int resp = -1;
	if(_n == 1) resp = maxPort1Addresses;

	return resp;
}

struct port1Table_entry *entry22[48];

struct port1Table_entry ** get_ts2Port1_table(void)
{
	return entry22;
}
 
/* create a new row in the table */
netsnmp_tdata_row *
ts2Port1_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int tabla) 
{
    netsnmp_tdata_row *row;
	
	switch(tabla)
	{
		case 22:
		
    	entry22[indice] = SNMP_MALLOC_TYPEDEF(struct port1Table_entry);
    	if (!entry22[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry22[indice]);
        	return NULL;
    	}
		entry22[indice]->port1Status = 1;
    	row->data = entry22[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry22[indice]->port1Number = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry22[indice]->port1Number),
                                 sizeof(entry22[indice]->port1Number));
		
  		break;
	
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_ts2Port1_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	int min_columna, int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 22:
    	reg = netsnmp_create_handler_registration( nombre,port1Table_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxPort1Addresses;
		break;
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=ts2Port1_tables_createEntry(table_data, i, tabla);
	}
}

void
init_ts2Port1(void)
{
    const oid maxPort1Addresses_oid[] = { 1,3,6,1,4,1,1206,4,2,1,10,1 };

    const oid port1Table_oid[] = {1,3,6,1,4,1,1206,4,2,1,10,2};
    const size_t port1Table_oid_len   = OID_LENGTH(port1Table_oid);

  	DEBUGMSGTL(("maxPort1Addresses", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxPort1Addresses", handle_ts2Port1,
                               maxPort1Addresses_oid, OID_LENGTH(maxPort1Addresses_oid),
                               HANDLER_CAN_RONLY
        ));

	initialize_ts2Port1_tables("port1Table",port1Table_oid,port1Table_oid_len,
				COLUMN_PORT1NUMBER, COLUMN_PORT1FAULTFRAME,22);

}

int
handle_ts2Port1(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
	{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxPort1Addresses
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}

        break;
  

    }

    return SNMP_ERR_NOERROR;
}

/** handles requests for the port1Table table */
int
port1Table_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct port1Table_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("port1Table:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct port1Table_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PORT1NUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->port1Number);
                break;
            case COLUMN_PORT1DEVICEPRESENT:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->port1DevicePresent);
                break;
            case COLUMN_PORT1FRAME40ENABLE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->port1Frame40Enable);
                break;
            case COLUMN_PORT1STATUS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->port1Status);
                break;
            case COLUMN_PORT1FAULTFRAME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->port1FaultFrame);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct port1Table_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PORT1DEVICEPRESENT:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PORT1FRAME40ENABLE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct port1Table_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PORT1DEVICEPRESENT:
                table_entry->old_port1DevicePresent = table_entry->port1DevicePresent;
                table_entry->port1DevicePresent     = *request->requestvb->val.integer;
                break;
            case COLUMN_PORT1FRAME40ENABLE:
                table_entry->old_port1Frame40Enable = table_entry->port1Frame40Enable;
                table_entry->port1Frame40Enable     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct port1Table_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PORT1DEVICEPRESENT:
                table_entry->port1DevicePresent     = table_entry->old_port1DevicePresent;
                table_entry->old_port1DevicePresent = 0;
                break;
            case COLUMN_PORT1FRAME40ENABLE:
                table_entry->port1Frame40Enable     = table_entry->old_port1Frame40Enable;
                table_entry->old_port1Frame40Enable = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}
