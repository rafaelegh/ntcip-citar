
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"

//Declaracion de variables

unsigned int timebaseAscPatternSync = 0;
int maxTimebaseAscActions=48;
int timebaseAscActionStatus = 0;

/** Initialize the phaseTable table by defining its contents and how it's structured */
//* Typical data structure for a row entry */

int get_timebase_variable(int _n) {
    
	int resp = -1;
    if(_n == 2) resp = maxTimebaseAscActions;
	else if(_n == 3) resp = timebaseAscActionStatus;

	return resp;
}

unsigned int get_timebase_variable2(int _n) {
    
	unsigned int resp = -1;
	if(_n == 1) resp = timebaseAscPatternSync;

	return resp;
}

void set_timebase_variable2(int _n, unsigned int valor) {
    
	unsigned int resp = -1;
	if(_n == 1) timebaseAscPatternSync = valor;

}

struct timebaseAscActionTable_entry *entry12[48];

struct timebaseAscActionTable_entry ** get_timebaseAsc_table(void) {
	return entry12;
}
 
/* create a new row in the table */
netsnmp_tdata_row *
timebaseAsc_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int tabla) 
{
    netsnmp_tdata_row *row;
	
	

	switch(tabla)
	{
		case 12:
		
    	entry12[indice] = SNMP_MALLOC_TYPEDEF(struct timebaseAscActionTable_entry);
    	if (!entry12[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry12[indice]);
        	return NULL;
    	}
    	row->data = entry12[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry12[indice]->timebaseAscActionNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry12[indice]->timebaseAscActionNumber),
                                 sizeof(entry12[indice]->timebaseAscActionNumber));
		
  		break;
	
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_timebaseAsc_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	int min_columna, int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 12:
    	reg = netsnmp_create_handler_registration( nombre, timebaseAscActionTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxTimebaseAscActions;
		break;
	
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=timebaseAsc_tables_createEntry(table_data, i, tabla);
	}
}

void
init_timebaseAsc(void)
{
    const oid timebaseAscPatternSync_oid[] = { 1,3,6,1,4,1,1206,4,2,1,5,1 };

    const oid maxTimebaseAscActions_oid[] = { 1,3,6,1,4,1,1206,4,2,1,5,2 };

    const oid timebaseAscActionTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,5,3};
    const size_t timebaseAscActionTable_oid_len   = OID_LENGTH(timebaseAscActionTable_oid);

    const oid timebaseAscActionStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,5,4 };


  DEBUGMSGTL(("timebaseAscPatternSync", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("timebaseAscPatternSync", handle_timebaseAsc,
                               timebaseAscPatternSync_oid, OID_LENGTH(timebaseAscPatternSync_oid),
                               HANDLER_CAN_RWRITE
        ));

  DEBUGMSGTL(("maxTimebaseAscActions", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxTimebaseAscActions", handle_timebaseAsc,
                               maxTimebaseAscActions_oid, OID_LENGTH(maxTimebaseAscActions_oid),
                               HANDLER_CAN_RONLY
        ));

	DEBUGMSGTL(("timebaseAscActionTable:init", "initializing table timebaseAscActionTable\n"));

	initialize_timebaseAsc_tables("timebaseAscActionTable",timebaseAscActionTable_oid,timebaseAscActionTable_oid_len,
				COLUMN_TIMEBASEASCACTIONNUMBER, COLUMN_TIMEBASEASCSPECIALFUNCTION,12);


  DEBUGMSGTL(("timebaseAscActionStatus", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("timebaseAscActionStatus", handle_timebaseAsc,
                               timebaseAscActionStatus_oid, OID_LENGTH(timebaseAscActionStatus_oid),
                               HANDLER_CAN_RONLY
        ));
}

int
handle_timebaseAsc(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&timebaseAscPatternSync
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==2)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxTimebaseAscActions
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
			else if (resul==4)
			{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&timebaseAscActionStatus
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
            break;

       /*
         * SET REQUEST
         *
         * multiple states in the transaction.  See:
         * http://www.net-snmp.org/tutorial-5/toolkit/mib_module/set-actions.jpg
         */
        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_INTEGER);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:
            /* XXX malloc "undo" storage buffer */
   //         if (/* XXX if malloc, or whatever, failed: */) {
   //             netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_RESOURCEUNAVAILABLE);
   //         }
            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
  			resul=compara_oid(requests->requestvb->name);
			switch(resul)
			{
				case 1:

				timebaseAscPatternSync=*requests->requestvb->val.integer;

				break;
			}
        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */
     //       if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_COMMITFAILED);
    //        }
            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */
    //        if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_UNDOFAILED);
    //        }
            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_unitRedRevert\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    

    }

    return SNMP_ERR_NOERROR;
}


/** handles requests for the timebaseAscActionTable table */
int
timebaseAscActionTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct timebaseAscActionTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("timebaseAscActionTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct timebaseAscActionTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIMEBASEASCACTIONNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->timebaseAscActionNumber);
                break;
            case COLUMN_TIMEBASEASCPATTERN:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->timebaseAscPattern);
                break;
            case COLUMN_TIMEBASEASCAUXILLARYFUNCTION:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->timebaseAscAuxillaryFunction);
                break;
            case COLUMN_TIMEBASEASCSPECIALFUNCTION:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->timebaseAscSpecialFunction);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct timebaseAscActionTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIMEBASEASCPATTERN:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_TIMEBASEASCAUXILLARYFUNCTION:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_TIMEBASEASCSPECIALFUNCTION:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct timebaseAscActionTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIMEBASEASCPATTERN:
                table_entry->old_timebaseAscPattern = table_entry->timebaseAscPattern;
                table_entry->timebaseAscPattern     = *request->requestvb->val.integer;
                break;
            case COLUMN_TIMEBASEASCAUXILLARYFUNCTION:
                table_entry->old_timebaseAscAuxillaryFunction = table_entry->timebaseAscAuxillaryFunction;
                table_entry->timebaseAscAuxillaryFunction     = *request->requestvb->val.integer;
                break;
            case COLUMN_TIMEBASEASCSPECIALFUNCTION:
                table_entry->old_timebaseAscSpecialFunction = table_entry->timebaseAscSpecialFunction;
                table_entry->timebaseAscSpecialFunction     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct timebaseAscActionTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIMEBASEASCPATTERN:
                table_entry->timebaseAscPattern     = table_entry->old_timebaseAscPattern;
                table_entry->old_timebaseAscPattern = 0;
                break;
            case COLUMN_TIMEBASEASCAUXILLARYFUNCTION:
                table_entry->timebaseAscAuxillaryFunction     = table_entry->old_timebaseAscAuxillaryFunction;
                table_entry->old_timebaseAscAuxillaryFunction = 0;
                break;
            case COLUMN_TIMEBASEASCSPECIALFUNCTION:
                table_entry->timebaseAscSpecialFunction     = table_entry->old_timebaseAscSpecialFunction;
                table_entry->old_timebaseAscSpecialFunction = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}
