#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"
#include <stdlib.h>

//coord

int   coordOperationalMode = 0;
const  oid coordOperationalMode_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,1 };

int   coordCorrectionMode = 0;
const  oid coordCorrectionMode_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,2 };

int   coordMaximumMode = 0;
const  oid coordMaximumMode_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,3 };

int   coordForceMode = 0;
const  oid coordForceMode_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,4 };

int   maxPatterns = 48;
const  oid  maxPatterns_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,5 };

int patternTableType = 2;
const oid patternTableType_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,6 };

const oid patternTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,4,7};
const size_t patternTable_oid_len   = OID_LENGTH(patternTable_oid);

int maxSplits = 32;
const oid maxSplits_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,8 };

const oid splitTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,4,9};
const size_t splitTable_oid_len   = OID_LENGTH(splitTable_oid);

int   coordPatternStatus;
const oid coordPatternStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,10 };

int   localFreeStatus = 1;
const  oid localFreeStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,11 };

int   coordCycleStatus;
const oid coordCycleStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,12 };

int   coordSyncStatus;
const oid coordSyncStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,13 };

int   systemPatternControl;
const  oid systemPatternControl_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,14 };

int   systemSyncControl;
const  oid systemSyncControl_oid[] = { 1,3,6,1,4,1,1206,4,2,1,4,15 };

 
struct patternTable_entry *entry10[48];
struct splitTable_entry *entry11[32][16];

/*
void get_patterns(int pattern_vacio[])
{
	for (int i=0;i<48;i++)
	{
		entry9[i]->patternCycleTime
}
*/
struct patternTable_entry ** get_pattern_table(void)
{
	return entry10;
}

void get_splits(int _data_split[][16])
{
	for(int i=0;i<32;i++)
	{
		for(int j=0;j<16;j++)
		{
			_data_split[i][j]=entry11[i][j]->splitTime;
		}
	}
}

void get_split_table (struct splitTable_entry _empty_table[][16])
{
    for(int i=0;i<32;i++)
    {
	for(int j=0;j<16;j++)
	{
	    _empty_table[i][j].splitTime = entry11[i][j]->splitTime;
	    _empty_table[i][j].splitMode = entry11[i][j]->splitMode;
	    _empty_table[i][j].splitCoordPhase = entry11[i][j]->splitCoordPhase;
	}
    }
}

void set_split_table (struct splitTable_entry _empty_table[][16])
{
	for(int i=0;i<32;i++)
	{
		for(int j=0;j<16;j++)
		{
			entry11[i][j]->splitTime = _empty_table[i][j].splitTime;
			entry11[i][j]->splitMode = _empty_table[i][j].splitMode;
			entry11[i][j]->splitCoordPhase = _empty_table[i][j].splitCoordPhase;
		}
	}
}

int get_coord_variable(int _numero)
{
	int _resp=-1;

	if(_numero==1)
	{
		_resp=coordOperationalMode;
	}
	else if(_numero==2)
	{
		_resp=coordCorrectionMode;
	}
	else if(_numero==3)
	{
		_resp=coordMaximumMode;
	}
	else if(_numero==4)
	{
		_resp=coordForceMode;
	}
	else if(_numero==6)
	{
		_resp=patternTableType;
	}
	else if(_numero==10)
	{
		_resp=coordPatternStatus;
	}
	else if(_numero==11)
	{
		_resp=localFreeStatus;
	}
	else if(_numero==12)
	{
		_resp=coordCycleStatus;
	}
	else if(_numero==13)
	{
		_resp=coordSyncStatus;
	}
	else if(_numero==14)
	{
		_resp=systemPatternControl;
	}
	else if(_numero==15)
	{
		_resp=systemSyncControl;
	}
	else if (_numero==16)
	{
		_resp=maxPatterns;
	}	
	else if (_numero==17)
	{
		_resp=maxSplits;
	}

	return _resp;
}

void set_coord_variable(int _numero, int _valor)
{
	int _resp=-1;

	if(_numero==1)
	{
		coordOperationalMode=_valor;
	}
	else if(_numero==2)
	{
		coordCorrectionMode=_valor;
	}
	else if(_numero==3)
	{
		coordMaximumMode=_valor;
	}
	else if(_numero==4)
	{
		coordForceMode=_valor;
	}
	else if(_numero==6)
	{
		patternTableType=_valor;
	}
	else if(_numero==10)
	{
		coordPatternStatus=_valor;
	}
	else if(_numero==11)
	{
		localFreeStatus=_valor;
	}
	else if(_numero==12)
	{
		coordCycleStatus=_valor;
	}
	else if(_numero==13)
	{
		coordSyncStatus=_valor;
	}
	else if(_numero==14)
	{
		systemPatternControl=_valor;
	}
	else if(_numero==15)
	{
		systemSyncControl=_valor;
	}
}

/* create a new row in the table */
netsnmp_tdata_row *
coord_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int indice2, int tabla) 
{
    netsnmp_tdata_row *row;

	int i=0;

	switch(tabla)
	{
		case 10:

    	entry10[indice] = SNMP_MALLOC_TYPEDEF(struct patternTable_entry);
    	if (!entry10[indice])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry10[indice]);
	        return NULL;
	    }
		entry10[indice]->patternSplitNumber    = 1;
		entry10[indice]->patternSequenceNumber = 1;
    	row->data = entry10[indice];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry10[indice]->patternNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry10[indice]->patternNumber),
                                 sizeof(entry10[indice]->patternNumber));

		break;
		case 11:

		entry11[indice][indice2] = SNMP_MALLOC_TYPEDEF(struct splitTable_entry);
   	 	if (!entry11[indice][indice2]) return NULL;

    	row = netsnmp_tdata_create_row();

    	if (!row) 
		{
        	SNMP_FREE(entry11[indice][indice2]);
        	return NULL;
    	}

		entry11[indice][indice2]->splitMode = 2;	
    	row->data = entry11[indice][indice2];

    	DEBUGMSGT(("splitTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry11[indice][indice2]->splitNumber = indice+1;
   	 	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry11[indice][indice2]->splitNumber),
                                 sizeof(entry11[indice][indice2]->splitNumber));
		
    	entry11[indice][indice2]->splitPhase = indice2+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 	&(entry11[indice][indice2]->splitPhase),
                                 	sizeof(entry11[indice][indice2]->splitPhase));


		
		break;

		
    }
			if (tabla_data)
        	netsnmp_tdata_add_row( tabla_data, row );
        return row;

    
}

void
initialize_coord_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	unsigned char min_columna, unsigned char max_columna, unsigned char tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i=0, n=0, j=0;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 10:
		reg = netsnmp_create_handler_registration(
              nombre,     patternTable_handler,
              patternTable_oid, patternTable_oid_len,
              HANDLER_CAN_RWRITE
              );
		n=maxPatterns;
		break;
		case 11:
		reg = netsnmp_create_handler_registration(
              nombre,     splitTable_handler,
              splitTable_oid, splitTable_oid_len,
              HANDLER_CAN_RWRITE
              );
		n=maxSplits;
		break;

	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }

	if(tabla==11)
	{
    	netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: splitNumber */
						   ASN_INTEGER,  /* index: splitPhase */
                           0);
	}
	else
	{
		netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: splitNumber */
                           0);
	}

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		
		if(tabla==11)
		{
				for(j=0;j<16;j++)
				{
					row=coord_tables_createEntry(table_data, i, j, tabla);
				}
		}
		else row=coord_tables_createEntry(table_data, i, j, tabla);
	}
}

void
init_coord(void)
{
	/* here we initialize all the tables we're planning on supporting */
		DEBUGMSGTL(("coordOperationalMode", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordOperationalMode", handle_coord,
                               coordOperationalMode_oid, 
							   OID_LENGTH(coordOperationalMode_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("coordCorrectionMode", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordCorrectionMode", handle_coord,
                               coordCorrectionMode_oid, 
							   OID_LENGTH(coordCorrectionMode_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("coordMaximumMode", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordMaximumMode", handle_coord,
                               coordMaximumMode_oid, 
							   OID_LENGTH(coordMaximumMode_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("coordForceMode", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordForceMode", handle_coord,
                               coordForceMode_oid, 
							   OID_LENGTH(coordForceMode_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("maxPatterns", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxPatterns", handle_coord,
                               maxPatterns_oid, 
							   OID_LENGTH(maxPatterns_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("patternTableType", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("patternTableType", handle_coord,
                               patternTableType_oid, 
							   OID_LENGTH(patternTableType_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("maxSplits", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxSplits", handle_coord,
                               maxSplits_oid, 
							   OID_LENGTH(maxSplits_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("coordPatternStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordPatternStatus", handle_coord,
                               coordPatternStatus_oid, 
							   OID_LENGTH(coordPatternStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("localFreeStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("localFreeStatus", handle_coord,
                               localFreeStatus_oid, 
							   OID_LENGTH(localFreeStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("coordCycleStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordCycleStatus", handle_coord,
                               coordCycleStatus_oid, 
							   OID_LENGTH(coordCycleStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("coordSyncStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("coordSyncStatus", handle_coord,
                               coordSyncStatus_oid, 
							   OID_LENGTH(coordSyncStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("systemPatternControl", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("systemPatternControl", handle_coord,
                               systemPatternControl_oid, 
							   OID_LENGTH(systemPatternControl_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("systemSyncControl", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("systemSyncControl", handle_coord,
                               systemSyncControl_oid, 
							   OID_LENGTH(systemSyncControl_oid),
                               HANDLER_CAN_RWRITE));
  
	initialize_coord_tables("patternTable",patternTable_oid,
patternTable_oid_len, COLUMN_PATTERNNUMBER, COLUMN_PATTERNSEQUENCENUMBER,10);

	initialize_coord_tables("splitTable",splitTable_oid,
splitTable_oid_len, COLUMN_SPLITNUMBER, COLUMN_SPLITCOORDPHASE,11);

 
}

int
handle_coord(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
    int ret;
    int  resul=0;
	unsigned char tcc[16],tr2[100];
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
{

        case MODE_GET:
			resul=compara_oid(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&coordOperationalMode,
				sizeof(int));
				break;
				case 2:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&coordCorrectionMode,
				sizeof(int));
				break;
				case 3:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&coordMaximumMode,sizeof(int));
				break;
				case 4:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&coordForceMode,sizeof(int));
				break;
				case 5:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxPatterns,sizeof(int));
				break;
				case 6:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&patternTableType,sizeof(int));
				break;
				case 8:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxSplits,sizeof(int));
				break;
				case 10:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&coordPatternStatus,
				sizeof(int));
				break;
				case 11:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&localFreeStatus,sizeof(int));
				break;
				case 12:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER, &coordCycleStatus, sizeof(int));
				break;
				case 13:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER, &coordSyncStatus, sizeof(int));
				break;
				case 14:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER, &systemPatternControl, sizeof(int));
				break;
				case 15:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER, &systemSyncControl, sizeof(int));
				break;
				
			}
            break;

        /*
         * SET REQUEST
         *
         * multiple states in the transaction.  See:
         * http://www.net-snmp.org/tutorial-5/toolkit/mib_module/set-actions.jpg
         */
        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_INTEGER);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:
            /* XXX malloc "undo" storage buffer */
   //         if (/* XXX if malloc, or whatever, failed: */) {
   //             netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_RESOURCEUNAVAILABLE);
   //         }
            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
	    resul=compara_oid(requests->requestvb->name);
	    if(resul == 1)
	    {
		coordOperationalMode=*requests->requestvb->val.integer;
		if(coordOperationalMode>0 && coordOperationalMode<255)
		{	
		    set_variable(2,coordOperationalMode);
		}
		else if(coordOperationalMode == 255)
		{
		    set_variable(2,0xF0);
		}
		else if(coordOperationalMode == 0)
		{
		    set_variable(2,systemPatternControl);
		}
	    }
	    else if(resul == 2)
	    {
		coordCorrectionMode=*requests->requestvb->val.integer;
	    }
	    else if(resul == 3)
	    {
		coordMaximumMode=*requests->requestvb->val.integer;
	    }
	    else if(resul == 4)
	    {
		coordForceMode=*requests->requestvb->val.integer;
	    }
	    else if(resul == 14)
	    {
		systemPatternControl=*requests->requestvb->val.integer;
		if(coordOperationalMode == 0)
		{
		   set_variable(2,systemPatternControl);
		}
	    }
	    else if(resul == 15)
	    {
		systemSyncControl=*requests->requestvb->val.integer;
	    }
	break;
        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */
     //       if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_COMMITFAILED);
    //        }
            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */
    //        if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_UNDOFAILED);
    //        }
            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_unitRedRevert\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    }

    return SNMP_ERR_NOERROR;
}


/** handles requests for the patternTable table */
int
patternTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct patternTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("patternTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct patternTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PATTERNNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->patternNumber);
                break;
            case COLUMN_PATTERNCYCLETIME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->patternCycleTime);
                break;
            case COLUMN_PATTERNOFFSETTIME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->patternOffsetTime);
                break;
            case COLUMN_PATTERNSPLITNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->patternSplitNumber);
                break;
            case COLUMN_PATTERNSEQUENCENUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->patternSequenceNumber);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct patternTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PATTERNCYCLETIME:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PATTERNOFFSETTIME:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PATTERNSPLITNUMBER:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PATTERNSEQUENCENUMBER:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct patternTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PATTERNCYCLETIME:
                table_entry->old_patternCycleTime = table_entry->patternCycleTime;
                table_entry->patternCycleTime     = *request->requestvb->val.integer;
				set_secuencia_programacion(8);
                break;
            case COLUMN_PATTERNOFFSETTIME:
                table_entry->old_patternOffsetTime = table_entry->patternOffsetTime;
                table_entry->patternOffsetTime     = *request->requestvb->val.integer;
				set_secuencia_programacion(9);
                break;
			case COLUMN_PATTERNSPLITNUMBER:
				table_entry->old_patternSplitNumber = table_entry->patternSplitNumber;
                table_entry->patternSplitNumber     = *request->requestvb->val.integer;
				set_secuencia_programacion(10);
				break;
            case COLUMN_PATTERNSEQUENCENUMBER:
                table_entry->old_patternSequenceNumber = table_entry->patternSequenceNumber;
                table_entry->patternSequenceNumber     = *request->requestvb->val.integer;
				set_secuencia_programacion(11);
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct patternTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PATTERNCYCLETIME:
                table_entry->patternCycleTime     = table_entry->old_patternCycleTime;
                table_entry->old_patternCycleTime = 0;
                break;
            case COLUMN_PATTERNOFFSETTIME:
                table_entry->patternOffsetTime     = table_entry->old_patternOffsetTime;
                table_entry->old_patternOffsetTime = 0;
                break;
            case COLUMN_PATTERNSPLITNUMBER:
                table_entry->patternSplitNumber     = table_entry->old_patternSplitNumber;
                table_entry->old_patternSplitNumber = 0;
                break;
            case COLUMN_PATTERNSEQUENCENUMBER:
                table_entry->patternSequenceNumber     = table_entry->old_patternSequenceNumber;
                table_entry->old_patternSequenceNumber = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the splitTable table */
int
splitTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct splitTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("splitTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct splitTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPLITNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->splitNumber);
                break;
            case COLUMN_SPLITPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->splitPhase);
                break;
            case COLUMN_SPLITTIME:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->splitTime);
                break;
            case COLUMN_SPLITMODE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->splitMode);
                break;
            case COLUMN_SPLITCOORDPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->splitCoordPhase);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct splitTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPLITTIME:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_SPLITMODE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_SPLITCOORDPHASE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct splitTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPLITTIME:
                table_entry->old_splitTime = table_entry->splitTime;
                table_entry->splitTime     = *request->requestvb->val.integer;
				set_secuencia_programacion(12);
                break;
            case COLUMN_SPLITMODE:
                table_entry->old_splitMode = table_entry->splitMode;
                table_entry->splitMode     = *request->requestvb->val.integer;
                break;
            case COLUMN_SPLITCOORDPHASE:
                table_entry->old_splitCoordPhase = table_entry->splitCoordPhase;
                table_entry->splitCoordPhase     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct splitTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPLITTIME:
                table_entry->splitTime     = table_entry->old_splitTime;
                table_entry->old_splitTime = 0;
                break;
            case COLUMN_SPLITMODE:
                table_entry->splitMode     = table_entry->old_splitMode;
                table_entry->old_splitMode = 0;
                break;
            case COLUMN_SPLITCOORDPHASE:
                table_entry->splitCoordPhase     = table_entry->old_splitCoordPhase;
                table_entry->old_splitCoordPhase = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}

