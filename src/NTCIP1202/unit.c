#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"

//unit
int  unitStartUpFlash = 0;
const  oid unitStartUpFlash_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,1 };

int   unitAutoPedestrianClear = 0;
const  oid unitAutoPedestrianClear_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,2 };

unsigned int  unitBackupTime = 0;
const  oid unitBackupTime_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,3 };

int   unitRedRevert = 0;
const  oid unitRedRevert_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,4 };

int  unitControlStatus=0;
const  oid unitControlStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,5 };

int  unitFlashStatus=0;
const oid unitFlashStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,6 };

int   unitAlarmStatus2 = 0;
const oid unitAlarmStatus2_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,7 };

int   unitAlarmStatus1 = 0;
const oid unitAlarmStatus1_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,8 };

int   shortAlarmStatus = 0;
const  oid shortAlarmStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,9 };

int   unitControl = 0;
const oid unitControl_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,10 };

int   maxAlarmGroups = 19;
const oid maxAlarmGroups_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,11 };

const oid alarmGroupTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,3,12};
const size_t alarmGroupTable_oid_len   = OID_LENGTH(alarmGroupTable_oid);

int  maxSpecialFunctionOutputs = 24;
const  oid maxSpecialFunctionOutputs_oid[] = { 1,3,6,1,4,1,1206,4,2,1,3,13 };

const oid specialFunctionOutputTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,3,14};
const size_t specialFunctionOutputTable_oid_len   = OID_LENGTH(specialFunctionOutputTable_oid);

struct alarmGroupTable_entry *entry8[19];
struct specialFunctionOutputTable_entry *entry9[24];

int get_unit_variable(int _numero)
{
	int _resp=-1;

	if(_numero==1)
	{
		_resp=unitStartUpFlash;
	}
	else if(_numero==2)
	{
		_resp=unitAutoPedestrianClear;
	}
	else if(_numero==3)
	{
		_resp=unitRedRevert;
	}

	return _resp;
}

void set_unit_variable(int _numero, int valor)
{
	int _resp=-1;

	if(_numero==1)
	{
		unitStartUpFlash=valor;
	}
	else if(_numero==2)
	{
		unitAutoPedestrianClear=valor;
	}
	else if(_numero==3)
	{
		unitRedRevert=valor;
	}

}

void set_unit_variable2(int _numero, unsigned int valor)
{
	unsigned int _resp=-1;

	if(_numero==1)
	{
	    unitBackupTime=valor;
	}
}


unsigned int get_unit_variable2(int _numero)
{
	unsigned int _resp=-1;
	if(_numero==1)
	{
	    _resp=unitBackupTime;
	}

	return _resp;
}

struct alarmGroupTable_entry ** get_alarmGroup_table(void)
{
    return entry8;
}


/* create a new row in the table */
netsnmp_tdata_row *
unit_tables_createEntry(netsnmp_tdata *tabla_data, unsigned char  indice, unsigned char tabla) 
{
    netsnmp_tdata_row *row;


   
	switch(tabla)
	{
		case 8:

 		entry8[indice] = SNMP_MALLOC_TYPEDEF(struct alarmGroupTable_entry);
   		if (!entry8[indice])
        return NULL;

   		row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry8[indice]);
	        return NULL;
	    }
	    row->data = entry8[indice];

	    DEBUGMSGT(("alarmGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
	    entry8[indice]->alarmGroupNumber = indice+1;
	    netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry8[indice]->alarmGroupNumber),
                                 sizeof(entry8[indice]->alarmGroupNumber));
		break;
		case 9:

   		entry9[indice] = SNMP_MALLOC_TYPEDEF(struct specialFunctionOutputTable_entry);
    	if (!entry9[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry9[indice]);
	        return NULL;
	    }
	    row->data = entry9[indice];
	    DEBUGMSGT(("specialFunctionOutputTable:entry:create", "row 0x%x\n", (uintptr_t)row));
	    entry9[indice]->specialFunctionOutputNumber = indice+1;
	    netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry9[indice]->specialFunctionOutputNumber),
                                 sizeof(entry9[indice]->specialFunctionOutputNumber));

		break;
		
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

/** Initialize the phaseTable table by defining its contents and how it's structured */
void
initialize_unit_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	unsigned int min_columna, unsigned int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 8:
		reg = netsnmp_create_handler_registration( nombre, alarmGroupTable_handler,
 		oid_table, oid_len, HANDLER_CAN_RONLY);
		n=maxAlarmGroups;
		break;
		case 9:
		reg = netsnmp_create_handler_registration( nombre, specialFunctionOutputTable_handler,
 		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxSpecialFunctionOutputs;
		break;
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=unit_tables_createEntry(table_data, i, tabla);
	}
}

void
init_unit(void)
{
	DEBUGMSGTL(("unitStartUpFlash", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitStartUpFlash", handle_unit,
                               unitStartUpFlash_oid, 
							   OID_LENGTH(unitStartUpFlash_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("unitAutoPedestrianClear", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitAutoPedestrianClear", handle_unit,
                               unitAutoPedestrianClear_oid, 
							   OID_LENGTH(unitAutoPedestrianClear_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("unitBackupTime", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitBackupTime", handle_unit,
                               unitBackupTime_oid, 
							   OID_LENGTH(unitBackupTime_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("unitRedRevert", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitRedRevert", handle_unit,
                               unitRedRevert_oid, 
							   OID_LENGTH(unitRedRevert_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("unitControlStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitControlStatus", handle_unit,
                               unitControlStatus_oid, 
							   OID_LENGTH(unitControlStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("unitFlashStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitFlashStatus", handle_unit,
                               unitFlashStatus_oid, 
							   OID_LENGTH(unitFlashStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("unitAlarmStatus2", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitAlarmStatus2", handle_unit,
                               unitAlarmStatus2_oid, 
							   OID_LENGTH(unitAlarmStatus2_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("unitAlarmStatus1", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitAlarmStatus1", handle_unit,
                               unitAlarmStatus1_oid, 
							   OID_LENGTH(unitAlarmStatus1_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("shortAlarmStatus", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("shortAlarmStatus", handle_unit,
                               shortAlarmStatus_oid, 
							   OID_LENGTH(shortAlarmStatus_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("unitControl", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("unitControl", handle_unit,
                               unitControl_oid, 
							   OID_LENGTH(unitControl_oid),
                               HANDLER_CAN_RWRITE));

	DEBUGMSGTL(("maxAlarmGroups", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxAlarmGroups", handle_unit,
                               maxAlarmGroups_oid, 
							   OID_LENGTH(maxAlarmGroups_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("maxSpecialFunctionOutputs", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxSpecialFunctionOutputs", handle_unit,
                               maxSpecialFunctionOutputs_oid, 
							   OID_LENGTH(maxSpecialFunctionOutputs_oid),
                               HANDLER_CAN_RONLY));
  
	initialize_unit_tables("alarmGroupTable",alarmGroupTable_oid,
alarmGroupTable_oid_len, COLUMN_ALARMGROUPNUMBER, COLUMN_ALARMGROUPSTATE,8);

	initialize_unit_tables("specialFunctionOutputTable",specialFunctionOutputTable_oid,
specialFunctionOutputTable_oid_len, COLUMN_SPECIALFUNCTIONOUTPUTNUMBER, 
COLUMN_SPECIALFUNCTIONOUTPUTSTATUS,9);



}

//--------------------------Manejadores de objetos------------------------------

int
handle_unit(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
    int ret;
    int  resul=0;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
{

        case MODE_GET:
			resul=compara_oid(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitStartUpFlash,sizeof(int));
				break;
				case 2:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitAutoPedestrianClear,sizeof(int));
				break;
				case 3:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitBackupTime,sizeof(int));
				break;
				case 4:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitRedRevert,sizeof(int));
				break;
				case 5:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitControlStatus,sizeof(int));
				break;
				case 6:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitFlashStatus,sizeof(int));
				break;
				case 7:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitAlarmStatus2,sizeof(int));
				break;
				case 8:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitAlarmStatus1,sizeof(int));
				break;
				case 9:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&shortAlarmStatus,sizeof(int));
				break;
				case 10:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&unitControl,sizeof(int));
				break;
				case 11:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxAlarmGroups,sizeof(int));
				break;
				case 13:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER, &maxSpecialFunctionOutputs, sizeof(int));
				break;
			}

            break;

        /*
         * SET REQUEST
         *
         * multiple states in the transaction.  See:
         * http://www.net-snmp.org/tutorial-5/toolkit/mib_module/set-actions.jpg
         */
        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_INTEGER);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:
            /* XXX malloc "undo" storage buffer */
   //         if (/* XXX if malloc, or whatever, failed: */) {
   //             netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_RESOURCEUNAVAILABLE);
   //         }
            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
  			resul=compara_oid(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				unitStartUpFlash=*requests->requestvb->val.integer;
				set_secuencia_programacion(6);
				break;
				case 2:
				unitAutoPedestrianClear=*requests->requestvb->val.integer;
				break;
				case 3:
				unitBackupTime=*requests->requestvb->val.integer;
				break;
				case 4:
				unitRedRevert=*requests->requestvb->val.integer;
				break;
				case 10:
				unitControl=*requests->requestvb->val.integer;
			    break;
			}
        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */
     //       if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_COMMITFAILED);
    //        }
            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */
    //        if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_UNDOFAILED);
    //        }
            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_unitRedRevert\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    }

    return SNMP_ERR_NOERROR;
}


/** handles requests for the alarmGroupTable table */
int
alarmGroupTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct alarmGroupTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("alarmGroupTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct alarmGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_ALARMGROUPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->alarmGroupNumber);
                break;
            case COLUMN_ALARMGROUPSTATE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->alarmGroupState);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the specialFunctionOutputTable table */
int
specialFunctionOutputTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct specialFunctionOutputTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("specialFunctionOutputTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct specialFunctionOutputTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPECIALFUNCTIONOUTPUTNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->specialFunctionOutputNumber);
                break;
            case COLUMN_SPECIALFUNCTIONOUTPUTCONTROL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->specialFunctionOutputControl);
                break;
            case COLUMN_SPECIALFUNCTIONOUTPUTSTATUS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->specialFunctionOutputStatus);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct specialFunctionOutputTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPECIALFUNCTIONOUTPUTCONTROL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct specialFunctionOutputTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPECIALFUNCTIONOUTPUTCONTROL:
                table_entry->old_specialFunctionOutputControl = table_entry->specialFunctionOutputControl;
                table_entry->specialFunctionOutputControl     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct specialFunctionOutputTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_SPECIALFUNCTIONOUTPUTCONTROL:
                table_entry->specialFunctionOutputControl= table_entry->old_specialFunctionOutputControl;
                table_entry->old_specialFunctionOutputControl = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}

