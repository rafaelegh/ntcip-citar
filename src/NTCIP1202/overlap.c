
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"

//Declaracion de variables

int maxOverlaps = 32;
int maxOverlapStatusGroups = 4;

/** Initialize the phaseTable table by defining its contents and how it's structured */
//* Typical data structure for a row entry */


struct overlapTable_entry *entry20[32];
struct overlapStatusGroupTable_entry *entry21[4];

struct overlapTable_entry ** get_overlap_table(void)
{
	return entry20;
}

int get_overlap_variable(int _n)
{
	int resp=-1;
	
	if(_n == 1) resp = maxOverlaps;
	else if(_n==2) resp = maxOverlapStatusGroups;

	return resp;
}
 
/* create a new row in the table */
netsnmp_tdata_row *
overlap_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int tabla) 
{
    netsnmp_tdata_row *row;
	
	switch(tabla)
	{
		case 20:
		
    	entry20[indice] = SNMP_MALLOC_TYPEDEF(struct overlapTable_entry);
    	if (!entry20[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry20[indice]);
        	return NULL;
    	}
		entry20[indice]->overlapType = 1;
    	row->data = entry20[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry20[indice]->overlapNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry20[indice]->overlapNumber),
                                 sizeof(entry20[indice]->overlapNumber));
		
  		break;

		case 21:
		
    	entry21[indice] = SNMP_MALLOC_TYPEDEF(struct overlapStatusGroupTable_entry);
    	if (!entry21[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry21[indice]);
        	return NULL;
    	}
    	row->data = entry21[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry21[indice]->overlapStatusGroupNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry21[indice]->overlapStatusGroupNumber),
                                 sizeof(entry21[indice]->overlapStatusGroupNumber));
		
  		break;
	
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_overlap_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	int min_columna, int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 20:
    	reg = netsnmp_create_handler_registration( nombre, overlapTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxOverlaps;
		break;
		case 21:
    	reg = netsnmp_create_handler_registration( nombre, overlapStatusGroupTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RONLY);
		n=maxOverlapStatusGroups;
		break;	
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=overlap_tables_createEntry(table_data, i, tabla);
	}
}

void
init_overlap(void)
{
    const oid maxOverlaps_oid[] = { 1,3,6,1,4,1,1206,4,2,1,9,1 };

    const oid overlapTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,9,2};
    const size_t overlapTable_oid_len   = OID_LENGTH(overlapTable_oid);

    const oid maxOverlapStatusGroups_oid[] = { 1,3,6,1,4,1,1206,4,2,1,9,3 };

    const oid overlapStatusGroupTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,9,4};
    const size_t overlapStatusGroupTable_oid_len   = OID_LENGTH(overlapStatusGroupTable_oid);

  	DEBUGMSGTL(("maxOverlaps", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxOverlaps", handle_overlap,
                               maxOverlaps_oid, OID_LENGTH(maxOverlaps_oid),
                               HANDLER_CAN_RONLY
        ));

	initialize_overlap_tables("overlapTable",overlapTable_oid,overlapTable_oid_len,
				COLUMN_OVERLAPNUMBER, COLUMN_OVERLAPTRAILRED,20);

  	DEBUGMSGTL(("maxOverlapStatusGroups", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxOverlapStatusGroups", handle_overlap,
                               maxOverlapStatusGroups_oid, OID_LENGTH(maxOverlapStatusGroups_oid),
                               HANDLER_CAN_RONLY
        ));


	initialize_overlap_tables("overlapStatusGroupTable",overlapStatusGroupTable_oid,overlapStatusGroupTable_oid_len,
				COLUMN_OVERLAPSTATUSGROUPNUMBER, COLUMN_OVERLAPSTATUSGROUPGREENS,21);
}

int
handle_overlap(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
	{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxOverlaps
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}

			else if(resul==3)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxOverlapStatusGroups
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
        break;
  

    }

    return SNMP_ERR_NOERROR;
}

/** handles requests for the overlapTable table */
int
overlapTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct overlapTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("overlapTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapNumber);
                break;
            case COLUMN_OVERLAPTYPE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapType);
                break;
            case COLUMN_OVERLAPINCLUDEDPHASES:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->overlapIncludedPhases,
                                          table_entry->overlapIncludedPhases_len);
                break;
            case COLUMN_OVERLAPMODIFIERPHASES:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->overlapModifierPhases,
                                          table_entry->overlapModifierPhases_len);
                break;
            case COLUMN_OVERLAPTRAILGREEN:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapTrailGreen);
                break;
            case COLUMN_OVERLAPTRAILYELLOW:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapTrailYellow);
                break;
            case COLUMN_OVERLAPTRAILRED:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapTrailRed);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPTYPE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_OVERLAPINCLUDEDPHASES:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->overlapIncludedPhases));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_OVERLAPMODIFIERPHASES:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->overlapModifierPhases));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_OVERLAPTRAILGREEN:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_OVERLAPTRAILYELLOW:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_OVERLAPTRAILRED:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPTYPE:
                table_entry->old_overlapType = table_entry->overlapType;
                table_entry->overlapType     = *request->requestvb->val.integer;
                break;
            case COLUMN_OVERLAPINCLUDEDPHASES:
                memcpy( table_entry->old_overlapIncludedPhases,
                        table_entry->overlapIncludedPhases,
                        sizeof(table_entry->overlapIncludedPhases));
                table_entry->old_overlapIncludedPhases_len =
                        table_entry->overlapIncludedPhases_len;
                memset( table_entry->overlapIncludedPhases, 0,
                        sizeof(table_entry->overlapIncludedPhases));
                memcpy( table_entry->overlapIncludedPhases,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->overlapIncludedPhases_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_OVERLAPMODIFIERPHASES:
                memcpy( table_entry->old_overlapModifierPhases,
                        table_entry->overlapModifierPhases,
                        sizeof(table_entry->overlapModifierPhases));
                table_entry->old_overlapModifierPhases_len =
                        table_entry->overlapModifierPhases_len;
                memset( table_entry->overlapModifierPhases, 0,
                        sizeof(table_entry->overlapModifierPhases));
                memcpy( table_entry->overlapModifierPhases,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->overlapModifierPhases_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_OVERLAPTRAILGREEN:
                table_entry->old_overlapTrailGreen = table_entry->overlapTrailGreen;
                table_entry->overlapTrailGreen     = *request->requestvb->val.integer;
                break;
            case COLUMN_OVERLAPTRAILYELLOW:
                table_entry->old_overlapTrailYellow = table_entry->overlapTrailYellow;
                table_entry->overlapTrailYellow     = *request->requestvb->val.integer;
                break;
            case COLUMN_OVERLAPTRAILRED:
                table_entry->old_overlapTrailRed = table_entry->overlapTrailRed;
                table_entry->overlapTrailRed     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPTYPE:
                table_entry->overlapType     = table_entry->old_overlapType;
                table_entry->old_overlapType = 0;
                break;
            case COLUMN_OVERLAPINCLUDEDPHASES:
                memcpy( table_entry->overlapIncludedPhases,
                        table_entry->old_overlapIncludedPhases,
                        sizeof(table_entry->overlapIncludedPhases));
                memset( table_entry->old_overlapIncludedPhases, 0,
                        sizeof(table_entry->overlapIncludedPhases));
                table_entry->overlapIncludedPhases_len =
                        table_entry->old_overlapIncludedPhases_len;
                break;
            case COLUMN_OVERLAPMODIFIERPHASES:
                memcpy( table_entry->overlapModifierPhases,
                        table_entry->old_overlapModifierPhases,
                        sizeof(table_entry->overlapModifierPhases));
                memset( table_entry->old_overlapModifierPhases, 0,
                        sizeof(table_entry->overlapModifierPhases));
                table_entry->overlapModifierPhases_len =
                        table_entry->old_overlapModifierPhases_len;
                break;
            case COLUMN_OVERLAPTRAILGREEN:
                table_entry->overlapTrailGreen     = table_entry->old_overlapTrailGreen;
                table_entry->old_overlapTrailGreen = 0;
                break;
            case COLUMN_OVERLAPTRAILYELLOW:
                table_entry->overlapTrailYellow     = table_entry->old_overlapTrailYellow;
                table_entry->old_overlapTrailYellow = 0;
                break;
            case COLUMN_OVERLAPTRAILRED:
                table_entry->overlapTrailRed     = table_entry->old_overlapTrailRed;
                table_entry->old_overlapTrailRed = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}


/** handles requests for the overlapStatusGroupTable table */
int
overlapStatusGroupTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct overlapStatusGroupTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("overlapStatusGroupTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapStatusGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPSTATUSGROUPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapStatusGroupNumber);
                break;
            case COLUMN_OVERLAPSTATUSGROUPREDS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapStatusGroupReds);
                break;
            case COLUMN_OVERLAPSTATUSGROUPYELLOWS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapStatusGroupYellows);
                break;
            case COLUMN_OVERLAPSTATUSGROUPGREENS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapStatusGroupGreens);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}
