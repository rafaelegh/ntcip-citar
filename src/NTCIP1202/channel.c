#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"
//channel

int   maxChannels = 24;
const oid maxChannels_oid[] = { 1,3,6,1,4,1,1206,4,2,1,8,1 };

const oid channelTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,8,2};
const size_t channelTable_oid_len= OID_LENGTH(channelTable_oid);

int   maxChannelStatusGroups = 3;
const  oid maxChannelStatusGroups_oid[] = { 1,3,6,1,4,1,1206,4,2,1,8,3 };

const oid channelStatusGroupTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,8,4};
const size_t channelStatusGroupTable_oid_len= OID_LENGTH(channelStatusGroupTable_oid);

struct channelTable_entry *entry18[24];
struct channelStatusGroupTable_entry *entry19[3];

struct channelTable_entry ** get_channel_table(void)
{
	return entry18;
}

int get_channel_variable(int _numero)
{
	int _resp=-1;

	if(_numero==1)
	{
		_resp=maxChannels;
	}
	else if(_numero==2)
	{
		_resp=maxChannelStatusGroups;
	}
	return _resp;
}


/* create a new row in the table */
netsnmp_tdata_row *
channel_tables_createEntry(netsnmp_tdata *tabla_data, unsigned int  indice, unsigned int tabla) 
{
    netsnmp_tdata_row *row;


	switch(tabla)
	{
		case 18:

    	entry18[indice] = SNMP_MALLOC_TYPEDEF(struct channelTable_entry);
   		if (!entry18[indice]) return NULL;

    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry18[indice]);
	        return NULL;
	    }
		
		entry18[indice]->channelControlType = 1;
	    row->data = entry18[indice];
	    DEBUGMSGT(("channelTable:entry:create", "row 0x%x\n", (uintptr_t)row));
	    entry18[indice]->channelNumber = indice+1;
	    netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry18[indice]->channelNumber),
                                 sizeof(entry18[indice]->channelNumber));
		break;
		case 19:

	    entry19[indice] = SNMP_MALLOC_TYPEDEF(struct channelStatusGroupTable_entry);
	    if (!entry19[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry19[indice]);
	        return NULL;
    	}
    	row->data = entry19[indice];
    	DEBUGMSGT(("channelStatusGroupTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry19[indice]->channelStatusGroupNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry19[indice]->channelStatusGroupNumber),
                                 sizeof(entry19[indice]->channelStatusGroupNumber));
		break;

    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_channel_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	unsigned char min_columna, unsigned char max_columna, unsigned char tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 18:
		reg = netsnmp_create_handler_registration( nombre, channelTable_handler,
 		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxChannels;
		break;
		case 19:
		reg = netsnmp_create_handler_registration( nombre, channelStatusGroupTable_handler,
 		oid_table, oid_len, HANDLER_CAN_RONLY);
		n=maxChannelStatusGroups;
		break;
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=channel_tables_createEntry(table_data, i, tabla);
	}
}


void init_channel(void)
{
	DEBUGMSGTL(("maxChannels", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxChannels", handle_channel,
                               maxChannels_oid, 
							   OID_LENGTH(maxChannels_oid),
                               HANDLER_CAN_RONLY));

	DEBUGMSGTL(("maxChannelStatusGroups", "Initializing\n"));
    netsnmp_register_scalar(
        		netsnmp_create_handler_registration("maxChannelStatusGroups", handle_channel,
                               maxChannelStatusGroups_oid, 
							   OID_LENGTH(maxChannelStatusGroups_oid),
                               HANDLER_CAN_RONLY));	

	initialize_channel_tables("channelTable",channelTable_oid,
channelTable_oid_len, COLUMN_CHANNELNUMBER, COLUMN_CHANNELDIM,18);

	initialize_channel_tables("channelStatusGroupTable",channelStatusGroupTable_oid,
channelStatusGroupTable_oid_len, COLUMN_CHANNELSTATUSGROUPNUMBER, 
COLUMN_CHANNELSTATUSGROUPGREENS,19);

}

int
handle_channel(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
    int ret;
    int  resul=0;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
{

        case MODE_GET:
			resul=compara_oid(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxChannels,sizeof(long));
				break;
				case 3:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxChannelStatusGroups
				,sizeof(long));
				break;
			}
            break;

        /*
         * SET REQUEST
         *
         * multiple states in the transaction.  See:
         * http://www.net-snmp.org/tutorial-5/toolkit/mib_module/set-actions.jpg
         */
        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_INTEGER);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:
            /* XXX malloc "undo" storage buffer */
   //         if (/* XXX if malloc, or whatever, failed: */) {
   //             netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_RESOURCEUNAVAILABLE);
   //         }
            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
  			resul=compara_oid(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				maxChannels=*requests->requestvb->val.integer;
				break;
				case 3:
				maxChannelStatusGroups=*requests->requestvb->val.integer;
				break;
			}
        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */
     //       if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_COMMITFAILED);
    //        }
            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */
    //        if (/* XXX: error? */) {
                /* try _really_really_ hard to never get to this point */
    //            netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_UNDOFAILED);
    //        }
            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_unitRedRevert\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    }

    return SNMP_ERR_NOERROR;
}

/** handles requests for the channelTable table */
int
channelTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct channelTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("channelTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct channelTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_CHANNELNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelNumber);
                break;
            case COLUMN_CHANNELCONTROLSOURCE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelControlSource);
                break;
            case COLUMN_CHANNELCONTROLTYPE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelControlType);
                break;
            case COLUMN_CHANNELFLASH:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelFlash);
                break;
            case COLUMN_CHANNELDIM:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelDim);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct channelTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_CHANNELCONTROLSOURCE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_CHANNELCONTROLTYPE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_CHANNELFLASH:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_CHANNELDIM:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct channelTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_CHANNELCONTROLSOURCE:
                table_entry->old_channelControlSource = table_entry->channelControlSource;
                table_entry->channelControlSource     = *request->requestvb->val.integer;
				set_secuencia_programacion(14);
                break;
            case COLUMN_CHANNELCONTROLTYPE:
                table_entry->old_channelControlType = table_entry->channelControlType;
                table_entry->channelControlType     = *request->requestvb->val.integer;
				set_secuencia_programacion(15);
                break;
            case COLUMN_CHANNELFLASH:
                table_entry->old_channelFlash = table_entry->channelFlash;
                table_entry->channelFlash     = *request->requestvb->val.integer;
                break;
            case COLUMN_CHANNELDIM:
                table_entry->old_channelDim = table_entry->channelDim;
                table_entry->channelDim     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct channelTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_CHANNELCONTROLSOURCE:
                table_entry->channelControlSource     = table_entry->old_channelControlSource;
                table_entry->old_channelControlSource = 0;
                break;
            case COLUMN_CHANNELCONTROLTYPE:
                table_entry->channelControlType     = table_entry->old_channelControlType;
                table_entry->old_channelControlType = 0;
                break;
            case COLUMN_CHANNELFLASH:
                table_entry->channelFlash     = table_entry->old_channelFlash;
                table_entry->old_channelFlash = 0;
                break;
            case COLUMN_CHANNELDIM:
                table_entry->channelDim     = table_entry->old_channelDim;
                table_entry->old_channelDim = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}

/** handles requests for the channelStatusGroupTable table */
int
channelStatusGroupTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct channelStatusGroupTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("channelStatusGroupTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct channelStatusGroupTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_CHANNELSTATUSGROUPNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelStatusGroupNumber);
                break;
            case COLUMN_CHANNELSTATUSGROUPREDS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelStatusGroupReds);
                break;
            case COLUMN_CHANNELSTATUSGROUPYELLOWS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelStatusGroupYellows);
                break;
            case COLUMN_CHANNELSTATUSGROUPGREENS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->channelStatusGroupGreens);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}
