#include "asc.h"
#include "citar.h"
#include <string.h>
#include "bytes.h"


//Declaracion de variables

char *	ascBlockGetControl=NULL;
size_t	ascBlockGetControl_len=1;
char *	ascBlockGetControl_old=NULL;
size_t	ascBlockGetControl_old_len=1;  

char *	ascBlockData=NULL;
size_t	ascBlockData_len=1;
char *	ascBlockData_old=NULL;
size_t	ascBlockData_old_len=1;
 
int 	ascBlockErrorStatus = 0;

int sec_prog_bloque[5]; 

/** Initialize the phaseTable table by defining its contents and how it's structured */
//* Typical data structure for a row entry */

int get_ascBlock_variable(int _numero)
{
	int _resp=-1;

	if(_numero==1)
	{
		_resp=ascBlockGetControl_len;
	}

	return _resp;
}

int get_secuencia_programacion_bloque(void)
{	
	int cont=0,resp=0;
	for(int x=0;x<5;x++)
	{
		if(sec_prog_bloque[x]==1)
		{
			cont++;
		}
	}

	printf("Secuencia de programacion: \n");

	for(int x=0;x<5;x++)
	{
		printf("%i ",sec_prog_bloque[x]);
	}

	printf("\n");

	if(cont==5) resp=1;
	else 		 resp=0;

	return resp;
}

void limpiar_secuencia_programacion_bloque(void)
{	
	for(int x=0;x<5;x++)
	{
		sec_prog_bloque[x]=0;
	}

}

void get_bloque_phase(void)
{
	int tamano_bloque = 6, indice=0, nFases=0, longitud_conc=0, aux[2],i=0;
	struct phaseTable_entry ** p_phase = NULL;

	p_phase = get_phase_table();
	indice = ascBlockGetControl[2]-1;
	nFases = ascBlockGetControl[3];	

	for(i=indice; i<indice+nFases ;i++)
	{
		tamano_bloque = tamano_bloque + p_phase[i]->phaseConcurrency_len + 23;
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	printf("\n");
	ascBlockData[4] = 0x01;
	ascBlockData[5] = nFases;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;
//	printf("phaseWalk.2: %i, tamano bloque: %i\n",p_phase[1]->phaseWalk,tamano_bloque);
		for(int j=indice; j<indice+nFases; j++)
		{
			ascBlockData[i+6]  = p_phase[j]->phaseWalk;
			ascBlockData[i+7]  = p_phase[j]->phasePedestrianClear;
			ascBlockData[i+8]  = p_phase[j]->phaseMinimumGreen;
    		ascBlockData[i+9]  = p_phase[j]->phasePassage;
   			ascBlockData[i+10] = p_phase[j]->phaseMaximum1;
    		ascBlockData[i+11] = p_phase[j]->phaseMaximum2;
   		 	ascBlockData[i+12] = p_phase[j]->phaseYellowChange;
    		ascBlockData[i+13] = p_phase[j]->phaseRedClear;
    		ascBlockData[i+14] = p_phase[j]->phaseRedRevert;
    		ascBlockData[i+15] = p_phase[j]->phaseAddedInitial;
    		ascBlockData[i+16] = p_phase[j]->phaseMaximumInitial;
    		ascBlockData[i+17] = p_phase[j]->phaseTimeBeforeReduction;
    		ascBlockData[i+18] = p_phase[j]->phaseCarsBeforeReduction;
    		ascBlockData[i+19] = p_phase[j]->phaseTimeToReduce;
    		ascBlockData[i+20] = p_phase[j]->phaseReduceBy;
    		ascBlockData[i+21] = p_phase[j]->phaseMinimumGap;
    		ascBlockData[i+22] = p_phase[j]->phaseDynamicMaxLimit;
    		ascBlockData[i+23] = p_phase[j]->phaseDynamicMaxStep;
    		ascBlockData[i+24] = p_phase[j]->phaseStartup;
			separar_byte_hex(aux,p_phase[j]->phaseOptions);
    		ascBlockData[i+25] = aux[0];
    		ascBlockData[i+26] = aux[1];
    		ascBlockData[i+27] = p_phase[j]->phaseRing;
			ascBlockData[i+28] = p_phase[j]->phaseConcurrency_len;

			longitud_conc = p_phase[j]->phaseConcurrency_len;

			for(int k=0;k<longitud_conc;k++)
			{
				ascBlockData[i+29+k] = p_phase[j]->phaseConcurrency[k];
			}

			i = longitud_conc + 23;
		}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_detector(void)
{
	int tamano_bloque = 6, indice=0, nDetector=0, aux[2],i=0;
	struct vehicleDetectorTable_entry ** p_detector = NULL;

	p_detector = get_vehicleDetector_table();
	indice = ascBlockGetControl[2]-1;
	nDetector = ascBlockGetControl[3];	

	for(i=indice; i<indice+nDetector ;i++)
	{
		tamano_bloque += 11;
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	printf("\n");
	ascBlockData[4] = 0x01;
	ascBlockData[5] = nDetector;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;
//	printf("phaseWalk.2: %i, tamano bloque: %i\n",p_phase[1]->phaseWalk,tamano_bloque);
		for(int j=indice; j<indice+nDetector; j++)
		{
			ascBlockData[i+6]  = p_detector[j]->vehicleDetectorOptions;
			ascBlockData[i+7]  = p_detector[j]->vehicleDetectorCallPhase;
			ascBlockData[i+8]  = p_detector[j]->vehicleDetectorSwitchPhase;
			separar_byte_hex(aux,p_detector[j]->vehicleDetectorDelay);
    		ascBlockData[i+9]  = aux[0];
   			ascBlockData[i+10] = aux[1];
    		ascBlockData[i+11] = p_detector[j]->vehicleDetectorExtend;
   		 	ascBlockData[i+12] = p_detector[j]->vehicleDetectorQueueLimit;
    		ascBlockData[i+13] = p_detector[j]->vehicleDetectorNoActivity;
    		ascBlockData[i+14] = p_detector[j]->vehicleDetectorMaxPresence;
    		ascBlockData[i+15] = p_detector[j]->vehicleDetectorErraticCounts;
    		ascBlockData[i+16] = p_detector[j]->vehicleDetectorFailTime;
  
			i += 11;
		}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_pedestrian(void)
{
	int tamano_bloque = 6, indice=0, nDetector=0, aux[2],i=0;
	struct pedestrianDetectorTable_entry ** p_pedestrian = NULL;

	p_pedestrian = get_pedestrian_table();
	indice = ascBlockGetControl[2]-1;
	nDetector = ascBlockGetControl[3];	

	for(i=indice; i<indice+nDetector ;i++)
	{
		tamano_bloque += 4;
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	printf("\n");
	ascBlockData[4] = 0x01;
	ascBlockData[5] = nDetector;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;
//	printf("phaseWalk.2: %i, tamano bloque: %i\n",p_phase[1]->phaseWalk,tamano_bloque);
		for(int j=indice; j<indice+nDetector; j++)
		{
			ascBlockData[i+6]  = p_pedestrian[j]->pedestrianDetectorCallPhase;
			ascBlockData[i+7]  = p_pedestrian[j]->pedestrianDetectorNoActivity;
			ascBlockData[i+8]  = p_pedestrian[j]->pedestrianDetectorMaxPresence;
    		ascBlockData[i+9]  = p_pedestrian[j]->pedestrianDetectorErraticCounts;  
			i += 4;
		}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_pattern(void)
{
	int tamano_bloque = 6, indice=0, nFases=0, i=0;
	struct patternTable_entry ** p_pattern=NULL;

	p_pattern=get_pattern_table();
	indice = ascBlockGetControl[2]-1;
	nFases = ascBlockGetControl[3];	

	for(i=indice; i<indice+nFases ;i++)
	{
		tamano_bloque = tamano_bloque + 3;
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
	printf("\n");
	ascBlockData[4] = 0x01;
	ascBlockData[5] = nFases;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;
//	printf("phaseWalk.2: %i, tamano bloque: %i\n",p_phase[1]->phaseWalk,tamano_bloque);
		for(int j=indice; j<indice+nFases; j++)
		{
			ascBlockData[i+6] = p_pattern[j]->patternCycleTime;
			ascBlockData[i+7] = p_pattern[j]->patternOffsetTime;
			ascBlockData[i+8] = p_pattern[j]->patternSequenceNumber;

			i += 3 ;
		}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_split(void)
{
	int tamano_bloque = 8,indice_fases=0,cantidad_fases=0,indice_split=0,cantidad_splits=0,i=0;

	struct splitTable_entry t_splits[32][16]; 
	
	get_split_table(t_splits);

	indice_fases = ascBlockGetControl[2]-1;
	cantidad_fases = ascBlockGetControl[3];	
	indice_split = ascBlockGetControl[4]-1;
	cantidad_splits = ascBlockGetControl[5];

	for(int j=indice_split; j<indice_split+cantidad_splits; j++)
	{
		for(i=indice_fases; i<indice_fases+cantidad_fases ;i++)
		{
			tamano_bloque = tamano_bloque + 3;
		}
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<6;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
//	printf("\n");

	ascBlockData[6] = 0x01;
	ascBlockData[7] = cantidad_fases*cantidad_splits;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;

	for(int j=indice_split; j<indice_split+cantidad_splits; j++)
	{
		for(int k=indice_fases; k<indice_fases+cantidad_fases; k++)
		{
			ascBlockData[i+8] = t_splits[j][k].splitTime;
			ascBlockData[i+9] = t_splits[j][k].splitMode;
			ascBlockData[i+10] = t_splits[j][k].splitCoordPhase;
			i += 3;
		}
	}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_timebase(void)
{
	int tamano_bloque = 6,indice_timebase=0,cantidad_timebase=0,i=0;

	struct timebaseAscActionTable_entry ** p_timebase=NULL;
	
	p_timebase = get_timebaseAsc_table();

	indice_timebase = ascBlockGetControl[2]-1;
	cantidad_timebase = ascBlockGetControl[3];	

	for(i=indice_timebase; i<indice_timebase+cantidad_timebase ;i++)
	{
		tamano_bloque = tamano_bloque + 3;
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);

	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	ascBlockData[4] = 0x01;
	ascBlockData[5] = cantidad_timebase;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;

	for(int k=indice_timebase; k<indice_timebase + cantidad_timebase; k++)
	{
		ascBlockData[i+6] = p_timebase[k]->timebaseAscPattern;
		ascBlockData[i+7] = p_timebase[k]->timebaseAscAuxillaryFunction;
		ascBlockData[i+8] = p_timebase[k]->timebaseAscSpecialFunction;
		i += 3;
	}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_preempt(void)
{
	int tamano_bloque = 6,indice_preempt=0,cantidad_preempt=0,i=0, aux[2],longitud=0,auxi=0;

	struct preemptTable_entry ** p_preempt=NULL;
	
	p_preempt = get_preempt_table();

	indice_preempt = ascBlockGetControl[2]-1;
	cantidad_preempt = ascBlockGetControl[3];	

	for(i=indice_preempt; i<indice_preempt+cantidad_preempt ;i++)
	{
		auxi=0;
		auxi += p_preempt[i]->preemptTrackPhase_len;
		auxi += p_preempt[i]->preemptDwellPhase_len;
		auxi +=	p_preempt[i]->preemptDwellPed_len;
		auxi +=	p_preempt[i]->preemptExitPhase_len;
		auxi +=	p_preempt[i]->preemptTrackOverlap_len;
		auxi +=	p_preempt[i]->preemptDwellOverlap_len;
		auxi +=	p_preempt[i]->preemptCyclingPhase_len;
		auxi +=	p_preempt[i]->preemptCyclingPed_len;
		auxi +=	p_preempt[i]->preemptCyclingOverlap_len;
		tamano_bloque = tamano_bloque +27+auxi;
		//printf("tamaño bloque: %i %i %i\n",tamano_bloque,auxi,indice_preempt+cantidad_preempt);
	}



	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);

	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	ascBlockData[4] = 0x01;
	ascBlockData[5] = cantidad_preempt;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;

	for(int k=indice_preempt; k<indice_preempt + cantidad_preempt; k++)
	{
		auxi=0;
		ascBlockData[i+6] = p_preempt[k]->preemptControl;
		ascBlockData[i+7] = p_preempt[k]->preemptLink;
		separar_byte_hex(aux,p_preempt[k]->preemptDelay);
		ascBlockData[i+8] = aux[0];
		ascBlockData[i+9] = aux[1];
		separar_byte_hex(aux,p_preempt[k]->preemptMinimumDuration);
		ascBlockData[i+10] = aux[0];
		ascBlockData[i+11] = aux[1];
		ascBlockData[i+12] = p_preempt[k]->preemptMinimumGreen;
		ascBlockData[i+13] = p_preempt[k]->preemptMinimumWalk;
		ascBlockData[i+14] = p_preempt[k]->preemptEnterPedClear;
		ascBlockData[i+15] = p_preempt[k]->preemptMinimumGreen;
		ascBlockData[i+16] = p_preempt[k]->preemptTrackGreen;
		ascBlockData[i+17] = p_preempt[k]->preemptDwellGreen;
		separar_byte_hex(aux,p_preempt[k]->preemptMaximumPresence);
		ascBlockData[i+18] = aux[0];
		ascBlockData[i+19] = aux[1];

		ascBlockData[i+20+auxi] = p_preempt[k]->preemptTrackPhase_len;
		longitud = p_preempt[k]->preemptTrackPhase_len;
		for(int l=0; l<longitud; l++)
		{
			ascBlockData[i+21+auxi+l] = p_preempt[k]->preemptTrackPhase[l];
		}
		auxi += longitud;

		ascBlockData[i+21+auxi] = p_preempt[k]->preemptDwellPhase_len;
		longitud = p_preempt[k]->preemptDwellPhase_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+22+auxi+l] = p_preempt[k]->preemptDwellPhase[l];
		}						
		auxi += longitud;

		ascBlockData[i+22+auxi] = p_preempt[k]->preemptDwellPed_len;
		longitud = p_preempt[k]->preemptDwellPed_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+23+l+auxi] = p_preempt[k]->preemptDwellPed[l];
		}						
		auxi += longitud;

		ascBlockData[i+23+auxi] = p_preempt[k]->preemptExitPhase_len;
		longitud = p_preempt[k]->preemptExitPhase_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+24+l+auxi] = p_preempt[k]->preemptExitPhase[l];
		}						
		auxi += longitud;

		ascBlockData[i+24+auxi] = p_preempt[k]->preemptTrackOverlap_len;
		longitud = p_preempt[k]->preemptTrackOverlap_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+25+auxi+l] = p_preempt[k]->preemptTrackOverlap[k];
		}						
		auxi += longitud;

		ascBlockData[i+25+auxi] = p_preempt[k]->preemptDwellOverlap_len;
		longitud = p_preempt[k]->preemptDwellOverlap_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+26+l+auxi] = p_preempt[k]->preemptDwellOverlap[k];
		}						
		auxi += longitud;

		ascBlockData[i+26+auxi] = p_preempt[k]->preemptCyclingPhase_len;
		longitud = p_preempt[k]->preemptCyclingPhase_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+27+l+auxi] = p_preempt[k]->preemptCyclingPhase[k];
		}						
		auxi += longitud;

		ascBlockData[i+27+auxi] = p_preempt[k]->preemptCyclingPed_len;
		longitud = p_preempt[k]->preemptCyclingPed_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+28+l+auxi] = p_preempt[k]->preemptCyclingPed[k];
		}						
		auxi += longitud;

		ascBlockData[i+28+auxi] = p_preempt[k]->preemptCyclingOverlap_len;
		longitud = p_preempt[k]->preemptCyclingOverlap_len;
		for(int l=0;l<longitud; l++)
		{
			ascBlockData[i+29+l+auxi] = p_preempt[k]->preemptCyclingOverlap[k];
		}						
		auxi += longitud;

		ascBlockData[i+29+auxi] = p_preempt[k]->preemptEnterYellowChange;
		ascBlockData[i+30+auxi] = p_preempt[k]->preemptEnterRedClear;
		ascBlockData[i+31+auxi] = p_preempt[k]->preemptTrackYellowChange;
		ascBlockData[i+32+auxi] = p_preempt[k]->preemptTrackRedClear;

		i+= auxi+27;
	}
	
			

}

void get_bloque_sequence(void)
{
	int tamano_bloque = 8,indice_anillo=0,cantidad_anillo=0,indice_seq=0,cantidad_seq=0,i=0;

	struct sequenceTable_entry t_secuencias[16][4];
	
	get_sequence_table(t_secuencias);

	indice_anillo = ascBlockGetControl[2]-1;
	cantidad_anillo = ascBlockGetControl[3];	
	indice_seq = ascBlockGetControl[4]-1;
	cantidad_seq = ascBlockGetControl[5];

	for(int j=indice_seq; j<indice_seq+cantidad_seq; j++)
	{
		for(i=indice_anillo; i<indice_anillo+cantidad_anillo ;i++)
		{
			tamano_bloque = tamano_bloque + t_secuencias[i][j].sequenceData_len + 1;
		}
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<6;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
//	printf("\n");

	ascBlockData[6] = 0x01;
	ascBlockData[7] = cantidad_anillo*cantidad_seq;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;

	for(int j=indice_seq; j<indice_seq+cantidad_seq; j++)
	{
		for(int k=indice_anillo; k<indice_anillo + cantidad_anillo; k++)
		{
			ascBlockData[i+8] = t_secuencias[j][k].sequenceData_len;
			for(int l=0;l<t_secuencias[j][k].sequenceData_len; l++)
			{
				ascBlockData[i+l+9] = t_secuencias[j][k].sequenceData[l];
				
			}
			i += t_secuencias[j][k].sequenceData_len + 1;
		}
	}
	
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_overlap(void)
{
	int tamano_bloque = 6,indice_overlap=0,cantidad_overlap=0,i=0,longitud=0,auxi=0;

	struct overlapTable_entry ** p_overlap=NULL;
	
	p_overlap=get_overlap_table();

	indice_overlap = ascBlockGetControl[2]-1;
	cantidad_overlap = ascBlockGetControl[3];	

	for(i=indice_overlap; i<indice_overlap+cantidad_overlap ;i++)
	{
		auxi=0;
		auxi += p_overlap[i]->overlapIncludedPhases_len;
		auxi += p_overlap[i]->overlapModifierPhases_len;
		tamano_bloque += auxi+ 5;
	}
	
	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);

	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	ascBlockData[4] = 0x01;
	ascBlockData[5] = cantidad_overlap;

	i=0;

	for(int k=indice_overlap; k<indice_overlap + cantidad_overlap; k++)
	{
		auxi=0;
		ascBlockData[i+6] = p_overlap[k]->overlapIncludedPhases_len;
		longitud = p_overlap[k]->overlapIncludedPhases_len;
		for(int l=0;l<longitud;l++)
		{
			ascBlockData[i+7+l] = p_overlap[k]->overlapIncludedPhases[l];
		}
		auxi += longitud;

		ascBlockData[i+7+auxi] = p_overlap[k]->overlapModifierPhases_len;
		longitud = p_overlap[k]->overlapModifierPhases_len;
		for(int l=0;l<longitud;l++)
		{
			ascBlockData[i+8+auxi+l] = p_overlap[k]->overlapModifierPhases[l];
		}

		auxi += longitud;

		ascBlockData[i+8+auxi] = p_overlap[k]->overlapTrailGreen;
		ascBlockData[i+9+auxi] = p_overlap[k]->overlapTrailYellow;
		ascBlockData[i+10+auxi] = p_overlap[k]->overlapTrailRed;
		i += auxi+5;
	}

}

void get_bloque_channel(void)
{
	int tamano_bloque = 6,indice_channel=0,cantidad_channel=0,i=0;

	struct channelTable_entry ** p_channel=NULL;
	
	p_channel=get_channel_table();

	indice_channel = ascBlockGetControl[2]-1;
	cantidad_channel = ascBlockGetControl[3];	

	for(i=indice_channel; i<indice_channel+cantidad_channel ;i++)
	{
		tamano_bloque = tamano_bloque + 4;
	}
	
	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
//	printf("\n");

	ascBlockData[4] = 0x01;
	ascBlockData[5] = cantidad_channel;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;


	for(int k=indice_channel; k<indice_channel + cantidad_channel; k++)
	{
		ascBlockData[i+6] = p_channel[k]->channelControlSource;
		ascBlockData[i+7] = p_channel[k]->channelControlType;
		ascBlockData[i+8] = p_channel[k]->channelFlash;
		ascBlockData[i+9] = p_channel[k]->channelDim;
		i += 4;
	}

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_port1(void)
{
	int tamano_bloque = 6,indice_port1=0,cantidad_port1=0,i=0;

	struct port1Table_entry ** p_port1=NULL;
	
	p_port1 = get_ts2Port1_table();

	indice_port1 = ascBlockGetControl[2]-1;
	cantidad_port1 = ascBlockGetControl[3];	

	for(i=indice_port1; i<indice_port1+cantidad_port1 ;i++)
	{
		tamano_bloque = tamano_bloque + 2;
	}
	
	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
//	printf("\n");

	ascBlockData[4] = 0x01;
	ascBlockData[5] = cantidad_port1;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;


	for(int k=indice_port1; k<indice_port1 + cantidad_port1; k++)
	{
		ascBlockData[i+6] = p_port1[k]->port1DevicePresent;
		ascBlockData[i+7] = p_port1[k]->port1Frame40Enable;
		i += 2;
	}

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_schedule(void)
{
	int tamano_bloque = 6,indice_schedule=0,cantidad_schedule=0,i=0, auxi[4];

	struct timeBaseScheduleTable_entry ** p_schedule=NULL;
	
	p_schedule = get_schedule_table();

	indice_schedule = ascBlockGetControl[2]-1;
	cantidad_schedule = ascBlockGetControl[3];	

	for(i=indice_schedule; i<indice_schedule+cantidad_schedule ;i++)
	{
		tamano_bloque = tamano_bloque + 7;
	}
	
	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<4;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
//	printf("\n");

	ascBlockData[4] = 0x01;
	ascBlockData[5] = cantidad_schedule;

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

	i=0;


	for(int k=indice_schedule; k<indice_schedule + cantidad_schedule; k++)
	{
		ascBlockData[i+6] = p_schedule[k]->timeBaseScheduleMonth;
		ascBlockData[i+7] = p_schedule[k]->timeBaseScheduleDay;
		separar_4byte_hex(auxi,p_schedule[k]->timeBaseScheduleDate);
		ascBlockData[i+8]  = auxi[3];
		ascBlockData[i+9]  = auxi[2];
		ascBlockData[i+10] = auxi[1];
		ascBlockData[i+11] = auxi[0];
		ascBlockData[i+12] = p_schedule[k]->timeBaseScheduleDayPlan;
		i += 7;
	}

	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");

}

void get_bloque_dayplan(void)
{
	int tamano_bloque = 8,indice_dayplan=0,cantidad_dayplan=0,indice_dayplanEvent=0,
		cantidad_dayplanEvent=0,i=0;

	struct timeBaseDayPlanTable_entry empty[12][10];
	
	get_dayplan_table(empty);

	indice_dayplan = ascBlockGetControl[2]-1;
	cantidad_dayplan = ascBlockGetControl[3];	
	indice_dayplanEvent = ascBlockGetControl[4]-1;
	cantidad_dayplanEvent = ascBlockGetControl[5];

	for(int j=indice_dayplan; j<indice_dayplan+cantidad_dayplan; j++)
	{
		for(i=indice_dayplanEvent; i<indice_dayplanEvent+cantidad_dayplanEvent ;i++)
		{
			tamano_bloque = tamano_bloque + 15 + 3;
		}
	}

	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i: %i ",i,ascBlockData[i]);
	}	
	printf("\n");
*/
	for(i=0;i<6;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}
//	printf("\n");

	ascBlockData[6] = 0x01;
	ascBlockData[7] = cantidad_dayplan*cantidad_dayplanEvent;
/*
	for(i=0;i<tamano_bloque;i++)
	{
		printf("%i ",ascBlockData[i]);
	}	
	printf("\n");
*/
	i=0;

	for(int j=indice_dayplan; j<indice_dayplan+cantidad_dayplan; j++)
	{
		for(int k=indice_dayplanEvent; k<indice_dayplanEvent + cantidad_dayplanEvent; k++)
		{
			ascBlockData[i+8]  = empty[j][k].dayPlanHour;
			ascBlockData[i+9]  = empty[j][k].dayPlanMinute;
			ascBlockData[i+10] = 0x0F;
			ascBlockData[i+11] = 0x2B;
			ascBlockData[i+12] = 0x06;
			ascBlockData[i+13] = 0x01;
			ascBlockData[i+14] = 0x04;
			ascBlockData[i+15] = 0x01;
			ascBlockData[i+16] = 0x89;
			ascBlockData[i+17] = 0x36;
			ascBlockData[i+18] = 0x04;
			ascBlockData[i+19] = 0x02;
			ascBlockData[i+20] = 0x01;
			ascBlockData[i+21] = 0x05;
			ascBlockData[i+22] = 0x03;
			ascBlockData[i+23] = 0x01;
			ascBlockData[i+24] = 0x01;
			ascBlockData[i+25] = 0x01;
			i += 15 + 3;
		}
	}
	
	set_dayplan_table(empty);
}

void get_bloque_miscelaneo(void)
{
	int tamano_bloque = 6, aux[2];

	tamano_bloque = 23;
	
	free(ascBlockData);
	ascBlockData = malloc(tamano_bloque * sizeof(char));
	ascBlockData_len = tamano_bloque;
	memset(ascBlockData,0,ascBlockData_len);

	for(int i=0;i<2;i++)
	{
		ascBlockData[i] = ascBlockGetControl[i];
	}

	ascBlockData[2] = 0x01;
	ascBlockData[3] = 0x01;

	ascBlockData[4] = 0x00;
	ascBlockData[5] = 0x00;
	ascBlockData[6] = get_detector_variable(5);
	ascBlockData[7] = get_unit_variable(1);
	ascBlockData[8] = get_unit_variable(2);
	separar_byte_hex(aux,get_unit_variable2(1));
	ascBlockData[9] = aux[0];
	ascBlockData[10] = aux[1];
	ascBlockData[11] = get_unit_variable(3);
	ascBlockData[12] = get_coord_variable(1);
	ascBlockData[13] = get_coord_variable(2);
	ascBlockData[14] = get_coord_variable(3);
	ascBlockData[15] = get_coord_variable(4);
	separar_byte_hex(aux,get_timebase_variable2(1));
	ascBlockData[16] = aux[0];
	ascBlockData[17] = aux[1];
	ascBlockData[18] = get_globalTimeManagement_variable(5);
	ascBlockData[19] = 0x00;
	ascBlockData[20] = 0x00;
	ascBlockData[21] = 0x00;
	ascBlockData[22] = 0x00;
}

void revision_bloque_get(void)
{
	int _resp=0, tipo_bloque=0,longitud_bloque_en=0, val=0, indice1=0,indice2=0,maxIndice1=0,maxIndice2=0;

	longitud_bloque_en = ascBlockGetControl_len;

	if(ascBlockGetControl[0] == 0) val++;
	if(ascBlockGetControl[1] >= 0 && ascBlockGetControl[1] <= 0x12) val++;

	if(ascBlockGetControl[1] == 0)
	{
		maxIndice1 = get_phase_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 1)
	{
		maxIndice1 = get_coord_variable(16);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 2)
	{
		maxIndice1 = get_detector_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 3)
	{
		maxIndice1 = get_detector_variable(3);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 4)
	{
		maxIndice1 = get_phase_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		maxIndice2 = get_coord_variable(16);
		indice2 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(indice2 > 0 && indice1 <= maxIndice2) val++;
	}
	else if(ascBlockGetControl[1] == 5)
	{
		maxIndice1 = get_timebase_variable(2);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 6)
	{
		maxIndice1 = get_preempt_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 7)
	{
		maxIndice1 = get_ring_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		maxIndice2 = get_ring_variable(2);
		indice2 = ascBlockGetControl[2] + ascBlockGetControl[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(indice2 > 0 && indice1 <= maxIndice2) val++;
	}
	else if(ascBlockGetControl[1] == 8)
	{
		maxIndice1 = get_channel_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 9)
	{
		maxIndice1 = get_overlap_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}

	else if(ascBlockGetControl[1] == 10)
	{
		maxIndice1 = get_port1_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}	
	else if(ascBlockGetControl[1] == 11)
	{
		maxIndice1 = get_globalTimeManagement_variable(1);
		indice1 = ascBlockGetControl[2] + ascBlockGetControl[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
	}
	else if(ascBlockGetControl[1] == 0x12)
	{
		val++;
	}
 

	if(val == 3 || val == 4) tipo_bloque = ascBlockGetControl[1];
	else tipo_bloque = -1;
		
	printf("val:%i \n",val);

	if(tipo_bloque == 0)
	{
		get_bloque_phase();
	}
	else if(tipo_bloque == 1)
	{
		get_bloque_detector();
	}
	else if(tipo_bloque == 2)
	{
		get_bloque_pedestrian();
	}
	else if(tipo_bloque == 3)
	{
		get_bloque_pattern();
	}
	else if(tipo_bloque == 4)
	{
		get_bloque_split();
	}
	else if(tipo_bloque == 5)
	{
		get_bloque_timebase();
	}
	else if(tipo_bloque == 6)
	{
		get_bloque_preempt();
	}
	else if(tipo_bloque == 7)
	{
		get_bloque_sequence();
	}
	else if(tipo_bloque == 8)
	{
		get_bloque_channel();
	}
	else if(tipo_bloque == 9)
	{
		get_bloque_overlap();
	}
	else if(tipo_bloque == 0xA)
	{
		get_bloque_port1();
	}
	else if(tipo_bloque == 0xB)
	{
		get_bloque_schedule();
	}
	else if(tipo_bloque == 0xC)
	{
		get_bloque_dayplan();
	}
	else if(tipo_bloque == 0xD)
	{

	}
	else if(tipo_bloque == 0xE)
	{

	}
	else if(tipo_bloque == 0xF)
	{

	}
	else if(tipo_bloque == 0x10)
	{

	}
	else if(tipo_bloque == 0x11)
	{

	}
	else if(tipo_bloque == 0x12)
	{
		get_bloque_miscelaneo();
	}
}

void set_bloque_phase(void)
{
	int indice=0, nFases=0, longitud_conc=0, aux[2],i=0;
	struct phaseTable_entry ** p_phase = NULL;

	p_phase = get_phase_table();
	indice = ascBlockData[2]-1;
	nFases = ascBlockData[3];	

//	printf("indice: %i, nFases: %i, long_bloque: %i\n",indice,nFases,ascBlockData_len);

	i=0;

		for(int j=indice; j<indice+nFases; j++)
		{
			p_phase[j]->phaseWalk 			     = ascBlockData[i+6];
			p_phase[j]->phasePedestrianClear     = ascBlockData[i+7];
			p_phase[j]->phaseMinimumGreen        = ascBlockData[i+8];
    		p_phase[j]->phasePassage             = ascBlockData[i+9];
   			p_phase[j]->phaseMaximum1            = ascBlockData[i+10];
    		p_phase[j]->phaseMaximum2            = ascBlockData[i+11];
   		 	p_phase[j]->phaseYellowChange        = ascBlockData[i+12];
    		p_phase[j]->phaseRedClear  		     = ascBlockData[i+13];
    		p_phase[j]->phaseRedRevert 		     = ascBlockData[i+14];
    		p_phase[j]->phaseAddedInitial 	     = ascBlockData[i+15];
    		p_phase[j]->phaseMaximumInitial  	 = ascBlockData[i+16];
    		p_phase[j]->phaseTimeBeforeReduction = ascBlockData[i+17];
    		p_phase[j]->phaseCarsBeforeReduction = ascBlockData[i+18];
			p_phase[j]->phaseTimeToReduce 		 = ascBlockData[i+19];
    		p_phase[j]->phaseReduceBy 			 = ascBlockData[i+20];
			p_phase[j]->phaseMinimumGap 		 = ascBlockData[i+21];
			p_phase[j]->phaseDynamicMaxLimit   	 = ascBlockData[i+22];
			p_phase[j]->phaseDynamicMaxStep 	 = ascBlockData[i+23];
			p_phase[j]->phaseStartup			 = ascBlockData[i+24];
			p_phase[j]->phaseOptions = unir_byte_hex(ascBlockData[i+25],ascBlockData[i+26]);
			p_phase[j]->phaseRing				 = ascBlockData[i+27];
			p_phase[j]->phaseConcurrency_len	 = ascBlockData[i+28];
    		
			longitud_conc = ascBlockData[i+28]; 

			for(int k=0;k<longitud_conc;k++)
			{
				p_phase[j]->phaseConcurrency[k] = ascBlockData[i+29+k];
			}

			i += longitud_conc + 23;
		}
	
}

void set_bloque_detector(void)
{
	int indice=0, nDetector=0,i=0;
	struct vehicleDetectorTable_entry ** p_vDetector=NULL;

	p_vDetector = get_vehicleDetector_table();
	indice = ascBlockData[2]-1;
	nDetector = ascBlockData[3];	

	i=0;

		for(int j=indice; j<indice+nDetector; j++)
		{
			p_vDetector[j]->vehicleDetectorOptions        = ascBlockData[i+6];
			p_vDetector[j]->vehicleDetectorCallPhase      = ascBlockData[i+7];
			p_vDetector[j]->vehicleDetectorSwitchPhase    = ascBlockData[i+8];
    		p_vDetector[j]->vehicleDetectorDelay = unir_byte_hex(ascBlockData[i+9],ascBlockData[i+10]);
   			p_vDetector[j]->vehicleDetectorExtend         = ascBlockData[i+11];
    		p_vDetector[j]->vehicleDetectorQueueLimit     = ascBlockData[i+12];
   		 	p_vDetector[j]->vehicleDetectorNoActivity     = ascBlockData[i+13];
    		p_vDetector[j]->vehicleDetectorMaxPresence   = ascBlockData[i+14];
    		p_vDetector[j]->vehicleDetectorErraticCounts  = ascBlockData[i+15];
    		p_vDetector[j]->vehicleDetectorFailTime       = ascBlockData[i+16];
			i += 11;
		}
	
}

void set_bloque_pedestrian(void)
{
	int indice=0, nDetector=0,i=0;
	struct pedestrianDetectorTable_entry ** p_vDetector=NULL;

	p_vDetector = get_pedestrian_table();
	indice = ascBlockData[2]-1;
	nDetector = ascBlockData[3];	

	i=0;

		for(int j=indice; j<indice+nDetector; j++)
		{
			p_vDetector[j]->pedestrianDetectorCallPhase     = ascBlockData[i+6];
			p_vDetector[j]->pedestrianDetectorNoActivity    = ascBlockData[i+7];
    		p_vDetector[j]->pedestrianDetectorMaxPresence 	= ascBlockData[i+8];
   			p_vDetector[j]->pedestrianDetectorErraticCounts = ascBlockData[i+9];
			i += 5;
		}
	
}

void set_bloque_pattern(void)
{
	int indice=0, nFases=0, i=0;
	struct patternTable_entry ** p_pattern=NULL;

	p_pattern=get_pattern_table();

	indice = ascBlockData[2]-1;
	nFases = ascBlockData[3];	

	i=0;

		for(int j=indice; j<indice+nFases; j++)
		{
			p_pattern[j]->patternCycleTime      = ascBlockData[i+6];
			printf("data: %i\n",ascBlockData[i+6]);
			p_pattern[j]->patternOffsetTime     = ascBlockData[i+7];
			p_pattern[j]->patternSequenceNumber = ascBlockData[i+8];

			i += 3 ;
		}
	
}

void set_bloque_split(void)
{
	int indice_fases=0,cantidad_fases=0,indice_split=0,cantidad_splits=0,i=0;

	struct splitTable_entry t_splits[32][16]; 
	
	indice_fases = ascBlockData[2]-1;
	cantidad_fases = ascBlockData[3];	
	indice_split = ascBlockData[4]-1;
	cantidad_splits = ascBlockData[5];

	for(int j=0;j<32;j++)
	{
		for(int k=0;k<16;k++)
		{
			t_splits[j][k].splitTime = 0;
			t_splits[j][k].splitMode = 0;
			t_splits[j][k].splitCoordPhase = 0;
		}
	}

	i=0;

	for(int j=indice_split; j<indice_split+cantidad_splits; j++)
	{
		for(int k=indice_fases; k<indice_fases+cantidad_fases; k++)
		{
			t_splits[j][k].splitTime = ascBlockData[i+8];
			t_splits[j][k].splitMode = ascBlockData[i+9];
			t_splits[j][k].splitCoordPhase = ascBlockData[i+10];
			i += 3;
		}
	}

	set_split_table(t_splits);

}

void set_bloque_timebase(void)
{
	int indice=0, nTimebaseAscActions=0, i=0;
	struct timebaseAscActionTable_entry ** p_timebase=NULL;

	p_timebase = get_timebaseAsc_table();

	indice = ascBlockData[2]-1;
	nTimebaseAscActions = ascBlockData[3];	

	i=0;

		for(int j=indice; j<indice+nTimebaseAscActions; j++)
		{
			p_timebase[j]->timebaseAscPattern    = ascBlockData[i+6];
			p_timebase[j]->timebaseAscAuxillaryFunction = ascBlockData[i+7];
			p_timebase[j]->timebaseAscSpecialFunction = ascBlockData[i+8];

			i += 3 ;
		}
	
}

void set_bloque_preempt(void)
{
	int indice=0, npreempt=0, i=0,preempt_len=0, aux=0;
	struct preemptTable_entry ** p_preempt=NULL;

	p_preempt = get_preempt_table();

	indice = ascBlockData[2]-1;
	npreempt = ascBlockData[3];	

	i=0;

		for(int j=indice; j<indice+npreempt; j++)
		{
			aux=0;
			p_preempt[j]->preemptControl    = ascBlockData[i+6];
			p_preempt[j]->preemptLink = ascBlockData[i+7];
			p_preempt[j]->preemptDelay = unir_byte_hex(ascBlockData[i+8],ascBlockData[i+9]);
			p_preempt[j]->preemptMinimumDuration = unir_byte_hex(ascBlockData[i+10],ascBlockData[i+11]);
			p_preempt[j]->preemptMinimumGreen = ascBlockData[i+12];
			p_preempt[j]->preemptMinimumWalk = ascBlockData[i+13];
			p_preempt[j]->preemptEnterPedClear = ascBlockData[i+14];
			p_preempt[j]->preemptTrackGreen = ascBlockData[i+15];
			p_preempt[j]->preemptDwellGreen = ascBlockData[i+16];
			p_preempt[j]->preemptMaximumPresence = unir_byte_hex(ascBlockData[i+17],ascBlockData[i+18]);

			preempt_len = ascBlockData[i+19];
			p_preempt[j]->preemptTrackPhase_len = preempt_len;
//			printf("preemptTrackPhase:\n");
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptTrackPhase[k] = ascBlockData[i+aux+20+k];
//				printf("%i ",p_preempt[j]->preemptTrackPhase[k]);
			}
			aux += preempt_len; 
			printf("\n");
			printf("preemptDwellPhase: %i\n",preempt_len);
			preempt_len = ascBlockData[i+20+aux];
			p_preempt[j]->preemptDwellPhase_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptDwellPhase[k] = ascBlockData[i+21+aux+k];
				printf("%i ",p_preempt[j]->preemptDwellPhase[k]);
			}
			printf("\n");
			aux += preempt_len; 

			preempt_len = ascBlockData[i+21+aux];
			p_preempt[j]->preemptDwellPed_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptDwellPed[k] = ascBlockData[i+aux+22+k];
			}
			aux += preempt_len; 
			
			preempt_len = ascBlockData[i+22+aux];
			p_preempt[j]->preemptExitPhase_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptExitPhase[k] = ascBlockData[i+23+aux+k];
			}
			aux += preempt_len;

			preempt_len = ascBlockData[i+23+aux];
			p_preempt[j]->preemptTrackOverlap_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptTrackOverlap[k] = ascBlockData[i+aux+24+k];
			}
			aux += preempt_len; 
			
			preempt_len = ascBlockData[i+24+aux];
			p_preempt[j]->preemptDwellOverlap_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptDwellOverlap[k] = ascBlockData[i+25+aux+k];
			}
			aux += preempt_len; 


			preempt_len = ascBlockData[i+25+aux];
			p_preempt[j]->preemptCyclingPhase_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptCyclingPhase[k] = ascBlockData[i+aux+26+k];
			}
			aux += preempt_len; 
			
			preempt_len = ascBlockData[i+26+aux];
			p_preempt[j]->preemptCyclingPed_len = preempt_len;
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptCyclingPed[k] = ascBlockData[i+27+aux+k];
			}
			aux += preempt_len;


			preempt_len = ascBlockData[i+27+aux];
			p_preempt[j]->preemptCyclingOverlap_len = preempt_len;
//			printf("preemptCyclingOverlap: %i\n",preempt_len);
			for(int k=0; k<preempt_len; k++)
			{
				p_preempt[j]->preemptCyclingOverlap[k] = ascBlockData[i+28+aux+k];
//				printf("%i ",p_preempt[j]->preemptCyclingOverlap[k]);
			}
			aux += preempt_len;  
//			printf("\n");
			p_preempt[j]->preemptEnterYellowChange = ascBlockData[i+28+aux];
			p_preempt[j]->preemptEnterRedClear = ascBlockData[i+29+aux];
			p_preempt[j]->preemptTrackYellowChange = ascBlockData[i+30+aux];
			p_preempt[j]->preemptTrackRedClear = ascBlockData[i+31+aux];
			i += aux+13+4+9 ;
		}
	
}

void set_bloque_sequence(void)
{
	int indice_anillo=0,cantidad_anillo=0,indice_seq=0,cantidad_seq=0,i=0,aux=0;

	struct sequenceTable_entry t_secuencias[16][4];
	
	indice_anillo = ascBlockData[2]-1;
	cantidad_anillo = ascBlockData[3];	
	indice_seq = ascBlockData[4]-1;
	cantidad_seq = ascBlockData[5];

	i=0;

	for(int p=0;p<16;p++)
	{
		for(int q=0;q<4;q++)
		{
				memset(t_secuencias[p][q].sequenceData,0,sizeof(t_secuencias[p][q].sequenceData));
				t_secuencias[p][q].sequenceData_len = 1;
		}
	}

	for(int j=indice_seq; j<indice_seq+cantidad_seq; j++)
	{
		for(int k=indice_anillo; k<indice_anillo + cantidad_anillo; k++)
		{
			t_secuencias[j][k].sequenceData_len = ascBlockData[i+8];
			for(int l=0;l<t_secuencias[j][k].sequenceData_len; l++)
			{
				t_secuencias[j][k].sequenceData[l] = ascBlockData[i+l+9];
				
			}
			i += t_secuencias[j][k].sequenceData_len + 1;
		}
	}
/*
	for(int p=0;p<16;p++)
	{
		for(int q=0;q<4;q++)
		{
			printf("long: %i\n",t_secuencias[p][q].sequenceData_len);
			
			for(int r=0;r<t_secuencias[p][q].sequenceData_len;r++)
			{
				printf("%i ",t_secuencias[p][q].sequenceData[r]);
			}
			printf("\n");
		}
	}
*/
	set_sequence_table(t_secuencias);
}

void set_bloque_overlap(void)
{
	int indice_overlap=0,cantidad_overlap=0,i=0,overlap_len=0,auxi=0;

	struct overlapTable_entry ** p_overlap=NULL;
	
	p_overlap=get_overlap_table();

	indice_overlap = ascBlockData[2]-1;
	cantidad_overlap = ascBlockData[3];	

	i=0;

	for(int k=indice_overlap; k<indice_overlap + cantidad_overlap; k++)
	{
		auxi=0;
		p_overlap[k]->overlapIncludedPhases_len = ascBlockData[i+6];
		overlap_len = p_overlap[k]->overlapIncludedPhases_len;
		for(int l=0;l<overlap_len; l++)
		{
			p_overlap[k]->overlapIncludedPhases[l] = ascBlockData[i+7+l];
		}
		auxi += overlap_len;

		p_overlap[k]->overlapModifierPhases_len = ascBlockData[i+7+auxi];
		overlap_len = p_overlap[k]->overlapModifierPhases_len;
		for(int l=0;l<overlap_len; l++)
		{
			p_overlap[k]->overlapModifierPhases[l] = ascBlockData[i+8+auxi+l];
		}
		auxi += overlap_len;

		p_overlap[k]->overlapTrailGreen = ascBlockData[i+8+auxi] ;
		p_overlap[k]->overlapTrailYellow = ascBlockData[i+9+auxi];
		p_overlap[k]->overlapTrailRed = ascBlockData[i+10+auxi];
		i += auxi+5;
	}
}


void set_bloque_channel(void)
{
	int indice_channel=0,cantidad_channel=0,i=0;

	struct channelTable_entry ** p_channel=NULL;
	
	p_channel=get_channel_table();

	indice_channel = ascBlockData[2]-1;
	cantidad_channel = ascBlockData[3];	

	i=0;

	for(int k=indice_channel; k<indice_channel + cantidad_channel; k++)
	{
		p_channel[k]->channelControlSource = ascBlockData[i+6];
		p_channel[k]->channelControlType = ascBlockData[i+7] ;
		p_channel[k]->channelFlash = ascBlockData[i+8];
		p_channel[k]->channelDim = ascBlockData[i+9];
		i += 4;
	}

}

void set_bloque_port1(void)
{
	int indice_port1=0,cantidad_port1=0,i=0;

	struct port1Table_entry ** p_port1=NULL;
	
	p_port1 = get_ts2Port1_table();

	indice_port1 = ascBlockData[2]-1;
	cantidad_port1 = ascBlockData[3];	

	i=0;

	for(int k=indice_port1; k<indice_port1 + cantidad_port1; k++)
	{
		p_port1[k]->port1DevicePresent = ascBlockData[i+6];
		p_port1[k]->port1Frame40Enable = ascBlockData[i+7] ;
		i += 2;
	}

}

void set_bloque_schedule(void)
{
	int indice_schedule=0,cantidad_schedule=0,i=0;

	struct timeBaseScheduleTable_entry ** p_schedule=NULL;
	
	p_schedule = get_schedule_table();

	indice_schedule = ascBlockData[2]-1;
	cantidad_schedule = ascBlockData[3];	

	i=0;

	for(int k=indice_schedule; k<indice_schedule + cantidad_schedule; k++)
	{
		p_schedule[k]->timeBaseScheduleMonth = ascBlockData[i+6];

		p_schedule[k]->timeBaseScheduleDay = ascBlockData[i+7];

		p_schedule[k]->timeBaseScheduleDate = unir_4nibble_hex(ascBlockData[i+8],
		ascBlockData[i+9],ascBlockData[i+10], ascBlockData[i+11]);
		p_schedule[k]->timeBaseScheduleDayPlan = ascBlockData[i+12];
		i += 7;
	}

}

void set_bloque_dayPlan(void)
{
	int indice_dayPlan=0,cantidad_dayPlan=0,indice_dayPlanEvent=0,cantidad_dayPlanEvent=0,i=0,longitud=0;

	struct timeBaseDayPlanTable_entry empty[12][10];
	
	get_dayplan_table(empty);

	indice_dayPlan = ascBlockData[2]-1;
	cantidad_dayPlan = ascBlockData[3];	
	indice_dayPlanEvent = ascBlockData[4]-1;
	cantidad_dayPlanEvent = ascBlockData[5];

	i=0;
	for(int j=indice_dayPlan;j<indice_dayPlan + cantidad_dayPlan; j++)
	{
		for(int k=indice_dayPlanEvent; k<indice_dayPlanEvent + cantidad_dayPlanEvent; k++)
		{
			empty[j][k].dayPlanHour = ascBlockData[i+8];

			empty[j][k].dayPlanMinute = ascBlockData[i+9];

			empty[j][k].dayPlanActionNumberOID_len = ascBlockData[i+10];
			longitud = empty[j][k].dayPlanActionNumberOID_len;

			memset( empty[j][k].dayPlanActionNumberOID, 0,
                        sizeof(empty[j][k].dayPlanActionNumberOID));

			for(int l=0;l<longitud;l++)
			{	
				empty[j][k].dayPlanActionNumberOID[l] = ascBlockData[i+11+l];
//				printf("%lu ",empty[j][k].dayPlanActionNumberOID[l]);
			}
//			printf("\nlongitud: %i\n",longitud);
			i += longitud+3;
		}
	}

	set_dayplan_table(empty);

}

void set_bloque_miscelaneo(void)
{
	int auxi = 0;
	long auxi2=0;
	unsigned long auxi3=0;


	//= unir_byte_hex(ascBlockData[i+4], ascBlockData[i+5]);
	auxi = ascBlockData[6]; 
	set_detector_variable(2, auxi);
	auxi = ascBlockData[7]; 
	set_unit_variable(1, auxi);
	auxi = ascBlockData[8]; 
	set_unit_variable(2, auxi);
	auxi2=unir_byte_hex(ascBlockData[9], ascBlockData[10]);
	set_unit_variable2(1, auxi2);
	auxi = ascBlockData[11]; 
	set_unit_variable(3, auxi);
	auxi = ascBlockData[12];
	set_coord_variable(1, auxi);
	auxi = ascBlockData[13];
	set_coord_variable(2, auxi);
	auxi = ascBlockData[14];
	set_coord_variable(3, auxi);
	auxi = ascBlockData[15];
	set_coord_variable(4, auxi);
	auxi2=unir_byte_hex(ascBlockData[16], ascBlockData[17]);
	set_timebase_variable2(1, auxi2);
	auxi2=ascBlockData[18];
	set_globalTimeManagement_variable2(5, auxi);
	auxi3 = unir_4nibble_hex(ascBlockData[19],ascBlockData[20],ascBlockData[21],ascBlockData[22]);
	set_globalTimeManagement_variable2(2, auxi3);
}

void revision_bloque_set(void)
{
	int _resp=0, tipo_bloque=0,llave=0, val=0, indice1=0,indice2=0,maxIndice1=0,maxIndice2=0;

	if(ascBlockData[0] == 0) val++;
	if(ascBlockData[1] >= 0 && ascBlockData[1] <= 0x12) val++;

	if(ascBlockData[1] == 0)
	{
		maxIndice1 = get_phase_variable(1);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
	}
	else if(ascBlockData[1] == 1)
	{
		maxIndice1 = get_detector_variable(1);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
		
	}
	else if(ascBlockData[1] == 2)
	{
		maxIndice1 = get_detector_variable(3);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
		
	}

	else if(ascBlockData[1] == 3)
	{
		maxIndice1 = get_coord_variable(16);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
		
	}
	else if(ascBlockData[1] == 4)
	{
		maxIndice1 = get_phase_variable(1);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		maxIndice2 = get_coord_variable(16);
		indice2 = ascBlockData[4] + ascBlockData[5] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(indice2 > 0 && indice1 <= maxIndice2) val++;
		if(ascBlockData[6] == 1) val++;
		if(ascBlockData[7] == ascBlockData[5]*ascBlockData[3]) val++;

		if(val == 6) llave = 1;
		else llave = 0;
	}
	else if(ascBlockData[1] == 5)
	{
		maxIndice1 = get_timebase_variable(2);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
		
	}
	else if(ascBlockData[1] == 6)
	{
		maxIndice1 = get_timebase_variable(2);
		indice1 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
		
	}
	else if(ascBlockData[1] == 7)
	{
		maxIndice1 = get_ring_variable(1);
		indice1 = ascBlockData[2] + ascBlockData[3]-1;
		maxIndice2 = get_ring_variable(2);
		indice2 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(indice2 > 0 && indice1 <= maxIndice2) val++;
		if(ascBlockData[6] == 1) val++;
		if(ascBlockData[7] == ascBlockData[5]*ascBlockData[3]) val++;

		if(val == 6) llave = 1;
		else llave = 0;
	}
	else if(ascBlockData[1] == 8)
	{
		maxIndice1 = get_channel_variable(1);

		indice1 = ascBlockData[2] + ascBlockData[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
	}
	else if(ascBlockData[1] == 9)
	{
		maxIndice1 = get_overlap_variable(1);

		indice1 = ascBlockData[2] + ascBlockData[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
	}
	else if(ascBlockData[1] == 10)
	{
		maxIndice1 = get_port1_variable(1);

		indice1 = ascBlockData[2] + ascBlockData[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
	}	 
	else if(ascBlockData[1] == 11)
	{
		maxIndice1 = get_globalTimeManagement_variable(1);

		indice1 = ascBlockData[2] + ascBlockData[3]-1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(ascBlockData[4] == 1) val++;
		if(ascBlockData[3] == ascBlockData[5]) val++;

		if(val == 5) llave = 1;
		else llave = 0;
	}
	else if(ascBlockData[1] == 12)
	{
		maxIndice1 = get_globalTimeManagement_variable(2);
		indice1 = ascBlockData[2] + ascBlockData[3]-1;
		maxIndice2 = get_globalTimeManagement_variable(3);
		indice2 = ascBlockData[2] + ascBlockData[3] -1;
		if(indice1 > 0 && indice1 <= maxIndice1) val++;
		if(indice2 > 0 && indice1 <= maxIndice2) val++;
		if(ascBlockData[6] == 1) val++;
		if(ascBlockData[7] == ascBlockData[5]*ascBlockData[3]) val++;

		if(val == 6) llave = 1;
		else llave = 0;
	}

	else if(ascBlockData[1] == 0x12)
	{
		llave = 1;
	}		
	if(llave) tipo_bloque = ascBlockData[1];
	else tipo_bloque = -1;
		
	printf("val:%i %i %i %i\n",val,ascBlockData[1],indice1,maxIndice1);

	if(tipo_bloque == 0)
	{
		set_bloque_phase();
		sec_prog_bloque[0]=1;
	}
	else if(tipo_bloque == 1)
	{
		set_bloque_detector();
	}
	else if(tipo_bloque == 2)
	{
		set_bloque_pedestrian();
	}
	else if(tipo_bloque == 3)
	{
		set_bloque_pattern();
		sec_prog_bloque[1]=1;
	}
	else if(tipo_bloque == 4)
	{
		set_bloque_split();
		sec_prog_bloque[2]=1;
	}
	else if(tipo_bloque == 5)
	{
		set_bloque_timebase();
	}
	else if(tipo_bloque == 6)
	{
		set_bloque_preempt();
	}
	else if(tipo_bloque == 7)
	{
		set_bloque_sequence();
		sec_prog_bloque[3]=1;
	}
	else if(tipo_bloque == 8)
	{
		set_bloque_channel();
		sec_prog_bloque[4]=1;
	}
	else if(tipo_bloque == 9)
	{
		set_bloque_overlap();
	}
	else if(tipo_bloque == 0xA)
	{
		set_bloque_port1();
	}
	else if(tipo_bloque == 0xB)
	{
		set_bloque_schedule();
	}
	else if(tipo_bloque == 0xC)
	{
		set_bloque_dayPlan();
	}
	else if(tipo_bloque == 0xD)
	{

	}
	else if(tipo_bloque == 0xE)
	{

	}
	else if(tipo_bloque == 0xF)
	{

	}
	else if(tipo_bloque == 0x10)
	{

	}
	else if(tipo_bloque == 0x11)
	{

	}
	else if(tipo_bloque == 0x12)
	{
		set_bloque_miscelaneo();
		printf("si\n");
	}
}

 void imprimir(void)
{
	if(ascBlockData != NULL)
	{
		for(int i=0;i<ascBlockData_len;i++)
		{
			printf("%i ",ascBlockData[i]);
		}
		printf("\n");
	}
}
void
init_ascBlock(void)
{
    const oid ascBlockGetControl_oid[] = { 1,3,6,1,4,1,1206,4,2,1,11,1 };
	const oid ascBlockData_oid[] = { 1,3,6,1,4,1,1206,4,2,1,11,2 };
	const oid ascBlockErrorStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,1,11,3 };

  	DEBUGMSGTL(("ascBlockGetControl", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("ascBlockGetControl", handle_ascBlock,
                               ascBlockGetControl_oid, OID_LENGTH(ascBlockGetControl_oid),
                               HANDLER_CAN_RWRITE
        ));

    

  	DEBUGMSGTL(("ascBlockData", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("ascBlockData", handle_ascBlock,
                               ascBlockData_oid, OID_LENGTH(ascBlockData_oid),
                               HANDLER_CAN_RWRITE
        ));

  	DEBUGMSGTL(("ascBlockErrorStatus", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("ascBlockErrorStatus", handle_ascBlock,
                               ascBlockErrorStatus_oid, OID_LENGTH(ascBlockErrorStatus_oid),
                               HANDLER_CAN_RONLY
        ));

	
	ascBlockGetControl = malloc(1 * sizeof(char));
	ascBlockData = malloc(1 * sizeof(char));

	memset(ascBlockGetControl, 0, sizeof(*ascBlockGetControl));
	memset(ascBlockData, 0, sizeof(*ascBlockData));

	ascBlockGetControl_old = malloc(1 * sizeof(char));
	ascBlockData_old = malloc(1 * sizeof(char));
  
	memset(ascBlockGetControl_old, 0, sizeof(*ascBlockGetControl_old));
	memset(ascBlockData_old, 0, sizeof(*ascBlockData_old));

	for(int i=0;i<5;i++) sec_prog_bloque[i]=0;
}

int
handle_ascBlock(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
	{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_OCTET_STR,ascBlockGetControl
                                     /* XXX: a pointer to the scalar's data */,ascBlockGetControl_len
                                     /* XXX: the length of the data in bytes */);
			}

			else if(resul==2)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_OCTET_STR,ascBlockData
                                     /* XXX: a pointer to the scalar's data */,ascBlockData_len
                                     /* XXX: the length of the data in bytes */);
			}

			else if(resul==3)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&ascBlockErrorStatus
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}

        break;
  
        /*
         * SET REQUEST
         *
         * multiple states in the transaction.  See:
         * http://www.net-snmp.org/tutorial-5/toolkit/mib_module/set-actions.jpg
         */
        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_OCTET_STR);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:
            /* XXX malloc "undo" storage buffer */
/*            if (/* XXX if malloc, or whatever, failed: ) {
                netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_RESOURCEUNAVAILABLE);
            }*/
            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
            /* XXX: perform the value change here */
			resul=compara_oid(requests->requestvb->name);
            if (resul==1) 
			{	
				free(ascBlockGetControl_old);
				ascBlockGetControl_old = malloc(sizeof(ascBlockGetControl));

				memcpy(ascBlockGetControl,ascBlockGetControl,sizeof(ascBlockGetControl));
				ascBlockGetControl_old_len = ascBlockGetControl_len;

				free(ascBlockGetControl);
				ascBlockGetControl = malloc(requests->requestvb->val_len * sizeof(char));

				memset(ascBlockGetControl,0,sizeof(ascBlockGetControl));
				memcpy(ascBlockGetControl,requests->requestvb->val.string,requests->requestvb->val_len);
				ascBlockGetControl_len = requests->requestvb->val_len;
				

				set_variable(3,2);


            }

            else if (resul==2) 
			{	
				if(get_globalDBManagement_variable(1)!=2)
				{
					netsnmp_set_request_error(reqinfo, 
					requests, SNMP_NOSUCHINSTANCE);
				}
				else
				{
				
					free(ascBlockData_old);
					ascBlockData_old = malloc(sizeof(ascBlockData));

					memcpy(ascBlockData_old,ascBlockData,sizeof(ascBlockData));
					ascBlockData_old_len = ascBlockData_len;

					free(ascBlockData);
					ascBlockData = malloc(requests->requestvb->val_len * sizeof(char));

					memset(ascBlockData,0,sizeof(ascBlockData));
					memcpy(ascBlockData,requests->requestvb->val.string,requests->requestvb->val_len);
					ascBlockData_len = requests->requestvb->val_len;


					revision_bloque_set();
				}

            }
			
            break;

        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */
/*            if (/* XXX: error? ) {
                 try _really_really_ hard to never get to this point 
                netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_COMMITFAILED);
            }*/
            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */
/*            if (/* XXX: error? ) {
                 try _really_really_ hard to never get to this point 
                netsnmp_set_request_error(reqinfo, requests, SNMP_ERR_UNDOFAILED);
            }*/
            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_ascBlockData\n", reqinfo->mode );
            return SNMP_ERR_GENERR;


    }

    return SNMP_ERR_NOERROR;
}

