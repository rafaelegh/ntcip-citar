
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"
#include "listas.h"

//Declaracion de variables

int maxPreempts = 32;

/** Initialize the phaseTable table by defining its contents and how it's structured */
//* Typical data structure for a row entry */


struct preemptTable_entry *entry13[32];
struct preemptControlTable_entry *entry14[32];

int get_preempt_variable(int _n)
{
	int resp=-1;
	if(_n == 1) resp = maxPreempts;

	return resp;
}

struct preemptTable_entry ** get_preempt_table(void)
{
	return entry13;
}
 
/* create a new row in the table */
netsnmp_tdata_row *
preempt_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int tabla) 
{
    netsnmp_tdata_row *row;
	
	switch(tabla)
	{
		case 13:
		
    	entry13[indice] = SNMP_MALLOC_TYPEDEF(struct preemptTable_entry);
    	if (!entry13[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry13[indice]);
        	return NULL;
    	}
    	row->data = entry13[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry13[indice]->preemptNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry13[indice]->preemptNumber),
                                 sizeof(entry13[indice]->preemptNumber));
		
  		break;

		case 14:
		
    	entry14[indice] = SNMP_MALLOC_TYPEDEF(struct preemptControlTable_entry);
    	if (!entry14[indice]) return NULL;
    	row = netsnmp_tdata_create_row();
    	if (!row) 
    	{
        	SNMP_FREE(entry14[indice]);
        	return NULL;
    	}
    	row->data = entry14[indice];
		

    	DEBUGMSGT(("phaseTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry14[indice]->preemptControlNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry14[indice]->preemptControlNumber),
                                 sizeof(entry14[indice]->preemptControlNumber));
		
  		break;
	
    }
  if (tabla_data)
        netsnmp_tdata_add_row( tabla_data, row );
    return row;
    
}

void
initialize_preempt_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	int min_columna, int max_columna, int tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i, n;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 13:
    	reg = netsnmp_create_handler_registration( nombre, preemptTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxPreempts;
		break;
		case 14:
    	reg = netsnmp_create_handler_registration( nombre, preemptControlTable_handler, 
		oid_table, oid_len, HANDLER_CAN_RWRITE);
		n=maxPreempts;
		break;	
	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }
    netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: phaseNumber */
                           0);

    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{
		row=preempt_tables_createEntry(table_data, i, tabla);
	}
}

void
init_preempt(void)
{
     const oid maxPreempts_oid[] = { 1,3,6,1,4,1,1206,4,2,1,6,1 };

     const oid preemptTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,6,2};
     const size_t preemptTable_oid_len   = OID_LENGTH(preemptTable_oid);

     const oid preemptControlTable_oid[] = {1,3,6,1,4,1,1206,4,2,1,6,3};
     const size_t preemptControlTable_oid_len   = OID_LENGTH(preemptControlTable_oid);


  DEBUGMSGTL(("maxPreempts", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("maxPreempts", handle_preempt,
                               maxPreempts_oid, OID_LENGTH(maxPreempts_oid),
                               HANDLER_CAN_RONLY
        ));

	initialize_preempt_tables("preemptTable",preemptTable_oid,preemptTable_oid_len,
				COLUMN_PREEMPTNUMBER, COLUMN_PREEMPTTRACKREDCLEAR,13);


	initialize_preempt_tables("preemptControlTable",preemptControlTable_oid,preemptControlTable_oid_len,
				COLUMN_PREEMPTCONTROLNUMBER, COLUMN_PREEMPTCONTROLSTATE,14);
}

int
handle_preempt(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{

    int  resul=0;
	int ret;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) 
	{

        case MODE_GET:
			
			resul=compara_oid(requests->requestvb->name);
		
			if(resul==1)
			{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&maxPreempts
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
			}
        break;
  

    }

    return SNMP_ERR_NOERROR;
}

/** handles requests for the preemptTable table */
int
preemptTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct preemptTable_entry          *table_entry;
    int                         ret;
	ptnodo * l_dbt=NULL;
	struct datos_lista_objetos preempt_dbt;

    DEBUGMSGTL(("preemptTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptNumber);
                break;
            case COLUMN_PREEMPTCONTROL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptControl);
                break;
            case COLUMN_PREEMPTLINK:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptLink);
                break;
            case COLUMN_PREEMPTDELAY:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptDelay);
                break;
            case COLUMN_PREEMPTMINIMUMDURATION:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptMinimumDuration);
                break;
            case COLUMN_PREEMPTMINIMUMGREEN:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptMinimumGreen);
                break;
            case COLUMN_PREEMPTMINIMUMWALK:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptMinimumWalk);
                break;
            case COLUMN_PREEMPTENTERPEDCLEAR:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptEnterPedClear);
                break;
            case COLUMN_PREEMPTTRACKGREEN:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptTrackGreen);
                break;
            case COLUMN_PREEMPTDWELLGREEN:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptDwellGreen);
                break;
            case COLUMN_PREEMPTMAXIMUMPRESENCE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptMaximumPresence);
                break;
            case COLUMN_PREEMPTTRACKPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptTrackPhase,
                                          table_entry->preemptTrackPhase_len);
                break;
            case COLUMN_PREEMPTDWELLPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptDwellPhase,
                                          table_entry->preemptDwellPhase_len);
                break;
            case COLUMN_PREEMPTDWELLPED:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptDwellPed,
                                          table_entry->preemptDwellPed_len);
                break;
            case COLUMN_PREEMPTEXITPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptExitPhase,
                                          table_entry->preemptExitPhase_len);
                break;
            case COLUMN_PREEMPTSTATE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptState);
                break;
            case COLUMN_PREEMPTTRACKOVERLAP:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptTrackOverlap,
                                          table_entry->preemptTrackOverlap_len);
                break;
            case COLUMN_PREEMPTDWELLOVERLAP:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptDwellOverlap,
                                          table_entry->preemptDwellOverlap_len);
                break;
            case COLUMN_PREEMPTCYCLINGPHASE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptCyclingPhase,
                                          table_entry->preemptCyclingPhase_len);
                break;
            case COLUMN_PREEMPTCYCLINGPED:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptCyclingPed,
                                          table_entry->preemptCyclingPed_len);
                break;
            case COLUMN_PREEMPTCYCLINGOVERLAP:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->preemptCyclingOverlap,
                                          table_entry->preemptCyclingOverlap_len);
                break;
            case COLUMN_PREEMPTENTERYELLOWCHANGE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptEnterYellowChange);
                break;
            case COLUMN_PREEMPTENTERREDCLEAR:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptEnterRedClear);
                break;
            case COLUMN_PREEMPTTRACKYELLOWCHANGE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptTrackYellowChange);
                break;
            case COLUMN_PREEMPTTRACKREDCLEAR:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptTrackRedClear);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTLINK:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTDELAY:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTMINIMUMDURATION:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTMINIMUMGREEN:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTMINIMUMWALK:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTENTERPEDCLEAR:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTTRACKGREEN:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTDWELLGREEN:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTMAXIMUMPRESENCE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTTRACKPHASE:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptTrackPhase));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTDWELLPHASE:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptDwellPhase));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTDWELLPED:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptDwellPed));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTEXITPHASE:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptExitPhase));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTTRACKOVERLAP:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptTrackOverlap));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTDWELLOVERLAP:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptDwellOverlap));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTCYCLINGPHASE:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptCyclingPhase));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTCYCLINGPED:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptCyclingPed));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTCYCLINGOVERLAP:
	        /* or possibly 'netsnmp_check_vb_type_and_size' */
                ret = netsnmp_check_vb_type_and_max_size(
                          request->requestvb, ASN_OCTET_STR, sizeof(table_entry->preemptCyclingOverlap));
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTENTERYELLOWCHANGE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTENTERREDCLEAR:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTTRACKYELLOWCHANGE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_PREEMPTTRACKREDCLEAR:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);

			//pregunto si hay transaccion de DB 
			if(get_globalDBManagement_variable(1)==2)
			{
				l_dbt=get_lista();
				preempt_dbt.tabla = 13;
				preempt_dbt.indice1	= table_info->index_oid[0];
				preempt_dbt.columna	= table_info->colnum;
				agregar_nodo(&(*l_dbt), &preempt_dbt);
			}    		
	
			if((table_info->colnum==COLUMN_PREEMPTTRACKPHASE || table_info->colnum==COLUMN_PREEMPTDWELLPHASE 
				|| table_info->colnum==COLUMN_PREEMPTDWELLPED || table_info->colnum==COLUMN_PREEMPTEXITPHASE 
				|| table_info->colnum==COLUMN_PREEMPTTRACKOVERLAP || COLUMN_PREEMPTDWELLOVERLAP || COLUMN_PREEMPTCYCLINGPHASE
				|| COLUMN_PREEMPTCYCLINGPED || COLUMN_PREEMPTCYCLINGOVERLAP) 
				&& get_globalDBManagement_variable(1)!=2)
			{
				netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
			}
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROL:
                table_entry->old_preemptControl = table_entry->preemptControl;
                table_entry->preemptControl     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTLINK:
                table_entry->old_preemptLink = table_entry->preemptLink;
                table_entry->preemptLink     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTDELAY:
                table_entry->old_preemptDelay = table_entry->preemptDelay;
                table_entry->preemptDelay     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTMINIMUMDURATION:
                table_entry->old_preemptMinimumDuration = table_entry->preemptMinimumDuration;
                table_entry->preemptMinimumDuration     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTMINIMUMGREEN:
                table_entry->old_preemptMinimumGreen = table_entry->preemptMinimumGreen;
                table_entry->preemptMinimumGreen     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTMINIMUMWALK:
                table_entry->old_preemptMinimumWalk = table_entry->preemptMinimumWalk;
                table_entry->preemptMinimumWalk     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTENTERPEDCLEAR:
                table_entry->old_preemptEnterPedClear = table_entry->preemptEnterPedClear;
                table_entry->preemptEnterPedClear     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTTRACKGREEN:
                table_entry->old_preemptTrackGreen = table_entry->preemptTrackGreen;
                table_entry->preemptTrackGreen     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTDWELLGREEN:
                table_entry->old_preemptDwellGreen = table_entry->preemptDwellGreen;
                table_entry->preemptDwellGreen     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTMAXIMUMPRESENCE:
                table_entry->old_preemptMaximumPresence = table_entry->preemptMaximumPresence;
                table_entry->preemptMaximumPresence     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTTRACKPHASE:
                memcpy( table_entry->old_preemptTrackPhase,
                        table_entry->preemptTrackPhase,
                        sizeof(table_entry->preemptTrackPhase));
                table_entry->old_preemptTrackPhase_len =
                        table_entry->preemptTrackPhase_len;
                memset( table_entry->preemptTrackPhase, 0,
                        sizeof(table_entry->preemptTrackPhase));
                memcpy( table_entry->preemptTrackPhase,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptTrackPhase_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTDWELLPHASE:
                memcpy( table_entry->old_preemptDwellPhase,
                        table_entry->preemptDwellPhase,
                        sizeof(table_entry->preemptDwellPhase));
                table_entry->old_preemptDwellPhase_len =
                        table_entry->preemptDwellPhase_len;
                memset( table_entry->preemptDwellPhase, 0,
                        sizeof(table_entry->preemptDwellPhase));
                memcpy( table_entry->preemptDwellPhase,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptDwellPhase_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTDWELLPED:
                memcpy( table_entry->old_preemptDwellPed,
                        table_entry->preemptDwellPed,
                        sizeof(table_entry->preemptDwellPed));
                table_entry->old_preemptDwellPed_len =
                        table_entry->preemptDwellPed_len;
                memset( table_entry->preemptDwellPed, 0,
                        sizeof(table_entry->preemptDwellPed));
                memcpy( table_entry->preemptDwellPed,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptDwellPed_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTEXITPHASE:
                memcpy( table_entry->old_preemptExitPhase,
                        table_entry->preemptExitPhase,
                        sizeof(table_entry->preemptExitPhase));
                table_entry->old_preemptExitPhase_len =
                        table_entry->preemptExitPhase_len;
                memset( table_entry->preemptExitPhase, 0,
                        sizeof(table_entry->preemptExitPhase));
                memcpy( table_entry->preemptExitPhase,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptExitPhase_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTTRACKOVERLAP:
                memcpy( table_entry->old_preemptTrackOverlap,
                        table_entry->preemptTrackOverlap,
                        sizeof(table_entry->preemptTrackOverlap));
                table_entry->old_preemptTrackOverlap_len =
                        table_entry->preemptTrackOverlap_len;
                memset( table_entry->preemptTrackOverlap, 0,
                        sizeof(table_entry->preemptTrackOverlap));
                memcpy( table_entry->preemptTrackOverlap,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptTrackOverlap_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTDWELLOVERLAP:
                memcpy( table_entry->old_preemptDwellOverlap,
                        table_entry->preemptDwellOverlap,
                        sizeof(table_entry->preemptDwellOverlap));
                table_entry->old_preemptDwellOverlap_len =
                        table_entry->preemptDwellOverlap_len;
                memset( table_entry->preemptDwellOverlap, 0,
                        sizeof(table_entry->preemptDwellOverlap));
                memcpy( table_entry->preemptDwellOverlap,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptDwellOverlap_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTCYCLINGPHASE:
                memcpy( table_entry->old_preemptCyclingPhase,
                        table_entry->preemptCyclingPhase,
                        sizeof(table_entry->preemptCyclingPhase));
                table_entry->old_preemptCyclingPhase_len =
                        table_entry->preemptCyclingPhase_len;
                memset( table_entry->preemptCyclingPhase, 0,
                        sizeof(table_entry->preemptCyclingPhase));
                memcpy( table_entry->preemptCyclingPhase,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptCyclingPhase_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTCYCLINGPED:
                memcpy( table_entry->old_preemptCyclingPed,
                        table_entry->preemptCyclingPed,
                        sizeof(table_entry->preemptCyclingPed));
                table_entry->old_preemptCyclingPed_len =
                        table_entry->preemptCyclingPed_len;
                memset( table_entry->preemptCyclingPed, 0,
                        sizeof(table_entry->preemptCyclingPed));
                memcpy( table_entry->preemptCyclingPed,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptCyclingPed_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTCYCLINGOVERLAP:
                memcpy( table_entry->old_preemptCyclingOverlap,
                        table_entry->preemptCyclingOverlap,
                        sizeof(table_entry->preemptCyclingOverlap));
                table_entry->old_preemptCyclingOverlap_len =
                        table_entry->preemptCyclingOverlap_len;
                memset( table_entry->preemptCyclingOverlap, 0,
                        sizeof(table_entry->preemptCyclingOverlap));
                memcpy( table_entry->preemptCyclingOverlap,
                        request->requestvb->val.string,
                        request->requestvb->val_len);
                table_entry->preemptCyclingOverlap_len =
                        request->requestvb->val_len;
                break;
            case COLUMN_PREEMPTENTERYELLOWCHANGE:
                table_entry->old_preemptEnterYellowChange = table_entry->preemptEnterYellowChange;
                table_entry->preemptEnterYellowChange     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTENTERREDCLEAR:
                table_entry->old_preemptEnterRedClear = table_entry->preemptEnterRedClear;
                table_entry->preemptEnterRedClear     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTTRACKYELLOWCHANGE:
                table_entry->old_preemptTrackYellowChange = table_entry->preemptTrackYellowChange;
                table_entry->preemptTrackYellowChange     = *request->requestvb->val.integer;
                break;
            case COLUMN_PREEMPTTRACKREDCLEAR:
                table_entry->old_preemptTrackRedClear = table_entry->preemptTrackRedClear;
                table_entry->preemptTrackRedClear     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROL:
                table_entry->preemptControl     = table_entry->old_preemptControl;
                table_entry->old_preemptControl = 0;
                break;
            case COLUMN_PREEMPTLINK:
                table_entry->preemptLink     = table_entry->old_preemptLink;
                table_entry->old_preemptLink = 0;
                break;
            case COLUMN_PREEMPTDELAY:
                table_entry->preemptDelay     = table_entry->old_preemptDelay;
                table_entry->old_preemptDelay = 0;
                break;
            case COLUMN_PREEMPTMINIMUMDURATION:
                table_entry->preemptMinimumDuration     = table_entry->old_preemptMinimumDuration;
                table_entry->old_preemptMinimumDuration = 0;
                break;
            case COLUMN_PREEMPTMINIMUMGREEN:
                table_entry->preemptMinimumGreen     = table_entry->old_preemptMinimumGreen;
                table_entry->old_preemptMinimumGreen = 0;
                break;
            case COLUMN_PREEMPTMINIMUMWALK:
                table_entry->preemptMinimumWalk     = table_entry->old_preemptMinimumWalk;
                table_entry->old_preemptMinimumWalk = 0;
                break;
            case COLUMN_PREEMPTENTERPEDCLEAR:
                table_entry->preemptEnterPedClear     = table_entry->old_preemptEnterPedClear;
                table_entry->old_preemptEnterPedClear = 0;
                break;
            case COLUMN_PREEMPTTRACKGREEN:
                table_entry->preemptTrackGreen     = table_entry->old_preemptTrackGreen;
                table_entry->old_preemptTrackGreen = 0;
                break;
            case COLUMN_PREEMPTDWELLGREEN:
                table_entry->preemptDwellGreen     = table_entry->old_preemptDwellGreen;
                table_entry->old_preemptDwellGreen = 0;
                break;
            case COLUMN_PREEMPTMAXIMUMPRESENCE:
                table_entry->preemptMaximumPresence     = table_entry->old_preemptMaximumPresence;
                table_entry->old_preemptMaximumPresence = 0;
                break;
            case COLUMN_PREEMPTTRACKPHASE:
                memcpy( table_entry->preemptTrackPhase,
                        table_entry->old_preemptTrackPhase,
                        sizeof(table_entry->preemptTrackPhase));
                memset( table_entry->old_preemptTrackPhase, 0,
                        sizeof(table_entry->preemptTrackPhase));
                table_entry->preemptTrackPhase_len =
                        table_entry->old_preemptTrackPhase_len;
                break;
            case COLUMN_PREEMPTDWELLPHASE:
                memcpy( table_entry->preemptDwellPhase,
                        table_entry->old_preemptDwellPhase,
                        sizeof(table_entry->preemptDwellPhase));
                memset( table_entry->old_preemptDwellPhase, 0,
                        sizeof(table_entry->preemptDwellPhase));
                table_entry->preemptDwellPhase_len =
                        table_entry->old_preemptDwellPhase_len;
                break;
            case COLUMN_PREEMPTDWELLPED:
                memcpy( table_entry->preemptDwellPed,
                        table_entry->old_preemptDwellPed,
                        sizeof(table_entry->preemptDwellPed));
                memset( table_entry->old_preemptDwellPed, 0,
                        sizeof(table_entry->preemptDwellPed));
                table_entry->preemptDwellPed_len =
                        table_entry->old_preemptDwellPed_len;
                break;
            case COLUMN_PREEMPTEXITPHASE:
                memcpy( table_entry->preemptExitPhase,
                        table_entry->old_preemptExitPhase,
                        sizeof(table_entry->preemptExitPhase));
                memset( table_entry->old_preemptExitPhase, 0,
                        sizeof(table_entry->preemptExitPhase));
                table_entry->preemptExitPhase_len =
                        table_entry->old_preemptExitPhase_len;
                break;
            case COLUMN_PREEMPTTRACKOVERLAP:
                memcpy( table_entry->preemptTrackOverlap,
                        table_entry->old_preemptTrackOverlap,
                        sizeof(table_entry->preemptTrackOverlap));
                memset( table_entry->old_preemptTrackOverlap, 0,
                        sizeof(table_entry->preemptTrackOverlap));
                table_entry->preemptTrackOverlap_len =
                        table_entry->old_preemptTrackOverlap_len;
                break;
            case COLUMN_PREEMPTDWELLOVERLAP:
                memcpy( table_entry->preemptDwellOverlap,
                        table_entry->old_preemptDwellOverlap,
                        sizeof(table_entry->preemptDwellOverlap));
                memset( table_entry->old_preemptDwellOverlap, 0,
                        sizeof(table_entry->preemptDwellOverlap));
                table_entry->preemptDwellOverlap_len =
                        table_entry->old_preemptDwellOverlap_len;
                break;
            case COLUMN_PREEMPTCYCLINGPHASE:
                memcpy( table_entry->preemptCyclingPhase,
                        table_entry->old_preemptCyclingPhase,
                        sizeof(table_entry->preemptCyclingPhase));
                memset( table_entry->old_preemptCyclingPhase, 0,
                        sizeof(table_entry->preemptCyclingPhase));
                table_entry->preemptCyclingPhase_len =
                        table_entry->old_preemptCyclingPhase_len;
                break;
            case COLUMN_PREEMPTCYCLINGPED:
                memcpy( table_entry->preemptCyclingPed,
                        table_entry->old_preemptCyclingPed,
                        sizeof(table_entry->preemptCyclingPed));
                memset( table_entry->old_preemptCyclingPed, 0,
                        sizeof(table_entry->preemptCyclingPed));
                table_entry->preemptCyclingPed_len =
                        table_entry->old_preemptCyclingPed_len;
                break;
            case COLUMN_PREEMPTCYCLINGOVERLAP:
                memcpy( table_entry->preemptCyclingOverlap,
                        table_entry->old_preemptCyclingOverlap,
                        sizeof(table_entry->preemptCyclingOverlap));
                memset( table_entry->old_preemptCyclingOverlap, 0,
                        sizeof(table_entry->preemptCyclingOverlap));
                table_entry->preemptCyclingOverlap_len =
                        table_entry->old_preemptCyclingOverlap_len;
                break;
            case COLUMN_PREEMPTENTERYELLOWCHANGE:
                table_entry->preemptEnterYellowChange     = table_entry->old_preemptEnterYellowChange;
                table_entry->old_preemptEnterYellowChange = 0;
                break;
            case COLUMN_PREEMPTENTERREDCLEAR:
                table_entry->preemptEnterRedClear     = table_entry->old_preemptEnterRedClear;
                table_entry->old_preemptEnterRedClear = 0;
                break;
            case COLUMN_PREEMPTTRACKYELLOWCHANGE:
                table_entry->preemptTrackYellowChange     = table_entry->old_preemptTrackYellowChange;
                table_entry->old_preemptTrackYellowChange = 0;
                break;
            case COLUMN_PREEMPTTRACKREDCLEAR:
                table_entry->preemptTrackRedClear     = table_entry->old_preemptTrackRedClear;
                table_entry->old_preemptTrackRedClear = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}


/** handles requests for the preemptControlTable table */
int
preemptControlTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct preemptControlTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("preemptControlTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptControlTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROLNUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptControlNumber);
                break;
            case COLUMN_PREEMPTCONTROLSTATE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->preemptControlState);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptControlTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROLSTATE:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptControlTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROLSTATE:
                table_entry->old_preemptControlState = table_entry->preemptControlState;
                table_entry->preemptControlState     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct preemptControlTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_PREEMPTCONTROLSTATE:
                table_entry->preemptControlState     = table_entry->old_preemptControlState;
                table_entry->old_preemptControlState = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}
