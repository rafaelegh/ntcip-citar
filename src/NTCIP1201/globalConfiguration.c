
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <time.h>
#include "asc.h"
#include "citar.h"

int globalSetIDParameter = 0;
int globalMaxModules = 1; 
char controllerBaseStandards[24];

struct globalModuleTable_entry * entry23[1];

/* create a new row in the table */
netsnmp_tdata_row *
globalConfiguration_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int indice2, int tabla) 
{
    netsnmp_tdata_row *row;

	int i=0;

	switch(tabla)
	{
		case 23:

    	entry23[indice] = SNMP_MALLOC_TYPEDEF(struct globalModuleTable_entry);
    	if (!entry23[indice])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry23[indice]);
	        return NULL;
	    }
    	row->data = entry23[indice];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry23[indice]->moduleNumber = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry23[indice]->moduleNumber),
                                 sizeof(entry23[indice]->moduleNumber));

		break;
		
    }
			if (tabla_data)
        	netsnmp_tdata_add_row( tabla_data, row );
        return row;

    
}

void
initialize_globalConfiguration_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	unsigned char min_columna, unsigned char max_columna, unsigned char tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int i=0, n=0, j=0;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 23:
		reg = netsnmp_create_handler_registration(
              nombre,     globalModuleTable_handler,
              oid_table, oid_len,
              HANDLER_CAN_RONLY
              );
		n=globalMaxModules;
		break;

	}
 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }


	netsnmp_table_helper_add_indexes(table_info, ASN_INTEGER,  /* index: splitNumber */ 0);


    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

/*    for(i=0;i<16;i++) initialize_row_Table(table_data,i+1);
*/
	for(i=0;i<n;i++)
	{		
		row=globalConfiguration_tables_createEntry(table_data, i, j, tabla);
	}
}

void
init_globalConfiguration(void)
{
  const oid globalSetIDParameter_oid[] = { 1,3,6,1,4,1,1206,4,2,6,1,1 };
  const oid globalMaxModules_oid[] = 	 { 1,3,6,1,4,1,1206,4,2,6,1,2 };
  const oid controllerBaseStandards_oid[] = { 1,3,6,1,4,1,1206,4,2,6,1,4 };

  const oid globalModuleTable_oid[] = {1,3,6,1,4,1,1206,4,2,6,1,3};
  const size_t globalModuleTable_oid_len   = OID_LENGTH(globalModuleTable_oid);

  DEBUGMSGTL(("globalSetIDParameter", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("globalSetIDParameter", handle_globalConfiguration,
                               globalSetIDParameter_oid, OID_LENGTH(globalSetIDParameter_oid),
                               HANDLER_CAN_RONLY
        ));

  DEBUGMSGTL(("globalMaxModules", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("globalMaxModules", handle_globalConfiguration,
                               globalMaxModules_oid, OID_LENGTH(globalMaxModules_oid),
                               HANDLER_CAN_RONLY
        ));



  DEBUGMSGTL(("controllerBaseStandards", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("controllerBaseStandards", handle_globalConfiguration,
                               controllerBaseStandards_oid, OID_LENGTH(controllerBaseStandards_oid),
                               HANDLER_CAN_RONLY
        ));

	initialize_globalConfiguration_tables("globalModuleTable",globalModuleTable_oid,
globalModuleTable_oid_len, COLUMN_MODULENUMBER, COLUMN_MODULETYPE,23);

}

int
handle_globalConfiguration(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
    /* We are never called for a GETNEXT if it's registered as a
       "instance", as it's "magically" handled for us.  */

    /* a instance handler also only hands us one request at a time, so
       we don't need to loop over a list of requests; we'll only get one. */
    int resul=0;
    switch(reqinfo->mode) {

        case MODE_GET:
           			resul=compara_oid_global(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&globalSetIDParameter,
				sizeof(int));
				break;
				case 2:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&globalMaxModules,
				sizeof(int));
				break;
				case 4:
				snmp_set_var_typed_value(requests->requestvb,  ASN_OCTET_STR,controllerBaseStandards,sizeof(controllerBaseStandards));
				break;
				
			}
            break;


        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_globalSetIDParameter\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    }

    return SNMP_ERR_NOERROR;
}

/** handles requests for the globalModuleTable table */
int
globalModuleTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct globalModuleTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("globalModuleTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct globalModuleTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_MODULENUMBER:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->moduleNumber);
                break;
            case COLUMN_MODULEDEVICENODE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OBJECT_ID,
                                          table_entry->moduleDeviceNode,
                                          table_entry->moduleDeviceNode_len);
                break;
            case COLUMN_MODULEMAKE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->moduleMake,
                                          table_entry->moduleMake_len);
                break;
            case COLUMN_MODULEMODEL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->moduleModel,
                                          table_entry->moduleModel_len);
                break;
            case COLUMN_MODULEVERSION:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_value( request->requestvb, ASN_OCTET_STR,
                                          table_entry->moduleVersion,
                                          table_entry->moduleVersion_len);
                break;
            case COLUMN_MODULETYPE:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->moduleType);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

    }
    return SNMP_ERR_NOERROR;
}
