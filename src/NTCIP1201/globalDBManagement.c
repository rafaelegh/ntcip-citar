
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <time.h>
#include "asc.h"
#include "citar.h"
#include "listas.h"
#include "prog_ntcip.h"

int	 dbCreateTransaction = 1;

int	 dbVerifyStatus = 0;
char dbVerifyError[35]="";
size_t dbVerifyError_len;


int get_globalDBManagement_variable(int _numero)
{
	int _resp=-1;

	if(_numero==1)
	{
		_resp=dbCreateTransaction;
	}
	else if(_numero==2)
	{
		_resp=dbVerifyStatus;
	}

	return _resp;
}



/*	funcion que chequea la regla de consistencia de la concurrencia:

	anillo_fase_ref : anillo de la fase de referencia a la que chequeras la regla
	fase_concurrente : fase concurrente de la fase referencia
	anillo_fase_concurrente: anillo de la fase concurrente 
	nFases: numero de fases concurrentes de la fase referencia

	se recorre la cadena hexadecimal de fases concurrentes y se obtiene los anillos
	a los que pertenece, si alguno de los anillos de las fases concurrentes es igual
	al anillo de la fase de referencia, se detecta la falla
*/
int chequea_falla_concurrencia(int fase_ref)
{
	struct phaseTable_entry ** _entry1=NULL;

	int anillo_fase_concurrente=0, fase_concurrente=0, anillo_fase_ref=0, falla=0, nFases=0;

	char c='0', mensaje[20]=""; 

	_entry1=get_phase_table();

//	printf("fase_ref: %i\n",fase_ref);

	anillo_fase_ref = _entry1[fase_ref-1]->phaseRing;

	nFases = _entry1[fase_ref-1]->phaseConcurrency_len; 

//	printf("anillo_fase_ref: %i, nFases: %i\n",anillo_fase_ref,nFases);

	if(anillo_fase_ref > 0)
	{

	for(int i=0;i<nFases;i++)
	{
		
		fase_concurrente = _entry1[fase_ref-1]->phaseConcurrency[i];

		anillo_fase_concurrente = _entry1[fase_concurrente-1]->phaseRing;

//		printf("fase_concurrente: %i, anillo_fase_concurrente:%i\n",
//		fase_concurrente,anillo_fase_concurrente);

		if(anillo_fase_concurrente == anillo_fase_ref)
		{
			falla=1;
		}	
	}

	}

	if(falla!=0)
	{
		sprintf(dbVerifyError, "PHASE %i%i CONCURRENCY FAULT",0,fase_ref);
	}
	else
	{
		memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));		
	}

	return falla;
}

/*	funcion que chequea la regla de consistencia mutua:

	concurrente_fase_ref: fase concurrente de la fase de referencia
	fase_concurrente : fase concurrente de la concurrente_fase_ref
	nFases_concurrente: numero de fases concurrente de la fase concurrente con respecto a la referencia
	nFases_concurrentes_ref: numero de fases concurrentes de la fase referencia

	se obtiene las fases concurrentes de la fase referencia, luego se recorre de cada una de esas fases
	sus respectivos concurrentes, entonces se compara si existe la fase de referencia en los concurrentes
	de los concurrente con respecto a la fase de referencia, si no existe en alguna de los concurrentes de 
	fase de referencia se detecta la falla.

	ejemplo:

	phaseConcurrency.1 = 05 06 "si posee 05 06, entonces debe existir 01 en phaseConcurrency.5 y phaseConcurrency.6"

	fase_ref = 1, nFases_concurrentes_ref = 2
	
	concurrente_fase_ref = 05 ó 06, nFases_concurrente = 2 

	phaseConcurrency.5 = 01 02	
*/

int chequea_falla_mutual(int fase_ref)
{
	struct phaseTable_entry ** _entry1=NULL;
//	FILE *fp=NULL;
	int fase_concurrente=0, concurrente_fase_ref=0, falla=0, *existe_concurrente=0, hay_concurrencias=0;
	char c='0', mensaje[20]=""; 

	size_t nFases_concurrentes_ref=0, nFases_concurrente=0;

	_entry1 = get_phase_table();
//	printf("fase_ref: %i\n", fase_ref);
	nFases_concurrentes_ref = _entry1[fase_ref-1]->phaseConcurrency_len;
//	printf("nFases_concurrentes_ref: %i\n", nFases_concurrentes_ref);
	existe_concurrente = malloc (nFases_concurrentes_ref * sizeof(int));

	if(nFases_concurrentes_ref >= 1)
	{
		hay_concurrencias = _entry1[fase_ref-1]->phaseConcurrency[0];
		if(hay_concurrencias > 0)
		{			

			for(int i=0; i<nFases_concurrentes_ref ;i++)
			{
				concurrente_fase_ref = _entry1[fase_ref-1]->phaseConcurrency[i];

				nFases_concurrente = _entry1[concurrente_fase_ref-1]->phaseConcurrency_len;

//				printf("concurrente_fase_ref: %i, nFases_concurrente: %i\n",
//				concurrente_fase_ref,nFases_concurrente);

				for(int j=0; j<nFases_concurrente; j++)
				{
					fase_concurrente = _entry1[concurrente_fase_ref-1]->phaseConcurrency[j];
//					printf("fase_concurrente: %i\n", fase_concurrente);
					if(fase_concurrente == fase_ref)
					{
						existe_concurrente[i]=1;
						
					}
				}	
			}

	/*se pregunto si se detecto la fase referencia en los concurrentes de los concurrentes
	con respecto a la fase referencia, si en alguno no se detecto se levanta la falla
	*/

			for(int k=0; k<nFases_concurrentes_ref; k++)
			{
				if(existe_concurrente[k]!=1) 
				{
					falla=1;
				}
			}

		}
	}

	if(falla == 1)
	{
		sprintf(dbVerifyError, "PHASE %i%i MUTUAL FAULT",0,fase_ref);
	}
	else
	{
		memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));
	}



	return falla;
}

int chequea_falla_misma_fase_seq(int seq_ref, int anillo_ref)
{
	int longitud_arreglo=0, nSequence=0, fase_a=0, fase_b=0, instancias=0, falla=0,
	seq_0=0;	

	struct sequenceTable_entry _entry14[30][4];

	get_sequence_table(_entry14);

//	nSequence = get_ring_variable(1);



//	for(int i=0;i<nSequence;i++)
//	{
		longitud_arreglo = _entry14[seq_ref-1][anillo_ref-1].sequenceData_len;
		seq_0 = _entry14[seq_ref-1][anillo_ref-1].sequenceData[0];

		if(longitud_arreglo > 0 && seq_0!=0)
		{
			for(int j=0; j<longitud_arreglo; j++)
			{
				fase_a = _entry14[seq_ref-1][anillo_ref-1].sequenceData[j];

				for(int k=0; k<longitud_arreglo; k++)
				{
					fase_b = _entry14[seq_ref-1][anillo_ref-1].sequenceData[k];

					if(fase_a == fase_b)
					{
						instancias++;
						if(instancias > 1) 
						{
							falla = 1;
						}
					}	
				}
				instancias = 0;
			}
		}
//	}

	if(falla == 1)
	{
		sprintf(dbVerifyError, "SEQ %i%i SAME PHASE FAULT",0,seq_ref);
	}
	else
	{
		memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));
	}

	return falla;

}

int chequea_falla_secuencia_anillo(int seq_ref, int anillo_ref)
{
	int nAnillos=0, anillo_fase=0, fase_sequencia=0, falla_anillo=0, longitud_arreglo=0, falla=0, seq_0=0;

	struct sequenceTable_entry _entry14[30][4];
	struct phaseTable_entry ** _entry1=NULL;

	char parte2[20];

	get_sequence_table(_entry14);
	_entry1 = get_phase_table();

//	nAnillos = get_ring_variable(1);
	
/*
	for(int i=0;i<nAnillos;i++)
	{
*/
		longitud_arreglo = _entry14[seq_ref-1][anillo_ref-1].sequenceData_len;

		seq_0 = _entry14[seq_ref-1][anillo_ref-1].sequenceData[0];

		if(longitud_arreglo > 0 && seq_0!=0)
		{
			for(int j=0;j<longitud_arreglo;j++)
			{
				fase_sequencia = _entry14[seq_ref-1][anillo_ref-1].sequenceData[j];

				anillo_fase = _entry1[fase_sequencia-1]->phaseRing;
				if(anillo_fase != anillo_ref)
				{
					falla = 1;
				}
			}
		}
//	}

	if(falla == 1)
	{
		sprintf(dbVerifyError, "SEQ %i%i RING ",0,seq_ref);
		sprintf(parte2,"%i FAULT",anillo_ref);
		strcat(dbVerifyError,parte2);
	}
	else
	{
		memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));
	}
			
	return falla;	
}	

int chequea_falla_secuencia_anillo_fase_omitida(int seq_ref, int anillo_ref)
{
	int falla=0, fase_sequencia=0,longitud_arreglo=0, nFases=0, existe=0, fase_anillo=0, anillo_fase=0,
	seq_0=0;

	struct sequenceTable_entry _entry14[30][4];
	struct phaseTable_entry ** _entry1=NULL;

	get_sequence_table(_entry14);
	_entry1 = get_phase_table();

	char parte2[15];

	nFases = get_phase_variable(1);

	longitud_arreglo = _entry14[seq_ref-1][anillo_ref-1].sequenceData_len;
	seq_0 = _entry14[seq_ref-1][anillo_ref-1].sequenceData[0];

	if(longitud_arreglo > 0 && seq_0!=0)
	{
		for(int i=0;i<nFases;i++)
		{
			anillo_fase = _entry1[i]->phaseRing;
		
			if(anillo_fase == anillo_ref)
			{			
				
				for(int j=0; j<longitud_arreglo; j++)
				{
					fase_sequencia = _entry14[seq_ref-1][anillo_ref-1].sequenceData[j];
					
					fase_anillo = i+1;
		
					if(fase_anillo == fase_sequencia)
					{
						existe = 1;
					}
				}
				if(existe == 0)
				{
					falla = 1;
				}
				existe = 0;
			}
		}
	}

	if(falla == 1)
	{
		sprintf(dbVerifyError, "SEQ %i%i RING ",0,seq_ref);
		sprintf(parte2,"%i PHS OMITTED",anillo_ref);
		strcat(dbVerifyError,parte2);
	}
	else
	{
		memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));
	}
	
	return falla;			
}

int compara_concurrencia(int a, int b)
{
	int _resp=0, longitud_a=0, longitud_b=0, cuenta_cmp=0, fase_concurrente_a=0, fase_concurrente_b=0;

	struct phaseTable_entry ** _entry1=NULL;

	_entry1 = get_phase_table();

	longitud_a = _entry1[a]->phaseConcurrency_len;
	longitud_b = _entry1[b]->phaseConcurrency_len;

	if(longitud_a == longitud_b)
	{
		for(int i=0;i<longitud_a;i++)
		{	
			fase_concurrente_a = _entry1[a]->phaseConcurrency[i];
			fase_concurrente_b = _entry1[b]->phaseConcurrency[i];

			if(fase_concurrente_a == fase_concurrente_b)
			{
				cuenta_cmp++;
			}
		}

		if(cuenta_cmp == longitud_a) _resp = 1;
		else _resp = 0;
	}
	else
	{
		_resp=0;
	}

	return _resp;
}

void asigna_CG(int _concurrency_group[])
{
	int nFases=0, cg_cuenta=1, longitud_concurrencia=0, res=0, anillo_a=0, anillo_b=0;

	struct phaseTable_entry ** _entry1=NULL;

	_entry1 = get_phase_table();

	nFases = get_phase_variable(1);

	for(int i=0;i<nFases;i++)
	{
		longitud_concurrencia = _entry1[i]->phaseConcurrency_len;

		if(longitud_concurrencia > 0)
		{		
			_concurrency_group[i]=cg_cuenta;
			anillo_a = _entry1[i]->phaseRing;

			for(int j=i+1;j<nFases;j++)
			{
				longitud_concurrencia = _entry1[j]->phaseConcurrency_len;
				anillo_b = _entry1[j]->phaseRing;

				if(longitud_concurrencia > 0)
				{
					res=compara_concurrencia(i,j);
//					printf("comparacion: %i, i: %i, j: %i, cuenta_cg: %i \n",res,i,j,cg_cuenta);
					if(res==1)
					{
						_concurrency_group[j]=cg_cuenta;
					}
					else 
					{
						if(anillo_b > anillo_a) cg_cuenta=1;
						else cg_cuenta++;
						i=j-1;
						j=nFases;
					}
				}
			}
		}

	}
}

int chequea_falla_anillo_secuencia(int seq_ref, int anillo_ref)
{
	int falla=0, concurrency_group[16], nFases=0, longitud_arreglo=0, cg_a=0, cg_b=1,fase_sequencia_a=0,
	fase_sequencia_b=0, bandera_1=0, seq_0=0;

	struct sequenceTable_entry _entry14[30][4];
	struct phaseTable_entry ** _entry1=NULL;

	get_sequence_table(_entry14);
	_entry1 = get_phase_table();

	nFases = get_phase_variable(1);

	longitud_arreglo = _entry14[seq_ref-1][anillo_ref-1].sequenceData_len;
	seq_0 = _entry14[seq_ref-1][anillo_ref-1].sequenceData[0];
//	printf("fase_ref: %i,anillo_ref: %i\n",seq_ref,anillo_ref);
	for(int p=0;p<16;p++) concurrency_group[p]=0;
	asigna_CG(concurrency_group);
//	for(int p=0;p<16;p++) printf("%i ",concurrency_group[p]);
//	printf("\n");

	if(seq_0 != 0)
	{
	for(int i=0;i<longitud_arreglo;i++)
	{
		fase_sequencia_a = _entry14[seq_ref-1][anillo_ref-1].sequenceData[i];

//		fase_sequencia_b = _entry14[seq_ref-1][anillo_ref-1].sequenceData[j+1];

		cg_a = concurrency_group[fase_sequencia_a-1];
//		printf("cg_a: %i, cg_b: %i\n",cg_a,cg_b);
		if( cg_b == cg_a)
		{
			cg_b = cg_a;
		}
		else if (cg_b+1 == cg_a)
		{
			cg_b++;
		}
		else
		{
			falla = 1;
		}
	}
	}
	if(falla == 0)
	{
		
	}

	if(falla == 1)
	{
		sprintf(dbVerifyError, "SEQ %i%i RING SEQ FAULT",0,seq_ref);
	}
	else if(falla == 2)
	{
		sprintf(dbVerifyError, "SEQ %i%i CG SEQ FAULT",0,seq_ref);
	}
	else
	{
		memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));
	}
	
	return falla;
}

/*	esta funcion revisa errores propios de la programacion por objetos publicos NTCIP
	esta funcion solo revisa el primer anillo y chequea el numero de split de cada fase y 
	revisa que la suma de los splits de sus concurrentes sea igual al split de referencia,
	solo revisa los concurrentes que estan en el anillo 2
	
*/
int chequea_error_propio1(int _data_split[][16], struct tiempos_ntcip _dato_tiempos[])
{
	int ciclos[16], split_ref=0, split_conc=0,nSplit=0, fase_conc=0, vresul=0, falla=0, longitud_seq=0,longitud_conc=0,nSeq=0,seq=0,seq_0=0,conc_0=0;

	struct patternTable_entry ** p_pattern=NULL;
	struct phaseTable_entry ** p_phase=NULL;
	struct sequenceTable_entry t_secuencias[30][4];

	//chequea si la suma de los splits es igual al ciclo

	for(int i=0;i<16;i++)
	{
		ciclos[i]=0;
	}

	p_pattern = get_pattern_table();
	p_phase = get_phase_table();
	get_sequence_table(t_secuencias);

	for(int i=0;i<16;i++)
	{
		if(p_pattern[i]->patternCycleTime > 0)
		{
			nSplit = p_pattern[i]->patternSplitNumber-1;

			nSeq = p_pattern[i]->patternSequenceNumber-1;

				longitud_seq = t_secuencias[nSeq][0].sequenceData_len;
				seq_0 = t_secuencias[nSeq][0].sequenceData[0];

				if(longitud_seq > 0 && seq_0!=0)
				{

					for(int k=0;k<longitud_seq;k++)
					{
						seq = t_secuencias[nSeq][0].sequenceData[k];
						split_ref = _data_split[nSplit][seq-1];
						longitud_conc = p_phase[seq-1]->phaseConcurrency_len;
						conc_0 = p_phase[seq-1]->phaseConcurrency[0];
						if(longitud_conc > 0 && conc_0 != 0)
						{
							for(int l=0;l<longitud_conc;l++)
							{
								fase_conc = p_phase[seq-1]->phaseConcurrency[l];
								split_conc += _data_split[nSplit][fase_conc-1];
								printf("fase_conc: %i, split_conc: %i\n",fase_conc,split_conc);
							}
							if(split_ref != split_conc)
							{
								sprintf(dbVerifyError, "ERROR SPLIT %i INSUF.",seq);
								falla = 1;
								break;
							}
							
						}
						split_conc = 0;
						ciclos[i] += _data_split[nSplit][seq-1]; 
					}
				}

			printf("ciclo %i: %i\n",i+1,ciclos[i]);

			if(ciclos[i] != p_pattern[i]->patternCycleTime && falla == 0)
			{
				sprintf(dbVerifyError, "ERROR SPLITS %i DIF. CICLO",i+1);
				falla = 2;
				break;
			}
		}

	}

	if(falla == 0)
	{
		//chequea el vresultante
		for(int i=0;i<16;i++)
		{
			for(int j=0;j<16;j++)
			{
				if( _data_split[i][j] > 0)
				{
/*
					if(_dato_tiempos[j].minimoVerde > _dato_tiempos[j].blancoPeatonal)
					{
						vresul = _data_split[i][j]-_dato_tiempos[j].RojoAmarillo-_dato_tiempos[j].minimoVerde-
						_dato_tiempos[j].despejePeatonal-_dato_tiempos[j].cambioAmarillo-_dato_tiempos[j].todoRojo;
					}
					else
					{
						vresul=_data_split[i][j]-_dato_tiempos[j].RojoAmarillo-_dato_tiempos[j].blancoPeatonal-
						_dato_tiempos[j].despejePeatonal-_dato_tiempos[j].cambioAmarillo-_dato_tiempos[j].todoRojo;
					}
*/
					vresul = _data_split[i][j]-_dato_tiempos[j].RojoAmarillo-_dato_tiempos[j].minimoVerde
					-_dato_tiempos[j].cambioAmarillo-_dato_tiempos[j].todoRojo;


					printf("vresul: %i\n",vresul);

					if(vresul < 0 && falla == 0)
					{
						sprintf(dbVerifyError, "ERROR SPLITS %i PHASE %i",i+1,j+1);
						falla = 1;
					}
				}
			}


		}
	}

	
	return falla;
	
}

void verificar_transacciones(void)
{
	ptnodo * l_dbt=NULL;

	l_dbt=get_lista();
	int resul=0, falla_detectada=0, aux=0, *conexion_gdbm=NULL;

	while(*l_dbt!=NULL)
	{

		if((*l_dbt)->data.tabla==1 && falla_detectada == 0)
		{
			if((*l_dbt)->data.columna==COLUMN_PHASECONCURRENCY || (*l_dbt)->data.columna==COLUMN_PHASERING)
			{
				if((*l_dbt)->data.columna == COLUMN_PHASECONCURRENCY)
				{
					falla_detectada = chequea_falla_concurrencia((*l_dbt)->data.indice1);
					if(falla_detectada == 0) falla_detectada = chequea_falla_mutual((*l_dbt)->data.indice1);
				}
				else falla_detectada = chequea_falla_concurrencia((*l_dbt)->data.indice1);
			}

		}

		else if ((*l_dbt)->data.tabla==14 && falla_detectada == 0)
		{
			if((*l_dbt)->data.columna==COLUMN_SEQUENCEDATA)
			{
				falla_detectada = chequea_falla_misma_fase_seq((*l_dbt)->data.indice1,(*l_dbt)->data.indice2);
				if(falla_detectada == 0)
				{
					falla_detectada = chequea_falla_secuencia_anillo((*l_dbt)->data.indice1,(*l_dbt)->data.indice2);
					if(falla_detectada == 0)
					{
						falla_detectada = chequea_falla_secuencia_anillo_fase_omitida((*l_dbt)->data.indice1,(*l_dbt)->data.indice2);
	/*					if(falla_detectada == 0)
						{
							falla_detectada = chequea_falla_anillo_secuencia((*l_dbt)->data.indice1,(*l_dbt)->data.indice2);
						}
	*/ 
					}
				}
			}
		}
		if(falla_detectada != 0) resul = 1;

		borrar_nodo(&(*l_dbt));
	}

	conexion_gdbm = get_conexion();

	if(resul == 0)
	{
		aux=get_secuencia_programacion();
		if(aux == 1)
		{
			aux = 0;
			conversion_ntcip_citar(conexion_gdbm);
			set_secuencia_programacion(15);
			memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));		
		}
		else
		{
			aux = get_secuencia_programacion_bloque();
			if(aux == 1)
			{
				conversion_ntcip_citar(conexion_gdbm);
				//convertir_estructura_ntcip_citar(conexion_gdbm);
				limpiar_secuencia_programacion_bloque();
				memcpy( dbVerifyError,"NO VERIFICATION ERROR", sizeof("NO VERIFICATION ERROR"));		
			}
		}
	}		

	dbCreateTransaction=6;

}

void
init_globalDBManagement(void)
{
    const oid dbCreateTransaction_oid[] = { 1,3,6,1,4,1,1206,4,2,6,2,1 };
    const oid dbVerifyStatus_oid[] = { 1,3,6,1,4,1,1206,4,2,6,2,6 };
    const oid dbVerifyError_oid[] = { 1,3,6,1,4,1,1206,4,2,6,2,7 };

  DEBUGMSGTL(("dbCreateTransaction", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("dbCreateTransaction", handle_globalDBManagement,
                               dbCreateTransaction_oid, OID_LENGTH(dbCreateTransaction_oid),
                               HANDLER_CAN_RWRITE
        ));

  DEBUGMSGTL(("dbVerifyStatus", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("dbVerifyStatus", handle_globalDBManagement,
                               dbVerifyStatus_oid, OID_LENGTH(dbVerifyStatus_oid),
                               HANDLER_CAN_RONLY
        ));

  DEBUGMSGTL(("dbVerifyError", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("dbVerifyError", handle_globalDBManagement,
                               dbVerifyError_oid, OID_LENGTH(dbVerifyError_oid),
                               HANDLER_CAN_RONLY
        ));



}

int
handle_globalDBManagement(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
    /* We are never called for a GETNEXT if it's registered as a
       "instance", as it's "magically" handled for us.  */

    /* a instance handler also only hands us one request at a time, so
       we don't need to loop over a list of requests; we'll only get one. */
    int resul=0, ret=0;
	if(get_variable(1)==0) set_variable(1,1);
    switch(reqinfo->mode) {

        case MODE_GET:
           			resul=compara_oid_global(requests->requestvb->name);
			switch(resul)
			{
				case 1:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&dbCreateTransaction,
				sizeof(int));
				break;
				case 6:
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&dbVerifyStatus,
				sizeof(int));
				break;
				case 7:
				dbVerifyError_len = strlen(dbVerifyError);
				snmp_set_var_typed_value(requests->requestvb,  ASN_OCTET_STR,&dbVerifyError,dbVerifyError_len);
				break;
				
			}
            break;


        case MODE_SET_RESERVE1:
                /* or you could use netsnmp_check_vb_type_and_size instead */
            ret = netsnmp_check_vb_type(requests->requestvb, ASN_INTEGER);
            if ( ret != SNMP_ERR_NOERROR ) {
                netsnmp_set_request_error(reqinfo, requests, ret );
            }
            break;

        case MODE_SET_RESERVE2:

            break;

        case MODE_SET_FREE:
            /* XXX: free resources allocated in RESERVE1 and/or
               RESERVE2.  Something failed somewhere, and the states
               below won't be called. */
            break;

        case MODE_SET_ACTION:
 			resul=compara_oid_global(requests->requestvb->name);
			if(resul==1) 
			{
				dbCreateTransaction=*requests->requestvb->val.integer;
				if(dbCreateTransaction==3)
				{


					set_variable(3,2);
				}
			}		
            break;

        case MODE_SET_COMMIT:
            /* XXX: delete temporary storage */

            break;

        case MODE_SET_UNDO:
            /* XXX: UNDO and return to previous value for the object */

            break;

        default:
            /* we should never get here, so this is a really bad error */
            snmp_log(LOG_ERR, "unknown mode (%d) in handle_dbCreateTransaction\n", reqinfo->mode );
            return SNMP_ERR_GENERR;
    }

    return SNMP_ERR_NOERROR;
}

/*	codigo de reserva
		if(longitud_arreglo > 0 && seq_0!=0)
	{
		for(int i=0;i<longitud_arreglo;i++)
		{
			fase_sequencia_a = _entry14[seq_ref-1][anillo_ref-1].sequenceData[i];

			if(i+1 < longitud_arreglo ) fase_sequencia_b = _entry14[seq_ref-1][anillo_ref-1].sequenceData[i+1];
			else fase_sequencia_b = _entry14[seq_ref-1][anillo_ref-1].sequenceData[longitud_arreglo-1];

			cg_a = concurrency_group[fase_sequencia_a-1];
			cg_b = concurrency_group[fase_sequencia_b-1];

			if(i == 0)
			{
				if(cg_a == 1)
				{
					bandera_1 = 1;
				}
				else falla = 2;
			}
			else
			{
				if(cg_b-1 > cg_a && cg_b != cg_a)
				{
					falla = 2;				
				}
			}
	}
	}
*/

