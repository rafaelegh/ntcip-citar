
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "estadoControlador.h"
#include "asc.h"

int temperaturaDisplay    = 0;
int temperaturaMother 	  = 0;
int temperaturaExtension1 = 0;
int temperaturaExtension2 = 0;
int temperaturaExtension3 = 0;
int temperaturaModuloCom1 = 0;
int temperaturaModuloCom2 = 0;

void set_estadoControlador_variable(int _numero, int _valor)
{
	switch(_numero) {
		
		case TEMPERATURADISPLAY: 	temperaturaDisplay 	  = _valor; break;
		case TEMPERATURAMOTHER: 	temperaturaMother 	  = _valor; break;
		case TEMPERATURAEXTENSION1: temperaturaExtension1 = _valor; break;
		case TEMPERATURAEXTENSION2: temperaturaExtension2 = _valor; break;
		case TEMPERATURAEXTENSION3: temperaturaExtension3 = _valor; break;
		case TEMPERATURAMODULOCOM1: temperaturaModuloCom1 = _valor; break;
		case TEMPERATURAMODULOCOM2: temperaturaModuloCom2 = _valor; break;

	}
}

/** Initializes the numeroControlador module */
void
init_estadoControlador(void)
{
    const oid temperaturaDisplay_oid[] = { 1,3,6,1,4,1,54879,4,1 };
    const oid temperaturaMother_oid[] = { 1,3,6,1,4,1,54879,4,2 };
    const oid temperaturaExtension1_oid[] = { 1,3,6,1,4,1,54879,4,3 };
    const oid temperaturaExtension2_oid[] = { 1,3,6,1,4,1,54879,4,4 };
    const oid temperaturaExtension3_oid[] = { 1,3,6,1,4,1,54879,4,5 };
    const oid temperaturaModuloCom1_oid[] = { 1,3,6,1,4,1,54879,4,6 };
    const oid temperaturaModuloCom2_oid[] = { 1,3,6,1,4,1,54879,4,7 };

	DEBUGMSGTL(("temperaturaDisplay", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaDisplay", handle_estadoControlador,
                               temperaturaDisplay_oid, OID_LENGTH(temperaturaDisplay_oid),
                               HANDLER_CAN_RONLY
        ));

	DEBUGMSGTL(("temperaturaMother", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaMother", handle_estadoControlador,
                               temperaturaMother_oid, OID_LENGTH(temperaturaMother_oid),
                               HANDLER_CAN_RONLY
        ));
        
	DEBUGMSGTL(("temperaturaExtension1", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaExtension1", handle_estadoControlador,
                               temperaturaExtension1_oid, OID_LENGTH(temperaturaExtension1_oid),
                               HANDLER_CAN_RONLY
        ));
        
    DEBUGMSGTL(("temperaturaExtension2", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaExtension2", handle_estadoControlador,
                               temperaturaExtension2_oid, OID_LENGTH(temperaturaExtension2_oid),
                               HANDLER_CAN_RONLY
        ));
        
    DEBUGMSGTL(("temperaturaExtension3", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaExtension3", handle_estadoControlador,
                               temperaturaExtension3_oid, OID_LENGTH(temperaturaExtension3_oid),
                               HANDLER_CAN_RONLY
        ));
        
    DEBUGMSGTL(("temperaturaModuloCom1", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaModuloCom1", handle_estadoControlador,
                               temperaturaModuloCom1_oid, OID_LENGTH(temperaturaModuloCom1_oid),
                               HANDLER_CAN_RONLY
        ));


	DEBUGMSGTL(("temperaturaModuloCom2", "Initializing\n"));

    netsnmp_register_scalar(
        netsnmp_create_handler_registration("temperaturaModuloCom2", handle_estadoControlador,
                               temperaturaModuloCom2_oid, OID_LENGTH(temperaturaModuloCom2_oid),
                               HANDLER_CAN_RONLY
        ));

}

int
handle_estadoControlador(netsnmp_mib_handler *handler,
                          netsnmp_handler_registration *reginfo,
                          netsnmp_agent_request_info   *reqinfo,
                          netsnmp_request_info         *requests)
{
	int resul=0;  
  /* We are never called for a GETNEXT if it's registered as a
       "instance", as it's "magically" handled for us.  */

    /* a instance handler also only hands us one request at a time, so
       we don't need to loop over a list of requests; we'll only get one. */
    
	

    switch(reqinfo->mode) {

        case MODE_GET:

		resul=compara_oid_mantelectric(requests->requestvb->name);

		if(resul == TEMPERATURADISPLAY)
		{
            	snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaDisplay
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}
		else if (resul == TEMPERATURAMOTHER)
		{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaMother
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}

		else if (resul == TEMPERATURAEXTENSION1)
		{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaExtension1
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}
		else if (resul == TEMPERATURAEXTENSION2)
		{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaExtension2
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}
		else if (resul == TEMPERATURAEXTENSION3)
		{
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaExtension3
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}
		else if (resul == TEMPERATURAMODULOCOM1) {
			
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaModuloCom1
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}
		else if (resul == TEMPERATURAMODULOCOM2) {
				snmp_set_var_typed_value(requests->requestvb, ASN_INTEGER,&temperaturaModuloCom2
                                     /* XXX: a pointer to the scalar's data */,sizeof(int)
                                     /* XXX: the length of the data in bytes */);
		}
		
        break;

   }

    return SNMP_ERR_NOERROR;
}
