

#ifndef ESTADOCONTROLADOR_H
#define ESTADOCONTROLADOR_H

/* function declarations */
void init_estadoControlador(void);
void set_estadoControlador_variable(int _numero, int _valor);
Netsnmp_Node_Handler handle_estadoControlador;

#define TEMPERATURADISPLAY 1
#define TEMPERATURAMOTHER  2 
#define TEMPERATURAEXTENSION1 3
#define TEMPERATURAEXTENSION2 4
#define TEMPERATURAEXTENSION3 5
#define TEMPERATURAMODULOCOM1 6
#define TEMPERATURAMODULOCOM2 7



#endif /* ESTADOCONTROLADOR_H */
