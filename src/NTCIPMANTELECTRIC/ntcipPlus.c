#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "asc.h"
#include "citar.h"
#include <stdlib.h>

//coord

int maxTiempos=0;
int maxDetectores=0;
int maxEstructuras=0;
int maxAnillos=0;
int maxOverlapsPlus=0;

struct tiemposTable_entry *entry110[16];
struct detectoresTable_entry *entry111[19];
struct anilloTable_entry *entry112[30][4];
struct overlapPlusTable_entry *entry113[16];

struct tiemposTable_entry ** get_tiempos_table(void)
{
	return entry110;
}

struct detectoresTable_entry ** get_detectores_plus(void)
{
	return entry111;
}
/*
struct anilloTable_entry ** get_anillo_table(void)
{
	return entry112;
}
*/

void get_anillo_table (struct anilloTable_entry _empty_table[][4])
{
    for(int i=0;i<maxEstructuras;i++)
    {
        for(int j=0;j<maxAnillos;j++)
        {
            _empty_table[i][j].desfasajeAnillo = entry112[i][j]->desfasajeAnillo;
        }
    }
}

struct overlapPlusTable_entry ** get_overlapPlus_table(void)
{
    return entry113;
}

/* create a new row in the table */
netsnmp_tdata_row *
tiempos_tables_createEntry(netsnmp_tdata *tabla_data, int  indice, int indice2, int tabla) 
{
    netsnmp_tdata_row *row;

	int i=0;

	switch(tabla)
	{
		case 110:

    	entry110[indice] = SNMP_MALLOC_TYPEDEF(struct tiemposTable_entry);
    	if (!entry110[indice])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry110[indice]);
	        return NULL;
	    }
    	row->data = entry110[indice];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry110[indice]->tiemposNumero = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry110[indice]->tiemposNumero),
                                 sizeof(entry110[indice]->tiemposNumero));

		break;

		case 111:

    	entry111[indice] = SNMP_MALLOC_TYPEDEF(struct detectoresTable_entry);
    	if (!entry111[indice])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry111[indice]);
	        return NULL;
	    }
    	row->data = entry111[indice];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry111[indice]->detectoresNumero = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry111[indice]->detectoresNumero),
                                 sizeof(entry111[indice]->detectoresNumero));

		break;

		case 112:

    	entry112[indice][indice2] = SNMP_MALLOC_TYPEDEF(struct anilloTable_entry);
    	if (!entry112[indice][indice2])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry112[indice][indice2]);
	        return NULL;
	    }
    	row->data = entry112[indice][indice2];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
        
    	entry112[indice][indice2]->estructuraAnilloNumero = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry112[indice][indice2]->estructuraAnilloNumero),
                                 sizeof(entry112[indice][indice2]->estructuraAnilloNumero));        
        
    	entry112[indice][indice2]->anilloNumero = indice2+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry112[indice][indice2]->anilloNumero),
                                 sizeof(entry112[indice][indice2]->anilloNumero));

		break;
        
        case 113:

    	entry113[indice] = SNMP_MALLOC_TYPEDEF(struct overlapPlusTable_entry);
    	if (!entry113[indice])
        return NULL;

   	 	row = netsnmp_tdata_create_row();
    	if (!row) 
		{
	        SNMP_FREE(entry113[indice]);
	        return NULL;
	    }
    	row->data = entry113[indice];

    	DEBUGMSGT(("patternTable:entry:create", "row 0x%x\n", (uintptr_t)row));
    	entry113[indice]->overlapPlusNumero = indice+1;
    	netsnmp_tdata_row_add_index( row, ASN_INTEGER,
                                 &(entry113[indice]->overlapPlusNumero),
                                 sizeof(entry113[indice]->overlapPlusNumero));

		break;
		
    }
		if (tabla_data) netsnmp_tdata_add_row( tabla_data, row );
        return row;

    
}

void
initialize_tiempos_tables(char nombre[],const oid oid_table[], const size_t oid_len,
	unsigned char min_columna, unsigned char max_columna, unsigned char tabla)
{
    netsnmp_handler_registration    *reg;
    netsnmp_tdata                   *table_data;
    netsnmp_table_registration_info *table_info;
    netsnmp_tdata_row *row;
	int n=0,n2=0;
    DEBUGMSGTL(("phaseTable:init", "initializing table phaseTable\n"));
	switch(tabla)
	{
		case 110:
		reg = netsnmp_create_handler_registration(
              nombre,     tiemposTable_handler,
              oid_table, oid_len,
              HANDLER_CAN_RWRITE);
		n=maxTiempos;
		break;
		case 111:
		reg = netsnmp_create_handler_registration(
              nombre,     detectoresTable_handler,
              oid_table, oid_len,
              HANDLER_CAN_RWRITE);
		n=maxDetectores;
		break;

		case 112:
		reg = netsnmp_create_handler_registration(
              nombre,     anilloTable_handler,
              oid_table, oid_len,
              HANDLER_CAN_RWRITE);
		n2 = maxAnillos;
        n = maxEstructuras;
		break;
        
        case 113:
		reg = netsnmp_create_handler_registration(
              nombre,     overlapPlusTable_handler,
              oid_table, oid_len,
              HANDLER_CAN_RWRITE);
		n=maxOverlapsPlus;
		break;
	}

 		table_data = netsnmp_tdata_create_table( nombre, 0 );

    if (NULL == table_data) {
        snmp_log(LOG_ERR,"error creating tdata table for phaseTable\n");
        return;
    }
    table_info = SNMP_MALLOC_TYPEDEF( netsnmp_table_registration_info );
    if (NULL == table_info) {
        snmp_log(LOG_ERR,"error creating table info for phaseTable\n");
        return;
    }

    if(tabla == 112) {
        netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: estructuraAnilloNumero */
						   ASN_INTEGER,  /* index: anilloNumero */
                           0);
    }
    else {
        netsnmp_table_helper_add_indexes(table_info,
                           ASN_INTEGER,  /* index: splitNumber */
                           0);
    }
    table_info->min_column = min_columna;
    table_info->max_column = max_columna;
    
    netsnmp_tdata_register( reg, table_data, table_info );

    /* Initialise the contents of the table here */

	for(int i=0;i<n;i++)
	{	
        if(tabla == 112) {	
            for(int j=0;j<n2;j++) {
                row=tiempos_tables_createEntry(table_data, i, j, tabla);
            }
        }
        else {
            row=tiempos_tables_createEntry(table_data, i, 0, tabla);
        }
	}
}

void
init_ntcipPlus(void)
{
	/* here we initialize all the tables we're planning on supporting */
	maxTiempos = get_phase_variable(1);
	maxDetectores = get_detector_variable(1);
	maxAnillos = get_ring_variable(1);
    maxEstructuras = get_ring_variable(2);
    maxOverlapsPlus = get_overlap_variable(1);

    const oid tiemposTable_oid[] = {1,3,6,1,4,1,54879,3,1};
    const size_t tiemposTable_oid_len = OID_LENGTH(tiemposTable_oid);

    const oid anilloTable_oid[] = {1,3,6,1,4,1,54879,3,3};
    const size_t anilloTable_oid_len = OID_LENGTH(anilloTable_oid);

    const oid detectoresTable_oid[] = {1,3,6,1,4,1,54879,3,2};
    const size_t detectoresTable_oid_len = OID_LENGTH(detectoresTable_oid);
    
    const oid overlapPlusTable_oid[] = {1,3,6,1,4,1,54879,3,4};
    const size_t overlapPlusTable_oid_len = OID_LENGTH(overlapPlusTable_oid);

initialize_tiempos_tables("tiemposTable",tiemposTable_oid,
tiemposTable_oid_len, COLUMN_TIEMPOSNUMERO, COLUMN_DESFASAJEPEATONAL,110);

	initialize_tiempos_tables("detectoresTable",detectoresTable_oid,
detectoresTable_oid_len, COLUMN_DETECTORESNUMERO, COLUMN_TIPODETECTOR,111);

	initialize_tiempos_tables("anilloTable",anilloTable_oid,
anilloTable_oid_len, COLUMN_ANILLONUMERO, COLUMN_DESFASAJEANILLO,112);

    initialize_tiempos_tables("overlapPlusTable",overlapPlusTable_oid,
overlapPlusTable_oid_len, COLUMN_OVERLAPPLUSNUMERO, COLUMN_OVERLAPTYPEPLUS,113);

}

/** handles requests for the tiemposTable table */
int
tiemposTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct tiemposTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("tiemposTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct tiemposTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIEMPOSNUMERO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->tiemposNumero);
                break;
            case COLUMN_TIEMPOSROJOAMARILLO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->tiemposRojoAmarillo);
                break;
            case COLUMN_DESFASAJEPEATONAL:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->desfasajePeatonal);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct tiemposTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIEMPOSROJOAMARILLO:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            case COLUMN_DESFASAJEPEATONAL:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct tiemposTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIEMPOSROJOAMARILLO:
                table_entry->old_tiemposRojoAmarillo = table_entry->tiemposRojoAmarillo;
                table_entry->tiemposRojoAmarillo     = *request->requestvb->val.integer;
				set_secuencia_programacion(16);
                break;
            case COLUMN_DESFASAJEPEATONAL:
                table_entry->old_desfasajePeatonal = table_entry->desfasajePeatonal;
                table_entry->desfasajePeatonal     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct tiemposTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIEMPOSROJOAMARILLO:
                table_entry->tiemposRojoAmarillo     = table_entry->old_tiemposRojoAmarillo;
                table_entry->old_tiemposRojoAmarillo = 0;
                break;
            case COLUMN_DESFASAJEPEATONAL:
                table_entry->desfasajePeatonal     = table_entry->old_desfasajePeatonal;
                table_entry->old_desfasajePeatonal = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}


/** handles requests for the detectoresTable table */
int
detectoresTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct detectoresTable_entry          *table_entry;
    int                         ret;
	if(get_variable(1)==0) set_variable(1,1);
    DEBUGMSGTL(("detectoresTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct detectoresTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_DETECTORESNUMERO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->detectoresNumero);
                break;
            case COLUMN_TIPODETECTOR:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->tipoDetector);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct detectoresTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIPODETECTOR:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct detectoresTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIPODETECTOR:
                table_entry->old_tipoDetector = table_entry->tipoDetector;
                table_entry->tipoDetector     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct detectoresTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_TIPODETECTOR:
                table_entry->tipoDetector     = table_entry->old_tipoDetector;
                table_entry->old_tipoDetector = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}


/** handles requests for the anilloTable table */
int
anilloTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct anilloTable_entry   *table_entry;
    int                         ret;

    DEBUGMSGTL(("anilloTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct anilloTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_ESTRUCTURAANILLONUMERO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->estructuraAnilloNumero);
                break;
            case COLUMN_ANILLONUMERO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->anilloNumero);
                break;
            case COLUMN_DESFASAJEANILLO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->desfasajeAnillo);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct anilloTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_DESFASAJEANILLO:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct anilloTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_DESFASAJEANILLO:
                table_entry->old_desfasajeAnillo = table_entry->desfasajeAnillo;
                table_entry->desfasajeAnillo     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct anilloTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_DESFASAJEANILLO:
                table_entry->desfasajeAnillo     = table_entry->old_desfasajeAnillo;
                table_entry->old_desfasajeAnillo = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}


/** handles requests for the overlapPlusTable table */
int
overlapPlusTable_handler(
    netsnmp_mib_handler               *handler,
    netsnmp_handler_registration      *reginfo,
    netsnmp_agent_request_info        *reqinfo,
    netsnmp_request_info              *requests) {

    netsnmp_request_info       *request;
    netsnmp_table_request_info *table_info;
    netsnmp_tdata              *table_data;
    netsnmp_tdata_row          *table_row;
    struct overlapPlusTable_entry          *table_entry;
    int                         ret;

    DEBUGMSGTL(("overlapPlusTable:handler", "Processing request (%d)\n", reqinfo->mode));

    switch (reqinfo->mode) {
        /*
         * Read-support (also covers GetNext requests)
         */
    case MODE_GET:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapPlusTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPPLUSNUMERO:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapPlusNumero);
                break;
            case COLUMN_OVERLAPTYPEPLUS:
                if ( !table_entry ) {
                    netsnmp_set_request_error(reqinfo, request,
                                              SNMP_NOSUCHINSTANCE);
                    continue;
                }
                snmp_set_var_typed_integer( request->requestvb, ASN_INTEGER,
                                            table_entry->overlapTypePlus);
                break;
            default:
                netsnmp_set_request_error(reqinfo, request,
                                          SNMP_NOSUCHOBJECT);
                break;
            }
        }
        break;

        /*
         * Write-support
         */
    case MODE_SET_RESERVE1:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapPlusTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPTYPEPLUS:
                /* or possibly 'netsnmp_check_vb_int_range' */
                ret = netsnmp_check_vb_int( request->requestvb );
                if ( ret != SNMP_ERR_NOERROR ) {
                    netsnmp_set_request_error( reqinfo, request, ret );
                    return SNMP_ERR_NOERROR;
                }
                break;
            default:
                netsnmp_set_request_error( reqinfo, request,
                                           SNMP_ERR_NOTWRITABLE );
                return SNMP_ERR_NOERROR;
            }
        }
        break;

    case MODE_SET_RESERVE2:
        break;

    case MODE_SET_FREE:
        break;

    case MODE_SET_ACTION:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapPlusTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPTYPEPLUS:
                table_entry->old_overlapTypePlus = table_entry->overlapTypePlus;
                table_entry->overlapTypePlus     = *request->requestvb->val.integer;
                break;
            }
        }
        break;

    case MODE_SET_UNDO:
        for (request=requests; request; request=request->next) {
            if (request->processed)
                continue;

            table_entry = (struct overlapPlusTable_entry *)
                              netsnmp_tdata_extract_entry(request);
            table_row   =     netsnmp_tdata_extract_row(  request);
            table_data  =     netsnmp_tdata_extract_table(request);
            table_info  =     netsnmp_extract_table_info( request);
    
            switch (table_info->colnum) {
            case COLUMN_OVERLAPTYPEPLUS:
                table_entry->overlapTypePlus     = table_entry->old_overlapTypePlus;
                table_entry->old_overlapTypePlus = 0;
                break;
            }
        }
        break;

    case MODE_SET_COMMIT:
        break;
    }
    return SNMP_ERR_NOERROR;
}
