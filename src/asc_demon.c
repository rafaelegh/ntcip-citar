#include <signal.h>
#include <sys/types.h>
#include <pthread.h>
#include <string.h>
#include <sys/msg.h>
#include <errno.h>
#include "listas.h"
#include "prog_envio.h"
#include "keepalive.h"
#include "detectores.h"
#include "asc.h"
#include "citar.h"
#include "agendas_ntcip.h"
#include "conexion.h"


void controlador (int);
void controlar_tramas (int);

static int keep_running;

int enviar_tramas = 1;

RETSIGTYPE
stop_server(int a) {
    keep_running = 0;
}
 
/* Funcion que se ejecuta en el thread hijo.*/

int
main (int argc, char **argv) {
  int agentx_subagent=1; /* change this if you want to be a SNMP master agent */
  int background = 0; /* change this if you want to run in the background */
  int syslog = 0; /* change this if you want to use syslog */
	/* print log errors to syslog or stderr */
  int error=0;

  pthread_t idHilo;					//Hilo de comunicacion con el equip									

  if (syslog) snmp_enable_calllog();
  else snmp_enable_stderrlog();

  /* we're an agentx subagent? */
  if (agentx_subagent) {
    /* make us a agentx client. */
    netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_AGENT_ROLE, 1);
  }

  /* run in background, if requested */
  if (background && netsnmp_daemonize(1, !syslog))
      exit(1);

  /* initialize tcpip, if necessary */
  SOCK_STARTUP;

  /* initialize the agent library */
  init_agent("asc_demon");

  /* initialize mib code here */

  /* mib code: init_nstAgentSubagentObject from nstAgentSubagentObject.C */

	init_phase();
	init_detector();
	init_unit();
	init_coord();
	init_timebaseAsc();
	init_preempt();
	init_ring();
	init_channel();
	init_overlap();
	init_ts2Port1();
	init_ascBlock();
	init_globalConfiguration();
	init_globalDBManagement();
	init_globalTimeManagement();
	init_estructuraTable();
	init_matrizConflictoTable();
	init_funcionesTable();
	init_programaTiempos1Table();
	init_programaTiempos2Table();	
	init_programaTiempos3Table();
	init_agendaDiariaTable();
	init_agendaAnualSemanalTable();
	init_agendaEspecialesTable();
	init_agendaFeriadosTable();
	init_progRemota();
	init_estadoExtendido();
	init_ntcipPlus();
	init_estadoControlador();
//	init_globalReport();

  /* initialize vacm/usm access control  */
  if (!agentx_subagent) {
      init_vacm_vars();
      init_usmUser();
  }

  /* example-demon will be used to read example-demon.conf files. */
  init_snmp("asc_demon");

  /* If we're going to be a snmp master agent, initial the ports */
  if (!agentx_subagent)
    init_master_agent();  /* open the port to listen on (defaults to udp:161) */

  /* In case we recevie a request to stop (kill -TERM or kill -INT) */
  keep_running = 1;

  if(signal(SIGINT, controlador)== SIG_ERR)
  {
	perror("No se puede cambiar signal");
  }
	
  if(signal(10, controlar_tramas)== SIG_ERR)
  {
	perror("No se puede cambiar signal");
  }

  snmp_log(LOG_INFO,"example-demon is up and running.\n");

  error = pthread_create (&idHilo, NULL, funcionThread, NULL);

	/* Comprobamos el error al arrancar el thread */
  if (error != 0)
	{
		perror ("No puedo crear thread");
		exit (-1);
	}

		  /* your main loop here... */
  while(keep_running) {
    /* if you use select(), see snmp_select_info() in snmp_api(3) */
    /*     --- OR ---  */
    agent_check_and_process(1); /* 0 == don't block */
  
  } 

  snmp_shutdown("asc_demon");
  SOCK_CLEANUP;
  
  return 0;
}

void *funcionThread (void *parametro)
{
   	int _puerto=0,flag=0,snmp=0, act=0,programacion=0;	
	short sitra_com=0;

	unsigned char te1[16],te3[15],te12[17],
	tr1[20],tr2[100];
	
	unsigned long keep_alive_old = 0, keep_alive_new = 0;

	//punteros
	char * _direccion_ip="NULL";
	int *_conexion=NULL;

	ptnodo * ld = NULL;
	
	//estructuras
	struct movimiento fases[16];

	time_t timeout=0, ini_timeout=0;
		
	inicializa_conteos();
	limpiar_secuencia();

	_direccion_ip=get_cadena();
	_puerto=get_variable(4);

	ld=get_lista();

	_conexion = get_conexion();

	set_variable(2,0x55);
	printf("Consultando programacion del Equipo Controlador: ...\n");
	
	realizar_consulta_generica();

	prepara_trama_consulta_extendido(te3, 0);
	prepara_trama_comando(te1);

//	cargar_consultas_prog(_conexion);
	close(*_conexion);
	ini_timeout = time(NULL);
	
	//consultamos el primer keepalive
//	keep_alive_new = check_keepalive();

	while(1)
	{
			act=get_variable(1);
			programacion=get_variable(3);
			printf("programacion: %i, actividad: %i\n",programacion, act);
			
			if(act == 1 && programacion == 0) {
				snmp = 1;	
			}
			else if(act == 1 && programacion == 1) {
				snmp = 3;
			}
			else if(programacion == 2) {
				snmp = 4;
			}
			
			if(snmp == 1) {							//se hizo consulta SNMP 
				timeout = 0;
				ini_timeout = time(NULL);
				prepara_trama_comando2(te12);
				if(enviar_tramas == 1) {
					enviar_trama(_conexion,te12,tr2,17,100);
				}
				snmp = 2;
				set_variable(1,0);
				//set_variable(2,0x55);
			}
			else if (snmp == 2) {
				
				if (timeout > 59)	{
					snmp = 0;
					set_variable(2,0x55);
					prepara_trama_comando2(te12);
				}
				if(enviar_tramas == 1) {
					
					printf("%i\n",enviar_tramas);
					enviar_trama(_conexion,te12,tr2,17,100);
				}
			}
			else if (snmp == 3) {							//Programacion Remota

				programacion_objetos_propietarios(_conexion);

			}
			else if (snmp == 4) {
				
				printf("snmp4\n");
				revision_bloque_get();
				verificar_transacciones();
				set_variable(2,0x55);
//				cargar_consultas_prog(_conexion);
				set_variable(3,0);

				if(get_variable(3) == 2) set_variable(3,0);
			}			
	
			else { 	
				
			//no hay consulta en mas de 1 min
				flag = 0;
				prepara_trama_consulta_extendido(te3, 0);
				if(enviar_tramas == 1) enviar_trama(_conexion,te3,tr2,15,100);		
			}

			analiza_lamparas(tr2,fases);
			actualiza_phaseStatus(fases);
			estado_extendido(tr2);
			
			timeout = (time(NULL)) - ini_timeout;
			
			//Codigo de los detectores
			hacer_conteos();
	
			//keepalive
			/*	
			keep_alive_old = keep_alive_new;
			keep_alive_new = check_keepalive();
			*/	
/*			
			if( keep_alive_new == keep_alive_old )
			{
				if(sitra_com)
				{
					system("/home/pi/Documents/Scripts/usar_sitra_com.sh");
					sitra_com = 0;
					enviar_tramas = 1;
				}
			}
			else
			{
				if(!sitra_com)
				{
					system("/home/pi/Documents/Scripts/usar_sitra_com.sh");
					sitra_com = 1;
					enviar_tramas = 0;
				}
			}

			printf("keepalive nuevo: %li\nkeepalive viejo: %li\n",keep_alive_new,keep_alive_old);

*/

//			conversion_schedule_ntcip_citar();

			printf("El timeout (s) transcurrido es: %li\n" ,timeout);
			
			//led de debuggin y retardo de 1s
			debugging_led();
			
		}

  /* at shutdown time */
}

void controlador (int numeroSenhal)
{
	signal(SIGTERM, stop_server);
  	signal(SIGINT, stop_server);
	exit(0);
}

void controlar_tramas (int numeroSenhal)
{
	int * __conexion = NULL;
	
	char t_env_estado_ext[17], t_rec_estado_ext[100];
	
	__conexion =  get_conexion();
	
	printf("SEÑAL RECIBIDA!!!\n");
	
	if(enviar_tramas == 1)
	{
		enviar_tramas = 0;
		set_variable(5, get_variable(5)); //guardando ultimo comando
	}
	else if(enviar_tramas == 0)
	{
		enviar_tramas = 1;
		set_variable(2,0x55);
		prepara_trama_comando2(t_env_estado_ext);
		enviar_trama(__conexion,t_env_estado_ext,t_rec_estado_ext,17,100);
		set_variable(2, get_variable(5)); //reestablece ultimo comando
	}
}
